/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup AbilityRuntime
 * @{
 *
 * @brief Provides the context constants of the AbilityRuntime module.
 *
 * @syscap SystemCapability.Ability.AbilityRuntime.Core
 * @since 13
 */

/**
 * @file context_constant.h
 *
 * @brief Declares the context constants of the AbilityRuntime module.
 *
 * @library libability_runtime.so
 * @kit AbilityKit
 * @syscap SystemCapability.Ability.AbilityRuntime.Core
 * @since 13
 */

#ifndef ABILITY_RUNTIME_CONTEXT_CONSTANT_H
#define ABILITY_RUNTIME_CONTEXT_CONSTANT_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the data encryption levels.
 *
 * @since 13
 */
typedef enum {
    /**
     * Device-level encryption. Directories with this encryption level are accessible after the device is powered on.
     */
    ABILITY_RUNTIME_AREA_MODE_EL1 = 0,
    /**
     * User-level encryption. Directories with this encryption level are accessible only after the device is powered on
     * and the password is entered (for the first time).
     */
    ABILITY_RUNTIME_AREA_MODE_EL2 = 1,
    /**
     * User-level encryption. The file permissions vary according to their scenarios.
     * An open file is always readable and writable regardless of whether the screen is locked.
     * When the screen is locked, a closed file cannot be opened, read, or written. When the screen is unlocked,
     * such a file can be opened, read, and written.
     * When the screen is locked, a file can be created and then opened and written but not read. When the screen is
     * unlocked, a file can be created and then opened, read, and written.
     */
    ABILITY_RUNTIME_AREA_MODE_EL3 = 2,
    /**
     * User-level encryption. The file permissions vary according to their scenarios.
     * When the screen is locked, an open file is readable and writable in FEB2.0, but not in FEB3.0. When the screen is
     * unlocked, such a file is always readable and writable.
     * When the screen is locked, a closed file cannot be opened, read, or written. When the screen is unlocked, such
     * a file can be opened, read, and written.
     * When the screen is locked, a file cannot be created. When the screen is unlocked, a file can be created and then
     * opened, read, and written.
     */
    ABILITY_RUNTIME_AREA_MODE_EL4 = 3,
    /**
     * Application-level encryption. The file permissions vary according to their scenarios.
     * An open file is always readable and writable regardless of whether the screen is locked.
     * When the screen is locked, a closed file can be opened, read, and written only if a DataAccessLock (JS API) is
     * obtained. When the screen is unlocked, such a file can be opened, read, and written.
     * A file can be created and then opened, read, and written regardless of whether the screen is locked.
     */
    ABILITY_RUNTIME_AREA_MODE_EL5 = 4,
} AbilityRuntime_AreaMode;

#ifdef __cplusplus
} // extern "C"
#endif

/** @} */
#endif // ABILITY_RUNTIME_CONTEXT_CONSTANT_H
