#ifndef VULKAN_OHOS_H_
#define VULKAN_OHOS_H_ 1

/*
** Copyright 2015-2022 The Khronos Group Inc.
**
** SPDX-License-Identifier: Apache-2.0
*/

/*
** This header is generated from the Khronos Vulkan XML API Registry.
**
*/


/**
 * @addtogroup Vulkan
 * @{
 *
 * @brief Provides Vulkan capabilities extended by OpenHarmony.
 * This module provides extended APIs for creating a Vulkan surface and obtaining <b>OH_NativeBuffer</b> and
 * <b>OH_NativeBuffer</b> properties through <b>OHNativeWindow</b>.
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @since 10
 * @version 1.0
 */

/**
 * @file vulkan_ohos.h
 *
 * @brief Declares the Vulkan APIs extended by OpenHarmony. File to include: <vulkan/vulkan.h>
 *
 * @library libvulkan.so
 * @since 10
 * @version 1.0
 */

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief Defines the surface extension of OpenHarmony.
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_surface 1

/**
 * @brief Defines the <b>OHNativeWindow</b> struct.
 * @since 10
 * @version 1.0
 */
typedef struct NativeWindow OHNativeWindow;

/**
 * @brief Defines the surface extension version of OpenHarmony.
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_SURFACE_SPEC_VERSION      1

/**
 * @brief Defines the surface extension name of OpenHarmony.
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_SURFACE_EXTENSION_NAME    "VK_OHOS_surface"

/**
 * @brief Defines the bit mask of the VkFlags type used for the creation of a Vulkan surface.
 * It is a reserved flag type.
 * @since 10
 * @version 1.0
 */
typedef VkFlags VkSurfaceCreateFlagsOHOS;

/**
 * @brief Defines the parameters required for creating a Vulkan surface.
 * @since 10
 * @version 1.0
 */
typedef struct VkSurfaceCreateInfoOHOS {
    /**
     * Struct type.
     */
    VkStructureType             sType;
    /**
     * Pointer to the next-level struct.
     */
    const void*                 pNext;
    /**
     * Reserved flag type.
     */
    VkSurfaceCreateFlagsOHOS    flags;
    /**
     * Pointer to an <b>OHNativeWindow</b> instance.
     */
    OHNativeWindow*             window;
} VkSurfaceCreateInfoOHOS;

/**
 * @brief Defines the function pointer for creating a Vulkan surface.
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param instance <b>Vulkan</b> instance.
 * @param pCreateInfo Pointer to the <b>VkSurfaceCreateInfoOHOS</b> struct,
 * including the parameters required for creating a Vulkan surface.
 * @param pAllocator Pointer to a callback function for custom memory allocation.
 * If custom memory allocation is not required, pass in <b>NULL</b>, and the default memory allocation function is used.
 * @param pSurface Pointer to the Vulkan surface created. The type is <b>VkSurfaceKHR</b>.
 * @return Returns <b>VK_SUCCESS</b> if the execution is successful;
 * returns an error code of the VkResult type otherwise.
 * @since 10
 * @version 1.0
 */
typedef VkResult (VKAPI_PTR *PFN_vkCreateSurfaceOHOS)(
    VkInstance                     instance,
    const VkSurfaceCreateInfoOHOS* pCreateInfo,
    const VkAllocationCallbacks*   pAllocator,
    VkSurfaceKHR*                  pSurface
);

#ifndef VK_NO_PROTOTYPES

/**
 * @brief Creates a Vulkan surface.
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param instance <b>Vulkan</b> instance.
 * @param pCreateInfo Pointer to the <b>VkSurfaceCreateInfoOHOS</b> struct,
 * including the parameters required for creating a Vulkan surface.
 * @param pAllocator Pointer to a callback function for custom memory allocation.
 * If custom memory allocation is not required, pass in <b>NULL</b>, and the default memory allocation function is used.
 * @param pSurface Pointer to the Vulkan surface created. The type is <b>VkSurfaceKHR</b>.
 * @return Returns <b>VK_SUCCESS</b> if the execution is successful;
 * returns an error code of the VkResult type otherwise.
 * @since 10
 * @version 1.0
 */
VKAPI_ATTR VkResult VKAPI_CALL vkCreateSurfaceOHOS(
    VkInstance                                  instance,
    const VkSurfaceCreateInfoOHOS*              pCreateInfo,
    const VkAllocationCallbacks*                pAllocator,
    VkSurfaceKHR*                               pSurface);
#endif


/**
 * @brief Defines the external memory extension of OpenHarmony.
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_external_memory 1

/**
 * @brief Defines the <b>OH_NativeBuffer</b> struct.
 * @since 10
 * @version 1.0
 */
struct OH_NativeBuffer;

/**
 * @brief Defines the external memory extension version of OpenHarmony.
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_EXTERNAL_MEMORY_SPEC_VERSION 1

/**
 * @brief Defines the external memory extension name of OpenHarmony.
 * @since 10
 * @version 1.0
 */
#define VK_OHOS_EXTERNAL_MEMORY_EXTENSION_NAME "VK_OHOS_external_memory"

/**
 * @brief Defines the usage of a <b>NativeBuffer</b>.
 * @since 10
 * @version 1.0
 */
typedef struct VkNativeBufferUsageOHOS {
    /**
     * Struct type.
     */
    VkStructureType    sType;
    /**
     * Pointer to the next-level struct.
     */
    void*              pNext;
    /**
     * @brief Defines the usage of a <b>NativeBuffer</b>.
     */
    uint64_t           OHOSNativeBufferUsage;
} VkNativeBufferUsageOHOS;

/**
 * @brief Defines the properties of a <b>NativeBuffer</b>.
 * @since 10
 * @version 1.0
 */
typedef struct VkNativeBufferPropertiesOHOS {
    /**
     * Struct type.
     */
    VkStructureType    sType;
    /**
     * Pointer to the next-level struct.
     */
    void*              pNext;
    /**
     * @brief Defines the size of the occupied memory.
     */
    VkDeviceSize       allocationSize;
    /**
     * @brief Defines the memory type.
     */
    uint32_t           memoryTypeBits;
} VkNativeBufferPropertiesOHOS;

/**
 * @brief Defines the format properties of a <b>NativeBuffer</b>.
 * @since 10
 * @version 1.0
 */
typedef struct VkNativeBufferFormatPropertiesOHOS {
    /**
     * Struct type.
     */
    VkStructureType                  sType;
    /**
     * Pointer to the next-level struct.
     */
    void*                            pNext;
    /**
     * Format properties.
     */
    VkFormat                         format;
    /**
     * Externally defined format.
     */
    uint64_t                         externalFormat;
    /**
     * Features of the externally defined format.
     */
    VkFormatFeatureFlags             formatFeatures;
    /**
     * A group of VkComponentSwizzles.
     */
    VkComponentMapping               samplerYcbcrConversionComponents;
    /**
     * Color model.
     */
    VkSamplerYcbcrModelConversion    suggestedYcbcrModel;
    /**
     * Color value range.
     */
    VkSamplerYcbcrRange              suggestedYcbcrRange;
    /**
     * X chrominance offset.
     */
    VkChromaLocation                 suggestedXChromaOffset;
    /**
     * Y chrominance offset.
     */
    VkChromaLocation                 suggestedYChromaOffset;
} VkNativeBufferFormatPropertiesOHOS;

/**
 * @brief Defines the pointer to an <b>OH_NativeBuffer</b> struct.
 * @since 10
 * @version 1.0
 */
typedef struct VkImportNativeBufferInfoOHOS {
    /**
     * Struct type.
     */
    VkStructureType            sType;
    /**
     * Pointer to the next-level struct.
     */
    const void*                pNext;
    /**
     * Pointer to an <b>OH_NativeBuffer</b> struct.
     */
    struct OH_NativeBuffer*    buffer;
} VkImportNativeBufferInfoOHOS;

/**
 * @brief Defines a struct used to obtain an <b>OH_NativeBuffer</b> from the Vulkan memory.
 * @since 10
 * @version 1.0
 */
typedef struct VkMemoryGetNativeBufferInfoOHOS {
    /**
     * Struct type.
     */
    VkStructureType    sType;
    /**
     * Pointer to the next-level struct.
     */
    const void*        pNext;
    /**
     * <b>VkDeviceMemory</b> instance.
     */
    VkDeviceMemory     memory;
} VkMemoryGetNativeBufferInfoOHOS;

/**
 * @brief Defines an externally defined format.
 * @since 10
 * @version 1.0
 */
typedef struct VkExternalFormatOHOS {
    /**
     * Struct type.
     */
    VkStructureType    sType;
    /**
     * Pointer to the next-level struct.
     */
    void*              pNext;
    /**
     * Externally defined format.
     */
    uint64_t           externalFormat;
} VkExternalFormatOHOS;

/**
 * @brief Defines a function pointer used to obtain <b>OH_NativeBuffer</b> properties.
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param device <b>VkDevice</b> instance.
 * @param buffer Pointer to the <b>OH_NativeBuffer</b> struct.
 * @param pProperties Pointer to the struct holding the properties of <b>OH_NativeBuffer</b>.
 * @return Returns <b>VK_SUCCESS</b> if the execution is successful;
 * returns an error code of the VkResult type otherwise.
 * @since 10
 * @version 1.0
 */
typedef VkResult (VKAPI_PTR *PFN_vkGetNativeBufferPropertiesOHOS)(
    VkDevice                      device,
    const struct OH_NativeBuffer* buffer,
    VkNativeBufferPropertiesOHOS* pProperties
);

/**
 * @brief Defines a function pointer used to obtain an <b>OH_NativeBuffer</b> instance.
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param device <b>VkDevice</b> instance.
 * @param pInfo Pointer to the <b>VkMemoryGetNativeBufferInfoOHOS</b> struct.
 * @param pBuffer Double pointer to the <b>OH_NativeBuffer</b> obtained.
 * @return Returns <b>VK_SUCCESS</b> if the execution is successful;
 * returns an error code of the VkResult type otherwise.
 * @since 10
 * @version 1.0
 */
typedef VkResult (VKAPI_PTR *PFN_vkGetMemoryNativeBufferOHOS)(
    VkDevice                               device,
    const VkMemoryGetNativeBufferInfoOHOS* pInfo,
    struct OH_NativeBuffer**               pBuffer
);

#ifndef VK_NO_PROTOTYPES

/**
 * @brief Obtains the properties of an <b>OH_NativeBuffer</b> instance.
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param device <b>VkDevice</b> instance.
 * @param buffer Pointer to the <b>OH_NativeBuffer</b> struct.
 * @param pProperties Pointer to the struct holding the properties of <b>OH_NativeBuffer</b>.
 * @return Returns <b>VK_SUCCESS</b> if the execution is successful;
 * returns an error code of the VkResult type otherwise.
 * @since 10
 * @version 1.0
 */
VKAPI_ATTR VkResult VKAPI_CALL vkGetNativeBufferPropertiesOHOS(
    VkDevice                                    device,
    const struct OH_NativeBuffer*               buffer,
    VkNativeBufferPropertiesOHOS*               pProperties);

/**
 * @brief Obtains an <b>OH_NativeBuffer</b> instance.
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param device <b>VkDevice</b> instance.
 * @param pInfo Pointer to the <b>VkMemoryGetNativeBufferInfoOHOS</b> struct.
 * @param pBuffer Double pointer to the <b>OH_NativeBuffer</b> obtained.
 * @return Returns <b>VK_SUCCESS</b> if the execution is successful;
 * returns an error code of the VkResult type otherwise.
 * @since 10
 * @version 1.0
 */
VKAPI_ATTR VkResult VKAPI_CALL vkGetMemoryNativeBufferOHOS(
    VkDevice                                    device,
    const VkMemoryGetNativeBufferInfoOHOS*      pInfo,
    struct OH_NativeBuffer**                    pBuffer);

/**
 * @brief Returns the appropriate gralloc usage flag based on
 * the given Vulkan device, image format, and image usage flag.
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param device <b>VkDevice</b> instance.
 * @param format Image format.
 * @param imageUsage Image usage flag.
 * @param grallocUsage Pointer to the gralloc usage flag.
 * @return Returns <b>VK_SUCCESS</b> if the execution is successful;
 * returns an error code of the VkResult type otherwise.
 * @since 10
 * @version 1.0
 */
VKAPI_ATTR VkResult VKAPI_CALL vkGetSwapchainGrallocUsageOHOS (
    VkDevice                                device,
    VkFormat                                format,
    VkImageUsageFlags                       imageUsage,
    uint64_t*                               grallocUsage);

/**
 * @brief Obtains the ownership of the swap chain image and imports the fence of the external signal
 * to the VkSemaphore and VkFence objects.
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param device <b>VkDevice</b> instance.
 * @param image Vulkan image to obtain.
 * @param nativeFenceFd File descriptor of the native fence.
 * @param semaphore Vulkan semaphore indicating that the image is available.
 * @param fence Vulkan fence used for synchronization when the image acquisition is complete.
 * @return Returns <b>VK_SUCCESS</b> if the execution is successful;
 * returns an error code of the VkResult type otherwise.
 * @since 10
 * @version 1.0
 */
VKAPI_ATTR VkResult VKAPI_CALL vkAcquireImageOHOS (
    VkDevice        device,
    VkImage         image,
    int32_t         nativeFenceFd,
    VkSemaphore     semaphore,
    VkFence         fence);

/**
 * @brief Sends a signal to the system hardware buffer to release an image once it is no longer needed
 * so that other components can access it.
 *
 * @syscap SystemCapability.Graphic.Vulkan
 * @param queue Handle to the Vulkan queue.
 * @param waitSemaphoreCount Number of semaphores to wait on.
 * @param pWaitSemaphores Pointer to the array of semaphores to wait on.
 * @param images Handle to the Vulkan image to be released.
 * @param pNativeFenceFd Pointer to the file descriptor of the fence.
 * @return Returns <b>VK_SUCCESS</b> if the execution is successful;
 * returns an error code of the VkResult type otherwise.
 * @since 10
 * @version 1.0
 */
VKAPI_ATTR VkResult VKAPI_CALL vkQueueSignalReleaseImageOHOS (
    VkQueue                 queue,
    uint32_t                waitSemaphoreCount,
    const VkSemaphore*      pWaitSemaphores,
    VkImage                 image,
    int32_t*                pNativeFenceFd);
#endif

#ifdef __cplusplus
}
#endif

#endif
