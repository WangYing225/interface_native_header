/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_EFFECT_FILTER_H
#define C_INCLUDE_EFFECT_FILTER_H

/**
 * @addtogroup effectKit
 * @{
 *
 * @brief Provides the basic image processing capabilities, including brightness adjustment, blurring,
 * and grayscale adjustment.
 *
 * @since 12
 * @version 1.0
 */

/**
 * @file effect_filter.h
 *
 * @brief Declares the APIs of an image effect filter.
 *
 * File to include: <native_effect/effect_filter.h>
 * @library libnative_effect.so
 * @syscap SystemCapability.Multimedia.Image.Core
 * @since 12
 */

#include "effect_types.h"
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates an <b>OH_Filter</b> object based on a pixel map.
 *
 * @param pixelmap Pointer to the pixel map.
 * @param filter Double pointer to the filter created.
 * @return Returns a status code defined in {@link EffectErrorCode}.
 * @since 12
 */
EffectErrorCode OH_Filter_CreateEffect(OH_PixelmapNative* pixelmap, OH_Filter** filter);

/**
 * @brief Releases an <b>OH_Filter</b> object.
 *
 * @param filter Pointer to the filter.
 * @return Returns a status code defined in {@link EffectErrorCode}.
 * @since 12
 */
EffectErrorCode OH_Filter_Release(OH_Filter* filter);

/**
 * @brief Creates the frosted glass effect and adds it to a filter.
 *
 * @param filter Pointer to the filter.
 * @param radius Blur radius of the frosted glass effect, in px.
 * @return Returns a status code defined in {@link EffectErrorCode}.
 * @since 12
 */
EffectErrorCode OH_Filter_Blur(OH_Filter* filter, float radius);

/**
 * @brief Creates the frosted glass effect and adds it to a filter.
 *
 * @param filter Pointer to the filter.
 * @param radius Blur radius of the frosted glass effect, in px.
 * @param tileMode Tile mode of the shader effect. For details about the available options, see {@link EffectTileMode}.
 * @return Returns a status code defined in {@link EffectErrorCode}.
 * @since 14
 */
EffectErrorCode OH_Filter_BlurWithTileMode(OH_Filter* filter, float radius, EffectTileMode tileMode);

/**
 * @brief Creates the brightening effect and adds it to a filter.
 *
 * @param filter Pointer to the filter.
 * @param brightness Luminance of the brightening effect. The value ranges from 0 to 1.
 * When the value is <b>0</b>, the image remains unchanged.
 * @return Returns a status code defined in {@link EffectErrorCode}.
  * @since 12
 */
EffectErrorCode OH_Filter_Brighten(OH_Filter* filter, float brightness);

/**
 * @brief Creates the grayscale effect and adds it to a filter.
 *
 * @param filter Pointer to the filter.
 * @return Returns a status code defined in {@link EffectErrorCode}.
 * @since 12
 */
EffectErrorCode OH_Filter_GrayScale(OH_Filter* filter);

/**
 * @brief Creates the inverted color effect and adds it to a filter.
 *
 * @param filter Pointer to the filter.
 * @return Returns a status code defined in {@link EffectErrorCode}.
 * @since 12
 */
EffectErrorCode OH_Filter_Invert(OH_Filter* filter);

/**
 * @brief Creates a custom effect through a matrix and adds it to a filter.
 *
 * @param filter Pointer to the filter.
 * @param matrix Pointer to a custom matrix, which is an {@link OH_Filter_ColorMatrix} object.
 * @return Returns a status code defined in {@link EffectErrorCode}.
 * @since 12
 */
EffectErrorCode OH_Filter_SetColorMatrix(OH_Filter* filter, OH_Filter_ColorMatrix* matrix);

/**
 * @brief Obtains the pixel map used to create a filter.
 *
 * @param filter Pointer to the filter.
 * @param pixelmap Double pointer to the pixel map obtained.
 * @return Returns a status code defined in {@link EffectErrorCode}.
 * @since 12
 */
EffectErrorCode OH_Filter_GetEffectPixelMap(OH_Filter* filter, OH_PixelmapNative** pixelmap);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
