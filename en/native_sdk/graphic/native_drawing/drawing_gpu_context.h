/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_GPU_CONTEXT_H
#define C_INCLUDE_DRAWING_GPU_CONTEXT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_gpu_context.h
 *
 * @brief Declares the functions related to the GPU context in the drawing module.
 *
 <b>File to include</b>: native_drawing/drawing_gpu_context.h
 * @library libnative_drawing.so
 * @since 12
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines a struct for the options about the GPU context.
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_GpuContextOptions {
    /**
     * Whether to allow path mask textures to be cached.
     * The value <b>true</b> means to allow the path mask textures to be cached, and <b>false</b> means the opposite.
     */
    bool allowPathMaskCaching;
} OH_Drawing_GpuContextOptions;

/**
 * @brief Creates an <b>OH_Drawing_GpuContext</b> object that uses OpenGL as the backend interface.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_GpuContextOptions GPU context option, which is an {@link OH_Drawing_GpuContextOptions} object.
 * @return Returns the pointer to the [OH_Drawing_GpuContext](#oh_drawing_gpucontext) object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_GpuContext* OH_Drawing_GpuContextCreateFromGL(OH_Drawing_GpuContextOptions);

/**
 * @brief Destroys an <b>OH_Drawing_GpuContext</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_GpuContext Pointer to an {@link OH_Drawing_GpuContext} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_GpuContextDestroy(OH_Drawing_GpuContext*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
