/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_COLOR_FILTER_H
#define C_INCLUDE_DRAWING_COLOR_FILTER_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_color_filter.h
 *
 * @brief Declares the functions related to the color filter in the drawing module.
 *
 * File to include: native_drawing/drawing_color_filter.h
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates an <b>OH_Drawing_ColorFilter</b> object with a given blend mode.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param color Color, which is a 32-bit (ARGB) variable.
 * @param OH_Drawing_BlendMode Blend mode. For details about the available options, see {@link OH_Drawing_BlendMode}.
 * @return Returns the pointer to the <b>OH_Drawing_ColorFilter</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_ColorFilter* OH_Drawing_ColorFilterCreateBlendMode(uint32_t color, OH_Drawing_BlendMode);

/**
 * @brief Creates an <b>OH_Drawing_ColorFilter</b> object by combining another two color filters.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>colorFilter1</b> or <b>colorFilter2</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param colorFilter1 Pointer to the first color filter.
 * @param colorFilter2 Pointer to the second color filter.
 * @return Returns the pointer to the <b>OH_Drawing_ColorFilter</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_ColorFilter* OH_Drawing_ColorFilterCreateCompose(OH_Drawing_ColorFilter* colorFilter1,
    OH_Drawing_ColorFilter* colorFilter2);

/**
 * @brief Creates an <b>OH_Drawing_ColorFilter</b> object with a given 5x4 color matrix.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>matrix</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param matrix Matrix, which is represented by a floating-point array with a length of 20.
 * @return Returns the pointer to the <b>OH_Drawing_ColorFilter</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_ColorFilter* OH_Drawing_ColorFilterCreateMatrix(const float matrix[20]);

/**
 * @brief Creates an <b>OH_Drawing_ColorFilter</b> object that applies the sRGB gamma curve to the RGB channels.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_ColorFilter</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_ColorFilter* OH_Drawing_ColorFilterCreateLinearToSrgbGamma(void);

/**
 * @brief Creates an <b>OH_Drawing_ColorFilter</b> object that applies the RGB channels to the sRGB gamma curve.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_ColorFilter</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_ColorFilter* OH_Drawing_ColorFilterCreateSrgbGammaToLinear(void);

/**
 * @brief Creates an <b>OH_Drawing_ColorFilter</b> object that multiplies the passed-in luma into the alpha channel
 * and sets the RGB channels to zero.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_ColorFilter</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_ColorFilter* OH_Drawing_ColorFilterCreateLuma(void);

/**
 * @brief Destroys an <b>OH_Drawing_ColorFilter</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_ColorFilter Pointer to an <b>OH_Drawing_ColorFilter</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_ColorFilterDestroy(OH_Drawing_ColorFilter*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
