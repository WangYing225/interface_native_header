/*
 * Copyright (c) 2023-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_FILTER_H
#define C_INCLUDE_DRAWING_FILTER_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_filter.h
 *
 * @brief Declares the functions related to the filter in the drawing module.
 *
 * File to include: native_drawing/drawing_filter.h
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates an <b>OH_Drawing_Filter</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_Filter</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_Filter* OH_Drawing_FilterCreate(void);

/**
 * @brief Sets an <b>OH_Drawing_ImageFilter</b> object for an <b>OH_Drawing_Filter</b> object.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Filter</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Filter Pointer to an {@link OH_Drawing_Filter} object.
 * @param OH_Drawing_ImageFilter Pointer to an {@link OH_Drawing_ImageFilter} object.
 * If NULL is passed in, the image filter effect of the object will be cleared.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FilterSetImageFilter(OH_Drawing_Filter*, OH_Drawing_ImageFilter*);

/**
 * @brief Sets an <b>OH_Drawing_MaskFilter</b> object for an <b>OH_Drawing_Filter</b> object.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Filter</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Filter Pointer to an {@link OH_Drawing_Filter} object.
 * @param OH_Drawing_MaskFilter Pointer to an {@link OH_Drawing_ColorFilter} object.
 * If NULL is passed in, the mask filter effect of the object will be cleared.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FilterSetMaskFilter(OH_Drawing_Filter*, OH_Drawing_MaskFilter*);

/**
 * @brief Sets an <b>OH_Drawing_ColorFilter</b> object for an <b>OH_Drawing_Filter</b> object.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Filter</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Filter Pointer to an {@link OH_Drawing_Filter} object.
 * @param OH_Drawing_ColorFilter Pointer to an {@link OH_Drawing_ColorFilter} object.
 * If NULL is passed in, the color filter effect of the object will be cleared.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FilterSetColorFilter(OH_Drawing_Filter*, OH_Drawing_ColorFilter*);

/**
 * @brief Obtains an <b>OH_Drawing_ColorFilter</b> object from an <b>OH_Drawing_Filter</b> object.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>OH_Drawing_Filter</b> or <b>OH_Drawing_ColorFilter</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Filter Pointer to an {@link OH_Drawing_Filter} object.
 * @param OH_Drawing_ColorFilter Pointer to the {@link OH_Drawing_ColorFilter} object obtained.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FilterGetColorFilter(OH_Drawing_Filter*, OH_Drawing_ColorFilter*);

/**
 * @brief Destroys an <b>OH_Drawing_Filter</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Filter Pointer to an {@link OH_Drawing_Filter} object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FilterDestroy(OH_Drawing_Filter*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
