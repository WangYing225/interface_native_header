/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup UDMF
 * @{
 *
 * @brief Defines unified data definitions for data interaction across applications, devices, and platforms,
 * and provides a unified OpenHarmony data language and standard data access and read channels.
 *
 * @syscap SystemCapability.DistributedDataManager.UDMF.Core
 *
 * @since 12
 */

 /**
 * @file udmf_meta.h
 *
 * @brief Defines uniform data types.
 * File to include: <database/udmf/udmf_meta.h>
 * @library libudmf.so
 * @syscap SystemCapability.DistributedDataManager.UDMF.Core
 *
 * @since 12
 */
#ifndef UDMF_META_H
#define UDMF_META_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines the generic type that represents all physical storage types.
 * It is used to define physical properties of a type. This type is uncategorized.
 *
 * @since 12
 */
#define UDMF_META_ENTITY "general.entity"

/**
 * @brief Represents the generic type for all logical content types. It is used to define physical properties of a type.
 * This type is uncategorized.
 *
 * @since 12
 */
#define UDMF_META_OBJECT "general.object"

/**
 * @brief Represents the generic composite content type. For example, a PDF file that contains text and image.
 * This type belongs to <b>OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_COMPOSITE_OBJECT "general.composite-object"

/**
 * @brief Represents the generic text type. This type belongs to <b>OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_TEXT "general.text"

/**
 * @brief Represents text without specific encoding or identifier. It belongs to <b>TEXT</b>.
 *
 * @since 12
 */
#define UDMF_META_PLAIN_TEXT "general.plain-text"

/**
 * @brief Represents HTML. This type belongs to <b>TEXT</b>.
 *
 * @since 12
 */
#define UDMF_META_HTML "general.html"

/**
 * @brief Represents Hyperlink. This type belongs to <b>TEXT</b>.
 *
 * @since 12
 */
#define UDMF_META_HYPERLINK "general.hyperlink"

/**
 * @brief Represents XML. This type belongs to <b>TEXT</b>.
 *
 * @since 12
 */
#define UDMF_META_XML "general.xml"

/**
 * @brief Represents the generic source code type. This type belongs to <b>PLAIN_TEXT</b>.
 *
 * @since 12
 */
#define UDMF_META_SOURCE_CODE "general.source-code"

/**
 * @brief Represents the source code in any scripting language. It belongs to <b>SOURCE_CODE</b>.
 *
 * @since 12
 */
#define UDMF_META_SCRIPT "general.script"

/**
 * @brief Represents a Shell script. This type belongs to <b>SCRIPT</b>.
 *
 * @since 12
 */
#define UDMF_META_SHELL_SCRIPT "general.shell-script"

/**
 * @brief Represents a C shell script. This type belongs to <b>SHELL_SCRIPT</b>.
 *
 * @since 12
 */
#define UDMF_META_CSH_SCRIPT "general.csh-script"

/**
 * @brief Represents a Perl script. This type belongs to <b>SHELL_SCRIPT</b>.
 *
 * @since 12
 */
#define UDMF_META_PERL_SCRIPT "general.perl-script"

/**
 * @brief Represents a PHP script. This type belongs to <b>SHELL_SCRIPT</b>.
 *
 * @since 12
 */
#define UDMF_META_PHP_SCRIPT "general.php-script"

/**
 * @brief Represents a Python script. This type belongs to <b>SHELL_SCRIPT</b>.
 *
 * @since 12
 */
#define UDMF_META_PYTHON_SCRIPT "general.python-script"

/**
 * @brief Represents a Ruby script. This type belongs to <b>SHELL_SCRIPT</b>.
 *
 * @since 12
 */
#define UDMF_META_RUBY_SCRIPT "general.ruby-script"

/**
 * @brief Represents TypeScript source code. This type belongs to <b>SCRIPT</b>.
 *
 * @since 12
 */
#define UDMF_META_TYPE_SCRIPT "general.type-script"

/**
 * @brief Represents JavaScript source code. This type belongs to <b>SCRIPT</b>.
 *
 * @since 12
 */
#define UDMF_META_JAVA_SCRIPT "general.java-script"

/**
 * @brief Represents a header file in C. This type belongs to <b>SOURCE_CODE</b>.
 *
 * @since 12
 */
#define UDMF_META_C_HEADER "general.c-header"

/**
 * @brief Represents the source code in C. This type belongs to <b>SOURCE_CODE</b>.
 *
 * @since 12
 */
#define UDMF_META_C_SOURCE "general.c-source"

/**
 * @brief Represents a header file in C++. This type belongs to <b>SOURCE_CODE</b>.
 *
 * @since 12
 */
#define UDMF_META_C_PLUS_PLUS_HEADER "general.c-plus-plus-header"

/**
 * @brief Represents the source code in C++. This type belongs to <b>SOURCE_CODE</b>.
 *
 * @since 12
 */
#define UDMF_META_C_PLUS_PLUS_SOURCE "general.c-plus-plus-source"

/**
 * @brief Represents Java source code. This type belongs to <b>SOURCE_CODE</b>.
 *
 * @since 12
 */
#define UDMF_META_JAVA_SOURCE "general.java-source"

/**
 * @brief Represents the generic eBook file format. This type belongs to <b>COMPOSITE_OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_EBOOK "general.ebook"

/**
 * @brief Represents EPUB. This type belongs to <b>EBOOK</b>.
 *
 * @since 12
 */
#define UDMF_META_EPUB "general.epub"

/**
 * @brief Represents AZW. This type belongs to <b>EBOOK</b>.
 *
 * @since 12
 */
#define UDMF_META_AZW "com.amazon.azw"

/**
 * @brief Represents AZW3. This type belongs to <b>EBOOK</b>.
 *
 * @since 12
 */
#define UDMF_META_AZW3 "com.amazon.azw3"

/**
 * @brief Represents KFX. This type belongs to <b>EBOOK</b>.
 *
 * @since 12
 */
#define UDMF_META_KFX "com.amazon.kfx"

/**
 * @brief Represents MOBI. This type belongs to <b>EBOOK</b>.
 *
 * @since 12
 */
#define UDMF_META_MOBI "com.amazon.mobi"

/**
 * @brief Represents the generic media type. This type belongs to <b>OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_MEDIA "general.media"

/**
 * @brief Represents the generic image type. This type belongs to <b>MEDIA</b>.
 *
 * @since 12
 */
#define UDMF_META_IMAGE "general.image"

/**
 * @brief Represents JPEG. This type belongs to <b>IMAGE</b>.
 *
 * @since 12
 */
#define UDMF_META_JPEG "general.jpeg"

/**
 * @brief Represents PNG. This type belongs to <b>IMAGE</b>.
 *
 * @since 12
 */
#define UDMF_META_PNG "general.png"

/**
 * @brief Represents a raw image. This type belongs to <b>IMAGE</b>.
 *
 * @since 12
 */
#define UDMF_META_RAW_IMAGE "general.raw-image"

/**
 * @brief Represents TIFF. This type belongs to <b>IMAGE</b>.
 *
 * @since 12
 */
#define UDMF_META_TIFF "general.tiff"

/**
 * @brief Represents BMP. This type belongs to <b>IMAGE</b>.
 *
 * @since 12
 */
#define UDMF_META_BMP "com.microsoft.bmp"

/**
 * @brief Represents Windows icon type. This type belongs to <b>IMAGE</b>.
 *
 * @since 12
 */
#define UDMF_META_ICO "com.microsoft.ico"

/**
 * @brief Represents an Adobe Photoshop image. This type belongs to <b>IMAGE</b>.
 *
 * @since 12
 */
#define UDMF_META_PHOTOSHOP_IMAGE "com.adobe.photoshop-image"

/**
 * @brief Represents adobe Illustrator image (.ai). This type belongs to <b>IMAGE</b>.
 *
 * @since 12
 */
#define UDMF_META_AI_IMAGE "com.adobe.illustrator.ai-image"

/**
 * @brief Represents Microsoft Word. This type belongs to <b>COMPOSITE_OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_WORD_DOC "com.microsoft.word.doc"

/**
 * @brief Represents Microsoft Excel. This type belongs to <b>COMPOSITE_OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_EXCEL "com.microsoft.excel.xls"

/**
 * @brief Represents Microsoft PowerPoint presentation format. This type belongs to <b>COMPOSITE_OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_PPT "com.microsoft.powerpoint.ppt"

/**
 * @brief Represents PDF. This type belongs to <b>COMPOSITE_OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_PDF "com.adobe.pdf"

/**
 * @brief Represents PostScript. This type belongs to <b>COMPOSITE_OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_POSTSCRIPT "com.adobe.postscript"

/**
 * @brief Represents encapsulated PostScript. This type belongs to <b>POSTSCRIPT</b>.
 *
 * @since 12
 */
#define UDMF_META_ENCAPSULATED_POSTSCRIPT "com.adobe.encapsulated-postscript"

/**
 * @brief Represents the generic video type. This type belongs to <b>MEDIA</b>.
 *
 * @since 12
 */
#define UDMF_META_VIDEO "general.video"

/**
 * @brief Represents AVI. This type belongs to <b>VIDEO</b>.
 *
 * @since 12
 */
#define UDMF_META_AVI "general.avi"

/**
 * @brief Represents MPGE-1 or MPGE-2. This type belongs to <b>VIDEO</b>.
 *
 * @since 12
 */
#define UDMF_META_MPEG "general.mpeg"

/**
 * @brief Represents MPGE-4. This type belongs to <b>VIDEO</b>.
 *
 * @since 12
 */
#define UDMF_META_MPEG4 "general.mpeg-4"

/**
 * @brief Represents 3GP (3GPP file format). This type belongs to <b>VIDEO</b>.
 *
 * @since 12
 */
#define UDMF_META_VIDEO_3GPP "general.3gpp"

/**
 * @brief Represents 3G2 (3GPP2 file format). This type belongs to <b>VIDEO</b>.
 *
 * @since 12
 */
#define UDMF_META_VIDEO_3GPP2 "general.3gpp2"

/**
 * @brief Represents Windows WM format. This type belongs to <b>VIDEO</b>.
 *
 * @since 12
 */
#define UDMF_META_WINDOWS_MEDIA_WM "com.microsoft.windows-media-wm"

/**
 * @brief Represents Windows WMV. This type belongs to <b>VIDEO</b>.
 *
 * @since 12
 */
#define UDMF_META_WINDOWS_MEDIA_WMV "com.microsoft.windows-media-wmv"

/**
 * @brief Represents Windows WMP. This type belongs to <b>VIDEO</b>.
 *
 * @since 12
 */
#define UDMF_META_WINDOWS_MEDIA_WMP "com.microsoft.windows-media-wmp"

/**
 * @brief Represents the generic audio type. This type belongs to <b>MEDIA</b>.
 *
 * @since 12
 */
#define UDMF_META_AUDIO "general.audio"

/**
 * @brief Represents AAC. This type belongs to <b>AUDIO</b>.
 *
 * @since 12
 */
#define UDMF_META_AAC "general.aac"

/**
 * @brief Represents AIFF. This type belongs to <b>AUDIO</b>.
 *
 * @since 12
 */
#define UDMF_META_AIFF "general.aiff"

/**
 * @brief Represents ALAC. This type belongs to <b>AUDIO</b>.
 *
 * @since 12
 */
#define UDMF_META_ALAC "general.alac"

/**
 * @brief Represents FLAC. This type belongs to <b>AUDIO</b>.
 *
 * @since 12
 */
#define UDMF_META_FLAC "general.flac"

/**
 * @brief Represents MP3. This type belongs to <b>AUDIO</b>.
 *
 * @since 12
 */
#define UDMF_META_MP3 "general.mp3"

/**
 * @brief Represents OGG. This type belongs to <b>AUDIO</b>.
 *
 * @since 12
 */
#define UDMF_META_OGG "general.ogg"

/**
 * @brief Represents PCM. This type belongs to <b>AUDIO</b>.
 *
 * @since 12
 */
#define UDMF_META_PCM "general.pcm"

/**
 * @brief Represents Windows WMA. This type belongs to <b>AUDIO</b>.
 *
 * @since 12
 */
#define UDMF_META_WINDOWS_MEDIA_WMA "com.microsoft.windows-media-wma"

/**
 * @brief Represents Windows Waveform. This type belongs to <b>AUDIO</b>.
 *
 * @since 12
 */
#define UDMF_META_WAVEFORM_AUDIO "com.microsoft.waveform-audio"

/**
 * @brief Represents Windows WMX. This type belongs to <b>VIDEO</b>.
 *
 * @since 12
 */
#define UDMF_META_WINDOWS_MEDIA_WMX "com.microsoft.windows-media-wmx"

/**
 * @brief Represents Windows WVX format. This type belongs to <b>VIDEO</b>.
 *
 * @since 12
 */
#define UDMF_META_WINDOWS_MEDIA_WVX "com.microsoft.windows-media-wvx"

/**
 * @brief Represents Windows WAX. This type belongs to <b>AUDIO</b>.
 *
 * @since 12
 */
#define UDMF_META_WINDOWS_MEDIA_WAX "com.microsoft.windows-media-wax"

/**
 * @brief Represents the generic file type. This type belongs to <b>ENTITY</b>.
 *
 * @since 12
 */
#define UDMF_META_GENERAL_FILE "general.file"

/**
 * @brief Represents the generic directory type. This type belongs to <b>ENTITY</b>.
 *
 * @since 12
 */
#define UDMF_META_DIRECTORY "general.directory"

/**
 * @brief Represents the generic folder type. This type belongs to <b>DIRECTORY</b>.
 *
 * @since 12
 */
#define UDMF_META_FOLDER "general.folder"

/**
 * @brief Represents the generic symbolic type. This type belongs to <b>ENTITY</b>.
 *
 * @since 12
 */
#define UDMF_META_SYMLINK "general.symlink"

/**
 * @brief Represents the generic archive file type. This type belongs to <b>OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_ARCHIVE "general.archive"

/**
 * @brief Represents BZ2. This type belongs to <b>ARCHIVE</b>.
 *
 * @since 12
 */
#define UDMF_META_BZ2_ARCHIVE "general.bz2-archive"

/**
 * @brief Represents the generic type of any file that can be mounted as a volume. This type belongs to <b>ARCHIVE</b>.
 *
 * @since 12
 */
#define UDMF_META_DISK_IMAGE "general.disk-image"

/**
 * @brief Represents GZIP TAR. This type belongs to <b>ARCHIVE</b>.
 *
 * @since 12
 */
#define UDMF_META_TAR_ARCHIVE "general.tar-archive"

/**
 * @brief Represents ZIP. This type belongs to <b>ARCHIVE</b>.
 *
 * @since 12
 */
#define UDMF_META_ZIP_ARCHIVE "general.zip-archive"

/**
 * @brief Represents JAR (Java archive). This type belongs to <b>ARCHIVE</b> and <b>EXECUTABLE</b>.
 *
 * @since 12
 */
#define UDMF_META_JAVA_ARCHIVE "com.sun.java-archive"

/**
 * @brief Represents GUN archive. This type belongs to <b>ARCHIVE</b>.
 *
 * @since 12
 */
#define UDMF_META_GNU_TAR_ARCHIVE "org.gnu.gnu-tar-archive"

/**
 * @brief Represents GZIP archive. This type belongs to <b>ARCHIVE</b>.
 *
 * @since 12
 */
#define UDMF_META_GNU_ZIP_ARCHIVE "org.gnu.gnu-zip-archive"

/**
 * @brief Represents GZIP TAR. This type belongs to <b>ARCHIVE</b>.
 *
 * @since 12
 */
#define UDMF_META_GNU_ZIP_TAR_ARCHIVE "org.gnu.gnu-zip-tar-archive"

/**
 * @brief Represents the generic calendar type. This type belongs to <b>OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_CALENDAR "general.calendar"

/**
 * @brief Represents the generic contact type. This type belongs to <b>OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_CONTACT "general.contact"

/**
 * @brief Represents the generic database file type. This type belongs to <b>OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_DATABASE "general.database"

/**
 * @brief Represents the generic message type. This type belongs to <b>OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_MESSAGE "general.message"

/**
 * @brief Represents the generic electronic business card type. This type belongs to <b>OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_VCARD "general.vcard"

/**
 Generic navigation data type. This type belongs to <b>OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_NAVIGATION "general.navigation"

/**
 * @brief Represents location data type. This type belongs to <b>NAVIGATION</b>.
 *
 * @since 12
 */
#define UDMF_META_LOCATION "general.location"

/**
 * @brief Represents the widget defined for the system. This type belongs to <b>OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENHARMONY_FORM "openharmony.form"

/**
 * @ Represents the home screen icon defined for the system. This type belongs to <b>OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENHARMONY_APP_ITEM "openharmony.app-item"

/**
 * @brief Represents the Pixel map defined for the system. This type belongs to <b>IMAGE</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENHARMONY_PIXEL_MAP "openharmony.pixel-map"

/**
 * @brief Represents the atomic service type defined for the system. This type belongs to <b>OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENHARMONY_ATOMIC_SERVICE "openharmony.atomic-service"

/**
 * @brief Represents the package (compressed folder) defined for the system. This type belongs to <b>DIRECTORY</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENHARMONY_PACKAGE "openharmony.package"

/**
 * @brief Represents the ability package defined for the system. This type belongs to <b>OPENHARMONY_PACKAGE</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENHARMONY_HAP "openharmony.hap"

/**
 * @brief Represents SMIL. This type belongs to <b>XML</b>.
 *
 * @since 12
 */
#define UDMF_META_SMIL "com.real.smil"

/**
 * @brief Represents Markdown. This type belongs to <b>PLAIN_TEXT</b>.
 *
 * @since 12
 */
#define UDMF_META_MARKDOWN "general.markdown"

/**
 * @brief Represents the generic fax type. This type belongs to <b>IMAGE</b>.
 *
 * @since 12
 */
#define UDMF_META_FAX "general.fax"

/**
 * @brief Represents the J2 jConnect fax file format. This type belongs to <b>FAX</b>.
 *
 * @since 12
 */
#define UDMF_META_JFX_FAX "com.j2.jfx-fax"

/**
 * @brief Represents the EFX file format. This type belongs to <b>FAX</b>.
 *
 * @since 12
 */
#define UDMF_META_EFX_FAX "com.js.efx-fax"

/**
 * @brief Represents X BitMAP (XBM) used in the X Window system (X11). This type belongs to <b>IMAGE</b>.
 *
 * @since 12
 */
#define UDMF_META_XBITMAP_IMAGE "general.xbitmap-image"

/**
 * @brief Represents TGA. This type belongs to <b>IMAGE</b>.
 *
 * @since 12
 */
#define UDMF_META_TGA_IMAGE "com.truevision.tga-image"

/**
 * @brief Represents SGI format. This type belongs to <b>IMAGE</b>.
 *
 * @since 12
 */
#define UDMF_META_SGI_IMAGE "com.sgi.sgi-image"

/**
 * @brief Represents OpenXR image format. This type belongs to <b>IMAGE</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENEXR_IMAGE "com.ilm.openexr-image"

/**
 * @brief Represents the FlashPix image format. This type belongs to <b>IMAGE</b>.
 *
 * @since 12
 */
#define UDMF_META_FLASHPIX_IMAGE "com.kodak.flashpix.image"

/**
 * @brief Represents RealMedia format. This type belongs to <b>VIDEO</b>.
 *
 * @since 12
 */
#define UDMF_META_REALMEDIA "com.real.realmedia"

/**
 * @brief Represents the AU format. This type belongs to <b>AUDIO</b>.
 *
 * @since 12
 */
#define UDMF_META_AU_AUDIO "general.au-audio"

/**
 * @brief Represents AIFC. This type belongs to <b>AUDIO</b>.
 *
 * @since 12
 */
#define UDMF_META_AIFC_AUDIO "general.aifc-audio"

/**
 * @brief Represents SDII. This type belongs to <b>AUDIO</b>.
 *
 * @since 12
 */
#define UDMF_META_SD2_AUDIO "com.digidesign.sd2-audio"

/**
 * @brief Represents RealAudio. This type belongs to <b>AUDIO</b>.
 *
 * @since 12
 */
#define UDMF_META_REALAUDIO "com.real.realaudio"

/**
 * @brief Represents OpenXML. This type belongs to <b>ARCHIVE</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENXML "org.openxmlformats.openxml"

/**
 * @brief Represents WordProcessingML format. This type belongs to <b>OPENXML</b> and <b>COMPOSITE_OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_WORDPROCESSINGML_DOCUMENT "org.openxmlformats.wordprocessingml.document"

/**
 * @brief Represents the SpreadsheetML format. This type belongs to <b>OPENXML</b> and <b>COMPOSITE_OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_SPREADSHEETML_SHEET "org.openxmlformats.spreadsheetml.sheet"

/**
 * @brief Represents PresentationML format. This type belongs to <b>OPENXML</b> and <b>COMPOSITE_OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_PRESENTATIONML_PRESENTATION "org.openxmlformats.presentationml.presentation"

/**
 * @brief Represents OpenDocument format for Office applications. This type belongs to <b>ARCHIVE</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENDOCUMENT "org.oasis.opendocument"

/**
 * @brief Represents OpenDocument format for word processing (text) documents.
 * This type belongs to <b>OPENDOCUMENT</b> and <b>COMPOSITE_OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENDOCUMENT_TEXT "org.oasis.opendocument.text"

/**
 * @brief Represents OpenDocument format for spreadsheets.
 * This type belongs to <b>OPENDOCUMENT</b> and <b>COMPOSITE_OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENDOCUMENT_SPREADSHEET "org.oasis.opendocument.spreadsheet"

/**
 * @brief Represents OpenDocument format for presentations.
 * This type belongs to <b>OPENDOCUMENT</b> and <b>COMPOSITE_OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENDOCUMENT_PRESENTATION "org.oasis.opendocument.presentation"

/**
 * @brief Represents OpenDocument format for graphics.
 * This type belongs to <b>OPENDOCUMENT</b> and <b>COMPOSITE_OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENDOCUMENT_GRAPHICS "org.oasis.opendocument.graphics"

/**
 * @brief Represents OpenDocument format for formula.
 * This type belongs to <b>OPENDOCUMENT</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENDOCUMENT_FORMULA "org.oasis.opendocument.formula"

/**
 * @brief Represents the Stuffit compression format (stuffit archive). This type belongs to <b>ARCHIVE</b>.
 *
 * @since 12
 */
#define UDMF_META_STUFFIT_ARCHIVE "com.allume.stuffit-archive"

/**
 * @brief Represents the VCS format. This type belongs to <b>CALENDAR</b> and <b>TEXT</b>.
 *
 * @since 12
 */
#define UDMF_META_VCS "general.vcs"

/**
 * @brief 	* @brief Represents the ICS format. This type belongs to <b>CALENDAR</b> and <b>TEXT</b>.
 *
 * @since 12
 */
#define UDMF_META_ICS "general.ics"

/**
 * @brief Represents the generic type of all executable files. This type belongs to <b>OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_EXECUTABLE "general.executable"

/**
 * @brief Represents the Microsoft Windows portable executable format. This type belongs to <b>EXECUTABLE</b>.
 *
 * @since 12
 */
#define UDMF_META_PORTABLE_EXECUTABLE "com.microsoft.portable-executable"

/**
 * @brief Represents the Java class file format. This type belongs to <b>EXECUTABLE</b>.
 *
 * @since 12
 */
#define UDMF_META_SUN_JAVA_CLASS "com.sun.java-class"

/**
 * @brief Represents the generic font type. This type belongs to <b>OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_FONT "general.font"

/**
 * @brief Represents the TrueType font format. This type belongs to <b>FONT</b>.
 *
 * @since 12
 */
#define UDMF_META_TRUETYPE_FONT "general.truetype-font"

/**
 * @brief Represents the TrueType Collection font format. This type belongs to <b>FONT</b>.
 *
 * @since 12
 */
#define UDMF_META_TRUETYPE_COLLECTION_FONT "general.truetype-collection-font"

/**
 * @brief Represents the OpenType font format. This type belongs to <b>FONT</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENTYPE_FONT "general.opentype-font"

/**
 * @brief Represents the PostScript font format. This type belongs to <b>FONT</b>.
 *
 * @since 12
 */
#define UDMF_META_POSTSCRIPT_FONT "com.adobe.postscript-font"

/**
 * @brief Represents PostScript Font Binary font format. This type belongs to <b>FONT</b>.
 *
 * @since 12
 */
#define UDMF_META_POSTSCRIPT_PFB_FONT "com.adobe.postscript-pfb-font"

/**
 * @brief Represents Adobe Type 1 font format. This type belongs to <b>FONT</b>.
 *
 * @since 12
 */
#define UDMF_META_POSTSCRIPT_PFA_FONT "com.adobe.postscript-pfa-font"

/**
 * @brief Represents the memo format defined for the system. This type belongs to <b>COMPOSITE_OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENHARMONY_HDOC "openharmony.hdoc"

/**
 * @brief Represents the note format defined for the system. This type belongs to <b>COMPOSITE_OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENHARMONY_HINOTE "openharmony.hinote"

/**
 * @brief Represents the style string type defined for the system. This type belongs to <b>COMPOSITE_OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENHARMONY_STYLED_STRING "openharmony.styled-string"

/**
 * @brief Represents the Want type defined for the system. This type belongs to <b>OBJECT</b>.
 *
 * @since 12
 */
#define UDMF_META_OPENHARMONY_WANT "openharmony.want"

/**
 * @brief Represents the file address type. This type belongs to <b>TEXT</b>.
 *
 * @since 13
 */
#define UDMF_META_GENERAL_FILE_URI "general.file-uri"

#ifdef __cplusplus
};
#endif

/** @} */
#endif
