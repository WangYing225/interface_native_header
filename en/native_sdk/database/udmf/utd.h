/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup UDMF
 * @{
 *
 * @brief Defines unified data definitions for data interaction across applications, devices, and platforms,
 * and provides a unified OpenHarmony data language and standard data access and read channels.
 *
 * @syscap SystemCapability.DistributedDataManager.UDMF.Core
 * @since 12
 */

/**
 * @file utd.h
 *
 * @brief Defines APIs and structs related to the Uniform Type Descriptors (UTDs).
 * File to include: <database/udmf/utd.h>
 * @library libudmf.so
 * @syscap SystemCapability.DistributedDataManager.UDMF.Core
 * @since 12
 */
#ifndef UTD_H
#define UTD_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines a struct for a UTD.
 *
 * @since 12
 */
typedef struct OH_Utd OH_Utd;

/**
 * @brief Creates a pointer to an {@link OH_Utd} instance.
 *
 * @param typeId Pointer to the ID of the UTD instance to create.
 * @return Returns the pointer to the {@link OH_Utd} instance created if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * If this pointer is no longer required, use {@link OH_Utd_Destroy} to destroy it. Otherwise, memory leaks may occur.
 * @see OH_Utd
 * @since 12
 */
OH_Utd* OH_Utd_Create(const char* typeId);

/**
 * @brief Destroys an {@link OH_Utd} instance.
 *
 * @param pThis Pointer to the {@link OH_Utd} instance to destroy.
 * @see OH_Utd
 * @since 12
 */
void OH_Utd_Destroy(OH_Utd* pThis);

/**
 * @brief Obtains the type ID from an {@link OH_Utd} instance.
 *
 * @param pThis Pointer to the target {@link OH_Utd} instance.
 * @return Returns the pointer to the type ID obtained if the operation is successful; returns <b>nullptr</b> otherwise.
 * @see OH_Utd
 * @since 12
 */
const char* OH_Utd_GetTypeId(OH_Utd* pThis);

/**
 * @brief Obtains the type description from an {@link OH_Utd} instance.
 *
 * @param pThis Pointer to the target {@link OH_Utd} instance.
 * @return Returns the pointer to the description obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_Utd
 * @since 12
 */
const char* OH_Utd_GetDescription(OH_Utd* pThis);

/**
 * @brief Obtains the URL from an {@link OH_Utd} instance.
 *
 * @param pThis Pointer to the target {@link OH_Utd} instance.
 * @return Returns the pointer to the URL obtained if the operation is successful; returns <b>nullptr</b> otherwise.
 * @see OH_Utd
 * @since 12
 */
const char* OH_Utd_GetReferenceUrl(OH_Utd* pThis);

/**
 * @brief Obtains the default icon file path from an {@link OH_Utd} instance.
 *
 * @param pThis Pointer to the target {@link OH_Utd} instance.
 * @return Returns the pointer to the path of the default icon file obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_Utd
 * @since 12
 */
const char* OH_Utd_GetIconFile(OH_Utd* pThis);

/**
 * @brief Obtains the types to which an {@link OH_Utd} belongs.
 *
 * @param pThis Pointer to the target {@link OH_Utd} instance.
 * @param count Pointer to the number of UTD types obtained.
 * @return Returns the pointer to the UTD types obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_Utd
 * @since 12
 */
const char** OH_Utd_GetBelongingToTypes(OH_Utd* pThis, unsigned int* count);

/**
 * @brief Obtains the file name extensions associated with an {@link OH_Utd} instance.
 *
 * @param pThis Pointer to the target {@link OH_Utd} instance.
 * @param count Pointer to the number of file name extensions obtained.
 * @return Returns the pointer to the file name extensions obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_Utd
 * @since 12
 */
const char** OH_Utd_GetFilenameExtensions(OH_Utd* pThis, unsigned int* count);

/**
 * @brief Obtains the MIME types associated with an {@link OH_Utd} instance.
 *
 * @param pThis Pointer to the target {@link OH_Utd} instance.
 * @param count Pointer to the number of MIME types obtained.
 * @return Returns the pointer to the MIME types obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_Utd
 * @since 12
 */
const char** OH_Utd_GetMimeTypes(OH_Utd* pThis, unsigned int* count);

/**
 * @brief Obtains the UTDs based on the given file name extension.
 *
 * @param extension Pointer to the file name extension.
 * @param count Pointer to the number of UTDs obtained.
 * @return Returns the pointer to the UTDs obtained.
 * If a pointer is not required, use {@link OH_Utd_DestroyStringList} to destroy it in a timely manner.
 * Otherwise, memory leakage occurs.
 * @since 12
 */
const char** OH_Utd_GetTypesByFilenameExtension(const char* extension, unsigned int* count);

/**
 * @brief Obtains the UTDs based on the given MIME type.
 *
 * @param mimeType Pointer to the MIME type.
 * @param count Pointer to the number of UTDs obtained.
 * @return Returns the pointer to the UTDs obtained.
 * If a pointer is not required, use {@link OH_Utd_DestroyStringList} to destroy it in a timely manner.
 * Otherwise, memory leakage occurs.
 * @since 12
 */
const char** OH_Utd_GetTypesByMimeType(const char* mimeType, unsigned int* count);

/**
 * @brief Checks whether a UTD belongs to the target UTD.
 *
 * @param srcTypeId Pointer to the UTD to check.
 * @param destTypeId Pointer to the target UTD.
 * @return Returns <b>true</b> if the UTD belongs to the target UTD; returns <b>false</b> otherwise.
 * @since 12
 */
bool OH_Utd_BelongsTo(const char* srcTypeId, const char* destTypeId);

/**
 * @brief Checks whether a UTD is a lower-level type of the target UTD.
 * For example, <b>TYPE_SCRIPT</b> is a lower-level type of <b>SOURCE_CODE</b>, and <b>TYPE_SCRIPT</b>
 * and <b>SOURCE_CODE</b> are lower-level types of <b>PLAIN_TEXT</b>.
 *
 * @param srcTypeId Pointer to the UTD to check.
 * @param destTypeId Pointer to the target UTD.
 * @return Returns <b>true</b> if the UTD is a lower-level type of the target UTD; returns <b>false</b> otherwise.
 * @since 12
 */
bool OH_Utd_IsLower(const char* srcTypeId, const char* destTypeId);

/**
 * @brief Checks whether a UTD is a higher-level type of the target UTD.
 * For example, <b>SOURCE_CODE</b> is a higher-level type of <b>TYPE_SCRIPT</b>, and <b>PLAIN_TEXT</b> is
 * a higher-level type of <b>SOURCE_CODE</b> and <b>TYPE_SCRIPT</b>.
 *
 * @param srcTypeId Pointer to the UTD to check.
 * @param destTypeId Pointer to the target UTD.
 * @return Returns <b>true</b> if the UTD is a higher-level type of the target UTD; returns <b>false</b> otherwise.
 * @since 12
 */
bool OH_Utd_IsHigher(const char* srcTypeId, const char* destTypeId);

/**
 * @brief Checks whether two UTDs are the same.
 *
 * @param desc1 Pointer to one {@link OH_Utd} instance to compare.
 * @param desc2 Pointer to the other {@link OH_Utd} instance to compare.
 * @return Returns <b>true</b> if the two instances are the same; returns <b>false</b> otherwise.
 * @since 12
 */
bool OH_Utd_Equals(OH_Utd* utd1, OH_Utd* utd2);

/**
 * @brief Destroys a UTD list.
 *
 * @param list Pointer to the UTD list to destroy.
 * @param count Length of the UTD list to destroy.
 * @since 12
 */
void OH_Utd_DestroyStringList(const char** list, unsigned int count);

#ifdef __cplusplus
};
#endif

/** @} */
#endif
