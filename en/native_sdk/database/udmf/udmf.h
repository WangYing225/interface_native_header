/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup UDMF
 * @{
 *
 * @brief Defines unified data definitions for data interaction across applications, devices, and platforms,
 * and provides a unified OpenHarmony data language and standard data access and read channels.
 *
 * @since 12
 */

 /**
 * @file udmf.h
 *
 * @brief Defines the APIs, data structs, and enums for accessing the Unified Data Management Framework (UDMF).
 * File to include: <database/udmf/udmf.h>
 * @library libudmf.so
 * @syscap SystemCapability.DistributedDataManager.UDMF.Core
 *
 * @since 12
 */
#ifndef UDMF_H
#define UDMF_H

#include <inttypes.h>
#include <stdbool.h>
#include "uds.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Minimum length of the buffer that holds the key (unique identifier) of a uniform data object.
 *
 * @since 12
 */
#define UDMF_KEY_BUFFER_LEN (512)

/**
 * @brief Enumerates the UDMF data channel types.
 *
 * @since 12
 */
typedef enum Udmf_Intention {
    /**
     * Channel for dragging data.
     */
    UDMF_INTENTION_DRAG,
    /**
     * Channel for pasteboard data.
     */
    UDMF_INTENTION_PASTEBOARD,
} Udmf_Intention;

/**
 * @brief Enumerates the scopes of the uniform data to be used on a device.
 *
 * @since 12
 */
typedef enum Udmf_ShareOption {
    /**
     * Invalid use.
     */
    SHARE_OPTIONS_INVALID,
    /**
     * Use the uniform data only in the same application of a device.
     */
    SHARE_OPTIONS_IN_APP,
    /**
     * Use the uniform data across applications of a device.
     */
    SHARE_OPTIONS_CROSS_APP
} Udmf_ShareOption;

/**
 * @brief Defines a uniform data object.
 *
 * @since 12
 */
typedef struct OH_UdmfData OH_UdmfData;

/**
 * @brief Defines a data record in a uniform data object.
 *
 * @since 12
 */
typedef struct OH_UdmfRecord OH_UdmfRecord;

/**
 * @brief Defines the provider of a data record.
 *
 * @since 13
 */
typedef struct OH_UdmfRecordProvider OH_UdmfRecordProvider;

/**
 * @brief Defines a data record property in a uniform data object.
 *
 * @since 12
 */
typedef struct OH_UdmfProperty OH_UdmfProperty;

/**
 * @brief Creates a pointer to an {@link OH_UdmfData} instance.
 * If this pointer is no longer required, use {@link OH_UdmfData_Destroy} to destroy it. Otherwise,
 * memory leaks may occur.
 *
 * @return Returns the pointer to the {@link OH_UdmfData} instance created if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdmfData
 * @since 12
 */
OH_UdmfData* OH_UdmfData_Create();

/**
 * @brief Destroys an {@link OH_UdmfData} instance.
 *
 * @param pThis Pointer to the {@link OH_UdmfData} instance to destroy.
 * @see OH_UdmfData
 * @since 12
 */
void OH_UdmfData_Destroy(OH_UdmfData* pThis);

/**
 * @brief Adds an {@link OH_UdmfRecord} to an {@link OH_UdmfData} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfData} instance.
 * @param record Pointer to the {@link OH_UdmfRecord} instance to add.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfData
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfData_AddRecord(OH_UdmfData* pThis, OH_UdmfRecord* record);

/**
 * @brief Checks whether an {@link OH_UdmfData} instance contains the specified type.
 *
 * @param pThis Pointer to the target {@link OH_UdmfData} instance.
 * @param type Pointer to the type to check.
 * @return Returns a Boolean value indicating the result. The value <b>true</b> means the {@link OH_UdmfData} instance
 * contains the specified type; the value <b>false</b> means the opposite.
 * @see OH_UdmfData
 * @since 12
 */
bool OH_UdmfData_HasType(OH_UdmfData* pThis, const char* type);

/**
 * @brief Obtains all data types contained in an {@link OH_UdmfData} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfData} instance.
 * @param count Pointer to the number of data types obtained.
 * @return Returns the data types obtained if the operation is successful; returns <b>nullptr</b> otherwise.
 * @see OH_UdmfData
 * @since 12
 */
char** OH_UdmfData_GetTypes(OH_UdmfData* pThis, unsigned int* count);

/**
 * @brief Obtains all records contained in an {@link OH_UdmfData} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfData} instance.
 * @param count Pointer to the number of records obtained.
 * @return Returns the {@link OH_UdmfRecord}s obtained if the operation is successful; returns <b>nullptr</b> otherwise.
 * @see OH_UdmfData
 * @see OH_UdmfRecord
 * @since 12
 */
OH_UdmfRecord** OH_UdmfData_GetRecords(OH_UdmfData* pThis, unsigned int* count);

/**
 * @brief Defines a callback used to release the context when the <b>OH_UdmfRecordProvider</b> instance is destroyed.
 * @param context Pointer to the context to release.
 * @since 13
 */
typedef void (*UdmfData_Finalize)(void* context);

/**
 * @brief Creates a pointer to an {@link OH_UdmfRecordProvider} instance.
 * If this pointer is no longer required, use {@link OH_UdmfRecordProvider_Destroy} to destroy it. Otherwise,
 * memory leaks may occur.
 *
 * @return Returns the pointer to the {@link OH_UdmfRecordProvider} instance created if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdmfRecordProvider
 * @since 13
 */
OH_UdmfRecordProvider* OH_UdmfRecordProvider_Create();

/**
 * @brief Destroys an {@link OH_UdmfRecordProvider} instance.
 *
 * @param provider Pointer to the {@link OH_UdmfRecordProvider} instance to destroy.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecordProvider
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecordProvider_Destroy(OH_UdmfRecordProvider* provider);

/**
 * @brief Defines a callback used to obtain data by type.
 * This callback will be invoked to return the data obtained from an <b>OH_UdmfRecord</b> object.
 *
 * @param context Pointer to the context set by using {@link OH_UdmfRecordProvider_SetData}.
 * @param type Pointer to the type of the data to obtain. For details, see {@link udmf_meta.h}.
 * @return Returns the Uniform data obtained.
 * @since 13
 */
typedef void* (*OH_UdmfRecordProvider_GetData)(void* context, const char* type);

/**
 * @brief Sets a callback for an <b>OH_UdmfRecordProvider</b> instance.
 *
 * @param provider Pointer to the target {@link OH_UdmfRecordProvider} instance.
 * @param context Pointer to the context, which is passed to {@link OH_UdmfRecordProvider_GetData} as
 * the first parameter.
 * @param callback Callback used to obtain data. For details, see {@link OH_UdmfRecordProvider_GetData}.
 * @param finalize Optional callback used to release the context data when the <b>OH_UdmfRecordProvider</b>
 * instance is destroyed. For details, see {@link UdmfData_Finalize}.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecordProvider
 * @see OH_UdmfRecordProvider_GetData
 * @see UdmfData_Finalize Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecordProvider_SetData(OH_UdmfRecordProvider* provider, void* context,
    const OH_UdmfRecordProvider_GetData callback, const UdmfData_Finalize finalize);

/**
 * @brief Creates a pointer to an {@link OH_UdmfRecord} instance.
 * If this pointer is no longer required, use {@link OH_UdmfRecord_Destroy} to destroy it. Otherwise,
 * memory leaks may occur.
 *
 * @return Returns the pointer to the {@link OH_UdmfRecord} instance created if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdmfRecord
 * @since 12
 */
OH_UdmfRecord* OH_UdmfRecord_Create();

/**
 * @brief Destroys an {@link OH_UdmfRecord} instance.
 *
 * @param pThis Pointer to the {@link OH_UdmfRecord} instance to destroy.
 * @see OH_UdmfRecord
 * @since 12
 */
void OH_UdmfRecord_Destroy(OH_UdmfRecord* pThis);

/**
 * @brief Adds custom data to an {@link OH_UdmfRecord} instance.
 * This API cannot be used to add data of UDS types (such as PlainText, Link, and Pixelmap).
 *
 * @param pThis Pointer to the target {@link OH_UdmfRecord} instance.
 * @param typeId Pointer to the data type identifier, which is used to distinguish custom data types from
 * system-defined types. It is recommended that the value start with 'ApplicationDefined'.
 * @param entry Pointer to the custom data to add.
 * @param count Size of custom data to add. The data size cannot exceed 4 KB.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecord
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_AddGeneralEntry(OH_UdmfRecord* pThis, const char* typeId, unsigned char* entry, unsigned int count);

/**
 * @brief Adds data of the {@link OH_UdsPlainText} type to an {@link OH_UdmfRecord} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfRecord} instance.
 * @param plainText Pointer to the {@link OH_UdsPlainText} instance to add.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecord
 * @see OH_UdsPlainText
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_AddPlainText(OH_UdmfRecord* pThis, OH_UdsPlainText* plainText);

/**
 * @brief Adds data of the {@link OH_UdsHyperlink} type to an {@link OH_UdmfRecord} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfRecord} instance.
 * @param hyperlink Pointer to the {@link OH_UdsHyperlink} instance to add.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecord
 * @see OH_UdsHyperlink
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_AddHyperlink(OH_UdmfRecord* pThis, OH_UdsHyperlink* hyperlink);

/**
 * @brief Adds data of the {@link OH_UdsHtml} type to an {@link OH_UdmfRecord} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfRecord} instance.
 * @param html Pointer to the {@link OH_UdsHtml} instance to add.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecord
 * @see OH_UdsHtml
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_AddHtml(OH_UdmfRecord* pThis, OH_UdsHtml* html);

/**
 * @brief Adds data of the {@link OH_UdsAppItem} type to an {@link OH_UdmfRecord} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfRecord} instance.
 * @param appItem Pointer to the {@link OH_UdsAppItem} instance to add.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecord
 * @see OH_UdsAppItem
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_AddAppItem(OH_UdmfRecord* pThis, OH_UdsAppItem* appItem);

/**
 * @brief Adds data of the {@link OH_UdsFileUri} type to an {@link OH_UdmfRecord} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfRecord} instance.
 * @param fileUri Pointer to the {@link OH_UdsFileUri} instance to add.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecord
 * @see OH_UdsFileUri
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecord_AddFileUri(OH_UdmfRecord* pThis, OH_UdsFileUri* fileUri);

/**
 * @brief Adds data of the {@link OH_UdsPixelMap} type to an {@link OH_UdmfRecord} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfRecord} instance.
 * @param pixelMap Pointer to the {@link OH_UdsPixelMap} instance to add.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecord
 * @see OH_UdsPixelMap
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecord_AddPixelMap(OH_UdmfRecord* pThis, OH_UdsPixelMap* pixelMap);

/**
 * @brief Adds data of the {@link OH_UdsArrayBuffer} type to an {@link OH_UdmfRecord} instance.
 *
 * @param record Pointer to the target {@link OH_UdmfRecord} instance.
 * @param type Pointer to the ArrayBuffer type ID, which must be unique.
 * @param buffer Pointer to the {@link OH_UdsArrayBuffer} instance to add.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecord
 * @see OH_UdsArrayBuffer
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecord_AddArrayBuffer(OH_UdmfRecord* record, const char* type, OH_UdsArrayBuffer* buffer);

/**
 * @brief Obtains all data types from an {@link OH_UdmfRecord} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfRecord} instance.
 * @param count Pointer to the number of data types obtained.
 * @return Returns a list of the data types obtained if the operation is successful; returns <b>nullptr</b> otherwise.
 * @see OH_UdmfRecord
 * @since 12
 */
char** OH_UdmfRecord_GetTypes(OH_UdmfRecord* pThis, unsigned int* count);

/**
 * @brief Obtains data of the specified type from an {@link OH_UdmfRecord} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfRecord} instance.
 * @param typeId Pointer to the data type ID.
 * @param entry Double pointer to the data obtained.
 * @param count Pointer to the length of the data obtained.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecord
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_GetGeneralEntry(OH_UdmfRecord* pThis, const char* typeId,
    unsigned char** entry, unsigned int* count);

/**
 * @brief Obtains data of the {@link OH_UdsPlainText} type from an {@link OH_UdmfRecord} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfRecord} instance.
 * @param plainText Pointer to the {@link OH_UdsPlainText} instance obtained.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecord
 * @see OH_UdsPlainText
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_GetPlainText(OH_UdmfRecord* pThis, OH_UdsPlainText* plainText);

/**
 * @brief Obtains data of the {@link OH_UdsHyperlink} type from an {@link OH_UdmfRecord} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfRecord} instance.
 * @param hyperlink Pointer to the {@link OH_UdsHyperlink} instance obtained.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecord
 * @see OH_UdsHyperlink
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_GetHyperlink(OH_UdmfRecord* pThis, OH_UdsHyperlink* hyperlink);

/**
 * @brief Obtains data of the {@link OH_UdsHtml} type from an {@link OH_UdmfRecord} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfRecord} instance.
 * @param html Pointer to the {@link OH_UdsHtml} instance obtained.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecord
 * @see OH_UdsHtml
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_GetHtml(OH_UdmfRecord* pThis, OH_UdsHtml* html);

/**
 * @brief Obtains data of the {@link OH_UdsAppItem} type from an {@link OH_UdmfRecord} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfRecord} instance.
 * @param appItem Pointer to the {@link OH_UdsAppItem} instance obtained.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecord
 * @see OH_UdsAppItem
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfRecord_GetAppItem(OH_UdmfRecord* pThis, OH_UdsAppItem* appItem);

/**
 * @brief Sets the {@link OH_UdmfRecordProvider} of the specified types to an {@link OH_UdmfRecord} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfRecord} instance.
 * @param types Pointer to the data types to be provided.
 * @param count Number of the data types.
 * @param provider Pointer to the {@link OH_UdmfRecordProvider} instance.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecord
 * @see OH_UdmfRecordProvider
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecord_SetProvider(OH_UdmfRecord* pThis, const char* const* types, unsigned int count,
    OH_UdmfRecordProvider* provider);

/**
 * @brief Obtains data of the {@link OH_UdsFileUri} type from an {@link OH_UdmfRecord} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfRecord} instance.
 * @param fileUri Pointer to the {@link OH_UdsFileUri} instance obtained.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecord
 * @see OH_UdsFileUri
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecord_GetFileUri(OH_UdmfRecord* pThis, OH_UdsFileUri* fileUri);

/**
 * @brief Obtains data of the {@link OH_UdsPixelMap} type from an {@link OH_UdmfRecord} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfRecord} instance.
 * @param pixelMap Pointer to the {@link OH_UdsPixelMap} instance obtained.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecord
 * @see OH_UdsPixelMap
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecord_GetPixelMap(OH_UdmfRecord* pThis, OH_UdsPixelMap* pixelMap);

/**
 * @brief Obtains data of the {@link OH_UdsArrayBuffer} type from an {@link OH_UdmfRecord} instance.
 *
 * @param record Pointer to the target {@link OH_UdmfRecord} instance.
 * @param type Pointer to the data type identifier of the ArrayBuffer data to be obtained.
 * @param buffer Pointer to the {@link OH_UdsArrayBuffer} instance obtained.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfRecord
 * @see OH_UdsArrayBuffer
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfRecord_GetArrayBuffer(OH_UdmfRecord* record, const char* type, OH_UdsArrayBuffer* buffer);

/**
 * @brief Obtains the first {@link OH_UdsPlainText} data from an {@link OH_UdmfData} instance.
 *
 * @param data Pointer to the target {@link OH_UdmfData} instance.
 * @param plainText Pointer to the {@link OH_UdsPlainText} instance obtained.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfData
 * @see OH_UdsPlainText
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfData_GetPrimaryPlainText(OH_UdmfData* data, OH_UdsPlainText* plainText);

/**
 * @brief Obtains the first {@link OH_UdsHtml} data from an {@link OH_UdmfData} instance.
 *
 * @param data Pointer to the target {@link OH_UdmfData} instance.
 * @param html Pointer to the {@link OH_UdsHtml} instance obtained.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfData
 * @see OH_UdsHtml
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdmfData_GetPrimaryHtml(OH_UdmfData* data, OH_UdsHtml* html);

/**
 * @brief Obtains the number of records in an {@link OH_UdmfData} instance.
 *
 * @param data Pointer to the target {@link OH_UdmfData} instance.
 * @return Returns the number of {@link OH_UdmfRecord}s obtained.
 * @see OH_UdmfData
 * @since 13
 */
int OH_UdmfData_GetRecordCount(OH_UdmfData* data);

/**
 * @brief Obtains the data record at the specified location in an {@link OH_UdmfData} instance.
 *
 * @param data Pointer to the target {@link OH_UdmfData} instance.
 * @param index Index of the data record to obtain.
 * @return Returns the pointer to the {@link OH_UdmfRecord} instance obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdmfData
 * @since 13
 */
OH_UdmfRecord* OH_UdmfData_GetRecord(OH_UdmfData* data, unsigned int index);

/**
 * @brief Checks whether the data of an {@link OH_UdmfData} instance is from the local device.
 *
 * @param data Pointer to the target {@link OH_UdmfData} instance.
 * @return Returns a Boolean value indicating whether the data is from the local device. The value <b>true</b>
 * means the data is from the local device; the value <b>false</b> means the opposite.
 * @see OH_UdmfData
 * @since 13
 */
bool OH_UdmfData_IsLocal(OH_UdmfData* data);

/**
 * @brief Creates a pointer to an {@link OH_UdmfProperty} instance.
 * If this pointer is no longer required, use {@link OH_UdmfProperty_Destroy} to destroy it. Otherwise,
 * memory leaks may occur.
 *
 * @param unifiedData Pointer to the target {@link OH_UdmfData} instance.
 * @return Returns the pointer to the {@link OH_UdmfProperty} instance created if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdmfData
 * @see OH_UdmfProperty
 * @since 12
 */
OH_UdmfProperty* OH_UdmfProperty_Create(OH_UdmfData* unifiedData);

/**
 * @brief Destroys an {@link OH_UdmfProperty} instance.
 *
 * @param pThis Pointer to the {@link OH_UdmfProperty} instance to destroy.
 * @see OH_UdmfProperty
 * @since 12
 */
void OH_UdmfProperty_Destroy(OH_UdmfProperty* pThis);

/**
 * @brief Obtains the custom tag value from an {@link OH_UdmfProperty} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfProperty} instance.
 * @return Returns the pointer to the custom tag value obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdmfProperty
 * @since 12
 */
const char* OH_UdmfProperty_GetTag(OH_UdmfProperty* pThis);

/**
 * @brief Obtains the timestamp from an {@link OH_UdmfProperty} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfProperty} instance.
 * @return Returns the timestamp obtained.
 * @see OH_UdmfProperty
 * @since 12
 */
int64_t OH_UdmfProperty_GetTimestamp(OH_UdmfProperty* pThis);

/**
 * @brief Obtains the data applicable scope from an {@link OH_UdmfProperty} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfProperty} instance.
 * @return Returns the {@link Udmf_ShareOption} obtained.
 * @see OH_UdmfProperty
 * @see Udmf_ShareOption
 * @since 12
 */
Udmf_ShareOption OH_UdmfProperty_GetShareOption(OH_UdmfProperty* pThis);

/**
 * @brief Obtains the custom additional integer from an {@link OH_UdmfProperty} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfProperty} instance.
 * @param key Pointer to the key of the key-value (KV) pair.
 * @param defaultValue Default value to be returned if the parameter fails to be obtained.
 * @return Returns the integer value obtained if the operation is successful;
 * returns <b>defaultValue</b> if the operation fails.
 * @see OH_UdmfProperty
 * @since 12
 */
int OH_UdmfProperty_GetExtrasIntParam(OH_UdmfProperty* pThis, const char* key, int defaultValue);

/**
 * @brief Obtains the custom additional string from an {@link OH_UdmfProperty} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfProperty} instance.
 * @param key Pointer to the key of the KV pair.
 * @return Returns a pointer to the string obtained if the operation is successful; returns <b>nullptr</b> otherwise.
 * @see OH_UdmfProperty
 * @since 12
 */
const char* OH_UdmfProperty_GetExtrasStringParam(OH_UdmfProperty* pThis, const char* key);

/**
 * @brief Sets the custom tag value for an {@link OH_UdmfProperty} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfProperty} instance.
 * @param tag Pointer to the tag value to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfProperty
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfProperty_SetTag(OH_UdmfProperty* pThis, const char* tag);

/**
 * @brief Sets {@link OH_Udmf_ShareOption} for an {@link OH_UdmfProperty} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfProperty} instance.
 * @param option {@link Udmf_ShareOption} to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfProperty
 * @see Udmf_ShareOption
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfProperty_SetShareOption(OH_UdmfProperty* pThis, Udmf_ShareOption option);

/**
 * @brief Sets an additional integer for an {@link OH_UdmfProperty} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdmfRecord} instance.
 * @param key Pointer to the key of the KV pair to set.
 * @param param Value to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfProperty
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfProperty_SetExtrasIntParam(OH_UdmfProperty* pThis, const char* key, int param);

/**
 * @brief Sets an additional string for an {@link OH_UdmfProperty} instance.
 *
 * @param pThis Pointer to the {@link OH_UdmfProperty} instance.
 * @param key Pointer to the key of the KV pair to set.
 * @param param String to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfProperty
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_UdmfProperty_SetExtrasStringParam(OH_UdmfProperty* pThis,
    const char* key, const char* param);

/**
 * @brief Obtains the {@link OH_UdmfData} from the UDMF database.
 *
 * @param key Pointer to the key that uniquely identifies the data in the database.
 * @param intention {@link Udmf_Intention}.
 * @param unifiedData Pointer to the {@link OH_UdmfData} obtained.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfProperty
 * @see Udmf_Intention
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_Udmf_GetUnifiedData(const char* key, Udmf_Intention intention, OH_UdmfData* unifiedData);

/**
 * @brief Sets an {@link OH_UdmfData} instance in the UDMF database.
 *
 * @param intention {@link Udmf_Intention}.
 * @param unifiedData Pointer to the {@link OH_UdmfData} instance to set.
 * @param key Pointer to the key that uniquely identifies the data in the database.
 * @param keyLen Length of the key. The memory size must be greater than or equal to 512 bytes.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdmfProperty
 * @see Udmf_Intention
 * @see Udmf_ErrCode
 * @since 12
 */
int OH_Udmf_SetUnifiedData(Udmf_Intention intention, OH_UdmfData* unifiedData,
    char* key, unsigned int keyLen);

#ifdef __cplusplus
};
#endif

/** @} */
#endif
