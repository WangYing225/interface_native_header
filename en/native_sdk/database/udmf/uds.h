/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup UDMF
 * @{
 *
 * @brief Defines unified data definitions for data interaction across applications, devices, and platforms,
 * and provides a unified OpenHarmony data language and standard data access and read channels.
 *
 * @syscap SystemCapability.DistributedDataManager.UDMF.Core
 *
 * @since 12
 */

/**
 * @file uds.h
 *
 * @brief Defines the APIs and structs related to the uniform data structs.
 * File to include: <database/udmf/uds.h>
 * @library libudmf.so
 * @syscap SystemCapability.DistributedDataManager.UDMF.Core
 * @since 12
 */

#ifndef UDS_H
#define UDS_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines a struct for the uniform data of the plaintext type.
 *
 * @since 12
 */
typedef struct OH_UdsPlainText OH_UdsPlainText;

/**
 * @brief Defines a struct for the uniform data of the hyperlink type.
 *
 * @since 12
 */
typedef struct OH_UdsHyperlink OH_UdsHyperlink;

/**
 * @brief Defines a struct for the uniform data of the Hypertext Markup Language (HTML) type.
 *
 * @since 12
 */
typedef struct OH_UdsHtml OH_UdsHtml;

/**
 * @brief Defines a struct for the uniform data of the home screen icon type.
 *
 * @since 12
 */
typedef struct OH_UdsAppItem OH_UdsAppItem;

/**
* @brief Defines a struct for the file URI type.
*
* @since 13
*/
typedef struct OH_UdsFileUri OH_UdsFileUri;

/**
* @brief Defines a struct for the pixel map type.
*
* @since 13
*/
typedef struct OH_UdsPixelMap OH_UdsPixelMap;

/**
* @brief Defines a struct for the ArrayBuffer type.
*
* @since 13
*/
typedef struct OH_UdsArrayBuffer OH_UdsArrayBuffer;

/**
 * @brief Creates a pointer to an {@link OH_UdsPlainText} instance.
 * If this pointer is no longer required, use {@link OH_UdsPlainText_Destroy} to destroy it.
 * Otherwise, memory leaks may occur.
 *
 * @return Returns the pointer to the {@link OH_UdsPlainText} instance created if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsPlainText
 * @since 12
 */
OH_UdsPlainText* OH_UdsPlainText_Create();

/**
 * @brief Destroys an {@link OH_UdsPlainText} instance.
 *
 * @param pThis Pointer to the {@link OH_UdsPlainText} instance to destroy.
 * @see OH_UdsPlainText
 * @since 12
 */
void OH_UdsPlainText_Destroy(OH_UdsPlainText* pThis);

/**
 * @brief Obtains the type ID from an {@link OH_UdsPlainText} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsPlainText} instance.
 * @return Returns the pointer to the type ID obtained if the operation is successful; returns <b>nullptr</b> otherwise.
 * @see OH_UdsPlainText
 * @since 12
 */
const char* OH_UdsPlainText_GetType(OH_UdsPlainText* pThis);

/**
 * @brief Obtains plain text content from an {@link OH_UdsPlainText} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsPlainText} instance.
 * @return Returns the pointer to the plaintext obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsPlainText
 * @since 12
 */
const char* OH_UdsPlainText_GetContent(OH_UdsPlainText* pThis);

/**
 * @brief Obtains the plain text abstract information from an {@link OH_UdsPlainText} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsPlainText} instance.
 * @return Returns the pointer to the abstract information obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsPlainText
 * @since 12
 */
const char* OH_UdsPlainText_GetAbstract(OH_UdsPlainText* pThis);

/**
 * @brief Sets the plain text content for an {@link OH_UdsPlainText} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsPlainText} instance.
 * @param content Pointer to the plain text content to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsPlainText
 * @since 12
 */
int OH_UdsPlainText_SetContent(OH_UdsPlainText* pThis, const char* content);

/**
 * @brief Sets the plain text abstract for an {@link OH_UdsPlainText} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsPlainText} instance.
 * @param abstract Pointer to the text abstract to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsPlainText
 * @since 12
 */
int OH_UdsPlainText_SetAbstract(OH_UdsPlainText* pThis, const char* abstract);

/**
 * @brief Creates a pointer to an {@link OH_UdsHyperlink} instance.
 * If this pointer is no longer required, use {@link OH_UdsHyperlink_Destroy} to destroy it.
 * Otherwise, memory leaks may occur.
 *
 * @return Returns the pointer to the {@link OH_UdsHyperlink} instance created if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsHyperlink
 * @since 12
 */
OH_UdsHyperlink* OH_UdsHyperlink_Create();

/**
 * @brief Destroys an {@link OH_UdsHyperlink} instance.
 *
 * @param pThis Pointer to the {@link OH_UdsHyperlink} instance to destroy.
 * @see OH_UdsHyperlink
 * @since 12
 */
void OH_UdsHyperlink_Destroy(OH_UdsHyperlink* pThis);

/**
 * @brief Obtains the type ID from an {@link OH_UdsHyperlink} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsHyperlink} instance.
 * @return Returns the pointer to the type ID obtained if the operation is successful; returns <b>nullptr</b> otherwise.
 * @see OH_UdsHyperlink
 * @since 12
 */
const char* OH_UdsHyperlink_GetType(OH_UdsHyperlink* pThis);

/**
 * @brief Obtains the URL from an {@link OH_UdsHyperlink} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsHyperlink} instance.
 * @return Returns the pointer to the URL obtained if the operation is successful; returns <b>nullptr</b> otherwise.
 * @see OH_UdsHyperlink
 * @since 12
 */
const char* OH_UdsHyperlink_GetUrl(OH_UdsHyperlink* pThis);

/**
 * @brief Obtains the hyperlink description from an {@link OH_UdsHyperlink} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsHyperlink} instance.
 * @return Returns the pointer to the hyperlink description obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsHyperlink
 * @since 12
 */
const char* OH_UdsHyperlink_GetDescription(OH_UdsHyperlink* pThis);

/**
 * @brief Sets the URL for an {@link OH_UdsHyperlink} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsHyperlink} instance.
 * @param url Pointer to the URL to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsHyperlink
 * @since 12
 */
int OH_UdsHyperlink_SetUrl(OH_UdsHyperlink* pThis, const char* url);

/**
 * @brief Sets the hyperlink description for an {@link OH_UdsHyperlink} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsHyperlink} instance.
 * @param description Pointer to the hyperlink description to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsHyperlink
 * @since 12
 */
int OH_UdsHyperlink_SetDescription(OH_UdsHyperlink* pThis, const char* description);

/**
 * @brief Creates a pointer to an {@link OH_UdsHtml} instance.
 * If this pointer is no longer required, use {@link OH_UdsHtml_Destroy} to destroy it.
 * Otherwise, memory leaks may occur.
 *
 * @return Returns the pointer to the {@link OH_UdsHtml} instance created if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsHtml
 * @since 12
 */
OH_UdsHtml* OH_UdsHtml_Create();

/**
 * @brief Destroys an {@link OH_UdsHtml} instance.
 *
 * @param pThis Pointer to the {@link OH_UdsHtml} instance to destroy.
 * @see OH_UdsHtml
 * @since 12
 */
void OH_UdsHtml_Destroy(OH_UdsHtml* pThis);

/**
 * @brief Obtains the type ID from an {@link OH_UdsHtml} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsHtml} instance.
 * @return Returns the pointer to the type ID obtained if the operation is successful; returns <b>nullptr</b> otherwise.
 * @see OH_UdsHtml
 * @since 12
 */
const char* OH_UdsHtml_GetType(OH_UdsHtml* pThis);

/**
 * @brief Obtains the HTML content from an {@link OH_UdsHtml} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsHtml} instance.
 * @return Returns the pointer to the HTML content obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsHtml
 * @since 12
 */
const char* OH_UdsHtml_GetContent(OH_UdsHtml* pThis);

/**
 * @brief Obtains the plaintext content from an {@link OH_UdsHtml} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsHtml} instance.
 * @return Returns the pointer to the plaintext obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsHtml
 * @since 12
 */
const char* OH_UdsHtml_GetPlainContent(OH_UdsHtml* pThis);

/**
 * @brief Sets the HTML content for an {@link OH_UdsHtml} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsHtml} instance.
 * @param content Pointer to the content to set in HTML format.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsHtml
 * @since 12
 */
int OH_UdsHtml_SetContent(OH_UdsHtml* pThis, const char* content);

/**
 * @brief Sets the plaintext content for an {@link OH_UdsHtml} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsHtml} instance.
 * @param plainContent Pointer to the plaintext content to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsHtml
 * @since 12
 */
int OH_UdsHtml_SetPlainContent(OH_UdsHtml* pThis, const char* plainContent);

/**
 * @brief Creates a pointer to an {@link OH_UdsAppItem} instance, which defines the home screen icon type.
 * If this pointer is no longer required, use {@link OH_UdsAppItem_Destroy} to destroy it.
 * Otherwise, memory leaks may occur.
 *
 * @return Returns the pointer to the {@link OH_UdsAppItem} instance created if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsAppItem
 * @since 12
 */
OH_UdsAppItem* OH_UdsAppItem_Create();

/**
 * @brief Destroys an {@link OH_UdsAppItem} instance.
 *
 * @param pThis Pointer to the {@link OH_UdsAppItem} instance to destroy.
 * @see OH_UdsAppItem
 * @since 12
 */
void OH_UdsAppItem_Destroy(OH_UdsAppItem* pThis);

/**
 * @brief Obtains the type ID from an {@link OH_UdsAppItem} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsAppItem} instance.
 * @return Returns the pointer to the type ID obtained if the operation is successful; returns <b>nullptr</b> otherwise.
 * @see OH_UdsAppItem
 * @since 12
 */
const char* OH_UdsAppItem_GetType(OH_UdsAppItem* pThis);

/**
 * @brief Obtains the application ID from an {@link OH_UdsAppItem} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsAppItem} instance.
 * @return Returns the pointer to the application ID obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsAppItem
 * @since 12
 */
const char* OH_UdsAppItem_GetId(OH_UdsAppItem* pThis);

/**
 * @brief Obtains the application name from an {@link OH_UdsAppItem} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsAppItem} instance.
 * @return Returns the pointer to the application name obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsAppItem
 * @since 12
 */
const char* OH_UdsAppItem_GetName(OH_UdsAppItem* pThis);

/**
 * @brief Obtains the image ID from an {@link OH_UdsAppItem} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsAppItem} instance.
 * @return Returns the pointer to the image ID obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsAppItem
 * @since 12
 */
const char* OH_UdsAppItem_GetIconId(OH_UdsAppItem* pThis);

/**
 * @brief Obtains the label ID from an {@link OH_UdsAppItem} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsAppItem} instance.
 * @return Returns the pointer to the application label ID obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsAppItem
 * @since 12
 */
const char* OH_UdsAppItem_GetLabelId(OH_UdsAppItem* pThis);

/**
 * @brief Obtains the bundle name from an {@link OH_UdsAppItem} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsAppItem} instance.
 * @return Returns the pointer to the bundle name obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsAppItem
 * @since 12
 */
const char* OH_UdsAppItem_GetBundleName(OH_UdsAppItem* pThis);

/**
 * @brief Obtains the ability name from an {@link OH_UdsAppItem} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsAppItem} instance.
 * @return Returns the pointer to the ability name obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsAppItem
 * @since 12
 */
const char* OH_UdsAppItem_GetAbilityName(OH_UdsAppItem* pThis);

/**
 * @brief Sets the application ID for an {@link OH_UdsAppItem} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsAppItem} instance.
 * @param appId Pointer to the application ID to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsAppItem
 * @since 12
 */
int OH_UdsAppItem_SetId(OH_UdsAppItem* pThis, const char* appId);

/**
 * @brief Sets the application name for an {@link OH_UdsAppItem} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsAppItem} instance.
 * @param appName Pointer to the application name to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsAppItem
 * @since 12
 */
int OH_UdsAppItem_SetName(OH_UdsAppItem* pThis, const char* appName);

/**
 * @brief Sets the image ID for an {@link OH_UdsAppItem} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsAppItem} instance.
 * @param appIconId Pointer to the image ID to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsAppItem
 * @since 12
 */
int OH_UdsAppItem_SetIconId(OH_UdsAppItem* pThis, const char* appIconId);

/**
 * @brief Sets the label ID for an {@link OH_UdsAppItem} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsAppItem} instance.
 * @param appLabelId Pointer to the label ID to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsAppItem
 * @since 12
 */
int OH_UdsAppItem_SetLabelId(OH_UdsAppItem* pThis, const char* appLabelId);

/**
 * @brief Sets the bundle name for an {@link OH_UdsAppItem} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsAppItem} instance.
 * @param bundleName Pointer to the bundle name to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsAppItem
 * @since 12
 */
int OH_UdsAppItem_SetBundleName(OH_UdsAppItem* pThis, const char* bundleName);

/**
 * @brief Sets the ability name for an {@link OH_UdsAppItem} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsAppItem} instance.
 * @param abilityName Pointer to the ability name to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsAppItem
 * @since 12
 */
int OH_UdsAppItem_SetAbilityName(OH_UdsAppItem* pThis, const char* abilityName);

/**
 * @brief Creates a pointer to an {@link OH_UdsFileUri} instance.
 * If this pointer is no longer required, use {@link OH_UdsFileUri_Destroy} to destroy it.
 * Otherwise, memory leaks may occur.
 *
 * @return Returns the pointer to the {@link OH_UdsFileUri} instance created if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsFileUri
 * @since 13
 */
OH_UdsFileUri* OH_UdsFileUri_Create();

/**
 * @brief Destroys an {@link OH_UdsFileUri} instance.
 *
 * @param pThis Pointer to the {@link OH_UdsFileUri} instance to destroy.
 * @see OH_UdsFileUri
 * @since 13
 */
void OH_UdsFileUri_Destroy(OH_UdsFileUri* pThis);

/**
 * @brief Obtains the type ID from an {@link OH_UdsFileUri} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsFileUri} instance.
 * @return Returns the pointer to the type ID obtained if the operation is successful; returns <b>nullptr</b> otherwise.
 * @see OH_UdsFileUri
 * @since 13
 */
const char* OH_UdsFileUri_GetType(OH_UdsFileUri* pThis);

/**
 * @brief Obtains the file URI from an {@link OH_UdsFileUri} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsFileUri} instance.
 * @return Returns the pointer to the file URI obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsFileUri
 * @since 13
 */
const char* OH_UdsFileUri_GetFileUri(OH_UdsFileUri* pThis);

/**
 * @brief Obtains the file type from an {@link OH_UdsFileUri} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsFileUri} instance.
 * @return Returns the pointer to the file type obtained if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsFileUri
 * @since 13
 */
const char* OH_UdsFileUri_GetFileType(OH_UdsFileUri* pThis);

/**
 * @brief Sets the URI for an {@link OH_UdsFileUri} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsFileUri} instance.
 * @param fileUri Pointer to the file URI to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsFileUri
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdsFileUri_SetFileUri(OH_UdsFileUri* pThis, const char* fileUri);

/**
 * @brief Sets the file type for an {@link OH_UdsFileUri} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsFileUri} instance.
 * @param fileType Pointer to the file type to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsFileUri
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdsFileUri_SetFileType(OH_UdsFileUri* pThis, const char* fileType);

/**
 * @brief Creates a pointer to an {@link OH_UdsPixelMap} instance.
 * If this pointer is no longer required, use {@link OH_UdsPixelMap_Destroy} to destroy it.
 * Otherwise, memory leaks may occur.
 *
 * @return Returns the pointer to the {@link OH_UdsPixelMap} instance created if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsPixelMap
 * @since 13
 */
OH_UdsPixelMap* OH_UdsPixelMap_Create();

/**
 * @brief Destroys an {@link OH_UdsPixelMap} instance.
 *
 * @param pThis Pointer to the {@link OH_UdsPixelMap} instance to destroy.
 * @see OH_UdsPixelMap
 * @since 13
 */
void OH_UdsPixelMap_Destroy(OH_UdsPixelMap* pThis);

/**
 * @brief Obtains the type ID from an {@link OH_UdsPixelMap} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsPixelMap} instance.
 * @return Returns the pointer to the type ID obtained if the operation is successful; returns <b>nullptr</b> otherwise.
 * @see OH_UdsPixelMap
 * @since 13
 */
const char* OH_UdsPixelMap_GetType(OH_UdsPixelMap* pThis);

/**
 * @brief Obtains the pointer to the {@link OH_PixelmapNative} instance from an {@link OH_UdsPixelMap} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsPixelMap} instance.
 * @param pixelmapNative Pointer to the {@link OH_PixelmapNative} instance obtained.
 * @see OH_UdsPixelMap
 * @see OH_PixelmapNative
 * @since 13
 */
void OH_UdsPixelMap_GetPixelMap(OH_UdsPixelMap* pThis, OH_PixelmapNative* pixelmapNative);

/**
 * @brief Sets the pixel image for an {@link OH_UdsPixelMap} instance.
 *
 * @param pThis Pointer to the target {@link OH_UdsPixelMap} instance.
 * @param pixelmapNative Pointer to the {@link OH_PixelmapNative} instance to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsPixelMap
 * @see OH_PixelmapNative
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdsPixelMap_SetPixelMap(OH_UdsPixelMap* pThis, OH_PixelmapNative* pixelmapNative);

/**
 * @brief Creates a pointer to an {@link OH_UdsArrayBuffer} instance.
 * If this pointer is no longer required, use {@link OH_UdsArrayBuffer_Destroy} to destroy it.
 * Otherwise, memory leaks may occur.
 *
 * @return Returns the pointer to the {@link OH_UdsArrayBuffer} instance created if the operation is successful;
 * returns <b>nullptr</b> otherwise.
 * @see OH_UdsArrayBuffer
 * @since 13
 */
OH_UdsArrayBuffer* OH_UdsArrayBuffer_Create();

/**
 * @brief Destroys an {@link OH_UdsArrayBuffer} instance.
 *
 * @param buffer Pointer to the {@link OH_UdsArrayBuffer} instance to destroy.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsArrayBuffer
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdsArrayBuffer_Destroy(OH_UdsArrayBuffer* buffer);

/**
 * @brief Sets the data content for an {@link OH_UdsArrayBuffer} instance.
 *
 * @param buffer Pointer to the target {@link OH_UdsArrayBuffer} instance.
 * @param data Pointer to the ArrayBuffer data to set.
 * @param len Length of the ArrayBuffer data to set.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsArrayBuffer
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdsArrayBuffer_SetData(OH_UdsArrayBuffer* buffer, unsigned char* data, unsigned int len);

/**
 * @brief Obtains the ArrayBuffer data from an {@link OH_UdsArrayBuffer} instance.
 *
 * @param buffer Pointer to the target {@link OH_UdsArrayBuffer} instance.
 * @param data Pointer to the ArrayBuffer data obtained.
 * @param len Pointer to the length of the ArrayBuffer data obtained.
 * @return Returns the error code. For details, see {@link Udmf_ErrCode}.
 *         If <b>UDMF_E_OK</b> is returned, the operation is successful.
 *         If <b>UDMF_E_INVALID_PARAM</b> is returned, invalid parameters are specified.
 * @see OH_UdsArrayBuffer
 * @see Udmf_ErrCode
 * @since 13
 */
int OH_UdsArrayBuffer_GetData(OH_UdsArrayBuffer* buffer, unsigned char** data, unsigned int* len);

#ifdef __cplusplus
};
#endif

/** @} */
#endif
