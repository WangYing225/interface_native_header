/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OH_CURSOR_H
#define OH_CURSOR_H

/**
 * @addtogroup RDB
 * @{
 *
 * @brief The relational database (RDB) store manages data based on relational models.
 * A complete set of mechanisms for managing local databases is provided based on the underlying SQLite.
 * To satisfy different needs in complicated scenarios, the RDB module provides a series of methods for performing
 * operations such as adding, deleting, modifying, and querying data, and supports direct execution of SQL statements.
 *
 * @syscap SystemCapability.DistributedDataManager.RelationalStore.Core
 * @since 10
 */

/**
 * @file oh_cursor.h
 *
 * @brief Provides methods to access the result set obtained by querying an RDB store.
 *
 * A result set is a set of results returned by query().
 * @library native_rdb_ndk_header.so
 * @since 10
 */

#include <cstdint>
#include <stddef.h>
#include <stdbool.h>
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the types of the fields in an RDB store.
 *
 * @since 10
 */
typedef enum OH_ColumnType {
    /** NULL */
    TYPE_NULL = 0,
    /** INT64 */
    TYPE_INT64,
    /** REAL */
    TYPE_REAL,
    /** Text */
    TYPE_TEXT,
    /** BLOB */
    TYPE_BLOB,
} OH_ColumnType;

/**
 * @brief Defines the result set.
 *
 * You can use the APIs to access the result set obtained by querying the RDB store.
 *
 * @since 10
 */
typedef struct OH_Cursor {
    /** @brief Unique identifier of the OH_Cursor struct. */
    int64_t id;

    /**
     * @brief Obtains the number of columns in a result set.
     *
     * @param cursor Indicates the pointer to the {@link OH_Cursor} instance.
     * @param count Indicates the pointer to the number of columns in the result set obtained.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_Cursor.
     * @since 10
     */
    int (*getColumnCount)(OH_Cursor *cursor, int *count);

    /**
     * @brief Obtains the type of the data in the column specified by the column index.
     *
     * @param cursor Indicates the pointer to the {@link OH_Cursor} instance.
     * @param columnIndex Indicates the index of the column in the result set.
     * @param columnType Indicates the pointer to the data type obtained. For details, see {@link OH_ColumnType}.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_Cursor, OH_ColumnType.
     * @since 10
     */
    int (*getColumnType)(OH_Cursor *cursor, int32_t columnIndex, OH_ColumnType *columnType);

    /**
     * @brief Obtains the column index based on the specified column name.
     *
     * @param cursor Indicates the pointer to the {@link OH_Cursor} instance.
     * @param name Indicates the pointer to the name of the column in the result set.
     * @param columnIndex Indicates the pointer to the index of the column obtained.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_Cursor.
     * @since 10
     */
    int (*getColumnIndex)(OH_Cursor *cursor, const char *name, int *columnIndex);

    /**
     * @brief Obtains the column name based on the specified column index.
     *
     * @param cursor Indicates the pointer to the {@link OH_Cursor} instance.
     * @param columnIndex Indicates the index of the column in the result set.
     * @param name Indicates the pointer to the column name obtained.
     * @param length Indicates the length of the column name obtained.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_Cursor.
     * @since 10
     */
    int (*getColumnName)(OH_Cursor *cursor, int32_t columnIndex, char *name, int length);

    /**
     * @brief Obtains the number of rows in a result set.
     *
     * @param cursor Indicates the pointer to the {@link OH_Cursor} instance.
     * @param count Indicates the pointer to the number of rows in the result set obtained.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_Cursor.
     * @since 10
     */
    int (*getRowCount)(OH_Cursor *cursor, int *count);

    /**
     * @brief Goes to the next row of the result set.
     *
     * @param cursor Indicates the pointer to the {@link OH_Cursor} instance.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_Cursor.
     * @since 10
     */
    int (*goToNextRow)(OH_Cursor *cursor);

    /**
     * @brief Obtains information about the memory required when the column data type in the result set is BLOB or TEXT.
     *
     * @param cursor Indicates the pointer to the {@link OH_Cursor} instance.
     * @param columnIndex Indicates the index of the column in the result set.
     * @param size Indicates the pointer to the memory size obtained.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_Cursor.
     * @since 10
     */
    int (*getSize)(OH_Cursor *cursor, int32_t columnIndex, size_t *size);

    /**
     * @brief Obtains the value in the form of a string based on the specified column and the current row.
     *
     * @param cursor Indicates the pointer to the {@link OH_Cursor} instance.
     * @param columnIndex Indicates the index of the column in the result set.
     * @param value Indicates the pointer to the string obtained.
     * @param length Indicates the length of the value, which is obtained by <b>getSize()</b>.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_Cursor.
     * @since 10
     */
    int (*getText)(OH_Cursor *cursor, int32_t columnIndex, char *value, int length);

    /**
     * @brief Obtains the value of the int64_t type based on the specified column and the current row.
     *
     * @param cursor Indicates the pointer to the {@link OH_Cursor} instance.
     * @param columnIndex Indicates the index of the column in the result set.
     * @param value Indicates the pointer to the value obtained.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_Cursor.
     * @since 10
     */
    int (*getInt64)(OH_Cursor *cursor, int32_t columnIndex, int64_t *value);

    /**
     * @brief Obtains the value of the double type based on the specified column and the current row.
     *
     * @param cursor Indicates the pointer to the {@link OH_Cursor} instance.
     * @param columnIndex Indicates the index of the column in the result set.
     * @param value Indicates the pointer to the value obtained.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_Cursor.
     * @since 10
     */
    int (*getReal)(OH_Cursor *cursor, int32_t columnIndex, double *value);

    /**
     * @brief Obtains the value in the form of a byte array based on the specified column and the current row.
     *
     * @param cursor Indicates the pointer to the {@link OH_Cursor} instance.
     * @param columnIndex Indicates the index of the column in the result set.
     * @param value Indicates the pointer to the string obtained.
     * @param length Indicates the length of the value, which is obtained by <b>getSize()</b>.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_Cursor.
     * @since 10
     */
    int (*getBlob)(OH_Cursor *cursor, int32_t columnIndex, unsigned char *value, int length);

    /**
     * @brief Checks whether the value in the specified column is null.
     *
     * @param cursor Indicates the pointer to the {@link OH_Cursor} instance.
     * @param columnIndex Indicates the index of the column in the result set.
     * @param isNull Indicates the pointer to the check result. The value <b>true</b> means the value is null;
     * the value <b>false</b> means the opposite.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_Cursor.
     * @since 10
     */
    int (*isNull)(OH_Cursor *cursor, int32_t columnIndex, bool *isNull);

    /**
     * @brief Destroys a result set.
     *
     * @param cursor Indicates the pointer to the {@link OH_Cursor} instance.
     * @return Returns the operation result. If the operation fails, an error code is returned.
     * @see OH_Cursor.
     * @since 10
     */
    int (*destroy)(OH_Cursor *cursor);
} OH_Cursor;

#ifdef __cplusplus
};
#endif

#endif // OH_CURSOR_H
