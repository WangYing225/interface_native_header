/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Preferences
 * @{
 *
 * @brief Provides APIs for key-value (KV) data processing, including querying, modifying, and persisting KV data.
 *
 * @syscap SystemCapability.DistributedDataManager.Preferences.Core
 *
 * @since 13
 */

/**
 * @file oh_preferences.h
 *
 * @brief Provides APIs and structs for accessing the <b>Preferences</b> object.
 *
 * File to include: <database/preferences/oh_preferences.h>
 * @library libohpreferences.so
 * @syscap SystemCapability.DistributedDataManager.Preferences.Core
 *
 * @since 13
 */

#ifndef OH_PREFERENCES_H
#define OH_PREFERENCES_H

#include <cstdint>

#include "oh_preferences_value.h"
#include "oh_preferences_option.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines the <b>Preferences</b> object.
 *
 * @since 13
 */
typedef struct OH_Preferences OH_Preferences;

/**
 * @brief Defines a callback used to return data changes.
 *
 * @param context Pointer to the application context.
 * @param pairs Pointer to the KV pairs whose data changes are observed.
 * @param count Number of the KV pairs to be observed.
 * @see OH_PreferencesPair
 * @since 13
 */
typedef void (*OH_PreferencesDataObserver)(void *context, const OH_PreferencesPair *pairs, uint32_t count);

/**
 * @brief Opens a <b>Preferences</b> instance and creates a pointer to it.
 * If this pointer is no longer required, use {@link OH_Preferences_Close} to close the instance.
 *
 * @param option Pointer to the {@link OH_PreferencesOption} instance.
 * @param errCode Pointer to the error code returned. For details, see {@link OH_Preferences_ErrCode}.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 *         If <b>PREFERENCES_ERROR_NOT_SUPPORTED</b> is returned, the system capability is not supported.
 *         If <b>PREFERENCES_ERROR_DELETE_FILE</b> is returned, the file fails to be deleted.
 *         If <b>PREFERENCES_ERROR_STORAGE</b> is returned, the data storage is abnormal.
 *         If <b>PREFERENCES_ERROR_MALLOC</b> is returned, memory allocation fails.
 * @return Returns the {@link OH_Preferences} instance opened if the operation is successful;
 * returns a null pointer otherwise.
 * @see OH_Preferences
 * @see OH_PreferencesOption
 * @see OH_Preferences_ErrCode
 * @since 13
 */
OH_Preferences *OH_Preferences_Open(OH_PreferencesOption *option, int *errCode);

/**
 * @brief Closes a <b>Preferences</b> instance.
 *
 * @param preference Pointer to the {@link OH_Preferences} instance to close.
 * @return Returns the error code. For details, see {@link OH_Preferences_ErrCode}.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 *         If <b>PREFERENCES_ERROR_STORAGE</b> is returned, the data storage is abnormal.
 *         If <b>PREFERENCES_ERROR_MALLOC</b> is returned, memory allocation fails.
 * @see OH_Preferences
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_Close(OH_Preferences *preference);

/**
 * @brief Obtains an integer in a <b>Preferences</b> instance based on the given key.
 *
 * @param preference Pointer to the target {@link OH_Preferences} instance.
 * @param key Pointer to the key.
 * @param value Pointer to the integer obtained.
 * @return Returns the error code.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 *         If <b>PREFERENCES_ERROR_STORAGE</b> is returned, the data storage is abnormal.
 *         If <b>PREFERENCES_ERROR_MALLOC</b> is returned, memory allocation fails.
 *         If <b>PREFERENCES_ERROR_KEY_NOT_FOUND</b> is returned, the specified key does not exist.
 * @see OH_Preferences
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_GetInt(OH_Preferences *preference, const char *key, int *value);

/**
 * @brief Obtains a Boolean value in a <b>Preferences</b> instance based on the given key.
 *
 * @param preference Pointer to the target {@link OH_Preferences} instance.
 * @param key Pointer to the key.
 * @param value Pointer to the Boolean value obtained.
 * @return Returns the error code.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 *         If <b>PREFERENCES_ERROR_STORAGE</b> is returned, the data storage is abnormal.
 *         If <b>PREFERENCES_ERROR_MALLOC</b> is returned, memory allocation fails.
 *         If <b>PREFERENCES_ERROR_KEY_NOT_FOUND</b> is returned, the specified key does not exist.
 * @see OH_Preferences
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_GetBool(OH_Preferences *preference, const char *key, bool *value);

/**
 * @brief Obtains a string in a <b>Preferences</b> instance based on the given key.
 *
 * @param preference Pointer to the target {@link OH_Preferences} instance.
 * @param key Pointer to the key.
 * @param value Double pointer to the string obtained. If the string is no longer required,
 * call {@link OH_Preferences_FreeString} to release the memory.
 * @param valueLen Pointer to the length of the string obtained.
 * @return Returns the error code.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 *         If <b>PREFERENCES_ERROR_STORAGE</b> is returned, the data storage is abnormal.
 *         If <b>PREFERENCES_ERROR_MALLOC</b> is returned, memory allocation fails.
 *         If <b>PREFERENCES_ERROR_KEY_NOT_FOUND</b> is returned, the specified key does not exist.
 * @see OH_Preferences
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_GetString(OH_Preferences *preference, const char *key, char **value, uint32_t *valueLen);

/**
 * @brief Releases a string, which is obtained from a <b>Preferences</b> instance.
 *
 * @param string Pointer to the string to release.
 * @see OH_Preferences
 * @since 13
 */
void OH_Preferences_FreeString(char *string);

/**
 * @brief Sets an integer based on the specified key in a <b>Preferences</b> instance.
 *
 * @param preference Pointer to the target {@link OH_Preferences} instance.
 * @param key Pointer to the key of the integer to set.
 * @param value Integer value to set.
 * @return Returns the error code.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 *         If <b>PREFERENCES_ERROR_STORAGE</b> is returned, the data storage is abnormal.
 *         If <b>PREFERENCES_ERROR_MALLOC</b> is returned, memory allocation fails.
 * @see OH_Preferences
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_SetInt(OH_Preferences *preference, const char *key, int value);

/**
 * @brief Sets a Boolean value based on the specified key in a <b>Preferences</b> instance.
 *
 * @param preference Pointer to the target {@link OH_Preferences} instance.
 * @param key Pointer to the key of the value to set.
 * @param value Boolean value to set.
 * @return Returns the error code.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 *         If <b>PREFERENCES_ERROR_STORAGE</b> is returned, the data storage is abnormal.
 *         If <b>PREFERENCES_ERROR_MALLOC</b> is returned, memory allocation fails.
 * @see OH_Preferences
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_SetBool(OH_Preferences *preference, const char *key, bool value);

/**
 * @brief Sets a string based on the specified key in a <b>Preferences</b> instance.
 *
 * @param preference Pointer to the target {@link OH_Preferences} instance.
 * @param key Pointer to the key of the string to set.
 * @param value Pointer to the string to set.
 * @return Returns the error code.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 *         If <b>PREFERENCES_ERROR_STORAGE</b> is returned, the data storage is abnormal.
 *         If <b>PREFERENCES_ERROR_MALLOC</b> is returned, memory allocation fails.
 * @see OH_Preferences
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_SetString(OH_Preferences *preference, const char *key, const char *value);

/**
 * @brief Deletes the KV pair of the specified key from a <b>Preferences</b> instance.
 *
 * @param preference Pointer to the target {@link OH_Preferences} instance.
 * @param key Pointer to the key of the KV pair to delete.
 * @return Returns the error code.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 *         If <b>PREFERENCES_ERROR_STORAGE</b> is returned, the data storage is abnormal.
 *         If <b>PREFERENCES_ERROR_MALLOC</b> is returned, memory allocation fails.
 * @see OH_Preferences
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_Delete(OH_Preferences *preference, const char *key);

/**
 * @brief Subscribes to data changes of the specified keys. If the value of the specified key changes,
 * a callback will be invoked after <b>OH_Preferences_Close()</b> is called.
 *
 * @param preference Pointer to the target {@link OH_Preferences} instance.
 * @param context Pointer to the application context.
 * @param observer Callback {@link OH_PreferencesDataObserver} to be invoked to return the data changes.
 * @param keys Pointer to the keys whose data changes are observed.
 * @param keyCount Number of keys whose data changes are observed.
 * @return Returns the error code.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 *         If <b>PREFERENCES_ERROR_STORAGE</b> is returned, the data storage is abnormal.
 *         If <b>PREFERENCES_ERROR_MALLOC</b> is returned, memory allocation fails.
 *         If <b>PREFERENCES_ERROR_GET_DATAOBSMGRCLIENT</b> is returned, the service for subscribing to data changes
 *         fails to be obtained.
 * @see OH_Preferences
 * @see OH_PreferencesDataObserver
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_RegisterDataObserver(OH_Preferences *preference, void *context,
    OH_PreferencesDataObserver observer, const char *keys[], uint32_t keyCount);

/**
 * @brief Unsubscribes from data changes of the specified keys.
 *
 * @param preference Pointer to the target {@link OH_Preferences} instance.
 * @param context Pointer to the application context.
 * @param observer Callback {@link OH_PreferencesDataObserver} to unregister.
 * @param keys Pointer to the keys whose data changes are not observed.
 * @param keyCount Number of keys.
 * @return Returns the error code.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 *         If <b>PREFERENCES_ERROR_STORAGE</b> is returned, the data storage is abnormal.
 *         If <b>PREFERENCES_ERROR_MALLOC</b> is returned, memory allocation fails.
 * @see OH_Preferences
 * @see OH_PreferencesDataObserver
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_Preferences_UnregisterDataObserver(OH_Preferences *preference, void *context,
    OH_PreferencesDataObserver observer, const char *keys[], uint32_t keyCount);

#ifdef __cplusplus
};
#endif

/** @} */
#endif // OH_PREFERENCES_H
