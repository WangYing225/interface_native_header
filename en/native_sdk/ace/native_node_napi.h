/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup ArkUI_NativeModule
 * @{
 *
 * @brief Provides UI capabilities of ArkUI on the native side, such as UI component creation and destruction,
 * tree node operations, attribute setting, and event listening.
 *
 * @since 12
 */

/**
 * @file native_node_napi.h
 *
 * @brief Declares APIs for converting <b>FrameNode</b> objects on the ArkTS side to <b>ArkUI_NodeHandle</b> objects on
 * the native side.
 *
 * @library libace_ndk.z.so
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @since 12
 */

#ifndef ARKUI_NATIVE_NODE_NAPI_H
#define ARKUI_NATIVE_NODE_NAPI_H

#include "drawable_descriptor.h"
#include "napi/native_api.h"
#include "native_type.h"

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief Obtains a <b>FrameNode</b> object on the ArkTS side and maps it to an <b>ArkUI_NodeHandle</b> object on the
 * native side.
 *
 * @param env Indicates the NAPI environment pointer.
 * @param frameNode Indicates the <b>FrameNode</b> object created on the ArkTS side.
 * @param handle Indicates the pointer to the <b>ArkUI_NodeHandle</b> object.
 * @return Returns the result code.
 *         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
 *         Returns {@link ARKUI_ERROR_CODE_PARAM_INVALID} if a parameter error occurs.
 * @since 12
 */
int32_t OH_ArkUI_GetNodeHandleFromNapiValue(napi_env env, napi_value frameNode, ArkUI_NodeHandle* handle);

/**
 * @brief Obtains a <b>UIContext</b> object on the ArkTS side and maps it to an <b>ArkUI_ContextHandle</b> object on the
 * native side.
 *
 * @param env Indicates the NAPI environment pointer.
 * @param value Indicates the <b>UIContext</b> object created on the ArkTS side.
 * @param context Indicates the pointer to the <b>ArkUI_ContextHandle</b> object.
 * @return Returns the result code.
 *         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
 *         Returns {@link ARKUI_ERROR_CODE_PARAM_INVALID} if a parameter error occurs.
 * @since 12
 */
int32_t OH_ArkUI_GetContextFromNapiValue(napi_env env, napi_value value, ArkUI_ContextHandle* context);

/**
 * @brief Obtains a <b>NodeContent</b> object on the ArkTS side and maps it to an <b>ArkUI_NodeContentHandle</b> object
 * on the native side.
 *
 * @param env Indicates the NAPI environment pointer.
 * @param value Indicates the <b>NodeContent</b> object created on the ArkTS side.
 * @param context Indicates the pointer to the <b>ArkUI_NodeContentHandle</b> object.
 * @return Returns the result code.
 *         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
 *         Returns {@link ARKUI_ERROR_CODE_PARAM_INVALID} if a parameter error occurs.
 * @since 12
 */
int32_t OH_ArkUI_GetNodeContentFromNapiValue(napi_env env, napi_value value, ArkUI_NodeContentHandle* content);

/**
 * @brief Obtains a <b>DrawableDescriptor</b> object on the ArkTS side and maps it to an <b>ArkUI_DrawableDescriptor</b>
 * object on the native side.
 *
 * @param env Indicates the NAPI environment pointer.
 * @param value Indicates the <b>DrawableDescriptor</b> object created on the ArkTS side.
 * @param drawableDescriptor Indicates the object that receives the pointer to the <b>ArkUI_DrawableDescriptor</b>
 * object.
 * @return Returns the result code.
 *         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
 *         Returns {@link ARKUI_ERROR_CODE_PARAM_INVALID} if a parameter error occurs.
 * @since 12
*/
int32_t OH_ArkUI_GetDrawableDescriptorFromNapiValue(
    napi_env env, napi_value value, ArkUI_DrawableDescriptor** drawableDescriptor);

/**
 * @brief Obtains an $r resource object on the ArkTS side and maps it to an <b>ArkUI_DrawableDescriptor</b> object on
 * the native side.
 *
 * @param env Indicates the NAPI environment pointer.
 * @param value Indicates the $r resource object created on the ArkTS side.
 * @param drawableDescriptor Indicates the object that receives the pointer to the <b>ArkUI_DrawableDescriptor</b>
 * object.
 * @return Returns the result code.
 *         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
 *         Returns {@link ARKUI_ERROR_CODE_PARAM_INVALID} if a parameter error occurs.
 * @since 12
*/
int32_t OH_ArkUI_GetDrawableDescriptorFromResourceNapiValue(
    napi_env env, napi_value value, ArkUI_DrawableDescriptor** drawableDescriptor);

/**
* @brief Obtains the ID of the <b>Navigation</b> component where the specified node is located.
*
* @param node Indicates the target node.
* @param buffer Indicates the buffer to which the obtained ID is written.
* @param Navigation Indicates the buffer size.
* @param writeLength Indicates the length of the string written to the buffer when {@link ARKUI_ERROR_CODE_NO_ERROR}
*                    is returned; indicates the minimum size of the buffer required to hold the target when
*                    {@link ARKUI_ERROR_CODE_BUFFER_SIZE_ERROR} is returned.
* @return Returns the result code.
*         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
*         Returns {@link ARKUI_ERROR_CODE_GET_INFO_FAILED} if information fails to be obtained, possibly because the
*         specified node is not in the navigation stack.
*         Returns {@link ARKUI_ERROR_CODE_BUFFER_SIZE_ERROR} if the specified buffer size is smaller than the minimum
*         buffer size required to hold the target.
* @since 12
*/
ArkUI_ErrorCode OH_ArkUI_GetNavigationId(
    ArkUI_NodeHandle node, char* buffer, int32_t bufferSize, int32_t* writeLength);

/**
* @brief Obtains the name of the <b>NavDestination</b> component where the specified node is located.
*
* @param node Indicates the target node.
* @param buffer Indicates the buffer to which the obtained name is written.
* @param Navigation Indicates the buffer size.
* @param writeLength Indicates the length of the string written to the buffer when {@link ARKUI_ERROR_CODE_NO_ERROR} is
*                    returned; indicates the minimum size of the buffer required to hold the target when
*                    {@link ARKUI_ERROR_CODE_BUFFER_SIZE_ERROR} is returned.
* @return Returns the result code.
*         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
*         Returns {@link ARKUI_ERROR_CODE_GET_INFO_FAILED} if information fails to be obtained, possibly because the
*         specified node is not in the navigation stack.
*         Returns {@link ARKUI_ERROR_CODE_BUFFER_SIZE_ERROR} if the specified buffer size is smaller than the minimum
*         buffer size required to hold the target.
* @since 12
*/
ArkUI_ErrorCode OH_ArkUI_GetNavDestinationName(
    ArkUI_NodeHandle node, char* buffer, int32_t bufferSize, int32_t* writeLength);

/**
* @brief Obtains the length of the navigation stack where the specified node is located.
*
* @param node Indicates the target node.
* @param length Indicates the length of the navigation stack. The result, if obtained successfully, is written back to
*               this parameter.
* @return Returns the result code.
*         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
*         Returns {@link ARKUI_ERROR_CODE_GET_INFO_FAILED} if information fails to be obtained, possibly because the
*         specified node is not in the navigation stack.
* @since 12
*/
ArkUI_ErrorCode OH_ArkUI_GetNavStackLength(ArkUI_NodeHandle node, int32_t* length);

/**
* @brief Obtains the page name that matches the specified index in the navigation stack where the specified node is
*        located. The index starts from 0, which indicates the bottom of the stack.
*
* @param node Indicates the target node.
* @param index Indicates the index of the target page in the navigation stack.
* @param buffer Indicates the buffer to which the obtained name is written.
* @param Navigation Indicates the buffer size.
* @param writeLength Indicates the length of the string written to the buffer when {@link ARKUI_ERROR_CODE_NO_ERROR}
*                    is returned; indicates the minimum size of the buffer required to hold the target when
*                    {@link ARKUI_ERROR_CODE_BUFFER_SIZE_ERROR} is returned.
* @return Returns the result code.
*         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
*         Returns {@link ARKUI_ERROR_CODE_NODE_INDEX_INVALID} if the index is invalid.
*         Returns {@link ARKUI_ERROR_CODE_GET_INFO_FAILED} if information fails to be obtained, possibly because the
*         specified node is not in the navigation stack.
*         Returns {@link ARKUI_ERROR_CODE_BUFFER_SIZE_ERROR} if the specified buffer size is smaller than the minimum
*         buffer size required to hold the target.
* @since 12
*/
ArkUI_ErrorCode OH_ArkUI_GetNavDestinationNameByIndex(
    ArkUI_NodeHandle node, int32_t index, char* buffer, int32_t bufferSize, int32_t* writeLength);

/**
* @brief Obtains the ID of the <b>NavDestination</b> component where the specified node is located.
*
* @param node Indicates the target node.
* @param buffer Indicates the buffer to which the obtained ID is written.
* @param Navigation Indicates the buffer size.
* @param writeLength Indicates the length of the string written to the buffer when {@link ARKUI_ERROR_CODE_NO_ERROR}
*                    is returned; indicates the minimum size of the buffer required to hold the target when
*                    {@link ARKUI_ERROR_CODE_BUFFER_SIZE_ERROR} is returned.
* @return Returns the result code.
*         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
*         Returns {@link ARKUI_ERROR_CODE_GET_INFO_FAILED} if information fails to be obtained, possibly because the
*         specified node is not in the navigation stack.
*         Returns {@link ARKUI_ERROR_CODE_BUFFER_SIZE_ERROR} if the specified buffer size is smaller than the minimum
*         buffer size required to hold the target.
* @since 12
*/
ArkUI_ErrorCode OH_ArkUI_GetNavDestinationId(
    ArkUI_NodeHandle node, char* buffer, int32_t bufferSize, int32_t* writeLength);

/**
* @brief Obtains the state of the <b>NavDestination</b> component where the specified node is located.
*
* @param node Indicates the target node.
* @param state Indicates the state of the <b>NavDestination</b> component. The result, if obtained successfully, is
*              written back to this parameter.
* @return Returns the result code.
*         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
*         Returns {@link ARKUI_ERROR_CODE_GET_INFO_FAILED} if information fails to be obtained, possibly because the
*         specified node is not in the navigation stack.
* @since 12
*/
ArkUI_ErrorCode OH_ArkUI_GetNavDestinationState(ArkUI_NodeHandle node, ArkUI_NavDestinationState* state);

/**
* @brief Obtains the index of the <b>NavDestination</b> component where the specified node is located in the navigation
*        stack.
*
* @param node Indicates the target node.
* @param index Indicates the index, starting from 0.
* @return Returns the result code.
*         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
*         Returns {@link ARKUI_ERROR_CODE_GET_INFO_FAILED} if information fails to be obtained, possibly because the
*         specified node is not in the navigation stack.
* @since 12
*/
ArkUI_ErrorCode OH_ArkUI_GetNavDestinationIndex(ArkUI_NodeHandle node, int32_t* index);

/**
* @brief Obtains the parameters of the <b>NavDestination</b> component where the specified node is located.
*
* @param node Indicates the target node.
* @return Returns the parameter object.
* @since 12
*/
napi_value OH_ArkUI_GetNavDestinationParam(ArkUI_NodeHandle node);

/**
* @brief Obtains the index of the page where the specified node is located in the page stack for routing.
*
* @param node Indicates the target node.
* @param index Indicates the index, starting from 1.
* @return Returns the result code.
*         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
*         Returns {@link ARKUI_ERROR_CODE_GET_INFO_FAILED} if information fails to be obtained, possibly because the
*         specified node is not in the navigation stack.
* @since 12
*/
ArkUI_ErrorCode OH_ArkUI_GetRouterPageIndex(ArkUI_NodeHandle node, int32_t* index);

/**
* @brief Obtains the name of the page where the specified node is located.
*
* @param node Indicates the target node.
* @param buffer Indicates the buffer to which the obtained name is written.
* @param Navigation Indicates the buffer size.
* @param writeLength Indicates the length of the string written to the buffer when {@link ARKUI_ERROR_CODE_NO_ERROR} is
*                    returned; indicates the minimum size of the buffer required to hold the target when
*                    {@link ARKUI_ERROR_CODE_BUFFER_SIZE_ERROR} is returned.
* @return Returns the result code.
*         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
*         Returns {@link ARKUI_ERROR_CODE_GET_INFO_FAILED} if the route navigation information failed to be obtained.
*         Returns {@link ARKUI_ERROR_CODE_BUFFER_SIZE_ERROR} if the specified buffer size is smaller than the minimum
*         buffer size required to hold the target.
* @since 12
*/
ArkUI_ErrorCode OH_ArkUI_GetRouterPageName(
    ArkUI_NodeHandle node, char* buffer, int32_t bufferSize, int32_t* writeLength);

/**
* @brief Obtains the path to the page where the specified node is located.
*
* @param node Indicates the target node.
* @param buffer Indicates the buffer to which the obtained path is written.
* @param Navigation Indicates the buffer size.
* @param writeLength Indicates the length of the string written to the buffer when {@link ARKUI_ERROR_CODE_NO_ERROR} is
*                    returned; indicates the minimum size of the buffer required to hold the target when
*                    {@link ARKUI_ERROR_CODE_BUFFER_SIZE_ERROR} is returned.
* @return Returns the result code.
*         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
*         Returns {@link ARKUI_ERROR_CODE_GET_INFO_FAILED} if the route navigation information failed to be obtained.
*         Returns {@link ARKUI_ERROR_CODE_BUFFER_SIZE_ERROR} if the specified buffer size is smaller than the minimum
*         buffer size required to hold the target.
* @since 12
*/
ArkUI_ErrorCode OH_ArkUI_GetRouterPagePath(
    ArkUI_NodeHandle node, char* buffer, int32_t bufferSize, int32_t* writeLength);
/**
* @brief Obtains the state of the page where the specified node is located.
*
* @param node Indicates the target node.
* @param state Indicates the state of the page. The result, if obtained successfully, is written back to this parameter.
* @return Returns the result code.
*         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
*         Returns {@link ARKUI_ERROR_CODE_GET_INFO_FAILED} if the route navigation information failed to be obtained.
* @since 12
*/
ArkUI_ErrorCode OH_ArkUI_GetRouterPageState(ArkUI_NodeHandle node, ArkUI_RouterPageState* state);

/**
* @brief Obtains the ID of the page where the specified node is located.
*
* @param node Indicates the target node.
* @param buffer Indicates the buffer to which the obtained ID is written.
* @param Navigation Indicates the buffer size.
* @param writeLength Indicates the length of the string written to the buffer when {@link ARKUI_ERROR_CODE_NO_ERROR} is
*                    returned; indicates the minimum size of the buffer required to hold the target when
*                    {@link ARKUI_ERROR_CODE_BUFFER_SIZE_ERROR} is returned.
* @return Returns the result code.
*         Returns {@link ARKUI_ERROR_CODE_NO_ERROR} if the operation is successful.
*         Returns {@link ARKUI_ERROR_CODE_GET_INFO_FAILED} if the route navigation information failed to be obtained.
*         Returns {@link ARKUI_ERROR_CODE_BUFFER_SIZE_ERROR} if the specified buffer size is smaller than the minimum
*         buffer size required to hold the target.
* @since 12
*/
ArkUI_ErrorCode OH_ArkUI_GetRouterPageId(
    ArkUI_NodeHandle node, char* buffer, int32_t bufferSize, int32_t* writeLength);

#ifdef __cplusplus
};
#endif

#endif // ARKUI_NATIVE_NODE_NAPI_H
/** @} */
