/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
/**
 * @addtogroup ArkUI_Accessibility
 * @{
 *
 * @brief Describes the native capabilities supported by ArkUI Accessibility, such as querying accessibility nodes and
 * reporting accessibility events.
 *
 * @since 13
 */

/**
 * @file native_interface_accessibility.h
 *
 * @brief Declares the APIs used to access the native Accessibility.
 *
 * @since 13
 */
#ifndef _NATIVE_INTERFACE_ACCESSIBILITY_H
#define _NATIVE_INTERFACE_ACCESSIBILITY_H

#include <cstdint>

#ifdef __cplusplus
extern "C"{
#endif

/**
 * @brief Defines a struct for accessibility element information.
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityElementInfo ArkUI_AccessibilityElementInfo;

/**
 * @brief Defines a struct for accessibility event information.
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityEventInfo ArkUI_AccessibilityEventInfo;

/**
 * @brief Defines a struct for the local provider of accessibility.
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityProvider ArkUI_AccessibilityProvider;

/**
 * @brief Defines a struct for accessibility action arguments.
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityActionArguments  ArkUI_AccessibilityActionArguments;

/**
 * @brief Defines an enum for accessibility action types.
 *
 * @since 13
 */
typedef enum {
    /** Invalid action. */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_INVALID = 0,
    /** Response to a click. */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_CLICK = 0x00000010,
    /** Response to a long click. */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_LONG_CLICK = 0x00000020,
    /** Accessibility focus acquisition. */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_ACCESSIBILITY_FOCUS = 0x00000040,
    /** Accessibility focus clearance. */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_CLEAR_ACCESSIBILITY_FOCUS = 0x00000080,
    /** Forward scroll action. */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_SCROLL_FORWARD = 0x00000100,
    /** Backward scroll action. */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_SCROLL_BACKWARD = 0x00000200,
    /** Copy action for text content. */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_COPY = 0x00000400,
    /** Paste action for text content. */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_PASTE = 0x00000800,
    /** Cut action for text content. */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_CUT = 0x00001000,
    /** Text selection action, requiring the setting of <b>selectTextBegin</b>, <b>TextEnd</b>, and <b>TextInForward</b>
     *  parameters to select a text segment in the text box. */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_SET_SELECTION = 0x00002000,
    /** Text content setting action. */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_SET_TEXT = 0x00004000,
    /** Cursor position setting action. */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_SET_CURSOR_POSITION = 0x00100000,
} ArkUI_Accessibility_ActionType;

/**
 * @brief Defines an enum for accessibility event types.
 *
 * @since 13
 */
typedef enum {
    /** Invalid event. */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_INVALID = 0,
    /** Click event, sent after the UI component responds. */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_CLICKED_EVENT = 0x00000001,
    /** Long click event, sent after the UI component responds. */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_LONG_CLICKED_EVENT = 0x00000002,
    /** Selection event, sent after the UI component responds. */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_SELECTED_EVENT = 0x00000004,
    /** Text update event, sent when text is updated. */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_TEXT_UPDATE_EVENT = 0x00000010,
    /** Page state update event, sent when the page transitions, switches, resizes, or moves. */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_PAGE_STATE_UPDATE = 0x00000020,
    /** Page content update event, sent when the page content changes. */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_PAGE_CONTENT_UPDATE = 0x00000800,
    /** Scrolled event, sent when a scrollable component experiences a scroll event. */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_SCROLLED_EVENT = 0x000001000,
    /** Accessibility focus event, sent after the UI component responds. */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_ACCESSIBILITY_FOCUSED_EVENT = 0x00008000,
    /** Accessibility focus cleared event, sent after the UI component responds. */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED_EVENT = 0x00010000,
    /** FOcus request for a specific node. */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_REQUEST_FOCUS_FOR_ACCESSIBILITY = 0x02000000,
    /** Page open event reported by the UI component. */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_PAGE_OPEN = 0x20000000,
    /** Page close event reported by the UI component. */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_PAGE_CLOSE = 0x08000000,
    /** Announcement event, indicating a request to proactively announce specified content. */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_ANNOUNCE_FOR_ACCESSIBILITY = 0x10000000,
} ArkUI_AccessibilityEventType;

/**
 * @brief Defines a struct for the accessible action.
 *
 * @since 13
 */
typedef struct {
    /** Action type. */
    ArkUI_Accessibility_ActionType actionType;
    /** Action description. */
    const char* description;
} ArkUI_AccessibleAction;

/**
 * @brief Defines a struct for the accessible rectangle.
 *
 * @since 13
 */
typedef struct {
    /** X coordinate of the upper left corner. */
    int32_t leftTopX;
    /** Y coordinate of the upper left corner. */
    int32_t leftTopY;
    /** X coordinate of the lower right corner. */
    int32_t rightBottomX;
    /** Y coordinate of the lower right corner. */
    int32_t rightBottomY;
} ArkUI_AccessibleRect;

/**
 * @brief Define a struct for the accessible range information.
 *
 * @since 13
 */
typedef struct {
    /** Minimum value. */
    double min;
    /** Maximum value. */
    double max;
    /** Current value. */
    double current;
} ArkUI_AccessibleRangeInfo;

/**
 * @brief Defines a struct for the accessible grid information.
 *
 * @since 13
 */
typedef struct {
    /** Number of rows. */
    int32_t rowCount;
    /** Number of columns. */
    int32_t columnCount;
    /** Selection mode. The value <b>0</b> indicates that only one row can be selected. */
    int32_t selectionMode;
} ArkUI_AccessibleGridInfo;

/**
 * @brief Defines a struct for the accessible grid item information.
 *
 * @since 13
 */
typedef struct {
    /** Whether it is a header. */
    bool heading;
    /** Whether it is selected. */
    bool selected;
    /** Column index. */
    int32_t columnIndex;
    /** Row index. */
    int32_t rowIndex;
    /** Column span. */
    int32_t columnSpan;
    /** Row span. */
    int32_t rowSpan;
} ArkUI_AccessibleGridItemInfo;

/**
 * @brief Enumerates the accessibility error codes.
 *
 * @since 13
 */
enum AcessbilityErrorCode{
    /** Success. */
    OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS = 0,
    /** Failure. */
    OH_ARKUI_ACCESSIBILITY_RESULT_FAILED = -1,
    /** Invalid parameter. */
    OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER = -2,
    /** Out of memory. */
    OH_ARKUI_ACCESSIBILITY_RESULT_OUT_OF_MEMORY = -3,
} ;

/**
 * @brief Defines an enum for the accessibility search modes.
 *
 * @since 13
 */
typedef enum {
    /** Search for parent nodes. */
    NATIVE_SEARCH_MODE_PREFETCH_PREDECESSORS = 1 << 0,
    /** Search for sibling nodes. */
    NATIVE_SEARCH_MODE_PREFETCH_SIBLINGS = 1 << 1,
    /** Search for child nodes at the next level. */
    NATIVE_SEARCH_MODE_PREFETCH_CHILDREN = 1 << 2,
    /** Search for all child nodes. */
    NATIVE_SEARCH_MODE_PREFETCH_RECURSIVE_CHILDREN = 1 << 3,
} ArkUI_AccessibilitySearchMode;

/**
 * @brief Defines an enum for the accessibility focus types.
 *
 * @since 13
 */
typedef enum {
    /** Invalid type. */
    NATIVE_FOCUS_TYPE_INVALID = -1,
    /** Input focus type. */
    NATIVE_FOCUS_TYPE_INPUT = 1 << 0,
    /** Accessibility focus type. */
    NATIVE_FOCUS_TYPE_ACCESSIBILITY = 1 << 1,
} ArkUI_AccessibilityFocusType;

/**
 * @brief Enumerates the directions for moving the accessibility focus.
 *
 * @since 13
 */
typedef enum {
    /** Invalid direction. */
    NATIVE_DIRECTION_INVALID = 0,
    /** Up. */
    NATIVE_DIRECTION_UP = 0x00000001,
    /** Down. */
    NATIVE_DIRECTION_DOWN = 0x00000002,
    /** Left. */
    NATIVE_DIRECTION_LEFT = 0x00000004,
    /** Right. */
    NATIVE_DIRECTION_RIGHT = 0x00000008,
    /** Forward. */
    NATIVE_DIRECTION_FORWARD = 0x00000010,
    /** Backward. */
    NATIVE_DIRECTION_BACKWARD = 0x00000020,
} ArkUI_AccessibilityFocusMoveDirection;

/**
 * @brief Defines a struct for the accessibility element information list.
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityElementInfoList ArkUI_AccessibilityElementInfoList;

/**
 * @brief Registers callbacks for the accessibility provider.
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityProviderCallbacks {
    /** Called to obtain element information based on a specified node. */
    int32_t (*FindAccessibilityNodeInfosById)(int64_t elementId, ArkUI_AccessibilitySearchMode mode,
        int32_t requestId, ArkUI_AccessibilityElementInfoList* elementList);
    /** Called to obtain element information based on a specified node and text content. */
    int32_t (*FindAccessibilityNodeInfosByText)(int64_t elementId, const char* text, int32_t requestId,
        ArkUI_AccessibilityElementInfoList* elementList);
    /** Called to obtain focused element information based on a specified node. */
    int32_t (*FindFocusedAccessibilityNode)(int64_t elementId, ArkUI_AccessibilityFocusType focusType,
        int32_t requestId, ArkUI_AccessibilityElementInfo* elementinfo);
    /** Called to find the next focusable node based on the reference node, in the specified mode and direction. */
    int32_t (*FindNextFocusAccessibilityNode)(
        int64_t elementId, ArkUI_AccessibilityFocusMoveDirection direction,
        int32_t requestId, ArkUI_AccessibilityElementInfo* elementList);
    /** Called to execute a specified action on a specified node. */
    int32_t (*ExecuteAccessibilityAction)(int64_t elementId, ArkUI_Accessibility_ActionType action,
        ArkUI_AccessibilityActionArguments  actionArguments, int32_t requestId);
    /** Called to clear the focus state of the current focused node. */
    int32_t (*ClearFocusedFocusAccessibilityNode)();
    /** Called to query the current cursor position of the specified node. */
    int32_t (*GetAccessibilityNodeCursorPosition)(int64_t elementId, int32_t requestId, int32_t* index);
} ArkUI_AccessibilityProviderCallbacks;

/**
 * @brief Registers a callback for this <b>ArkUI_AccessibilityProvider</b> instance.
 *
 * @param provider Indicates the pointer to the <b>ArkUI_AccessibilityProvider</b> instance.
 * @param callbacks Indicates the pointer to the <b>GetAccessibilityNodeCursorPosition</b> callback.
 * @return Returns the error code.
 * @since 13
 */
int32_t OH_ArkUI_AccessibilityProviderRegisterCallback(
    ArkUI_AccessibilityProvider* provider, ArkUI_AccessibilityProviderCallbacks* callbacks);

/**
 * @brief Sends accessibility event information.
 * 
 * @param provider Indicates the pointer to the <b>ArkUI_AccessibilityProvider</b> instance.
 * @param eventInfo Indicates the pointer to the accessibility event information.
 * @param callback Indicates the pointer to the callback that is called after the event is sent.
 * @since 13
 */
void OH_ArkUI_SendAccessibilityAsyncEvent(
    ArkUI_AccessibilityProvider* provider, ArkUI_AccessibilityEventInfo* eventInfo,
    void (*callback)(int32_t errorCode));

/**
 * @brief Adds and obtains the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
 * 
 * @param list Indicates the pointer to an <b>ArkUI_AccessibilityElementInfoList</b> object.
 * @return Returns the pointer to the <b>ArkUI_AccessibilityElementInfo</b> object.
 * @since 13
 */
ArkUI_AccessibilityElementInfo* OH_ArkUI_AddAndGetAccessibilityElementInfo(
    ArkUI_AccessibilityElementInfoList* list);

/**
* @brief Sets the page ID for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param pageId Indicates the page ID.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoPageId(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t pageId);

/**
* @brief Sets the component ID for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param componentId Indicates the component ID.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoComponentId(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t componentId);

/**
* @brief Sets the parent ID for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param parentId Indicates the parent ID.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoParentId(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t parentId);

/**
* @brief Sets the component type for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param componentType Indicates the component type.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoComponentType(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* componentType);

/**
* @brief Sets the component content for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param contents Indicates the component content.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoContents(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* contents);

/**
* @brief Sets the hint text for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param hintText Indicates the hint text.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoHintText(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* hintText);

/**
* @brief Sets the accessibility text for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param accessibilityText Indicates the accessibility text.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoAccessibilityText(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* accessibilityText);

/**
* @brief Sets the accessibility description for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param accessibilityDescription Indicates the accessibility description.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoAccessibilityDescription(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* accessibilityDescription);

/**
* @brief Set the number of child nodes and child node IDs for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param childCount Indicates the number of child nodes.
* @param childNodeIds Indicates an array of child node IDs.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoChildNodeIds(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t childCount, int64_t* childNodeIds);

/**
* @brief Sets the operation actions for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param operationActions Indicates the operation actions.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoOperationActions(ArkUI_AccessibilityElementInfo* elementInfo,
    int32_t operationCount, ArkUI_AccessibleAction* operationActions);

/**
* @brief Sets the screen area for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param screenRect Indicates the screen area.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoScreenRect(
    ArkUI_AccessibilityElementInfo* elementInfo, ArkUI_AccessibleRect* screenRect);

/**
* @brief Sets whether the element is checkable for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param checkable Indicates whether the element is checkable.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoCheckable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool checkable);

/**
* @brief Sets whether the element is checked for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param checked Indicates whether the element is checked.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoChecked(
    ArkUI_AccessibilityElementInfo* elementInfo, bool checked);

/**
* @brief Sets whether the element is focusable for an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param focusable Indicates whether the element is focusable.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoFocusable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool focusable);

/**
* @brief Sets whether the element is focused for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param isFocused Indicates whether the element is focused.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoFocused(
    ArkUI_AccessibilityElementInfo* elementInfo, bool isFocused);

/**
* @brief Sets whether the element is visible for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param isVisible Indicates whether the element is visible.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoVisible(
    ArkUI_AccessibilityElementInfo* elementInfo, bool isVisible);

/**
* @brief Sets the accessibility focus state for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param accessibilityFocused Indicates whether the element has accessibility focus.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoAccessibilityFocused(
    ArkUI_AccessibilityElementInfo* elementInfo, bool accessibilityFocused);

/**
* @brief Sets whether the element is selected for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param selected Indicates whether the element is selected.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoSelected(
    ArkUI_AccessibilityElementInfo* elementInfo, bool selected);

/**
* @brief Sets whether the element is clickable for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param clickable Indicates whether the element is clickable.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoClickable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool clickable);

/**
* @brief Sets whether the element is long clickable for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param longClickable Indicates whether the element is long clickable.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoLongClickable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool longClickable);

/**
* @brief Sets whether the element is enabled for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param isEnable Indicates whether the element is enabled.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoEnabled(
    ArkUI_AccessibilityElementInfo* elementInfo, bool isEnabled);

/**
* @brief Sets whether the element is a password for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param isPassword Indicates whether the element is a password.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoIsPassword(
    ArkUI_AccessibilityElementInfo* elementInfo, bool isPassword);

/**
* @brief Sets whether the element is scrollable for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param scrollable Indicates whether the element is scrollable.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoScrollable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool scrollable);

/**
* @brief Sets whether the element is editable for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param editable Indicates whether the element is editable.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoEditable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool editable);

/**
* @brief Sets whether the element is a hint for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param isHint Indicates whether the element is a hint.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoIsHint(
    ArkUI_AccessibilityElementInfo* elementInfo, bool isHint);

/**
* @brief Sets the range information for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param rangeInfo Indicates the range information.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoRangeInfo(
    ArkUI_AccessibilityElementInfo* elementInfo, ArkUI_AccessibleRangeInfo* rangeInfo);

/**
* @brief Sets the grid information for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param gridInfo Indicates the grid information.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoGridInfo(
    ArkUI_AccessibilityElementInfo* elementInfo, ArkUI_AccessibleGridInfo* gridInfo);

/**
* @brief Sets the grid item for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param gridItem Indicates the grid item.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoGridItemInfo(
    ArkUI_AccessibilityElementInfo* elementInfo, ArkUI_AccessibleGridItemInfo* gridItem);

/**
* @brief Sets the starting index of the selected text for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param textBeginSelected Indicates the starting index of the selected text
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoTextBeginSelected(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t textBeginSelected);

/**
* @brief Sets the end index of the selected text for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param textEndSelected Indicates the end index of the selected text
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoTextEndSelected(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t textEndSelected);

/**
* @brief Sets the index of the currently selected item for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param currentItemIndex Indicates the index of the currently selected item.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoCurrentItemIndex(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t currentItemIndex);

/**
* @brief Sets the index of the first item for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param beginItemIndex Indicates the index of the first item.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoBeginItemIndex(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t beginItemIndex);

/**
* @brief Sets the index of the last item for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param endItemIndex Indicates the index of the last item.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoEndItemIndex(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t endItemIndex);

/**
* @brief Sets the number of items for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param itemCount Indicates the number of items.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoItemCount(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t itemCount);

/**
* @brief Sets the offset for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param offset Indicates the scroll pixel offset relative to the top of the element.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoAccessibilityOffset(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t offset);

/**
* @brief Sets the accessibility group for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param accessibilityGroup Indicates the accessibility group.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoAccessibilityGroup(
    ArkUI_AccessibilityElementInfo* elementInfo, bool accessibilityGroup);

/**
* @brief Sets the accessibility level for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param accessibilityLevel Indicates the accessibility level.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoAccessibilityLevel(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* accessibilityLevel);

/**
* @brief Sets the z-index for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param zIndex Indicates the z-index value.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoZIndex(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t zIndex);

/**
* @brief Sets the opacity for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param opacity Indicates the opacity.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoAccessibilityOpacity(
    ArkUI_AccessibilityElementInfo* elementInfo, float opacity);

/**
* @brief Sets the background color for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param backgroundColor Indicates the background color.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoBackgroundColor(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* backgroundColor);

/**
* @brief Sets the background image for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param backgroundImage Indicates the backgroundImage.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoBackgroundImage(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* backgroundImage);

/**
* @brief Sets the blur effect for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param blur Indicates the blur effect.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoBlur(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* blur);

/**
* @brief Sets the hit test behavior for an <b>ArkUI_AccessibilityElementInfo</b> object.
*
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @param hitTestBehavior Indicates the hit test behavior.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoHitTestBehavior(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* hitTestBehavior);

/**
 * @brief Creates an <b>ArkUI_AccessibilityEventInfo</b> object.
 *
 * @return Returns the <b>ArkUI_AccessibilityEventInfo</b> object.
 * @since 13
 */
ArkUI_AccessibilityEventInfo* OH_ArkUI_CreateAccessibilityEventInfo(void);

/**
 * @brief Destroys an <b>ArkUI_AccessibilityEventInfo</b> object.
 *
 * @param eventInfo Indicates the pointer to the <b>ArkUI_AccessibilityEventInfo</b> object to destroy.
 * @since 13
 */
void OH_ArkUI_DestoryAccessibilityEventInfo(ArkUI_AccessibilityEventInfo* eventInfo);

/**
* @brief Sets the event type for an <b>ArkUI_AccessibilityEventInfo</b> object.
*
* @param eventInfo Indicates the pointer to an <b>ArkUI_AccessibilityEventInfo</b> object.
* @param eventType Indicates the event type.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityEventEventType(
    ArkUI_AccessibilityEventInfo* eventInfo,  ArkUI_AccessibilityEventType eventType);

/**
* @brief Sets the page ID for an <b>ArkUI_AccessibilityEventInfo</b> object.
*
* @param eventInfo Indicates the pointer to an <b>ArkUI_AccessibilityEventInfo</b> object.
* @param pageId Indicates the page ID.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityEventPageId(
    ArkUI_AccessibilityEventInfo* eventInfo,  int32_t pageId);

/**
* @brief Sets the text announced for accessibility for an <b>ArkUI_AccessibilityEventInfo</b> object.
*
* @param eventInfo Indicates the pointer to an <b>ArkUI_AccessibilityEventInfo</b> object.
* @param textAnnouncedForAccessibility Indicates the text announced for accessibility.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityEventTextAnnouncedForAccessibility(
    ArkUI_AccessibilityEventInfo* eventInfo,  const char* textAnnouncedForAccessibility);

/**
* @brief Sets the request focus ID for an <b>ArkUI_AccessibilityEventInfo</b> object.
*
* @param eventInfo Indicates the pointer to an <b>ArkUI_AccessibilityEventInfo</b> object.
* @param requestFocusId Indicates the request focus ID.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityEventRequestFocusId(
    ArkUI_AccessibilityEventInfo* eventInfo,  int32_t requestFocusId);

/**
* @brief Sets the element information for an <b>ArkUI_AccessibilityEventInfo</b> object.
*
* @param eventInfo Indicates the pointer to an <b>ArkUI_AccessibilityEventInfo</b> object.
* @param elementInfo Indicates the pointer to an <b>ArkUI_AccessibilityElementInfo</b> object.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityEventElementInfo(
    ArkUI_AccessibilityEventInfo* eventInfo,  ArkUI_AccessibilityElementInfo* elementInfo);

/**
* @brief Obtains the value of a key from an <b>ArkUI_AccessibilityActionArguments</b> object.
*
* @param arguments Indicates the pointer to an <b>ArkUI_AccessibilityActionArguments</b> object.
* @param key Indicates the key.
* @param value Indicates the value.
* @return Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS} if the operation is successful.
*         Returns {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER} if a parameter is incorrect.
* @since 13
*/
int32_t OH_ArkUI_FindAccessibilityActionArgumentByKey(
    ArkUI_AccessibilityActionArguments* arguments, const char* key, char* value);
#ifdef __cplusplus
};
#endif
#endif // _NATIVE_INTERFACE_ACCESSIBILITY_H
