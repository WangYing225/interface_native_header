/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef OHOS_INPUTMETHOD_ATTACH_OPTIONS_CAPI_H
#define OHOS_INPUTMETHOD_ATTACH_OPTIONS_CAPI_H
/**
 * @addtogroup InputMethod
 * @{
 *
 * @brief InputMethod模块提供方法来使用输入法和开发输入法。
 *
 * @since 12
 */

/**
 * @file inputmethod_attach_options_capi.h
 *
 * @brief 提供输入法绑定选项对象的创建、销毁与读写方法。
 *
 * @library libohinputmethod.so
 * @kit IMEKit
 * @syscap SystemCapability.MiscServices.InputMethodFramework
 * @since 12
 * @version 1.0
 */
#include "inputmethod_types_capi.h"
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
/**
 * @brief 输入法绑定选项。
 *
 * 绑定输入法时携带的选项。
 *
 * @since 12
 */
typedef struct InputMethod_AttachOptions InputMethod_AttachOptions;

/**
 * @brief 创建一个新的{@link InputMethod_AttachOptions}实例。
 *
 * @param showKeyboard 表示是否显示键盘。
 * @return 如果创建成功，返回一个指向新创建的{@link InputMethod_AttachOptions}实例的指针。
 * 如果创建失败，对象返回NULL，可能的失败原因有应用地址空间满。
 * @since 12
 */
InputMethod_AttachOptions *OH_AttachOptions_Create(bool showKeyboard);
/**
 * @brief 销毁一个{@link InputMethod_AttachOptions}实例.
 *
 * @param options 表示即将被销毁的{@link InputMethod_AttachOptions}实例。
 * @since 12
 */
void OH_AttachOptions_Destroy(InputMethod_AttachOptions *options);
/**
 * @brief 从{@link InputMethod_AttachOptions}中获取是否显示键盘的值。
 *
 * @param options 表示被读取值的{@link InputMethod_AttachOptions}实例。
 * @param showKeyboard 表示绑定时是否显示键盘。
 *     true - 表示绑定完成时需要显示键盘。
 *     false - 表示绑定完成时不需要显示键盘.
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_AttachOptions_IsShowKeyboard(InputMethod_AttachOptions *options, bool *showKeyboard);
#ifdef __cplusplus
}
#endif /* __cplusplus */
/** @} */
#endif // OHOS_INPUTMETHOD_ATTACH_OPTIONS_CAPI_H