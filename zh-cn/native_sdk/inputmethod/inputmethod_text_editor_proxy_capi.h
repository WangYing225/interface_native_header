/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef OHOS_INPUTMETHOD_TEXT_EDITOR_PROXY_CAPI_H
#define OHOS_INPUTMETHOD_TEXT_EDITOR_PROXY_CAPI_H
/**
 * @addtogroup InputMethod
 * @{
 *
 * @brief InputMethod模块提供方法来使用输入法和开发输入法。
 *
 * @since 12
 */

/**
 * @file inputmethod_text_editor_proxy_capi.h
 *
 * @brief 提供一套方法支持应用开发的自绘输入框获取来自输入法应用的通知和请求。
 *
 * @library libohinputmethod.so
 * @kit IMEKit
 * @syscap SystemCapability.MiscServices.InputMethodFramework
 * @since 12
 * @version 1.0
 */
#include <stddef.h>

#include "inputmethod_private_command_capi.h"
#include "inputmethod_text_config_capi.h"
#include "inputmethod_types_capi.h"
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
/**
 * @brief 输入框代理。
 *
 * 提供了获取来自输入法应用的通知和请求的方法。
 *
 * @since 12
 */
typedef struct InputMethod_TextEditorProxy InputMethod_TextEditorProxy;

/**
 * @brief 输入法获取输入框配置时触发的函数。
 *
 * 您需要实现此函数，将它设置到{@link InputMethod_TextEditorProxy}中，并通过{@link OH_InputMethodController_Attach}完成注册。
 *
 * @param textEditorProxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param config 输入框配置。
 * @since 12
 */
typedef void (*OH_TextEditorProxy_GetTextConfigFunc)(
    InputMethod_TextEditorProxy *textEditorProxy, InputMethod_TextConfig *config);
/**
 * @brief 输入法应用插入文本时触发的函数。
 *
 * 您需要实现此函数，将它设置到{@link InputMethod_TextEditorProxy}中，并通过{@link OH_InputMethodController_Attach}完成注册。
 *
 * @param textEditorProxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * in.
 * @param text 插入的字符。
 * @param length 插入字符的长度。
 * @since 12
 */
typedef void (*OH_TextEditorProxy_InsertTextFunc)(
    InputMethod_TextEditorProxy *textEditorProxy, const char16_t *text, size_t length);
/**
 * @brief 输入法删除光标右侧文本时触发的函数。
 *
 * 您需要实现此函数，将它设置到{@link InputMethod_TextEditorProxy}中，并通过{@link OH_InputMethodController_Attach}完成注册。
 *
 * @param textEditorProxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * in.
 * @param length 要删除字符的长度。
 * @since 12
 */
typedef void (*OH_TextEditorProxy_DeleteForwardFunc)(InputMethod_TextEditorProxy *textEditorProxy, int32_t length);
/**
 * @brief 输入法删除光标左侧文本时触发的函数。
 *
 * 您需要实现此函数，将它设置到{@link InputMethod_TextEditorProxy}中，并通过{@link OH_InputMethodController_Attach}完成注册。
 *
 * @param textEditorProxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * in.
 * @param length 要删除字符的长度。
 * @since 12
 */
typedef void (*OH_TextEditorProxy_DeleteBackwardFunc)(InputMethod_TextEditorProxy *textEditorProxy, int32_t length);
/**
 * @brief 输入法通知键盘状态时触发的函数。
 *
 * 您需要实现此函数，将它设置到{@link InputMethod_TextEditorProxy}中，并通过{@link OH_InputMethodController_Attach}完成注册。
 *
 * @param textEditorProxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param keyboardStatus 键盘状态，具体定义详见{@link InputMethod_KeyboardStatus}。
 * @since 12
 */
typedef void (*OH_TextEditorProxy_SendKeyboardStatusFunc)(
    InputMethod_TextEditorProxy *textEditorProxy, InputMethod_KeyboardStatus keyboardStatus);
/**
 * @brief 输入法发送回车键时触发的函数。
 *
 * 您需要实现此函数，将它设置到{@link InputMethod_TextEditorProxy}中，并通过{@link OH_InputMethodController_Attach}完成注册。
 *
 * @param textEditorProxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param enterKeyType 回车键类型，具体定义详见{@link InputMethod_EnterKeyType}.
 * @since 12
 */
typedef void (*OH_TextEditorProxy_SendEnterKeyFunc)(
    InputMethod_TextEditorProxy *textEditorProxy, InputMethod_EnterKeyType enterKeyType);
/**
 * @brief 输入法移动光标时触发的函数。
 *
 * 您需要实现此函数，将它设置到{@link InputMethod_TextEditorProxy}中，并通过{@link OH_InputMethodController_Attach}完成注册。
 *
 * @param textEditorProxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param direction 光标移动方向，具体定义详见{@link InputMethod_Direction}.
 * @since 12
 */
typedef void (*OH_TextEditorProxy_MoveCursorFunc)(
    InputMethod_TextEditorProxy *textEditorProxy, InputMethod_Direction direction);
/**
 * @brief 输入法请求选中文本时触发的函数。
 *
 * 您需要实现此函数，将它设置到{@link InputMethod_TextEditorProxy}中，并通过{@link OH_InputMethodController_Attach}完成注册。
 *
 * @param textEditorProxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param start 表示选中文本的起始位置。
 * @param end 表示选中文本的结束位置。
 * @since 12
 */
typedef void (*OH_TextEditorProxy_HandleSetSelectionFunc)(
    InputMethod_TextEditorProxy *textEditorProxy, int32_t start, int32_t end);
/**
 * @brief 输入法发送扩展编辑操作时触发的函数。
 *
 * 您需要实现此函数，将它设置到{@link InputMethod_TextEditorProxy}中，并通过{@link OH_InputMethodController_Attach}完成注册。
 *
 * @param textEditorProxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param action 扩展编辑操作，具体定义详见{@link InputMethod_ExtendAction}.
 * @since 12
 */
typedef void (*OH_TextEditorProxy_HandleExtendActionFunc)(
    InputMethod_TextEditorProxy *textEditorProxy, InputMethod_ExtendAction action);
/**
 * @brief 输入法获取光标左侧文本时触发的函数。
 *
 * 您需要实现此函数，将它设置到{@link InputMethod_TextEditorProxy}中，并通过{@link OH_InputMethodController_Attach}完成注册。
 *
 * @param textEditorProxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param number 目标获取文本的长度。
 * @param text 光标左侧指定长度的文本内容，需要在函数实现中对它赋值。
 * @since 12
 */
typedef void (*OH_TextEditorProxy_GetLeftTextOfCursorFunc)(
    InputMethod_TextEditorProxy *textEditorProxy, int32_t number, char16_t text[], size_t *length);
/**
 * @brief 输入法获取光标右侧文本时触发的函数。
 *
 * 您需要实现此函数，将它设置到{@link InputMethod_TextEditorProxy}中，并通过{@link OH_InputMethodController_Attach}完成注册。
 *
 * @param textEditorProxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param number 目标获取文本的长度。
 * @param text 光标右侧指定长度的文本内容，需要在函数实现中对它赋值。
 * @since 12
 */
typedef void (*OH_TextEditorProxy_GetRightTextOfCursorFunc)(
    InputMethod_TextEditorProxy *textEditorProxy, int32_t number, char16_t text[], size_t *length);
/**
 * @brief 输入法获取光标所在输入框文本索引时触发的函数。
 *
 * 您需要实现此函数，将它设置到{@link InputMethod_TextEditorProxy}中，并通过{@link OH_InputMethodController_Attach}完成注册。
 *
 * @param textEditorProxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @return 返回光标所在输入框文本索引。
 * @since 12
 */
typedef int32_t (*OH_TextEditorProxy_GetTextIndexAtCursorFunc)(InputMethod_TextEditorProxy *textEditorProxy);
/**
 * @brief 输入法应用发送私有数据命令时触发的函数。
 *
 * 您需要实现此函数，将它设置到{@link InputMethod_TextEditorProxy}中，并通过{@link OH_InputMethodController_Attach}完成注册。
 *
 * @param textEditorProxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param privateCommand 私有数据命令。
 * @param size 私有数据的大小。
 * @return 返回对私有数据命令处理的处理结果。
 * @since 12
 */
typedef int32_t (*OH_TextEditorProxy_ReceivePrivateCommandFunc)(
    InputMethod_TextEditorProxy *textEditorProxy, InputMethod_PrivateCommand *privateCommand[], size_t size);
/**
 * @brief 输入法设置预上屏文本时触发的函数。
 *
 * 您需要实现此函数，将它设置到{@link InputMethod_TextEditorProxy}中，并通过{@link OH_InputMethodController_Attach}完成注册。
 *
 * @param textEditorProxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param text 请求设置为预上屏样式的文本内容。
 * @param length 预上屏文本长度。
 * @param start 预上屏文本起始光标位置。
 * @param end 预上屏文本结束光标位置。
 * @return 返回设置预上屏文本的处理结果。
 * @since 12
 */
typedef int32_t (*OH_TextEditorProxy_SetPreviewTextFunc)(
    InputMethod_TextEditorProxy *textEditorProxy, const char16_t text[], size_t length, int32_t start, int32_t end);
/**
 * @brief 输入法结束预上屏时触发的函数。
 *
 * 您需要实现此函数，将它设置到{@link InputMethod_TextEditorProxy}中，并通过{@link OH_InputMethodController_Attach}完成注册。
 *
 * @param textEditorProxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @since 12
 */
typedef void (*OH_TextEditorProxy_FinishTextPreviewFunc)(InputMethod_TextEditorProxy *textEditorProxy);

/**
 * @brief 创建一个新的{@link InputMethod_TextEditorProxy}实例。
 *
 * @return 如果创建成功，返回一个指向新创建的{@link InputMethod_TextEditorProxy}实例的指针。
 * 如果创建失败，对象返回NULL，可能的失败原因有应用地址空间满。
 * @since 12
 */
InputMethod_TextEditorProxy *OH_TextEditorProxy_Create();
/**
 * @brief 销毁一个{@link InputMethod_TextEditorProxy}实例。
 *
 * @param proxy 表示指向即将被销毁的{@link InputMethod_TextEditorProxy}实例的指针。
 * @since 12
 */
void OH_TextEditorProxy_Destroy(InputMethod_TextEditorProxy *proxy);
/**
 * @brief 将函数{@link OH_TextEditorProxy_GetTextConfigFunc}设置到{@link InputMethod_TextEditorProxy}中。
 *
 * @param proxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param getTextConfigFunc 表示被设置到proxy的函数{@link OH_TextEditorProxy_GetTextConfigFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_SetGetTextConfigFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_GetTextConfigFunc getTextConfigFunc);
/**
 * @brief 将函数{@link OH_TextEditorProxy_InsertTextFunc}设置到{@link InputMethod_TextEditorProxy}中。
 *
 * @param proxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param insertTextFunc 表示被设置到proxy的函数{@link OH_TextEditorProxy_InsertTextFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_SetInsertTextFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_InsertTextFunc insertTextFunc);
/**
 * @brief 将函数{@link OH_TextEditorProxy_DeleteForwardFunc}设置到{@link InputMethod_TextEditorProxy}中。
 *
 * @param proxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param deleteForwardFunc 表示被设置到proxy的函数{@link OH_TextEditorProxy_DeleteForwardFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_SetDeleteForwardFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_DeleteForwardFunc deleteForwardFunc);
/**
 * @brief 将函数{@link OH_TextEditorProxy_DeleteBackwardFunc}设置到{@link InputMethod_TextEditorProxy}中。
 *
 * @param proxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param deleteBackwardFunc 表示被设置到proxy的函数{@link OH_TextEditorProxy_DeleteBackwardFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_SetDeleteBackwardFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_DeleteBackwardFunc deleteBackwardFunc);
/**
 * @brief 将函数{@link OH_TextEditorProxy_SendKeyboardStatusFunc}设置到{@link InputMethod_TextEditorProxy}中。
 *
 * @param proxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param sendKeyboardStatusFunc 表示被设置到proxy的函数{@link OH_TextEditorProxy_SendKeyboardStatusFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_SetSendKeyboardStatusFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_SendKeyboardStatusFunc sendKeyboardStatusFunc);
/**
 * @brief 将函数{@link OH_TextEditorProxy_SetSendEnterKeyFunc}设置到{@link InputMethod_TextEditorProxy}中。
 *
 * @param proxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param sendEnterKeyFunc 表示被设置到proxy的函数{@link OH_TextEditorProxy_SendEnterKeyFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_SetSendEnterKeyFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_SendEnterKeyFunc sendEnterKeyFunc);
/**
 * @brief 将函数{@link OH_TextEditorProxy_SetMoveCursorFunc}设置到{@link InputMethod_TextEditorProxy}中。
 *
 * @param proxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param moveCursorFunc 表示被设置到proxy的函数{@link OH_TextEditorProxy_MoveCursorFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_SetMoveCursorFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_MoveCursorFunc moveCursorFunc);
/**
 * @brief 将函数{@link OH_TextEditorProxy_HandleSetSelectionFunc}设置到{@link InputMethod_TextEditorProxy}中。
 *
 * @param proxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param handleSetSelectionFunc 表示被设置到proxy的函数{@link OH_TextEditorProxy_HandleSetSelectionFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_SetHandleSetSelectionFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_HandleSetSelectionFunc handleSetSelectionFunc);
/**
 * @brief 将函数{@link OH_TextEditorProxy_HandleExtendActionFunc}设置到{@link InputMethod_TextEditorProxy}中。
 *
 * @param proxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param handleExtendActionFunc 表示被设置到proxy的函数{@link OH_TextEditorProxy_HandleExtendActionFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_SetHandleExtendActionFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_HandleExtendActionFunc handleExtendActionFunc);
/**
 * @brief 将函数{@link OH_TextEditorProxy_GetLeftTextOfCursorFunc}设置到{@link InputMethod_TextEditorProxy}中。
 *
 * @param proxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param getLeftTextOfCursorFunc 表示被设置到proxy的函数{@link OH_TextEditorProxy_GetLeftTextOfCursorFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_SetGetLeftTextOfCursorFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_GetLeftTextOfCursorFunc getLeftTextOfCursorFunc);
/**
 * @brief 将函数{@link OH_TextEditorProxy_GetRightTextOfCursorFunc}设置到{@link InputMethod_TextEditorProxy}中。
 *
 * @param proxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param getRightTextOfCursorFunc 表示被设置到proxy的函数{@link OH_TextEditorProxy_GetRightTextOfCursorFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_SetGetRightTextOfCursorFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_GetRightTextOfCursorFunc getRightTextOfCursorFunc);
/**
 * @brief 将函数{@link OH_TextEditorProxy_GetTextIndexAtCursorFunc}设置到{@link InputMethod_TextEditorProxy}中。
 *
 * @param proxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param getTextIndexAtCursorFunc 表示被设置到proxy的函数{@link OH_TextEditorProxy_GetTextIndexAtCursorFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_SetGetTextIndexAtCursorFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_GetTextIndexAtCursorFunc getTextIndexAtCursorFunc);
/**
 * @brief 将函数{@link OH_TextEditorProxy_ReceivePrivateCommandFunc}设置到{@link InputMethod_TextEditorProxy}中。
 *
 * @param proxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param receivePrivateCommandFunc 表示被设置到proxy的函数{@link OH_TextEditorProxy_ReceivePrivateCommandFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_SetReceivePrivateCommandFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_ReceivePrivateCommandFunc receivePrivateCommandFunc);
/**
 * @brief 将函数{@link OH_TextEditorProxy_SetPreviewTextFunc}设置到{@link InputMethod_TextEditorProxy}中。
 *
 * @param proxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param setPreviewTextFunc 表示被设置到proxy的函数{@link OH_TextEditorProxy_SetPreviewTextFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_SetSetPreviewTextFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_SetPreviewTextFunc setPreviewTextFunc);
/**
 * @brief 将函数{@link OH_TextEditorProxy_FinishTextPreviewFunc}设置到{@link InputMethod_TextEditorProxy}中。
 *
 * @param proxy 指向即将被设置的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param finishTextPreviewFunc 表示被设置到proxy的函数{@link OH_TextEditorProxy_FinishTextPreviewFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_SetFinishTextPreviewFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_FinishTextPreviewFunc finishTextPreviewFunc);

/**
 * @brief 从{@link InputMethod_TextEditorProxy}中获取{@link OH_TextEditorProxy_GetTextConfigFunc}函数。
 *
 * @param proxy 指向被读取的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param getTextConfigFunc 表示从proxy获取到的函数{@link OH_TextEditorProxy_GetTextConfigFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_GetGetTextConfigFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_GetTextConfigFunc *getTextConfigFunc);
/**
 * @brief 从{@link InputMethod_TextEditorProxy}中获取{@link OH_TextEditorProxy_InsertTextFunc}函数。
 *
 * @param proxy 指向被读取的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param insertTextFunc 表示从proxy获取到的函数{@link OH_TextEditorProxy_InsertTextFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_GetInsertTextFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_InsertTextFunc *insertTextFunc);
/**
 * @brief 从{@link InputMethod_TextEditorProxy}中获取{@link OH_TextEditorProxy_DeleteForwardFunc}函数。
 *
 * @param proxy 指向被读取的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param deleteForwardFunc 表示从proxy获取到的函数{@link OH_TextEditorProxy_DeleteForwardFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_GetDeleteForwardFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_DeleteForwardFunc *deleteForwardFunc);
/**
 * @brief 从{@link InputMethod_TextEditorProxy}中获取{@link OH_TextEditorProxy_DeleteBackwardFunc}函数。
 *
 * @param proxy 指向被读取的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param deleteBackwardFunc 表示从proxy获取到的函数{@link OH_TextEditorProxy_DeleteBackwardFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_GetDeleteBackwardFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_DeleteBackwardFunc *deleteBackwardFunc);
/**
 * @brief 从{@link InputMethod_TextEditorProxy}中获取{@link OH_TextEditorProxy_SendKeyboardStatusFunc}函数。
 *
 * @param proxy 指向被读取的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param sendKeyboardStatusFunc 表示从proxy获取到的函数{@link OH_TextEditorProxy_SendKeyboardStatusFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_GetSendKeyboardStatusFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_SendKeyboardStatusFunc *sendKeyboardStatusFunc);
/**
 * @brief 从{@link InputMethod_TextEditorProxy}中获取{@link OH_TextEditorProxy_SendEnterKeyFunc}函数。
 *
 * @param proxy 指向被读取的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param sendEnterKeyFunc 表示从proxy获取到的函数{@link OH_TextEditorProxy_SendEnterKeyFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_GetSendEnterKeyFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_SendEnterKeyFunc *sendEnterKeyFunc);
/**
 * @brief 从{@link InputMethod_TextEditorProxy}中获取{@link OH_TextEditorProxy_MoveCursorFunc}函数。
 *
 * @param proxy 指向被读取的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param moveCursorFunc 表示从proxy获取到的函数{@link OH_TextEditorProxy_MoveCursorFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_GetMoveCursorFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_MoveCursorFunc *moveCursorFunc);
/**
 * @brief 从{@link InputMethod_TextEditorProxy}中获取{@link OH_TextEditorProxy_HandleSetSelectionFunc}函数。
 *
 * @param proxy 指向被读取的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param handleSetSelectionFunc 表示从proxy获取到的函数{@link OH_TextEditorProxy_HandleSetSelectionFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_GetHandleSetSelectionFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_HandleSetSelectionFunc *handleSetSelectionFunc);
/**
 * @brief 从{@link InputMethod_TextEditorProxy}中获取{@link OH_TextEditorProxy_HandleExtendActionFunc}函数。
 *
 * @param proxy 指向被读取的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param handleExtendActionFunc 表示从proxy获取到的函数{@link OH_TextEditorProxy_HandleExtendActionFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_GetHandleExtendActionFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_HandleExtendActionFunc *handleExtendActionFunc);
/**
 * @brief 从{@link InputMethod_TextEditorProxy}中获取{@link OH_TextEditorProxy_GetLeftTextOfCursorFunc}函数。
 *
 * @param proxy 指向被读取的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param getLeftTextOfCursorFunc 表示从proxy获取到的函数{@link OH_TextEditorProxy_GetLeftTextOfCursorFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_GetGetLeftTextOfCursorFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_GetLeftTextOfCursorFunc *getLeftTextOfCursorFunc);
/**
 * @brief 从{@link InputMethod_TextEditorProxy}中获取{@link OH_TextEditorProxy_GetRightTextOfCursorFunc}函数。
 *
 * @param proxy 指向被读取的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param getRightTextOfCursorFunc 表示从proxy获取到的函数{@link OH_TextEditorProxy_GetRightTextOfCursorFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_GetGetRightTextOfCursorFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_GetRightTextOfCursorFunc *getRightTextOfCursorFunc);
/**
 * @brief 从{@link InputMethod_TextEditorProxy}中获取{@link OH_TextEditorProxy_GetTextIndexAtCursorFunc}函数。
 *
 * @param proxy 指向被读取的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param getTextIndexAtCursorFunc 表示从proxy获取到的函数{@link OH_TextEditorProxy_GetTextIndexAtCursorFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_GetGetTextIndexAtCursorFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_GetTextIndexAtCursorFunc *getTextIndexAtCursorFunc);
/**
 * @brief 从{@link InputMethod_TextEditorProxy}中获取{@link OH_TextEditorProxy_ReceivePrivateCommandFunc}函数。
 *
 * @param proxy 指向被读取的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param receivePrivateCommandFunc 表示从proxy获取到的函数{@link OH_TextEditorProxy_ReceivePrivateCommandFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_GetReceivePrivateCommandFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_ReceivePrivateCommandFunc *receivePrivateCommandFunc);
/**
 * @brief 从{@link InputMethod_TextEditorProxy}中获取{@link OH_TextEditorProxy_SetPreviewTextFunc}函数。
 *
 * @param proxy 指向被读取的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param setPreviewTextFunc 表示从proxy获取到的函数{@link OH_TextEditorProxy_SetPreviewTextFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_GetSetPreviewTextFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_SetPreviewTextFunc *setPreviewTextFunc);
/**
 * @brief 从{@link InputMethod_TextEditorProxy}中获取{@link OH_TextEditorProxy_FinishTextPreviewFunc}函数。
 *
 * @param proxy 指向被读取的{@link InputMethod_TextEditorProxy}实例的指针。
 * @param finishTextPreviewFunc 表示从proxy获取到的函数{@link OH_TextEditorProxy_FinishTextPreviewFunc}。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考 {@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_TextEditorProxy_GetFinishTextPreviewFunc(
    InputMethod_TextEditorProxy *proxy, OH_TextEditorProxy_FinishTextPreviewFunc *finishTextPreviewFunc);
#ifdef __cplusplus
}
#endif /* __cplusplus */
/** @} */
#endif // OHOS_INPUTMETHOD_TEXT_EDITOR_PROXY_CAP_H