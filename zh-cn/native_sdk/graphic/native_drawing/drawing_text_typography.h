/*
 * Copyright (c) 2021-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_TEXT_TYPOGRAPHY_H
#define C_INCLUDE_DRAWING_TEXT_TYPOGRAPHY_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_text_typography.h
 *
 * @brief 定义绘制模块中排版相关的函数。
 *
 * 引用文件"native_drawing/drawing_text_typography.h"
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_canvas.h"
#include "drawing_color.h"
#include "drawing_font.h"
#include "drawing_text_declaration.h"
#include "drawing_types.h"

#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 文字方向
 */
enum OH_Drawing_TextDirection {
    /** 方向：从右到左 */
    TEXT_DIRECTION_RTL,
    /** 方向：从左到右 */
    TEXT_DIRECTION_LTR,
};

/**
 * @brief 文字对齐方式
 */
enum OH_Drawing_TextAlign {
    /** 左对齐 */
    TEXT_ALIGN_LEFT,
    /** 右对齐 */
    TEXT_ALIGN_RIGHT,
    /** 居中对齐 */
    TEXT_ALIGN_CENTER,
    /**
     * 两端对齐，即紧靠左和右边缘，中间单词空隙由空格填充，最后一行除外。
     */
    TEXT_ALIGN_JUSTIFY,
    /**
     * 当OH_Drawing_TextDirection是TEXT_DIRECTION_LTR时，
     * TEXT_ALIGN_START和TEXT_ALIGN_LEFT相同；
     * 类似地，当OH_Drawing_TextDirection是TEXT_DIRECTION_RTL时，
     * TEXT_ALIGN_START和TEXT_ALIGN_RIGHT相同。
     */
    TEXT_ALIGN_START,
    /**
     * 当OH_Drawing_TextDirection是TEXT_DIRECTION_LTR时，
     * TEXT_ALIGN_END和TEXT_ALIGN_RIGHT相同；
     * 类似地，当OH_Drawing_TextDirection是TEXT_DIRECTION_RTL时，
     * TEXT_ALIGN_END和TEXT_ALIGN_LEFT相同。
     */
    TEXT_ALIGN_END,
};

/**
 * @brief 字重
 */
enum OH_Drawing_FontWeight {
    /** 字重为thin */
    FONT_WEIGHT_100,
    /** 字重为extra-light */
    FONT_WEIGHT_200,
    /** 字重为light */
    FONT_WEIGHT_300,
    /** 字重为normal/regular */
    FONT_WEIGHT_400,
    /** 字重为medium */
    FONT_WEIGHT_500,
    /** 字重为semi-bold */
    FONT_WEIGHT_600,
    /** 字重为bold */
    FONT_WEIGHT_700,
    /** 字重为extra-bold */
    FONT_WEIGHT_800,
    /** 字重为black */
    FONT_WEIGHT_900,
};

/**
 * @brief 基线位置
 */
enum OH_Drawing_TextBaseline {
    /** 用于表音文字，基线在中间偏下的位置 */
    TEXT_BASELINE_ALPHABETIC,
    /** 用于表意文字，基线位于底部 */
    TEXT_BASELINE_IDEOGRAPHIC,
};

/**
 * @brief 文本装饰
 */
enum OH_Drawing_TextDecoration {
    /** 无装饰 */
    TEXT_DECORATION_NONE = 0x0,
    /** 下划线 */
    TEXT_DECORATION_UNDERLINE = 0x1,
    /** 上划线 */
    TEXT_DECORATION_OVERLINE = 0x2,
    /** 删除线 */
    TEXT_DECORATION_LINE_THROUGH = 0x4,
};

/**
 * @brief 区分字体是否为斜体
 */
enum OH_Drawing_FontStyle {
    /** 非斜体 */
    FONT_STYLE_NORMAL,
    /** 斜体 */
    FONT_STYLE_ITALIC,
    /**
     * @brief 倾斜字体
     *
     * @since 12
     */
    FONT_STYLE_OBLIQUE,
};

/**
 * @brief 占位符垂直对齐枚举
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_PlaceholderVerticalAlignment {
    /** 偏移于基线对齐 */
    ALIGNMENT_OFFSET_AT_BASELINE,
    /** 高于基线对齐 */
    ALIGNMENT_ABOVE_BASELINE,
    /** 低于基线对齐 */
    ALIGNMENT_BELOW_BASELINE,
    /** 行框顶部对齐 */
    ALIGNMENT_TOP_OF_ROW_BOX,
    /** 行框底部对齐 */
    ALIGNMENT_BOTTOM_OF_ROW_BOX,
    /** 行框中心对齐 */
    ALIGNMENT_CENTER_OF_ROW_BOX,
} OH_Drawing_PlaceholderVerticalAlignment;

/**
 * @brief 用于描述位占位符跨度的结构体
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_PlaceholderSpan {
    /** 占位符宽度 */
    double width;
    /** 占位符高度 */
    double height;
    /** 占位符对齐方式 */
    OH_Drawing_PlaceholderVerticalAlignment alignment;
    /** 占位符基线 */
    OH_Drawing_TextBaseline baseline;
    /** 占位符基线偏移 */
    double baselineOffset;
} OH_Drawing_PlaceholderSpan;

/**
 * @brief 文本装饰样式枚举
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_TextDecorationStyle {
    /** 实心样式 */
    TEXT_DECORATION_STYLE_SOLID,
    /** 双重样式 */
    TEXT_DECORATION_STYLE_DOUBLE,
    /** 圆点样式 */
    TEXT_DECORATION_STYLE_DOTTED,
    /** 虚线样式 */
    TEXT_DECORATION_STYLE_DASHED,
    /** 波浪样式 */
    TEXT_DECORATION_STYLE_WAVY,
} OH_Drawing_TextDecorationStyle;

/**
 * @brief 省略号样式枚举
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_EllipsisModal {
    /** 头部模式，即省略号放在文本头部 */
    ELLIPSIS_MODAL_HEAD = 0,
    /** 中部模式，即省略号放在文本中部 */
    ELLIPSIS_MODAL_MIDDLE = 1,
    /** 尾部模式，即省略号放在文本尾部 */
    ELLIPSIS_MODAL_TAIL = 2,
} OH_Drawing_EllipsisModal;

/**
 * @brief 文本的中断策略枚举
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_BreakStrategy {
    /** 贪心策略，换行时尽可能填满每一行 */
    BREAK_STRATEGY_GREEDY = 0,
    /** 高质量策略，换行时优先考虑文本的连续性 */
    BREAK_STRATEGY_HIGH_QUALITY = 1,
    /** 平衡策略，换行时在单词边界换行 */
    BREAK_STRATEGY_BALANCED = 2,
} OH_Drawing_BreakStrategy;

/**
 * @brief 单词的断词方式枚举
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_WordBreakType {
    /** 常规方式 */
    WORD_BREAK_TYPE_NORMAL = 0,
    /** 全部中断方式 */
    WORD_BREAK_TYPE_BREAK_ALL = 1,
    /** 单词中断方式 */
    WORD_BREAK_TYPE_BREAK_WORD = 2,
} OH_Drawing_WordBreakType;

/**
 * @brief 矩形框高度样式枚举
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_RectHeightStyle {
    /** 紧密样式 */
    RECT_HEIGHT_STYLE_TIGHT,
    /** 最大样式 */
    RECT_HEIGHT_STYLE_MAX,
    /** 包含行间距中间样式 */
    RECT_HEIGHT_STYLE_INCLUDELINESPACEMIDDLE,
    /** 包含行间距顶部样式 */
    RECT_HEIGHT_STYLE_INCLUDELINESPACETOP,
    /** 包含行间距底部样式 */
    RECT_HEIGHT_STYLE_INCLUDELINESPACEBOTTOM,
    /** 结构样式 */
    RECT_HEIGHT_STYLE_STRUCT,
} OH_Drawing_RectHeightStyle;

/**
 * @brief 矩形框宽度样式枚举
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_RectWidthStyle {
    /** 紧密样式 */
    RECT_WIDTH_STYLE_TIGHT,
    /** 最大样式 */
    RECT_WIDTH_STYLE_MAX,
} OH_Drawing_RectWidthStyle;

/**
* @brief 获取系统字体配置信息列表结果枚举。
*
* @since 12
* @version 1.0
*/
enum OH_Drawing_FontConfigInfoErrorCode {
   /** 获取系统字体配置信息列表成功 */
   SUCCESS_FONT_CONFIG_INFO = 0,
   /** 未知错误 */
   ERROR_FONT_CONFIG_INFO_UNKNOWN = 1,
   /** 解析系统配置文件失败 */
   ERROR_FONT_CONFIG_INFO_PARSE_FILE = 2,
   /** 申请内存失败 */
   ERROR_FONT_CONFIG_INFO_ALLOC_MEMORY = 3,
   /** 拷贝字符串数据失败 */
   ERROR_FONT_CONFIG_INFO_COPY_STRING_DATA = 4,
};

/**
 * @brief 描述系统字体详细信息的结构体。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_FontDescriptor {
    /** 系统字体的文件路径 */
    char* path;
    /** 唯一标识字体的名称 */
    char* postScriptName;
    /** 系统字体的名称 */
    char* fullName;
    /** 系统字体的字体家族 */
    char* fontFamily;
    /** 系统字体的子字体家族 */
    char* fontSubfamily;
    /** 系统字体的粗细程度 */
    int weight;
    /** 系统字体的宽窄风格属性 */
    int width;
    /** 系统字体倾斜度 */
    int italic;
    /** 系统字体是否紧凑，true表示字体紧凑，false表示字体非紧凑 */
    bool monoSpace;
    /** 系统字体是否支持符号字体，true表示支持符号字体，false表示不支持符号字体 */
    bool symbolic;
} OH_Drawing_FontDescriptor;

/**
 * @brief 文字行位置信息。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_LineMetrics {
    /** 文字相对于基线以上的高度 */
    double ascender;
    /** 文字相对于基线以下的高度 */
    double descender;
    /** 大写字母的高度 */
    double capHeight;
    /** 小写字母的高度 */
    double xHeight;
    /** 文字宽度 */
    double width;
    /** 行高 */
    double height;
    /** 文字左端到容器左端距离，左对齐为0，右对齐为容器宽度减去行文字宽度 */
    double x;
    /** 文字上端到容器上端高度，第一行为0，第二行为第一行高度 */
    double y;
    /** 行起始位置字符索引 */
    size_t startIndex;
    /** 行结束位置字符索引 */
    size_t endIndex;
    /** 第一个字的度量信息 */
    OH_Drawing_Font_Metrics firstCharMetrics;
} OH_Drawing_LineMetrics;

/**
 * @brief 备用字体信息结构体。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_FontFallbackInfo {
    /** 字体集所支持的语言类型，语言格式为bcp47 */
    char* language;
    /** 字体家族名 */
    char* familyName;
} OH_Drawing_FontFallbackInfo;

/**
 * @brief 备用字体集信息结构体。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_FontFallbackGroup {
    /** 备用字体集所对应的字体集名称，如果值为空，表示可以使用备用字体集列表集所有的字体 */
    char* groupName;
    /** 备用字体集数量 */
    size_t fallbackInfoSize;
    /** 备用字体字体集列表*/
    OH_Drawing_FontFallbackInfo* fallbackInfoSet;
} OH_Drawing_FontFallbackGroup;

/**
 * @brief 字重映射信息结构体。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_FontAdjustInfo {
    /** 字体原本的字重值 */
    int weight;
    /** 字体在应用中显示的字重值 */
    int to;
} OH_Drawing_FontAdjustInfo;

/**
 * @brief 别名字体信息结构体。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_FontAliasInfo {
    /** 字体家族名 */
    char* familyName;
    /** 字体字重值，当字重值大于0时，表示此字体集只包含所指定weight的字体，当字重值等于0时，表示此字体集包含所有字体 */
    int weight;
} OH_Drawing_FontAliasInfo;

/**
 * @brief 系统所支持的通用字体集信息结构体。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_FontGenericInfo {
    /** 字体家族名 */
    char* familyName;
    /** 别名字体列表的数量 */
    size_t aliasInfoSize;
    /** 字重映射列表的数量 */
    size_t adjustInfoSize;
    /** 别名字体列表 */
    OH_Drawing_FontAliasInfo* aliasInfoSet;
    /** 字重映射列表 */
    OH_Drawing_FontAdjustInfo* adjustInfoSet;
} OH_Drawing_FontGenericInfo;

/**
 * @brief 系统字体配置信息结构体。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_FontConfigInfo {
    /** 系统字体文件路径数量 */
    size_t fontDirSize;
    /** 通用字体集列表数量 */
    size_t fontGenericInfoSize;
    /** 备用字体集列表数量 */
    size_t fallbackGroupSize;
    /** 系统字体文件路径列表 */
    char** fontDirSet;
    /** 通用字体集列表 */
    OH_Drawing_FontGenericInfo* fontGenericInfoSet;
    /** 备用字体集列表 */
    OH_Drawing_FontFallbackGroup* fallbackGroupSet;
} OH_Drawing_FontConfigInfo;

/**
 * @brief 字体宽度的枚举。
 *
 * @since 12
 * @version 1.0
 */
enum OH_Drawing_FontWidth {
    /* 表示超窄的字宽 */
    ULTRA_CONDENSED_WIDTH = 1,
    /* 表示特窄的字宽 */
    EXTRA_CONDENSED_WIDTH = 2,
    /* 表示窄的字宽 */
    CONDENSED_WIDTH = 3,
    /* 表示半窄的字宽 */
    SEMI_CONDENSED_WIDTH = 4,
    /* 表示常规的字宽 */
    NORMAL_WIDTH = 5,
    /* 表示半宽的字宽 */
    SEMI_EXPANDED_WIDTH = 6,
    /* 表示宽的字宽 */
    EXPANDED_WIDTH = 7,
    /* 表示特宽的字宽 */
    EXTRA_EXPANDED_WIDTH = 8,
    /* 表示超宽的字宽 */
    ULTRA_EXPANDED_WIDTH = 9,
};

/**
 * @brief 定义字体样式信息的结构体。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_FontStyleStruct {
    /** 字体字重 */
    OH_Drawing_FontWeight weight;
    /** 字体字宽 */
    OH_Drawing_FontWidth width;
    /** 字体斜体 */
    OH_Drawing_FontStyle slant;
} OH_Drawing_FontStyleStruct;

/**
 * @brief 描述文本字体特征结构体。
 *
 * @since 12
 * @version 1.0
 */
typedef struct {
    /** 字体特征的描述 */
    char* tag;
    /** 字体特征的值 */
    int value;
} OH_Drawing_FontFeature;

/**
 * @brief 文本高度修饰符模式枚举。
 *
 * @since 12
 * @version 1.0
 */
enum OH_Drawing_TextHeightBehavior {
    /** 高度修饰符设置为段落中第一行和最后一行都上升 */
    TEXT_HEIGHT_ALL = 0x0,
    /** 高度修饰符设置为禁止段落中第一行上升 */
    TEXT_HEIGHT_DISABLE_FIRST_ASCENT = 0x1,
     /** 高度修饰符设置为禁止段落中最后一行上升 */
    TEXT_HEIGHT_DISABLE_LAST_ASCENT = 0x2,
      /** 高度修饰符设置为段落中第一行和最后一行都不上升 */
    TEXT_HEIGHT_DISABLE_ALL = 0x1 | 0x2,
};

/**
 * @brief 文本样式类型枚举。
 *
 * @since 12
 * @version 1.0
 */
enum OH_Drawing_TextStyleType {
    /** 无文本样式 */
    TEXT_STYLE_NONE,
    /** 所有文本样式 */
    TEXT_STYLE_ALL_ATTRIBUTES,
    /** 字体样式 */
    TEXT_STYLE_FONT,
    /** 文本前景样式 */
    TEXT_STYLE_FOREGROUND,
    /** 文本背景样式 */
    TEXT_STYLE_BACKGROUND,
    /** 文本阴影样式 */
    TEXT_STYLE_SHADOW,
    /** 文本装饰样式 */
    TEXT_STYLE_DECORATIONS,
    /** 文本字符间距样式 */
    TEXT_STYLE_LETTER_SPACING,
    /** 文本单词间距样式 */
    TEXT_STYLE_WORD_SPACING
};

/**
 * @brief 用于描述支柱样式的结构体。支柱样式用于控制绘制文本时行之间的间距、基线对齐方式以及其他与行高相关的属性。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_StrutStyle {
    /** 计算支柱时使用的字体粗细 */
    OH_Drawing_FontWeight weight;
    /** 计算支柱时使用的字体格式 */
    OH_Drawing_FontStyle style;
    /** 逻辑像素中的上升加下降的大小 */
    double size;
    /** 行高 */
    double heightScale;
    /** 是否启用高度覆盖，true表示启用，false表示不启用 */
    bool heightOverride;
    /** 半行距是否启用，true表示启用，false表示不启用 */
    bool halfLeading;
    /** 以自定义行距应用于支柱的行距 */
    double leading;
    /** 是否所有行都将使用支柱的高度，true表示使用，false表示不使用 */
    bool forceStrutHeight;
    /** 字体家族的数量 */
    size_t familiesSize;
    /** 计算支柱时使用的字体名称  */
    char** families;
} OH_Drawing_StrutStyle;

/**
 * @brief 创建指向OH_Drawing_TypographyStyle对象的指针。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 指向创建的OH_Drawing_TypographyStyle对象的指针。
 * @since 8
 * @version 1.0
 */
OH_Drawing_TypographyStyle* OH_Drawing_CreateTypographyStyle(void);

/**
 * @brief 释放被OH_Drawing_TypographyStyle对象占据的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTypographyStyle(OH_Drawing_TypographyStyle*);

/**
 * @brief 设置文本方向。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置文本方向，设置1为从左到右，设置0或其它为从右到左，具体可见{@link OH_Drawing_TextDirection}枚举。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextDirection(OH_Drawing_TypographyStyle*, int /* OH_Drawing_TextDirection */);

/**
 * @brief 设置文本对齐方式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置文本对齐方式，设置1为右对齐，设置2为居中对齐，设置3为两端对齐，设置4为与文字方向相同，设置5为文字方向相反，设置0或其它为左对齐，具体可见{@link OH_Drawing_TextAlign}枚举。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextAlign(OH_Drawing_TypographyStyle*, int /* OH_Drawing_TextAlign */);

/**
 * @brief 获取文字对齐方式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回文字对齐方式。
 * @since 12
 * @version 1.0
 */
int OH_Drawing_TypographyGetEffectiveAlignment(OH_Drawing_TypographyStyle* style);

/**
 * @brief 设置文本最大行数。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 最大行数。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextMaxLines(OH_Drawing_TypographyStyle*, int /* maxLines */);

/**
 * @brief 创建指向OH_Drawing_TextStyle对象的指针。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 指向创建的OH_Drawing_TextStyle对象的指针。
 * @since 8
 * @version 1.0
 */
OH_Drawing_TextStyle* OH_Drawing_CreateTextStyle(void);

/**
 * @brief 获取字体风格。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回指向字体风格对象{@link OH_Drawing_TextStyle}的指针。
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextStyle* OH_Drawing_TypographyGetTextStyle(OH_Drawing_TypographyStyle* style);

/**
 * @brief 释放被OH_Drawing_TextStyle对象占据的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTextStyle(OH_Drawing_TextStyle*);

/**
 * @brief 设置文本颜色。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param uint32_t 颜色。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleColor(OH_Drawing_TextStyle*, uint32_t /* color */);

/**
 * @brief 设置字号。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param double 字号。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontSize(OH_Drawing_TextStyle*, double /* fontSize */);

/**
 * @brief 设置字重。目前只有系统默认字体支持字重的调节，其他字体设置字重值小于semi-bold时字体粗细无变化，当设置字重值大于等于semi-bold时可能会触发伪加粗效果。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 设置字重，设置0字重为thin，设置1字重为extra-light，设置2字重为light，设置4字重为medium，设置5字重为semi-bold，
 * 设置6字重为bold，设置7字重为extra-bold，设置8字重为black，设置3或其它字重为normal/regular，具体可见{@link OH_Drawing_FontWeight}枚举。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontWeight(OH_Drawing_TextStyle*, int /* OH_Drawing_FontWeight */);

/**
 * @brief 设置字体基线位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 设置字体基线位置，设置1基线位于底部，设置0或其它基线在中间偏下的位置，具体可见{@link OH_Drawing_TextBaseline}枚举。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleBaseLine(OH_Drawing_TextStyle*, int /* OH_Drawing_TextBaseline */);

/**
 * @brief 设置装饰。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 设置装饰，设置1为下划线，设置2为上划线，设置4为删除线，设置0或其它为无装饰，具体可见{@link OH_Drawing_TextDecoration}枚举。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecoration(OH_Drawing_TextStyle*, int /* OH_Drawing_TextDecoration */);

/**
 * @brief 新增指定装饰，可同时显示多种装饰线。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 要新增的装饰。设置1为新增下划线，设置2为新增上划线，设置4为新增删除线。可通过位或操作一次新增多种装饰线，设置非{@link OH_Drawing_TextDecoration}枚举的装饰样式则保持原有装饰。
 * @since 14
 * @version 1.0
 */
void OH_Drawing_AddTextStyleDecoration(OH_Drawing_TextStyle*, int /* OH_Drawing_TextDecoration */);

/**
 * @brief 删除指定装饰。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 要删除的装饰。应该与现有的装饰相匹配，设置1为删除下划线，设置2为删除上划线，设置4为删除删除线，可通过位或操作一次删除多种装饰线。
 * 设置非{@link OH_Drawing_TextDecoration}枚举的装饰样式则保持原有装饰。
 * @since 14
 * @version 1.0
 */
void OH_Drawing_RemoveTextStyleDecoration(OH_Drawing_TextStyle*, int /* OH_Drawing_TextDecoration */);

/**
 * @brief 设置装饰颜色。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param uint32_t 颜色。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecorationColor(OH_Drawing_TextStyle*, uint32_t /* color */);

/**
 * @brief 设置行高，按当前字体大小的倍数进行设置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param double 当前字体大小的倍数。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontHeight(OH_Drawing_TextStyle*, double /* fontHeight */);

/**
 * @brief 设置字体类型。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 字体名称数量。
 * @param char 指向字体类型的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontFamilies(OH_Drawing_TextStyle*,
    int /* fontFamiliesNumber */, const char* fontFamilies[]);

/**
 * @brief 设置字体风格。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 设置字体风格，设置1为斜体，设置0或其它为非斜体，具体可见{@link OH_Drawing_FontStyle}枚举。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontStyle(OH_Drawing_TextStyle*, int /* OH_Drawing_FontStyle */);

/**
 * @brief 设置文本语言类型。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param char 语言类型，数据类型为指向char的指针，如'en'代表英文，'zh-Hans'代表简体中文，'zh-Hant'代表繁体中文。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleLocale(OH_Drawing_TextStyle*, const char*);

/**
 * @brief 设置前景色画刷。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格{@link OH_Drawing_TextStyle}对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Brush 指向画刷{@link OH_Drawing_Brush}对象的指针，由{@link OH_Drawing_BrushCreate}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTextStyleForegroundBrush(OH_Drawing_TextStyle*, OH_Drawing_Brush*);

/**
 * @brief 返回设置的前景色画刷。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格{@link OH_Drawing_TextStyle}对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Brush 指向画刷{@link OH_Drawing_Brush}对象的指针，由{@link OH_Drawing_BrushCreate}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleGetForegroundBrush(OH_Drawing_TextStyle*, OH_Drawing_Brush*);

/**
 * @brief 设置前景色画笔。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格{@link OH_Drawing_TextStyle}对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Pen 指向画笔{@link OH_Drawing_Pen}对象的指针，由{@link OH_Drawing_PenCreate}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTextStyleForegroundPen(OH_Drawing_TextStyle*, OH_Drawing_Pen*);

/**
 * @brief 返回设置的前景色画笔。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格{@link OH_Drawing_TextStyle}对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Pen 指向画笔{@link OH_Drawing_Pen}对象的指针，由{@link OH_Drawing_PenCreate}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleGetForegroundPen(OH_Drawing_TextStyle*, OH_Drawing_Pen*);

/**
 * @brief 设置背景色画刷。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格{@link OH_Drawing_TextStyle}对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Brush 指向画刷{@link OH_Drawing_Brush}对象的指针，由{@link OH_Drawing_BrushCreate}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTextStyleBackgroundBrush(OH_Drawing_TextStyle*, OH_Drawing_Brush*);

/**
 * @brief 返回设置的背景色画刷。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格{@link OH_Drawing_TextStyle}对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Brush 指向画刷{@link OH_Drawing_Brush}对象的指针，由{@link OH_Drawing_BrushCreate}获取。
 * @since 12
 * @version 1.0
 */
 void OH_Drawing_TextStyleGetBackgroundBrush(OH_Drawing_TextStyle*, OH_Drawing_Brush*);

/**
 * @brief 设置背景色画笔。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格{@link OH_Drawing_TextStyle}对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Pen 指向画笔{@link OH_Drawing_Pen}对象的指针，由{@link OH_Drawing_PenCreate}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTextStyleBackgroundPen(OH_Drawing_TextStyle*, OH_Drawing_Pen*);

/**
 * @brief 返回设置的背景色画笔。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格{@link OH_Drawing_TextStyle}对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Pen 指向画笔{@link OH_Drawing_Pen}对象的指针，由{@link OH_Drawing_PenCreate}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleGetBackgroundPen(OH_Drawing_TextStyle*, OH_Drawing_Pen*);

/**
 * @brief 创建指向OH_Drawing_TypographyCreate对象的指针。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param OH_Drawing_FontCollection 指向OH_Drawing_FontCollection的指针，由{@link OH_Drawing_CreateFontCollection}获取。
 * @return 指向新创建的OH_Drawing_TypographyCreate对象的指针。
 * @since 8
 * @version 1.0
 */
OH_Drawing_TypographyCreate* OH_Drawing_CreateTypographyHandler(OH_Drawing_TypographyStyle*,
    OH_Drawing_FontCollection*);

/**
 * @brief 释放被OH_Drawing_TypographyCreate对象占据的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针，由{@link OH_Drawing_CreateTypographyHandler}获取。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTypographyHandler(OH_Drawing_TypographyCreate*);

/**
 * @brief 设置排版风格。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针，由{@link OH_Drawing_CreateTypographyHandler}获取。
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerPushTextStyle(OH_Drawing_TypographyCreate*, OH_Drawing_TextStyle*);

/**
 * @brief 设置文本内容。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针，由{@link OH_Drawing_CreateTypographyHandler}获取。
 * @param char 指向文本内容的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerAddText(OH_Drawing_TypographyCreate*, const char*);

/**
 * @brief 排版弹出。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针，由{@link OH_Drawing_CreateTypographyHandler}获取。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerPopTextStyle(OH_Drawing_TypographyCreate*);

/**
 * @brief 创建指向OH_Drawing_Typography对象的指针。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针，由{@link OH_Drawing_CreateTypographyHandler}获取。
 * @return 指向OH_Drawing_Typography对象的指针。
 * @since 8
 * @version 1.0
 */
OH_Drawing_Typography* OH_Drawing_CreateTypography(OH_Drawing_TypographyCreate*);

/**
 * @brief 释放OH_Drawing_Typography对象占据的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTypography(OH_Drawing_Typography*);

/**
 * @brief 排版布局。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param double 文本最大宽度
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyLayout(OH_Drawing_Typography*, double /* maxWidth */);

/**
 * @brief 显示文本。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param OH_Drawing_Canvas 指向OH_Drawing_Canvas对象的指针，由OH_Drawing_CanvasCreate()获取。
 * @param double x坐标。
 * @param double y坐标。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyPaint(OH_Drawing_Typography*, OH_Drawing_Canvas*,
    double /* potisionX */, double /* potisionY */);

/**
 * @brief 沿路径绘制文本。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param OH_Drawing_Canvas 指向OH_Drawing_Canvas对象的指针，由{@link OH_Drawing_CanvasCreate}获取。
 * @param OH_Drawing_Path 指向OH_Drawing_Path对象的指针，由{@link OH_Drawing_PathCreate}获取。
 * @param double 沿路径方向偏置，从路径起点向前为正，向后为负。
 * @param double 沿路径垂直方向偏置，沿路径方向左侧为负，右侧为正。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TypographyPaintOnPath(OH_Drawing_Typography*, OH_Drawing_Canvas*, OH_Drawing_Path*,
    double /* hOffset */, double /* vOffset */);

/**
 * @brief 获取最大宽度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回最大宽度。
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetMaxWidth(OH_Drawing_Typography*);

/**
 * @brief 获取高度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回高度。
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetHeight(OH_Drawing_Typography*);

/**
 * @brief 获取最长行的宽度，建议实际使用时将返回值向上取整。当文本内容为空时，返回float的最小值，
 * 即：-340282346638528859811704183484516925440.000000。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回最长行的宽度。
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetLongestLine(OH_Drawing_Typography*);

/**
 * @brief 获取最长行的宽度（该宽度包含当前行缩进的宽度），建议实际使用时将返回值向上取整。当文本内容为空时，返回0.0。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向{@link OH_Drawing_Typography}对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回最长行的宽度（该宽度包含当前行缩进的宽度），单位：物理像素px。
 * @since 13
 * @version 1.1
 */
double OH_Drawing_TypographyGetLongestLineWithIndent(OH_Drawing_Typography*);

/**
 * @brief 获取最小固有宽度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回最小固有宽度。
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetMinIntrinsicWidth(OH_Drawing_Typography*);

/**
 * @brief 获取最大固有宽度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回最大固有宽度。
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetMaxIntrinsicWidth(OH_Drawing_Typography*);

/**
 * @brief 获取字母文字基线。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回字母文字基线。
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetAlphabeticBaseline(OH_Drawing_Typography*);

/**
 * @brief 获取表意文字基线。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回表意文字基线。
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetIdeographicBaseline(OH_Drawing_Typography*);

/**
 * @brief 设置占位符。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针，由{@link OH_Drawing_CreateTypographyHandler}获取。
 * @param OH_Drawing_PlaceholderSpan 指向OH_Drawing_PlaceholderSpan对象的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerAddPlaceholder(OH_Drawing_TypographyCreate*, OH_Drawing_PlaceholderSpan*);

/**
 * @brief 获取文本是否超过最大行。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回文本是否超过最大行，true表示超过，false表示未超过。
 * @since 11
 * @version 1.0
 */
bool OH_Drawing_TypographyDidExceedMaxLines(OH_Drawing_Typography*);

/**
 * @brief 获取指定范围内的文本框。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param size_t 设置开始位置。
 * @param size_t 设置结束位置。
 * @param OH_Drawing_RectHeightStyle 设置高度样式，支持可选的高度样式具体可见{@link OH_Drawing_RectHeightStyle}枚举。
 * @param OH_Drawing_RectWidthStyle 设置宽度样式，支持可选的宽度样式具体可见{@link OH_Drawing_RectWidthStyle}枚举。
 * @return 返回指定范围内的文本框，具体可见{@link OH_Drawing_TextBox}结构体。
 * @since 11
 * @version 1.0
 */
OH_Drawing_TextBox* OH_Drawing_TypographyGetRectsForRange(OH_Drawing_Typography*,
    size_t, size_t, OH_Drawing_RectHeightStyle, OH_Drawing_RectWidthStyle);

/**
 * @brief 获取占位符的文本框。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回占位符的文本框，返回类型为{@link OH_Drawing_TextBox}结构体。
 * @since 11
 * @version 1.0
 */
OH_Drawing_TextBox* OH_Drawing_TypographyGetRectsForPlaceholders(OH_Drawing_Typography*);

/**
 * @brief 获取文本框左侧位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox 指向OH_Drawing_TextBox对象的指针，由{@link OH_Drawing_TypographyGetRectsForRange}或
 * {@link OH_Drawing_TypographyGetRectsForPlaceholders}获取。
 * @param int 文本框的索引。
 * @return 返回文本框左侧位置。
 * @since 11
 * @version 1.0
 */
float OH_Drawing_GetLeftFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief 获取文本框右侧位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox 指向OH_Drawing_TextBox对象的指针，由{@link OH_Drawing_TypographyGetRectsForRange}或
 * {@link OH_Drawing_TypographyGetRectsForPlaceholders}获取。
 * @param int 文本框的索引。
 * @return 返回文本框右侧位置。
 * @since 11
 * @version 1.0
 */
float OH_Drawing_GetRightFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief 获取文本框顶部位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox 指向OH_Drawing_TextBox对象的指针，由{@link OH_Drawing_TypographyGetRectsForRange}或
 * {@link OH_Drawing_TypographyGetRectsForPlaceholders}获取。
 * @param int 文本框的索引。
 * @return 返回文本框顶部位置。
 * @since 11
 * @version 1.0
 */
float OH_Drawing_GetTopFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief 获取文本框底部位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox 指向OH_Drawing_TextBox对象的指针，由{@link OH_Drawing_TypographyGetRectsForRange}或
 * {@link OH_Drawing_TypographyGetRectsForPlaceholders}获取。
 * @param int 文本框的索引。
 * @return 返回文本框底部位置。
 * @since 11
 * @version 1.0
 */
float OH_Drawing_GetBottomFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief 获取文本框方向。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox 指向OH_Drawing_TextBox对象的指针，由{@link OH_Drawing_TypographyGetRectsForRange}或
 * {@link OH_Drawing_TypographyGetRectsForPlaceholders}获取。
 * @param int 文本框的索引。
 * @return 返回文本框方向。
 * @since 11
 * @version 1.0
 */
int OH_Drawing_GetTextDirectionFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief 获取文本框数量大小。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox 指向OH_Drawing_TextBox对象的指针，由{@link OH_Drawing_TypographyGetRectsForRange}或
 * {@link OH_Drawing_TypographyGetRectsForPlaceholders}获取。
 * @return 返回文本框数量大小。
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_GetSizeOfTextBox(OH_Drawing_TextBox*);

/**
 * @brief 获取坐标处文本的索引位置和亲和性。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param double 光标的x坐标。
 * @param double 光标的y坐标。
 * @return 返回坐标处字体的索引位置和亲和性，返回类型为{@link OH_Drawing_PositionAndAffinity}结构体。
 * @since 11
 * @version 1.0
 */
OH_Drawing_PositionAndAffinity* OH_Drawing_TypographyGetGlyphPositionAtCoordinate(OH_Drawing_Typography*,
    double, double);

/**
 * @brief 获取坐标处文本所属字符簇的索引位置和亲和性，字符簇指一个或多个字符组成的整体。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param double 光标的x坐标。
 * @param double 光标的y坐标。
 * @return 返回坐标处指定类型字体的索引位置和亲和性，返回类型为{@link OH_Drawing_PositionAndAffinity}结构体。
 * @since 11
 * @version 1.0
 */
OH_Drawing_PositionAndAffinity* OH_Drawing_TypographyGetGlyphPositionAtCoordinateWithCluster(OH_Drawing_Typography*,
    double, double);

/**
 * @brief 获取OH_Drawing_PositionAndAffinity对象的位置属性。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_PositionAndAffinity 指向OH_Drawing_PositionAndAffinity对象的指针，
 * 由{@link OH_Drawing_TypographyGetGlyphPositionAtCoordinate}或
 * {@link OH_Drawing_TypographyGetGlyphPositionAtCoordinateWithCluster}获取。
 * @return 返回OH_Drawing_PositionAndAffinity对象的位置属性。
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_GetPositionFromPositionAndAffinity(OH_Drawing_PositionAndAffinity*);

/**
 * @brief 获取OH_Drawing_PositionAndAffinity对象的亲和性，根据亲和性可判断字体会靠近前方文本还是后方文本。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_PositionAndAffinity 指向OH_Drawing_PositionAndAffinity对象的指针，
 * 由{@link OH_Drawing_TypographyGetGlyphPositionAtCoordinate}或
 * {@link OH_Drawing_TypographyGetGlyphPositionAtCoordinateWithCluster}获取。
 * @return 返回OH_Drawing_PositionAndAffinity对象的亲和性。
 * @since 11
 * @version 1.0
 */
int OH_Drawing_GetAffinityFromPositionAndAffinity(OH_Drawing_PositionAndAffinity*);

/**
 * @brief 获取单词的边界。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param size_t 单词索引。
 * @return 返回单词边界，返回类型为{@link OH_Drawing_Range}结构体。
 * @since 11
 * @version 1.0
 */
OH_Drawing_Range* OH_Drawing_TypographyGetWordBoundary(OH_Drawing_Typography*, size_t);

/**
 * @brief 获取OH_Drawing_Range对象开始位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Range 指向OH_Drawing_Range对象的指针，由{@link OH_Drawing_TypographyGetWordBoundary}获取。
 * @return 返回OH_Drawing_Range对象开始位置。
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_GetStartFromRange(OH_Drawing_Range*);

/**
 * @brief 获取OH_Drawing_Range对象结束位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Range 指向OH_Drawing_Range对象的指针，由{@link OH_Drawing_TypographyGetWordBoundary}获取。
 * @return 返回OH_Drawing_Range对象结束位置。
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_GetEndFromRange(OH_Drawing_Range*);

/**
 * @brief 获取文本行数。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回行数。
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_TypographyGetLineCount(OH_Drawing_Typography*);

/**
 * @brief 设置文本装饰样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 设置的文本装饰样式，支持可选的装饰样式具体可见{@link OH_Drawing_TextDecorationStyle}枚举。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecorationStyle(OH_Drawing_TextStyle*, int);

/**
 * @brief 设置文本装饰线的厚度缩放比例。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param double 缩放比例。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecorationThicknessScale(OH_Drawing_TextStyle*, double);

/**
 * @brief 设置文本的字符间距。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param double 间距大小。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleLetterSpacing(OH_Drawing_TextStyle*, double);

/**
 * @brief 设置文本的单词间距。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param double 间距大小。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleWordSpacing(OH_Drawing_TextStyle*, double);

/**
 * @brief 设置文本为一半行间距。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param bool 设置一半行间距是否生效，true表示生效，false表示不生效。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleHalfLeading(OH_Drawing_TextStyle*, bool);

/**
 * @brief 设置文本的省略号内容。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param char* 设置省略号内容，数据类型为指向char的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleEllipsis(OH_Drawing_TextStyle*, const char*);

/**
 * @brief 设置文本的省略号样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 设置省略号样式，支持可选的省略号样式具体可见{@link OH_Drawing_EllipsisModal}枚举。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleEllipsisModal(OH_Drawing_TextStyle*, int);

/**
 * @brief 设置文本的中断策略。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置中断策略，支持可选的中断策略具体可见{@link OH_Drawing_BreakStrategy}枚举。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextBreakStrategy(OH_Drawing_TypographyStyle*, int);

/**
 * @brief 设置单词的断词方式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置断词方式，支持可选的断词方式样式具体可见{@link OH_Drawing_WordBreakType}枚举。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextWordBreakType(OH_Drawing_TypographyStyle*, int);

/**
 * @brief 设置文本的省略号样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置省略号样式，支持可选的省略号样式样式具体可见{@link OH_Drawing_EllipsisModal}枚举。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextEllipsisModal(OH_Drawing_TypographyStyle*, int);

/**
 * @brief 设置省略号样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param char 省略号样式。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextEllipsis(OH_Drawing_TypographyStyle* style, const char* ellipsis);

/**
 * @brief 获取指定行的行高
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param int 要指定的行数。
 * @return 返回指定行的行高。
 * @since 11
 * @version 1.0
 */
double OH_Drawing_TypographyGetLineHeight(OH_Drawing_Typography*, int);

/**
 * @brief 获取指定行的行宽。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param int 要指定的行数。
 * @return 返回指定行的行宽。
 * @since 11
 * @version 1.0
 */
double OH_Drawing_TypographyGetLineWidth(OH_Drawing_Typography*, int);

/**
 * @brief 设置文本划分比率。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param float 文本划分比率。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextSplitRatio(OH_Drawing_TypographyStyle* style, float textSplitRatio);

/**
 * @brief 获取文本是否有最大行数限制。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回文本是否有最大行数限制，true表示有最大行数限制，false表示无最大行数限制。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyIsLineUnlimited(OH_Drawing_TypographyStyle* style);

/**
 * @brief 获取文本是否有省略号。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回文本是否有省略号，true表示有省略号，false表示无省略号。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyIsEllipsized(OH_Drawing_TypographyStyle* style);

/**
 * @brief 设置段落语言类型。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param char 语言类型，数据类型为指向char的指针，如'en'代表英文，'zh-Hans'代表简体中文，'zh-Hant'代表繁体中文。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLocale(OH_Drawing_TypographyStyle* style, const char* locale);

/**
 * @brief 获取文本字体属性。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向文本对象{@link OH_Drawing_Typography}的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Font_Metrics 指向字体属性对象{@link OH_Drawing_Font_Metrics}的指针，由{@link OH_Drawing_Font_Metrics}获取。
 * @return 是否获取到字体属性，true表示获取到字体属性，false表示未获取到字体属性。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TextStyleGetFontMetrics(OH_Drawing_Typography*, OH_Drawing_TextStyle*, OH_Drawing_Font_Metrics*);

/**
 * @brief 设置段落样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextStyle(OH_Drawing_TypographyStyle*,OH_Drawing_TextStyle*);

/**
 * @brief 构造字体描述对象，用于描述系统字体详细信息。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 返回指向已创建的字体描述对象{@link OH_Drawing_FontDescriptor}的指针。
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontDescriptor* OH_Drawing_CreateFontDescriptor(void);

/**
 * @brief 释放字体描述对象占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
* @param OH_Drawing_FontDescriptor 指向字体描述对象{@link OH_Drawing_FontDescriptor}的指针，由{@link OH_Drawing_CreateFontDescriptor}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroyFontDescriptor(OH_Drawing_FontDescriptor*);

/**
 * @brief 构造字体解析对象，用于解析系统字体。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 返回指向已创建的字体解析对象{@link OH_Drawing_FontParser}的指针。
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontParser* OH_Drawing_CreateFontParser(void);

/**
 * @brief 释放字体解析对象占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontParser 指向字体解析对象{@link OH_Drawing_FontParser}的指针，由{@link OH_Drawing_CreateFontParser}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroyFontParser(OH_Drawing_FontParser*);

/**
 * @brief 获取系统字体名称列表，此接口仅在2in1设备上可用。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontParser 指向字体解析对象{@link OH_Drawing_FontParser}的指针，由{@link OH_Drawing_CreateFontParser}获取。
 * @param size_t 返回获取到的系统字体名称数量。
 * @return 返回获取到的系统字体列表。
 * @since 12
 * @version 1.0
 */
char** OH_Drawing_FontParserGetSystemFontList(OH_Drawing_FontParser*, size_t*);

/**
 * @brief 释放系统字体名称列表占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param char** 指向系统字体名称列表的指针。
 * @param size_t* 系统字体名称列表的数量。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroySystemFontList(char**, size_t);

/**
 * @brief 根据传入的系统字体名称获取系统字体的相关信息。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontParser 指向字体解析对象{@link OH_Drawing_FontParser}的指针，由{@link OH_Drawing_CreateFontParser}获取。
 * @param char* 系统字体名。
 * @return 返回系统字体。
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontDescriptor* OH_Drawing_FontParserGetFontByName(OH_Drawing_FontParser*, const char*);

/**
 * @brief 获取行位置信息。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向文本对象{@link OH_Drawing_Typography}的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回指向行位置信息对象{@link OH_Drawing_LineMetrics}的指针。
 * @since 12
 * @version 1.0
 */
OH_Drawing_LineMetrics* OH_Drawing_TypographyGetLineMetrics(OH_Drawing_Typography*);

/**
 * @brief 获取行数量。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_LineMetrics 指向行位置信息对象{@link OH_Drawing_LineMetrics}的指针，由{@link OH_Drawing_LineMetrics}获取。
 * @return 返回行数量。
 * @since 12
 * @version 1.0
 */
size_t OH_Drawing_LineMetricsGetSize(OH_Drawing_LineMetrics*);

/**
 * @brief 释放行位置信息对象占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_LineMetrics 指向行位置信息对象{@link OH_Drawing_LineMetrics}的指针，由{@link OH_Drawing_LineMetrics}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroyLineMetrics(OH_Drawing_LineMetrics*);

/**
 * @brief 获取指定行位置信息对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向文本对象{@link OH_Drawing_Typography}的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param int 要获取的行数。
 * @param OH_Drawing_LineMetrics 指向行位置信息对象{@link OH_Drawing_LineMetrics}的指针，由{@link OH_Drawing_LineMetrics}获取。
 * @return 行位置信息对象是否成功获取，true表示成功获取，false表示未成功获取。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyGetLineMetricsAt(OH_Drawing_Typography*, int, OH_Drawing_LineMetrics*);

/**
 * @brief 获取指定行的位置信息或指定行第一个字符的位置信息。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向文本对象{@link OH_Drawing_Typography}的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param int 行号。
 * @param bool true为获取整行的位置信息，false为获取第一个字符的位置信息。
 * @param bool 文字宽度是否包含空白符，true表示不包含空白符，false表示包含空白符。
 * @param OH_Drawing_LineMetrics 指向行位置信息对象{@link OH_Drawing_LineMetrics}的指针，由{@link OH_Drawing_LineMetrics}获取。
 * @return 指定行的行位置信息或第一个字符的位置信息是否成功获取，true表示成功获取，false表示未成功获取。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyGetLineInfo(OH_Drawing_Typography*, int, bool, bool, OH_Drawing_LineMetrics*);

/**
 * @brief 设置文本排版字重。目前只有系统默认字体支持字重的调节，其他字体设置字重值小于semi-bold时字体粗细无变化，当设置字重值大于等于semi-bold时可能会触发伪加粗效果。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置字重，设置0字重为thin，设置1字重为extra-light，设置2字重为light，设置4字重为medium，设置5字重为semi-bold，
 * 设置6字重为bold，设置7字重为extra-bold，设置8字重为black，设置3或其它字重为normal/regular，具体可见{@link OH_Drawing_FontWeight}枚举。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextFontWeight(OH_Drawing_TypographyStyle*, int);

/**
 * @brief 设置字体风格。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置字体风格，设置1为斜体，设置0或其它为非斜体，具体可见{@link OH_Drawing_FontStyle}枚举。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextFontStyle(OH_Drawing_TypographyStyle*, int);

/**
 * @brief 设置字体家族的名称。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param char 字体家族的名称，数据类型为指向char的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextFontFamily(OH_Drawing_TypographyStyle*, const char*);

/**
 * @brief 设置文本排版字号。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param double 字号（大于0）。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextFontSize(OH_Drawing_TypographyStyle*, double);

/**
 * @brief 设置文本排版字体高度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param double 字体高度。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextFontHeight(OH_Drawing_TypographyStyle*, double);

/**
 * @brief 设置文本排版是否为一半行间距。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param bool 设置一半行间距是否生效，true表示生效，false表示不生效。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextHalfLeading(OH_Drawing_TypographyStyle*, bool);

/**
 * @brief 设置文本排版是否启用行样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param bool 设置行样式是否启用，true表示启用，false表示不启用。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextUseLineStyle(OH_Drawing_TypographyStyle*, bool);

/**
 * @brief 设置文本排版行样式字重。目前只有系统默认字体支持字重的调节，其他字体设置字重值小于semi-bold时字体粗细无变化，当设置字重值大于等于semi-bold时可能会触发伪加粗效果。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置字重，设置0字重为thin，设置1字重为extra-light，设置2字重为light，设置4字重为medium，设置5字重为semi-bold，
 * 设置6字重为bold，设置7字重为extra-bold，设置8字重为black，设置3或其它字重为normal/regular，具体可见{@link OH_Drawing_FontWeight}枚举。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleFontWeight(OH_Drawing_TypographyStyle*, int);

/**
 * @brief 设置文本排版行样式风格。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置字体风格，设置1为斜体，设置0或其它为非斜体，具体可见{@link OH_Drawing_FontStyle}枚举。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleFontStyle(OH_Drawing_TypographyStyle*, int);

/**
 * @brief 设置文本排版行样式字体类型。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 字体名称数量。
 * @param char 指向字体类型的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleFontFamilies(OH_Drawing_TypographyStyle*,
    int, const char* fontFamilies[]);

/**
 * @brief 设置文本排版行样式字号。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param double 字号（大于0）。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleFontSize(OH_Drawing_TypographyStyle*, double);

/**
 * @brief 设置文本排版行样式字体高度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param double 字体高度。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleFontHeight(OH_Drawing_TypographyStyle*, double);

/**
 * @brief 设置文本排版行样式是否为一半行间距。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param bool 设置一半行间距是否生效，true表示生效，false表示不生效。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleHalfLeading(OH_Drawing_TypographyStyle*, bool);

/**
 * @brief 设置文本排版行样式间距比例。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param double 间距比例。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleSpacingScale(OH_Drawing_TypographyStyle*, double);

/**
 * @brief 设置文本排版是否仅启用行样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param bool 设置仅启用行样式是否生效，true表示生效，false表示不生效。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleOnly(OH_Drawing_TypographyStyle*, bool);

/**
 * @brief 创建指向字体阴影对象的指针。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 指向创建的字体阴影对象。
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextShadow* OH_Drawing_CreateTextShadow(void);

/**
 * @brief 释放被字体阴影对象占据的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextShadow 指向字体阴影对象{@link OH_Drawing_TextShadow}的指针，由{@link OH_Drawing_CreateTextShadow}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroyTextShadow(OH_Drawing_TextShadow*);

/**
 * @brief 获取字体阴影容器。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return 返回指向字体阴影容器{@link OH_Drawing_TextShadow}的指针。
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextShadow* OH_Drawing_TextStyleGetShadows(OH_Drawing_TextStyle*);

/**
 * @brief 获取字体阴影容器的大小。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return int 返回字体阴影容器的大小。
 * @since 12
 * @version 1.0
 */
int OH_Drawing_TextStyleGetShadowCount(OH_Drawing_TextStyle*);

/**
 * @brief 字体阴影容器中添加字体阴影元素。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_TextShadow 指向字体阴影对象{@link OH_Drawing_TextShadow}的指针，由{@link OH_Drawing_CreateTextShadow}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleAddShadow(OH_Drawing_TextStyle*, const OH_Drawing_TextShadow*);

/**
 * @brief 清除字体阴影容器中的所有元素。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleClearShadows(OH_Drawing_TextStyle*);

/**
 * @brief 根据下标获取字体阴影容器中的元素。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 下标索引。
 * @return 返回指向字体阴影对象{@link OH_Drawing_TextShadow}的指针。
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextShadow* OH_Drawing_TextStyleGetShadowWithIndex(OH_Drawing_TextStyle*, int);

/**
 * @brief 设置文本的排版缩进，不调用此接口默认文本无缩进。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向文本对象{@link OH_Drawing_Typography}的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param int 为段落设置的缩进数量。该值应小于或等于 indents 数组的长度，以避免访问数组越界导致的显示异常。
 * @param float 指向浮点类型数组的指针，每个数组元素表示一个缩进宽度，单位为物理像素（px）。在应用{@link OH_Drawing_Typography}接口时，需要先声明并初始化该浮点数组。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TypographySetIndents(OH_Drawing_Typography*, int, const float indents[]);

/**
 * @brief 根据下标获取排版缩进容器中的元素。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向文本对象{@link OH_Drawing_Typography}的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param int 下标索引。
 * @return float 返回索引对应的元素值。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_TypographyGetIndentsWithIndex(OH_Drawing_Typography*, int);

/**
 * @brief 获取行的边界。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向文本对象{@link OH_Drawing_Typography}的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param int 行索引
 * @param bool 设置返回的边界是否包含空格，true为包含空格，false为不包含空格。
 * @return 返回指向行边界对象的指针{@link OH_Drawing_Range}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Range* OH_Drawing_TypographyGetLineTextRange(OH_Drawing_Typography*, int, bool);

/**
 * @brief 释放由被字体阴影对象OH_Drawing_TextShadow构成的vector占据的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextShadow 指向字体阴影对象{@link OH_Drawing_TextShadow}的指针，由{@link OH_Drawing_CreateTextShadow}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroyTextShadows(OH_Drawing_TextShadow*);

/**
 * @brief 获取系统字体配置信息。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontConfigJsonInfoCode 错误码，具体可见{@link OH_Drawing_FontConfigInfoErrorCode}枚举。
 * @return 返回系统字体配置信息的指针。
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontConfigInfo* OH_Drawing_GetSystemFontConfigInfo(OH_Drawing_FontConfigInfoErrorCode*);

/**
 * @brief 释放系统字体配置信息占用的的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontConfigInfo 指向系统字体配置信息{@link OH_Drawing_FontConfigInfo}的指针，
 * 由{@link OH_Drawing_GetSystemFontConfigInfo}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroySystemFontConfigInfo(OH_Drawing_FontConfigInfo*);

/**
 * @brief 设置字体样式，包括字体字重、字体宽度和字体斜度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_FontStyleStruct 字体样式对象，包括字体字重、字体宽度和字体斜度信息。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontStyleStruct(OH_Drawing_TextStyle* drawingTextStyle,
    OH_Drawing_FontStyleStruct fontStyle);

/**
 * @brief 获取字体样式，包括字体字重、字体宽度和字体斜度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return 返回获取到的字体样式对象，包括字体字重、字体宽度和字体斜度信息。
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontStyleStruct OH_Drawing_TextStyleGetFontStyleStruct(OH_Drawing_TextStyle* drawingTextStyle);

/**
 * @brief 设置文本字体样式，包括字体字重、字体宽度和字体斜度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param OH_Drawing_FontStyleStruct 字体样式对象，包括字体字重、字体宽度和字体斜度信息。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyStyleFontStyleStruct(OH_Drawing_TypographyStyle* drawingStyle,
    OH_Drawing_FontStyleStruct fontStyle);

/**
 * @brief 获取文本字体样式，包括字体字重、字体宽度和字体斜度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回获取到的字体样式对象，包括字体字重、字体宽度和字体斜度信息。
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontStyleStruct OH_Drawing_TypographyStyleGetFontStyleStruct(OH_Drawing_TypographyStyle* drawingStyle);

/**
 * @brief 设置文本背景矩形框和样式id。样式id仅当背景框为圆角矩形时有效。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_RectStyle_Info 指向{@link OH_Drawing_RectStyle_Info}对象的指针。
 * @param int 要设置的样式id，仅当背景框为圆角矩形时有效。文本处理时会被划分为多个分段，每个分段都有自己的TextStyle，id标识着这个分段将被绘制于第几个背景矩形框中。
 *            若一行中每个分段的id全为0，表示所有分段绘制在同一个圆角矩形背景框中；
 *            若一行中的id为0, 1，则id为0的分段绘制在一个圆角矩形背景框内，id为1的分段绘制在另一个圆角矩形背景框内，以此类推。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleSetBackgroundRect(OH_Drawing_TextStyle*, const OH_Drawing_RectStyle_Info*, int styleId);

/**
 * @brief 设置排版创建过程中的符号。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针，由{@link OH_Drawing_CreateTypographyHandler}获取。
 * @param uint32_t 要设置的符号，可支持设置的符号参见下面链接json文件中的value值。
 * https://gitee.com/openharmony/global_system_resources/blob/master/systemres/main/resources/base/element/symbol.json
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerAddSymbol(OH_Drawing_TypographyCreate*, uint32_t symbol);

/**
 * @brief 添加文本字体特征。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param char 指向字体特征键值对中关键字所标识的字符串。
 * @param int 要设置的字体特征键值对的值。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleAddFontFeature(OH_Drawing_TextStyle*, const char* tag, int value);

/**
 * @brief 添加可变字体属性。对应的字体文件（.ttf文件）需要支持可变调节，此接口才能生效。当对应的字体不支持可变调节时，此接口调用不生效。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param char* 可变字体属性键值对中的键。目前仅支持'wght'，表示字重属性。
 * @param float 设置的可变字体属性键值对的值。目前默认字体下字重属性支持的取值范围为[0,900]。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleAddFontVariation(OH_Drawing_TextStyle*, const char* /* axis */, const float /* value */);

/**
 * @brief 获取字体特征map容器中的所有内容。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return OH_Drawing_FontFeature 要获取的字体特征容器的所有内容，指向存放容器所有键值对的一个结构体数组。
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontFeature* OH_Drawing_TextStyleGetFontFeatures(OH_Drawing_TextStyle*);

/**
 * @brief 释放存放字体特征所有内容的结构体数组所占用的空间。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontFeature 指向存放容器所有键值对的结构体数组指针，由{@link OH_Drawing_TextStyleGetFontFeatures}获取。
 * @param fontFeatureSize 存放容器所有键值对的结构体数组的大小。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleDestroyFontFeatures(OH_Drawing_FontFeature*, size_t fontFeatureSize);

/**
 * @brief 获取字体特征map容器的大小。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return size_t 字体特征map容器的大小。
 * @since 12
 * @version 1.0
 */
size_t OH_Drawing_TextStyleGetFontFeatureSize(OH_Drawing_TextStyle*);

/**
 * @brief 清除字体特征map容器中的所有内容。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleClearFontFeature(OH_Drawing_TextStyle*);

/**
 * @brief 获取文本的基线漂移。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return double 基线漂移的值。
 * @since 12
 * @version 1.0
 */
double OH_Drawing_TextStyleGetBaselineShift(OH_Drawing_TextStyle*);

/**
 * @brief 设置文本的基线漂移。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param double 文本的基线漂移。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleSetBaselineShift(OH_Drawing_TextStyle*, double lineShift);

/**
 * @brief 设置文本高度修饰符模式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param heightMode 文本高度修饰符模式，为{@link OH_Drawing_TextHeightBehavior}类型的枚举值。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TypographyTextSetHeightBehavior(OH_Drawing_TypographyStyle*, OH_Drawing_TextHeightBehavior heightMode);

/**
 * @brief 获取文本高度修饰符模式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回文本高度修饰符模式，为{@link OH_Drawing_TextHeightBehavior}类型的枚举值。
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextHeightBehavior OH_Drawing_TypographyTextGetHeightBehavior(OH_Drawing_TypographyStyle*);

/**
 * @brief 从目标行获取所有字体指标。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向文本对象{@link OH_Drawing_Typography}的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param lineNumber 指定行数，取值为整数，最小有效值为1，最大行取决于文本输入后字体引擎解析出来的行数，若输入值大于最大行会返回错误值并打印错误消息。
 * @param fontMetricsSize 指示从当前行返回的字体度量结构的大小。
 * @return 返回当前行的所有字符度量。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Font_Metrics* OH_Drawing_TypographyGetLineFontMetrics(OH_Drawing_Typography*,
    size_t lineNumber, size_t* fontMetricsSize);

/**
 * @brief 释放指定行所有字体度量结构体集合所占用的所有空间。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font_Metrics 指示要释放空间的指定行所有字体度量结构体集合的第一个地址。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TypographyDestroyLineFontMetrics(OH_Drawing_Font_Metrics*);

/**
 * @brief 判断两个字体风格对象是否相等。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param style 比较的字体风格对象。
 * @param comparedStyle 比较的字体风格对象。
 * @return 返回两个字体风格对象是否相等的结果，true表示相等，false表示不相等。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TextStyleIsEqual(const OH_Drawing_TextStyle* style, const OH_Drawing_TextStyle* comparedStyle);

/**
 * @brief 判断两个字体风格对象的字体样式属性是否相等。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param style 比较的字体风格对象。
 * @param comparedStyle 比较的字体风格对象。
 * @return 返回两个字体风格对象的字体样式属性是否相等的结果。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TextStyleIsEqualByFont(const OH_Drawing_TextStyle* style, const OH_Drawing_TextStyle* comparedStyle);

/**
 * @brief 判断两个字体风格对象是否有一样的文本样式类型。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param style 比较的字体风格对象。
 * @param comparedStyle 比较的字体风格对象。
 * @param textStyleType 文本样式类型枚举{@link OH_Drawing_TextStyleType}。
 * @return 返回两个TextStyle对象是否有对应的文本样式类型一样的结果，true表示其文本样式类型一样，false表示不一样。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TextStyleIsAttributeMatched(const OH_Drawing_TextStyle* style,
    const OH_Drawing_TextStyle* comparedStyle, OH_Drawing_TextStyleType textStyleType);

/**
 * @brief 设置占位符。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleSetPlaceholder(OH_Drawing_TextStyle* style);

/**
 * @brief 返回是否有设置文本占位符。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return 返回是否有设置文本占位符，true表示有设置文本占位符，false表示未设置文本占位符。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TextStyleIsPlaceholder(OH_Drawing_TextStyle* style);

/**
 * @brief 获取文本对齐模式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回文本对齐模式的枚举值{@link OH_Drawing_TextAlign}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextAlign OH_Drawing_TypographyStyleGetEffectiveAlignment(OH_Drawing_TypographyStyle* style);

/**
 * @brief 获取文本是否启用字体提示。字体提示用于在渲染小字号文本时改善其可读性和外观。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回文本是否启用字体提示，true表示启用，false表示不启用。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyStyleIsHintEnabled(OH_Drawing_TypographyStyle* style);

/**
 * @brief 设置文本支柱样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param OH_Drawing_StrutStyle 指向支柱样式对象{@link OH_Drawing_StrutStyle}的指针，由{@link OH_Drawing_TypographyStyleGetStrutStyle}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyStyleTextStrutStyle(OH_Drawing_TypographyStyle*, OH_Drawing_StrutStyle*);

/**
 * @brief 获取文本支柱样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回指向支柱样式对象{@link OH_Drawing_StrutStyle}的指针。
 * @since 12
 * @version 1.0
 */
OH_Drawing_StrutStyle* OH_Drawing_TypographyStyleGetStrutStyle(OH_Drawing_TypographyStyle*);

/**
 * @brief 释放被支柱样式对象占据的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_StrutStyle 指向支柱样式对象{@link OH_Drawing_StrutStyle}的指针，由{@link OH_Drawing_TypographyStyleGetStrutStyle}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TypographyStyleDestroyStrutStyle(OH_Drawing_StrutStyle*);

/**
 * @brief 判断支柱样式结构体是否相同。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param from 被比较的支柱样式结构体。
 * @param to 用于比较的支柱样式结构体。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyStyleStrutStyleEquals(OH_Drawing_StrutStyle* from, OH_Drawing_StrutStyle* to);

/**
 * @brief 设置文本是否启用字体提示。字体提示用于在渲染小字号文本时改善其可读性和外观。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param hintsEnabled 是否启用字体提示，true表示启用，false表示不启用。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TypographyStyleSetHintsEnabled(OH_Drawing_TypographyStyle* style, bool hintsEnabled);

/**
 * @brief 将排版标记为脏数据，用于初始化排版状态。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 表示指向文本{@link OH_Drawing_Typography}对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @since 12
 * @version 1.0
 */
void  OH_Drawing_TypographyMarkDirty(OH_Drawing_Typography*);

/**
 * @brief 获取文本中尚未解析的字形的数量。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 表示指向文本{@link OH_Drawing_Typography}对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回文本中尚未解析的字形的数量。
 * @since 12
 * @version 1.0
 */
int32_t OH_Drawing_TypographyGetUnresolvedGlyphsCount(OH_Drawing_Typography*);

/**
 * @brief 更新文本中的字体大小。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 表示指向文本{@link OH_Drawing_Typography}对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param from 表示原来的字体大小。
 * @param to 表示更新后字体大小。
 * @param fontSize 表示字体的大小。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TypographyUpdateFontSize(OH_Drawing_Typography*, size_t from, size_t to, float fontSize);

/**
 * @brief 获取文本排版是否启用行样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 表示指向{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回行样式是否启用的结果，true表示启用，false表示不启用。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyTextGetLineStyle(OH_Drawing_TypographyStyle*);

/**
 * @brief 获取文本排版行样式字重。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 表示指向{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回文本排版行样式字重。
 * 0字重为thin，1字重为extra-light，2字重为light，4字重为medium，5字重为semi-bold，
 * 6字重为bold，7字重为extra-bold，8字重为black，3或其它字重为normal/regular，具体可见{@link OH_Drawing_FontWeight}枚举
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontWeight OH_Drawing_TypographyTextlineStyleGetFontWeight(OH_Drawing_TypographyStyle*);

/**
 * @brief 获取文本排版行样式风格。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 表示指向{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回获取文本排版行样式风格。1为斜体，0或其它为非斜体，具体可见{@link OH_Drawing_FontStyle}枚举。
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontStyle OH_Drawing_TypographyTextlineStyleGetFontStyle(OH_Drawing_TypographyStyle*);

/**
 * @brief 获取文本排版行样式字体家族名。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 表示指向{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回文本排版行样式字体家族名。
 * @since 12
 * @version 1.0
 */
char** OH_Drawing_TypographyTextlineStyleGetFontFamilies(OH_Drawing_TypographyStyle*);

/**
 * @brief 释放字体类型占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param fontFamilies 表示指向字体字体类型的指针。
 * @param fontFamiliesNum 字体名称的数量。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TypographyTextlineStyleDestroyFontFamilies(char** fontFamilies, size_t fontFamiliesNum);

/**
 * @brief 获取文本排版行样式字号。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 表示指向{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回文本排版行样式字号。
 * @since 12
 * @version 1.0
 */
double OH_Drawing_TypographyTextlineStyleGetFontSize(OH_Drawing_TypographyStyle*);

/**
 * @brief 获取文本排版中字体高度规模。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 表示指向{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回文本排版中字体高度规模。
 * @since 12
 * @version 1.0
 */
double OH_Drawing_TypographyTextlineStyleGetHeightScale(OH_Drawing_TypographyStyle*);

/**
 * @brief 获取字体渲染过程中计算字体块高度相关参数的方法。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 表示指向{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回计算字体块高度相关参数的方法，true表示以字号为准计算，false表示以行距计算。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyTextlineStyleGetHeightOnly(OH_Drawing_TypographyStyle*);

/**
 * @brief 获取文本排版行样式是否为一半行间距。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 表示指向{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 文本排版行样式是否为一半行间距，true表示是一半行间距，false表示不是。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyTextlineStyleGetHalfLeading(OH_Drawing_TypographyStyle*);

/**
 * @brief 获取文本排版行样式间距比例。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 表示指向{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回文本排版行样式间距比例。
 * @since 12
 * @version 1.0
 */
double OH_Drawing_TypographyTextlineStyleGetSpacingScale(OH_Drawing_TypographyStyle*);

/**
 * @brief 获取文本排版是否仅启用行样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 表示指向{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回文本排版是否仅启用行样式，true表示启用，false表示不启用。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyTextlineGetStyleOnly(OH_Drawing_TypographyStyle*);

/**
 * @brief 获取文本对齐方式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 表示指向{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回文本对齐方式。1为右对齐，2为居中对齐，3为两端对齐，4为与文字方向相同，5为文字方向相反，0或其它为左对齐,
 * 具体可见{@link OH_Drawing_TextAlign}枚举。
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextAlign OH_Drawing_TypographyGetTextAlign(OH_Drawing_TypographyStyle*);

/**
 * @brief 获取文本方向。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 表示指向{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回文本方向。1为从左到右，0或其它为从右到左，具体可见{@link OH_Drawing_TextDirection}枚举。
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextDirection OH_Drawing_TypographyGetTextDirection(OH_Drawing_TypographyStyle*);

/**
 * @brief 获取文本的最大行数。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 表示指向{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回结果为文本最大行数。
 * @since 12
 * @version 1.0
 */
size_t OH_Drawing_TypographyGetTextMaxLines(OH_Drawing_TypographyStyle*);

/**
 * @brief 获取设置的省略号内容。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 表示指向{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回设置的省略号内容。
 * @since 12
 * @version 1.0
 */
char* OH_Drawing_TypographyGetTextEllipsis(OH_Drawing_TypographyStyle*);

/**
 * @brief 释放省略号名称列表占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param ellipsis 表示指向省略号名称列表的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TypographyDestroyEllipsis(char* ellipsis);

/**
 * @brief 判断排版样式是否相同。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param from 被比较的排版样式。
 * @param to 用于比较的排版样式。
 * @return 返回排版样式是否相同。true表示相同，false表示不相同。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyStyleEquals(OH_Drawing_TypographyStyle* from, OH_Drawing_TypographyStyle* to);

/**
 * @brief 获取文本颜色。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return uint32_t 返回文本颜色。
 * @since 12
 * @version 1.0
 */
uint32_t OH_Drawing_TextStyleGetColor(OH_Drawing_TextStyle*);

/**
 * @brief 获取文本装饰样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return 返回文本装饰样式，具体可见{@link OH_Drawing_TextDecorationStyle}枚举。
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextDecorationStyle OH_Drawing_TextStyleGetDecorationStyle(OH_Drawing_TextStyle*);

/**
 * @brief 获取字重。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return 返回字重，具体可见{@link OH_Drawing_FontWeight}枚举。
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontWeight OH_Drawing_TextStyleGetFontWeight(OH_Drawing_TextStyle*);

/**
 * @brief 获取字体风格。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return 返回字体风格，具体可见{@link OH_Drawing_FontStyle}枚举。
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontStyle OH_Drawing_TextStyleGetFontStyle(OH_Drawing_TextStyle*);

/**
 * @brief 获取字体基线位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return 返回字体基线位置，具体可见{@link OH_Drawing_TextBaseline}枚举。
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextBaseline OH_Drawing_TextStyleGetBaseline(OH_Drawing_TextStyle*);

/**
 * @brief 获取字体类型名称列表。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param size_t 指向字体名称数量的指针。
 * @return 返回获取到的字体类型列表。
 * @since 12
 * @version 1.0
 */
char** OH_Drawing_TextStyleGetFontFamilies(OH_Drawing_TextStyle*, size_t* num);

/**
 * @brief 释放字体类型名称列表占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param char** 指向字体类型名称列表的指针。
 * @param size_t 字体类型名称列表的数量。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleDestroyFontFamilies(char** fontFamilies, size_t num);

/**
 * @brief 获取字号。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return double 返回字号。
 * @since 12
 * @version 1.0
 */
double OH_Drawing_TextStyleGetFontSize(OH_Drawing_TextStyle*);

/**
 * @brief 获取文本的字符间距。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return double 返回字符间距大小。
 * @since 12
 * @version 1.0
 */
double OH_Drawing_TextStyleGetLetterSpacing(OH_Drawing_TextStyle*);

/**
 * @brief 获取文本的单词间距。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return double 返回单词间距大小。
 * @since 12
 * @version 1.0
 */
double OH_Drawing_TextStyleGetWordSpacing(OH_Drawing_TextStyle*);

/**
 * @brief 获取字体高度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return double 返回字体高度。
 * @since 12
 * @version 1.0
 */
double OH_Drawing_TextStyleGetFontHeight(OH_Drawing_TextStyle*);

/**
 * @brief 获取当前文档是否设置为一半行间距。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return bool 返回当前文档设置为一半行间距的结果，true表示设置为一半行间距，false表示未设置为一半行间距。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TextStyleGetHalfLeading(OH_Drawing_TextStyle*);

/**
 * @brief 获取语言文本语言类型。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return char 返回语言区域，数据类型为指向char的指针。语言区域格式为：语言-国家。如zh-CN表示中文（中国），en-US表示英语（美国）等。具体参考BCP 47语言标签标准。
 * @since 12
 * @version 1.0
 */
const char* OH_Drawing_TextStyleGetLocale(OH_Drawing_TextStyle*);

/**
 * @brief 释放文本框占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox 指向文本框对象{@link OH_Drawing_TextBox}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TypographyDestroyTextBox(OH_Drawing_TextBox*);

/**
 * @brief 设置字体阴影对象的参数。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextShadow 指向字体阴影对象{@link OH_Drawing_TextShadow}的指针，由{@link OH_Drawing_CreateTextShadow}获取。
 * @param color 字体阴影的颜色，例如入参为0xAABBCCDD，AA代表透明度，BB代表红色分量的值，CC代表绿色分量的值，DD代表蓝色分量的值。
 * @param OH_Drawing_Point 指向坐标点对象{@link OH_Drawing_Point}的指针，字体阴影基于当前文本的偏移位置。
 * @param blurRadius 模糊半径，浮点数，没有单位，值为0.0时表示没有模糊效果。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTextShadow(OH_Drawing_TextShadow* shadow, uint32_t color, OH_Drawing_Point* offset,
    double blurRadius);

/**
 * @brief 创建一个排版行对象{@link OH_Drawing_LineTypography}的指针，排版行对象保存着文本内容以及样式的载体，
 * 可以用于计算单行排版信息。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param handler 指向{@link OH_Drawing_TypographyCreate}对象的指针，由{@link OH_Drawing_CreateTypographyHandler}获取。
 * @return 返回一个指向排版行对象{@link OH_Drawing_LineTypography}的指针。
 * @since 14
 * @version 1.0
 */
OH_Drawing_LineTypography* OH_Drawing_CreateLineTypography(OH_Drawing_TypographyCreate* handler);

/**
 * @brief 释放排版行对象{@link OH_Drawing_LineTypography}占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param lineTypography 指向排版行对象{@link OH_Drawing_LineTypography}的指针，由{@link OH_Drawing_CreateLineTypography}获取。
 * @since 14
 * @version 1.0
 */
void OH_Drawing_DestroyLineTypography(OH_Drawing_LineTypography* lineTypography);

/**
 * @brief 计算在限定排版宽度的情况下，从指定位置处开始可以排版的字符个数。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param lineTypography 指向排版行对象{@link OH_Drawing_LineTypography}的指针，由{@link OH_Drawing_CreateLineTypography}获取。
 * @param startIndex 开始计算排版的起始位置（包括起始位置）。取值范围需要为[0,文本字符总数）的整数。
 * @param width 换行宽度，大于0的浮点数，单位为物理像素px。
 * @return 返回在限定排版宽度的情况下，从指定位置处开始可以排版的字符总数，取值为整数。 
 * @since 14
 * @version 1.0
 */
size_t OH_Drawing_LineTypographyGetLineBreak(OH_Drawing_LineTypography* lineTypography,
                                             size_t startIndex, double width);

/**
 * @brief 根据指定区间文本内容创建一个指向文本行对象{@link OH_Drawing_TextLine}的指针。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param lineTypography 指向排版行对象{@link OH_Drawing_LineTypography}的指针，由{@link OH_Drawing_CreateLineTypography}获取。
 * @param startIndex 表示计算排版的起始位置，整数，取值范围为[0, 文本字符总数)。
 * @param count 表示从指定排版起始位置开始进行排版的字符个数，取值为[0,文本字符总数)的整数，startIndex和count之和不能大于文本字符总数。
 * 可以先使用{@link OH_Drawing_LineTypographyGetLineBreak}获得合理的可用于进行排版的字符总数。如果该值设置为0，则返回nullptr。
 * @return 返回一个指向文本行对象{@link OH_Drawing_TextLine}的指针。
 * @since 14
 * @version 1.0
 */
OH_Drawing_TextLine* OH_Drawing_LineTypographyCreateLine(OH_Drawing_LineTypography* lineTypography,
                                                         size_t startIndex, size_t count);

/**
 * @brief 创建文本制表符对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param alignment 制表符之后的文本对齐方式。1为右对齐，2为居中对齐，0或其它为左对齐。
 * @param float 文本制表符之后的文本对齐的位置，相对于当前文本起始位置的偏移。单位为物理像素px，最小值为1.0。
 * @return OH_Drawing_TextTab 返回指向文本制表符对象的指针。如果返回空指针，表示创建失败。失败的原因可能为没有可用的内存。
 * @since 14
 * @version 1.0
 */
OH_Drawing_TextTab* OH_Drawing_CreateTextTab(OH_Drawing_TextAlign alignment, float location);

/**
 * @brief 释放文本制表符对象占据的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextTab 指向文本制表符对象的指针。
 * @since 14
 * @version 1.0
 */
void OH_Drawing_DestroyTextTab(OH_Drawing_TextTab*);

/**
 * @brief 获取文本制表符对象的对齐方式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextTab 指向文本制表符对象的指针。
 * @return 返回文本对齐方式。1为右对齐，2为居中对齐，0或其它为左对齐。
 * @since 14
 * @version 1.0
 */
OH_Drawing_TextAlign OH_Drawing_GetTextTabAlignment(OH_Drawing_TextTab*);

/**
 * @brief 获取文本制表符的位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextTab 指向文本制表符对象的指针。
 * @return 返回文本制表符对象的位置。
 * @since 14
 * @version 1.0
 */
float OH_Drawing_GetTextTabLocation(OH_Drawing_TextTab*);

/**
 * @brief 设置文本制表符对齐方式及位置。当设置了文本对齐方式或者省略号风格时制表符不生效，当制表符位置小于1.0时为替换成空格的效果。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针。
 * @param OH_Drawing_TextTab 指向文本制表符对象的指针。
 * @since 14
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextTab(OH_Drawing_TypographyStyle*, OH_Drawing_TextTab* TextTab);

/**
 * @brief 获取传入类型为对象数组{@link OH_Drawing_Array}中的对象个数。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param drawingArray 指向对象数组{@link OH_Drawing_Array}的指针。
 * @return 数组中的对象个数。
 * @since 14
 * @version 1.0
 */
size_t OH_Drawing_GetDrawingArraySize(OH_Drawing_Array* drawingArray);
#ifdef __cplusplus
}
#endif
/** @} */
#endif