/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Sensor
 * @{
 *
 * @brief 提供标准的开放api，定义常用传感器属性。
 *
 * @since 11
 */

/**
 * @file oh_sensor_type.h
 *
 * @brief 定义常用传感器属性。
 * @library libohsensor.so
 * @syscap SystemCapability.Sensors.Sensor
 * @since 11
 */

#ifndef OH_SENSOR_TYPE_H
#define OH_SENSOR_TYPE_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 枚举传感器类型。
 *
 * @since 11
 */
typedef enum Sensor_Type {
    /**
     * 加速度传感器。
     * @since 11
     */
    SENSOR_TYPE_ACCELEROMETER = 1,
    /**
     * 陀螺仪传感器。
     * @since 11
     */
    SENSOR_TYPE_GYROSCOPE = 2,
    /**
     * 环境光传感器。
     * @since 11
     */
    SENSOR_TYPE_AMBIENT_LIGHT = 5,
    /**
     * 地磁传感器。
     * @since 11
     */
    SENSOR_TYPE_MAGNETIC_FIELD = 6,
    /**
     * 气压传感器。
     * @since 11
     */
    SENSOR_TYPE_BAROMETER = 8,
    /**
     * 霍尔传感器。
     * @since 11
     */
    SENSOR_TYPE_HALL = 10,
    /**
     * 接近光传感器。
     * @since 11
     */
    SENSOR_TYPE_PROXIMITY = 12,
    /**
     * 方向传感器。
     * @since 11
     */
    SENSOR_TYPE_ORIENTATION = 256,
    /**
     * 重力传感器。
     * @since 11
     */
    SENSOR_TYPE_GRAVITY = 257,
    /**
     * 旋转矢量传感器。
     * @since 11
     */
    SENSOR_TYPE_ROTATION_VECTOR = 259,
    /**
     * 计步器检测传感器。
     * @since 11
     */
    SENSOR_TYPE_PEDOMETER_DETECTION = 265,
    /**
     * 计步器传感器。
     * @since 11
     */
    SENSOR_TYPE_PEDOMETER = 266,
    /**
     * 心率传感器。
     * @since 11
     */
    SENSOR_TYPE_HEART_RATE = 278,
} Sensor_Type;

/**
 * @brief 定义传感器错误码。
 *
 * @since 11
 */
typedef enum Sensor_Result {
    /**
     * 操作成功。
     * @since 11
     */
    SENSOR_SUCCESS = 0,
    /**
     * 权限验证失败。
     * @since 11
     */
    SENSOR_PERMISSION_DENIED = 201,
    /**
     * 参数检查失败。例如，没有传入强制参数，或者传入的参数类型不正确。
     * @since 11
     */
    SENSOR_PARAMETER_ERROR = 401,
    /**
     * 传感器服务异常。
     * @since 11
     */
    SENSOR_SERVICE_EXCEPTION = 14500101,
} Sensor_Result;

/**
 * @brief 枚举传感器报告的数据的精度级别。
 *
 * @since 11
 */
typedef enum Sensor_Accuracy {
    /**
     * 传感器数据不可靠。有可能传感器不与设备接触而进行测量。
     * @since 11
     */
    SENSOR_ACCURACY_UNRELIABLE = 0,
    /**
     * 传感器数据精度较低。数据在使用前必须根据环境进行校准。
     * @since 11
     */
    SENSOR_ACCURACY_LOW = 1,
    /**
     * 传感器数据处于中等精度水平。建议用户在使用前根据实际环境进行数据校准。
     * @since 11
     */
    SENSOR_ACCURACY_MEDIUM = 2,
    /**
     * 传感器数据具有很高的精度。数据可以直接使用。
     * @since 11
     */
    SENSOR_ACCURACY_HIGH = 3
} Sensor_Accuracy;

/**
 * @brief 定义传感器信息。
 * @since 11
 */
typedef struct Sensor_Info Sensor_Info;

/**
 * @brief 用给定的数字创建一个实例数组，请参考{@link Sensor_Info}。
 *
 * @param count - 要创建的实例的数量，请参考 {@link Sensor_Info}。
 * @return 如果操作成功，返回指向{@link Sensor_Info}实例数组的双指针；否则返回<b>NULL</b>。
 * @since 11
 */
Sensor_Info **OH_Sensor_CreateInfos(uint32_t count);

/**
 * @brief 销毁实例数组并回收内存，请参考{@link Sensor_Info}。
 *
 * @param sensors - 指向{@link Sensor_Info}实例数组的双指针。
 * @param count - 要销毁的{@link Sensor_Info}实例的数量。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_Sensor_DestroyInfos(Sensor_Info **sensors, uint32_t count);

/**
 * @brief 获取传感器名称。
 * @param sensor - 指向传感器信息的指针。
 * @param sensorName - 指向传感器名称的指针。
 * @param length - 指向长度的指针，以字节为单位。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_SensorInfo_GetName(Sensor_Info* sensor, char *sensorName, uint32_t *length);

/**
 * @brief 获取传感器的厂商名称。
 *
 * @param sensor - 指向传感器信息的指针。
 * @param vendorName - 指向供应商名称的指针。
 * @param length - 指向长度的指针，以字节为单位。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_SensorInfo_GetVendorName(Sensor_Info* sensor, char *vendorName, uint32_t *length);

/**
 * @brief 获取传感器类型。
 *
 * @param sensor - 指向传感器信息的指针。
 * @param sensorType - 指向传感器类型的指针。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_SensorInfo_GetType(Sensor_Info* sensor, Sensor_Type *sensorType);

/**
 * @brief 获取传感器分辨率。
 *
 * @param sensor - 指向传感器信息的指针。
 * @param resolution - 指向传感器分辨率的指针。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_SensorInfo_GetResolution(Sensor_Info* sensor, float *resolution);

/**
 * @brief 获取传感器的最小数据上报间隔。
 *
 * @param sensor - 指向传感器信息的指针。
 * @param minSamplingInterval - 指向最小数据报告间隔的指针，以纳秒为单位。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_SensorInfo_GetMinSamplingInterval(Sensor_Info* sensor, int64_t *minSamplingInterval);

/**
 * @brief 获取传感器的最大数据上报间隔时间。
 *
 * @param sensor - 指向传感器信息的指针。
 * @param maxSamplingInterval - -指向最大数据报告间隔的指针，单位为纳秒。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_SensorInfo_GetMaxSamplingInterval(Sensor_Info* sensor, int64_t *maxSamplingInterval);

/**
 * @brief 定义传感器数据信息。
 * @since 11
 */
typedef struct Sensor_Event Sensor_Event;

/**
 * @brief 获取传感器类型。
 *
 * @param sensorEvent - 指向传感器数据信息的指针。
 * @param sensorType - 指向传感器类型的指针。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_SensorEvent_GetType(Sensor_Event* sensorEvent, Sensor_Type *sensorType);

/**
 * @brief 获取传感器数据的时间戳。
 *
 * @param sensorEvent - 指向传感器数据信息的指针。
 * @param timestamp - 时间戳指针。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_SensorEvent_GetTimestamp(Sensor_Event* sensorEvent, int64_t *timestamp);

/**
 * @brief 获取传感器数据的精度。
 *
 * @param sensorEvent - 指向传感器数据信息的指针。
 * @param accuracy - 指向精度的指针。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_SensorEvent_GetAccuracy(Sensor_Event* sensorEvent, Sensor_Accuracy *accuracy);

/**
 * @brief 获取传感器数据。数据的长度和内容依赖于监听的传感器类型，传感器上报的数据格式如下：
 * SENSOR_TYPE_ACCELEROMETER: data[0]、data[1]、data[2]分别表示设备x、y、z轴的加速度分量，单位m/s2；
 * SENSOR_TYPE_GYROSCOPE: data[0]、data[1]、data[2]分别表示设备x、y、z轴的旋转角速度，单位弧度/s；
 * SENSOR_TYPE_AMBIENT_LIGHT: data[0]表示环境光强度，in lux；
 * SENSOR_TYPE_MAGNETIC_FIELD: data[0]、data[1]、data[2]分别表示设备x、y、z轴的地磁分量，单位微特斯拉；
 * SENSOR_TYPE_BAROMETER：data[0]表示气压值，单位hPa；
 * SENSOR_TYPE_HALL: data[0]表示皮套吸合状态，0表示打开，大于0表示吸附；
 * SENSOR_TYPE_PROXIMITY：data[0]表示接近状态，0表示接近，大于0表示远离；
 * SENSOR_TYPE_ORIENTATION:data[0]、data[1]、data[2]分别表示设备绕z、x、y轴的角度，单位度；
 * SENSOR_TYPE_GRAVITY：data[0]、data[1]、data[2]分别表示设备x、y、z轴的重力加速度分量，单位m/s2；
 * SENSOR_TYPE_ROTATION_VECTOR:data[0]、data[1]、data[2]分别表示设备x、y、z轴的旋转角度，单位度，data[3]表示旋转向量元素；
 * SENSOR_TYPE_PEDOMETER_DETECTION:data[0]表示几步检测状态，1表示检测到了步数变化；
 * SENSOR_TYPE_PEDOMETER:data[0]表示步数；
 * SENSOR_TYPE_HEART_RATE：data[0]表示心率数值；
 * 
 * @param sensorEvent - 传感器数据信息。
 * @param data - 出参，传感器数据。
 * @param length - 出参，数组长度。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_SensorEvent_GetData(Sensor_Event* sensorEvent, float **data, uint32_t *length);

/**
 * @brief 定义传感器订阅ID，唯一标识传感器。
 * @since 11
 */
typedef struct Sensor_SubscriptionId Sensor_SubscriptionId;

/**
 * @brief 创建一个{@link Sensor_SubscriptionId}实例。
 *
 * @return 如果操作成功，返回指向{@link Sensor_SubscriptionId}实例的指针;否则返回<b>NULL</b>。
 * @since 11
 */
Sensor_SubscriptionId *OH_Sensor_CreateSubscriptionId(void);

/**
 * @brief 销毁{@link Sensor_SubscriptionId}实例并回收内存。
 *
 * @param id - 指向{@link Sensor_SubscriptionId}实例的指针。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_Sensor_DestroySubscriptionId(Sensor_SubscriptionId *id);

/**
 * @brief 获取传感器类型。
 *
 * @param id - 指向传感器订阅ID的指针。
 * @param sensorType - 指向传感器类型的指针。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_SensorSubscriptionId_GetType(Sensor_SubscriptionId* id, Sensor_Type *sensorType);

/**
 * @brief 设置传感器类型。
 *
 * @param id - 指向传感器订阅ID的指针。
 * @param sensorType - 要设置的传感器类型。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_SensorSubscriptionId_SetType(Sensor_SubscriptionId* id, const Sensor_Type sensorType);

/**
 * @brief 定义传感器订阅属性。
 * @since 11
 */
typedef struct Sensor_SubscriptionAttribute Sensor_SubscriptionAttribute;

/**
 * @brief 创建{@link Sensor_SubscriptionAttribute}实例。
 *
 * @return 如果操作成功，返回指向{@link Sensor_SubscriptionAttribute]实例的指针；否则返回<b>NULL</b>。
 * @since 11
 */
Sensor_SubscriptionAttribute *OH_Sensor_CreateSubscriptionAttribute(void);

/**
 * @brief 销毁{@link Sensor_SubscriptionAttribute}实例并回收内存。
 *
 * @param attribute - 指向{@link Sensor_SubscriptionAttribute}实例的指针。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_Sensor_DestroySubscriptionAttribute(Sensor_SubscriptionAttribute *attribute);

/**
 * @brief 设置传感器数据报告间隔。
 *
 * @param attribute - 指向传感器订阅属性的指针。
 * @param samplingInterval - 要设置的数据报告间隔，以纳秒为单位。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_SensorSubscriptionAttribute_SetSamplingInterval(Sensor_SubscriptionAttribute* attribute,
    const int64_t samplingInterval);

/**
 * @brief 获取传感器数据报告间隔。
 *
 * @param attribute - 指向传感器订阅属性的指针。
 * @param samplingInterval - 指向数据报告间隔的指针，以纳秒为单位。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_SensorSubscriptionAttribute_GetSamplingInterval(Sensor_SubscriptionAttribute* attribute,
    int64_t *samplingInterval);

/**
 * @brief 定义用于报告传感器数据的回调函数。
 * @since 11
 */
typedef void (*Sensor_EventCallback)(Sensor_Event *event);

/**
 * @brief 定义传感器订阅者信息。
 * @since 11
 */
typedef struct Sensor_Subscriber Sensor_Subscriber;

/**
 * @brief 创建一个{@link Sensor_Subscriber}实例。
 *
 * @return 如果操作成功，返回指向{@link Sensor_Subscriber}实例的指针;否则返回<b>NULL</b>。
 * @since 11
 */
Sensor_Subscriber *OH_Sensor_CreateSubscriber(void);

/**
 * @brief 销毁{@link Sensor_Subscriber}实例并回收内存。
 *
 * @param subscriber - 指向{@link Sensor_Subscriber}实例的指针。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_Sensor_DestroySubscriber(Sensor_Subscriber *subscriber);

/**
 * @brief 设置一个回调函数来报告传感器数据。
 *
 * @param subscriber - 指向传感器订阅者信息的指针。
 * @param callback - 设置回调函数。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_SensorSubscriber_SetCallback(Sensor_Subscriber* subscriber, const Sensor_EventCallback callback);

/**
 * @brief 获取用于报告传感器数据的回调函数。
 *
 * @param subscriber - 指向传感器订阅者信息的指针。
 * @param callback - 指向回调函数的指针。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @since 11
 */
int32_t OH_SensorSubscriber_GetCallback(Sensor_Subscriber* subscriber, Sensor_EventCallback *callback);
#ifdef __cplusplus
}
#endif
#endif // OH_SENSOR_TYPE_H