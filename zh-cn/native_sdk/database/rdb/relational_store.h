/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RELATIONAL_STORE_H
#define RELATIONAL_STORE_H

/**
 * @addtogroup RDB
 * @{
 *
 * @brief 关系型数据库（Relational Database，RDB）是一种基于关系模型来管理数据的数据库。关系型数据库基于SQLite组件提供了一套完整的
 * 对本地数据库进行管理的机制，对外提供了一系列的增、删、改、查等接口，也可以直接运行用户输入的SQL语句来满足复杂的场景需要。
 *
 * @syscap SystemCapability.DistributedDataManager.RelationalStore.Core
 * @since 10
 */


/**
 * @file relational_store.h
 *
 * @brief 提供管理关系数据库（RDB）方法的接口。
 * 引用文件: <database/rdb/relational_store.h>
 * @library libnative_rdb_ndk.z.so
 * @since 10
 */

#include "oh_cursor.h"
#include "oh_predicates.h"
#include "oh_value_object.h"
#include "oh_values_bucket.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 数据库的安全级别枚举。
 *
 * @since 10
 */
typedef enum OH_Rdb_SecurityLevel {
    /**
     * @brief S1: 表示数据库的安全级别为低级别。
     *
     * 当数据泄露时会产生较低影响。
     */
    S1 = 1,
    /**
     * @brief S2: 表示数据库的安全级别为中级别。
     *
     * 当数据泄露时会产生较大影响。
     */
    S2,
    /**
     * @brief S3: 表示数据库的安全级别为高级别。
     *
     * 当数据泄露时会产生重大影响。
     */
    S3,
    /**
     * @brief S4: 表示数据库的安全级别为关键级别。
     *
     * 当数据泄露时会产生严重影响。
     */
    S4
} OH_Rdb_SecurityLevel;

/**
 * @brief 描述数据库的安全区域等级。
 *
 * @since 11
 */
typedef enum Rdb_SecurityArea {
    /**
     * @brief 安全区域等级为1。
     */
    RDB_SECURITY_AREA_EL1 = 1,
    /**
     * @brief 安全区域等级为2。
     */
    RDB_SECURITY_AREA_EL2,
    /**
     * @brief 安全区域等级为3。
     */
    RDB_SECURITY_AREA_EL3,
    /**
     * @brief 安全区域等级为4。
     */
    RDB_SECURITY_AREA_EL4,
} Rdb_SecurityArea;

/**
 * @brief 管理关系数据库配置。
 *
 * @since 10
 */
#pragma pack(1)
typedef struct {
    /** 该结构体的大小。 */
    int selfSize;
    /** 数据库文件路径。 */
    const char *dataBaseDir;
    /** 数据库名称。 */
    const char *storeName;
    /** 应用包名。 */
    const char *bundleName;
    /** 应用模块名。 */
    const char *moduleName;
    /** 指定数据库是否加密。 */
    bool isEncrypt;
    /** 设置数据库安全级别{@link OH_Rdb_SecurityLevel}。 */
    int securityLevel;
    /**
     * 设置数据库安全区域等级{@link Rdb_SecurityArea}。
     *
     * @since 11
     */
    int area;
} OH_Rdb_Config;
#pragma pack()

/**
 * @brief 表示数据库类型。
 *
 * @since 10
 */
typedef struct {
    /** OH_Rdb_Store结构体的唯一标识符。 */
    int64_t id;
} OH_Rdb_Store;

/**
 * @brief 管理关系数据库配置，与{@link OH_Rdb_Config}的区别是该结构体成员变量不对外暴露，使用一系列方法配置该结构体的属性。
 *
 * @since 14
 */
typedef struct OH_Rdb_ConfigV2 OH_Rdb_ConfigV2;

/**
 * @brief 描述数据库的内核类型。
 *
 * @since 14
 */
typedef enum Rdb_DBType {
    /**
     * @brief 表示使用sqlite作为数据库内核。
     */
    RDB_SQLITE = 1,
    /**
     * @brief 表示使用凯莱数据库作为数据库内核。
     */
    RDB_CAYLEY = 2,
    /**
     * @brief 表示内核类型枚举值允许取值的最大值，这是一个非法值。
     */
    DBTYPE_BUTT = 64,
} Rdb_DBType;

/**
 * @brief 创建一个{@link OH_Rdb_ConfigV2}实例，并返回指向该实例的指针。
 *
 * @return 返回一个指向{@link OH_Rdb_ConfigV2}实例的指针。
 * @see OH_Rdb_ConfigV2
 * @since 14
 */
OH_Rdb_ConfigV2 *OH_Rdb_CreateConfig();

/**
 * @brief 销毁由{@link OH_Rdb_CreateConfig}创建的{@link OH_Rdb_ConfigV2}对象。
 *
 * @param config 表示指向{@link OH_Rdb_ConfigV2}对象的指针，即与此RDB存储相关的数据库配置。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @since 14
 */
int OH_Rdb_DestroyConfig(OH_Rdb_ConfigV2 *config);

/**
 * @brief 给指定的数据库文件配置{@link OH_Rdb_ConfigV2}，设置数据库文件路径。
 *
 * @param config 表示指向{@link OH_Rdb_ConfigV2}对象的指针，即与此RDB存储相关的数据库配置。
 * @param dataBaseDir 表示数据库文件路径。包含数据库名称在内的全路径长度不超过1024个字符。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @since 14
 */
int OH_Rdb_SetDatabaseDir(OH_Rdb_ConfigV2 *config, const char *databaseDir);

/**
 * @brief 给指定的数据库文件配置{@link OH_Rdb_ConfigV2}，设置数据库名称。
 *
 * @param config 表示指向{@link OH_Rdb_ConfigV2}对象的指针，即与此RDB存储相关的数据库配置。
 * @param storeName 表示数据库名称。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @since 14
 */
int OH_Rdb_SetStoreName(OH_Rdb_ConfigV2 *config, const char *storeName);

/**
 * @brief 给指定的数据库文件配置{@link OH_Rdb_ConfigV2}，设置应用包名。
 *
 * @param config 表示指向{@link OH_Rdb_ConfigV2}对象的指针，即与此RDB存储相关的数据库配置。
 * @param bundleName 表示数据库应用包名。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @since 14
 */
int OH_Rdb_SetBundleName(OH_Rdb_ConfigV2 *config, const char *bundleName);

/**
 * @brief 给指定的数据库文件配置{@link OH_Rdb_ConfigV2}，设置应用模块名。
 *
 * @param config 表示指向{@link OH_Rdb_ConfigV2}对象的指针，即与此RDB存储相关的数据库配置。
 * @param moduleName 表示数据库应用模块名。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @since 14
 */
int OH_Rdb_SetModuleName(OH_Rdb_ConfigV2 *config, const char *moduleName);

/**
 * @brief 给指定的数据库文件配置{@link OH_Rdb_ConfigV2}，设置数据库是否加密。
 *
 * @param config 表示指向{@link OH_Rdb_ConfigV2}对象的指针，即与此RDB存储相关的数据库配置。
 * @param isEncrypted 表示数据库是否加密, true表示加密, false表示不加密。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @since 14
 */
int OH_Rdb_SetEncrypted(OH_Rdb_ConfigV2 *config, bool isEncrypted);

/**
 * @brief 给指定的数据库文件配置{@link OH_Rdb_ConfigV2}，设置数据库安全级别{@link OH_Rdb_SecurityLevel}。
 *
 * @param config 表示指向{@link OH_Rdb_ConfigV2}对象的指针，即与此RDB存储相关的数据库配置。
 * @param securityLevel 表示数据库安全级别 {@link OH_Rdb_SecurityLevel}。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @since 14
 */
int OH_Rdb_SetSecurityLevel(OH_Rdb_ConfigV2 *config, int securityLevel);

/**
 * @brief 给指定的数据库文件配置{@link OH_Rdb_ConfigV2}，设置数据库安全区域等级{@link Rdb_SecurityArea}。
 *
 * @param config 表示指向{@link OH_Rdb_ConfigV2}对象的指针，即与此RDB存储相关的数据库配置。
 * @param area 表示数据库安全区域等级{@link Rdb_SecurityArea}。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @since 14
 */
int OH_Rdb_SetArea(OH_Rdb_ConfigV2 *config, int area);

/**
 * @brief 给指定的数据库文件配置{@link OH_Rdb_ConfigV2}，设置数据库类型{@link Rdb_DBType}。
 * @param config 表示指向{@link OH_Rdb_ConfigV2}对象的指针，即与此RDB存储相关的数据库配置。
 * @param dbType 表示数据库的数据库类型 {@link Rdb_DBType}。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * RDB_E_NOT_SUPPORTED 表示不支持当前操作。
 * @since 14
 */
int OH_Rdb_SetDbType(OH_Rdb_ConfigV2 *config, int dbType);

/**
 * @brief 获得支持的数据库类型{@link Rdb_DBType}。
 * @param typeCount 表示支持的数据库类型的数组的长度, 作为出参使用。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @since 14
 */
const int *OH_Rdb_GetSupportedDbType(int *typeCount);

/**
 * @brief 创建{@link OH_VObject}实例。
 *
 * @return 创建成功则返回一个指向{@link OH_VObject}结构体实例的指针，否则返回NULL。
 * @see OH_VObject.
 * @since 10
 */
OH_VObject *OH_Rdb_CreateValueObject(void);

/**
 * @brief 创建{@link OH_VBucket}实例。
 *
 * @return 创建成功则返回一个指向{@link OH_VBucket}结构体实例的指针，否则返回NULL。
 * @see OH_VBucket.
 * @since 10
 */
OH_VBucket *OH_Rdb_CreateValuesBucket(void);

/**
 * @brief 创建{@link OH_Predicates}实例。
 *
 * @param table 表示数据库表名。
 * @return 创建成功则返回一个指向{@link OH_Predicates}结构体实例的指针，否则返回NULL。
 * @see OH_Predicates.
 * @since 10
 */
OH_Predicates *OH_Rdb_CreatePredicates(const char *table);

/**
 * @brief 获得一个相关的{@link OH_Rdb_Store}实例，操作关系型数据库。
 *
 * @param config 表示指向{@link OH_Rdb_Config}实例的指针，与此RDB存储相关的数据库配置。
 * @param errCode 表示函数执行状态, 作为出参使用。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @return 创建成功则返回一个指向{@link OH_Rdb_Store}结构体实例的指针，否则返回NULL。
 * @see OH_Rdb_Config, OH_Rdb_Store.
 * @since 10
 */
OH_Rdb_Store *OH_Rdb_GetOrOpen(const OH_Rdb_Config *config, int *errCode);

/**
 * @brief 使用指定的数据库文件配置{@link OH_Rdb_ConfigV2}, 获得一个对应的{@link OH_Rdb_Store}实例，用来操作关系型数据库。
 *
 * @param config 表示指向{@link OH_Rdb_ConfigV2}对象的指针，即与此RDB存储相关的数据库配置。
 * @param errCode 表示函数执行状态, 作为出参使用。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @return 创建成功则返回一个指向{@link OH_Rdb_Store}结构体实例的指针，否则返回NULL。
 * @see OH_Rdb_ConfigV2, OH_Rdb_Store.
 * @since 14
 */
OH_Rdb_Store *OH_Rdb_CreateOrOpen(const OH_Rdb_ConfigV2 *config, int *errCode);

/**
 * @brief 销毁{@link OH_Rdb_Store}对象，并回收该对象占用的内存。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_CloseStore(OH_Rdb_Store *store);

/**
 * @brief 使用指定的数据库文件配置删除数据库。
 *
 * @param config 表示数据库的配置。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @since 10
 */
int OH_Rdb_DeleteStore(const OH_Rdb_Config *config);

/**
 * @brief 使用指定的数据库文件配置{@link OH_Rdb_ConfigV2}删除数据库。
 * 当使用向量数据库时，在调用接口前，应当确保向量数据库已经被正确关闭。
 *
 * @param config 表示数据库的配置。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_ErrCode.
 * @since 14
 */
int OH_Rdb_DeleteStoreV2(const OH_Rdb_ConfigV2 *config);

/**
 * @brief 向目标表中插入一行数据。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param table 表示指定的目标表名。
 * @param valuesBucket 表示要插入到表中的数据行{@link OH_VBucket}。
 * @return 如果插入成功，返回rowID，否则返回的结果小于0。
 *     {@link RDB_ERR} - 表示插入失败。
 *     {@link RDB_E_INVALID_ARGS} - 表示无效参数。
 * @see OH_Rdb_Store, OH_VBucket.
 * @since 10
 */
int OH_Rdb_Insert(OH_Rdb_Store *store, const char *table, OH_VBucket *valuesBucket);

/**
 * @brief 根据指定的条件更新数据库中的数据。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param valuesBucket 表示要更新到表中的数据行{@link OH_VBucket}。
 * @param predicates 表示指向{@link OH_Predicates}实例的指针，指定更新条件。
 * @return 如果更新成功，返回更新的行数，否则返回的结果小于0。
 *     {@link RDB_ERR} - 表示更新失败。
 *     {@link RDB_E_INVALID_ARGS} - 表示无效参数。
 * @see OH_Rdb_Store, OH_Bucket, OH_Predicates.
 * @since 10
 */
int OH_Rdb_Update(OH_Rdb_Store *store, OH_VBucket *valuesBucket, OH_Predicates *predicates);

/**
 * @brief 根据指定的条件删除数据库中的数据。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param predicates 表示指向{@link OH_Predicates}实例的指针，指定删除条件。
 * @return 如果删除成功，返回删除的行数；如果失败，则返回的结果小于0。
 *     {@link RDB_ERR} - 表示删除失败。
 *     {@link RDB_E_INVALID_ARGS} - 表示无效参数。
 * @see OH_Rdb_Store, OH_Predicates.
 * @since 10
 */
int OH_Rdb_Delete(OH_Rdb_Store *store, OH_Predicates *predicates);

/**
 * @brief 根据指定条件查询数据库中的数据
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param predicates 表示指向{@link OH_Predicates}实例的指针，指定查询条件。
 * @param columnNames 表示要查询的列。如果值为空，则查询应用于所有列。
 * @param length 表示columnNames数组的长度。若length大于columnNames数组的实际长度，则会访问越界。
 * @return 如果查询成功则返回一个指向{@link OH_Cursor}结构体实例的指针，否则返回NULL。
 * @see OH_Rdb_Store, OH_Predicates, OH_Cursor.
 * @since 10
 */
OH_Cursor *OH_Rdb_Query(OH_Rdb_Store *store, OH_Predicates *predicates, const char *const *columnNames, int length);

/**
 * @brief 执行无返回值的SQL语句。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param sql 指定要执行的SQL语句。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_Execute(OH_Rdb_Store *store, const char *sql);

/**
 * @brief 使用指定的事务ID执行无返回值的SQL语句。
 *
 * @param store 表示一个指向 {@link OH_Rdb_Store} 实例的指针。
 * @param trxId 调用{@link OH_Rdb_BeginTransWithTrxId}获得的事务ID，当设置为0时，表示不启用事务。
 * @param sql 指定要执行的SQL语句。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数，可能情况如下：
 *      传入参数为空指针。
 *      当前事务ID不是调用{@link OH_Rdb_BeginTransWithTrxId}获得的。
 *      当前事务ID已经调用{@link OH_Rdb_CommitByTrxId}提交。
 *      当前事务ID已经调用{@link OH_Rdb_RollBackByTrxId}回滚。
 *      当store或者sql为NULL时。
 * RDB_E_NOT_SUPPORTED 表示不支持当前操作。
 * @see OH_Rdb_Store.
 * @since 14
 */
int OH_Rdb_ExecuteByTrxId(OH_Rdb_Store *store, int64_t trxId, const char *sql);

/**
 * @brief 根据指定SQL语句查询数据库中的数据。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param sql 指定要执行的SQL语句。
 * @return 如果查询成功则返回一个指向{@link OH_Cursor}结构体实例的指针，否则返回NULL。
 * @see OH_Rdb_Store.
 * @since 10
 */
OH_Cursor *OH_Rdb_ExecuteQuery(OH_Rdb_Store *store, const char *sql);

/**
 * @brief 在开始执行SQL语句之前，开始事务。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_BeginTransaction(OH_Rdb_Store *store);

/**
 * @brief 回滚已经执行的SQL语句。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_RollBack(OH_Rdb_Store *store);

/**
 * @brief 提交已执行的SQL语句
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_Commit(OH_Rdb_Store *store);

/**
 * @brief 在开始执行SQL语句之前，开始事务, 并获得该事务的ID。
 *
 * @param store 表示一个指向 {@link OH_Rdb_Store} 实例的指针。
 * @param trxId 事务ID, 作为出参使用。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * RDB_E_NOT_SUPPORTED 表示不支持当前操作。
 * @see OH_Rdb_Store.
 * @since 14
 */
int OH_Rdb_BeginTransWithTrxId(OH_Rdb_Store *store, int64_t *trxId);

/**
 * @brief 使用指定的事务ID, 回滚已经执行的SQL语句。
 *
 * @param store 表示一个指向 {@link OH_Rdb_Store} 实例的指针。
 * @param trxId 表示需要回滚的事务的ID。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数, 可能情况如下：
 *      传入参数为空指针。
 *      当前事务ID不是调用{@link OH_Rdb_BeginTransWithTrxId}获得的。
 *      当前事务ID已经调用{@link OH_Rdb_CommitByTrxId}提交。
 *      当前事务ID已经调用{@link OH_Rdb_RollBackByTrxId}回滚。
 * RDB_E_NOT_SUPPORTED 表示不支持当前操作。
 * @see OH_Rdb_Store.
 * @since 14
 */
int OH_Rdb_RollBackByTrxId(OH_Rdb_Store *store, int64_t trxId);

/**
 * @brief 使用指定的事务ID, 提交已经执行的SQL语句。
 *
 * @param store 表示一个指向 {@link OH_Rdb_Store} 实例的指针。
 * @param trxId 表示需要提交的事务的ID。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功.
 * RDB_E_INVALID_ARGS 表示无效参数，可能情况如下：
 *      传入参数为空指针。
 *      当前事务ID不是调用{@link OH_Rdb_BeginTransWithTrxId}获得的。
 *      当前事务ID已经调用{@link OH_Rdb_CommitByTrxId}提交。
 *      当前事务ID已经调用{@link OH_Rdb_RollBackByTrxId}回滚。
 * RDB_E_NOT_SUPPORTED 表示不支持当前操作。
 * @see OH_Rdb_Store.
 * @since 14
 */
int OH_Rdb_CommitByTrxId(OH_Rdb_Store *store, int64_t trxId);

/**
 * @brief 以指定路径备份数据库。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param databasePath 指定数据库的备份文件路径。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_Backup(OH_Rdb_Store *store, const char *databasePath);

/**
 * @brief 从指定的数据库备份文件恢复数据库。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param databasePath 指定数据库的备份文件路径。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_Restore(OH_Rdb_Store *store, const char *databasePath);

/**
 * @brief 获取数据库版本。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param version 表示版本号, 作为出参使用。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_GetVersion(OH_Rdb_Store *store, int *version);

/**
 * @brief 设置数据库版本。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param version 表示版本号。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_Store.
 * @since 10
 */
int OH_Rdb_SetVersion(OH_Rdb_Store *store, int version);

/**
 * @brief 描述表的分布式类型的枚举。
 *
 * @since 11
 */
typedef enum Rdb_DistributedType {
    /** 表示在设备和云端之间分布式的数据库表。 */
    RDB_DISTRIBUTED_CLOUD
} Rdb_DistributedType;

/**
 * @brief 描述{@link Rdb_DistributedConfig}的版本。
 *
 * @since 11
 */
#define DISTRIBUTED_CONFIG_VERSION 1

/**
 * @brief 记录表的分布式配置信息。
 *
 * @since 11
 */
typedef struct Rdb_DistributedConfig {
    /** 用于唯一标识Rdb_DistributedConfig结构的版本。 */
    int version;
    /** 表示该表是否支持自动同步。 */
    bool isAutoSync;
} Rdb_DistributedConfig;

/**
 * @brief 设置分布式数据库表。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param tables 要设置的分布式数据库表表名。
 * @param count 要设置的分布式数据库表的数量。
 * @param type 表的分布式类型 {@link Rdb_DistributedType}。
 * @param config 表的分布式配置信息。{@link Rdb_DistributedConfig}。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_Store.
 * @since 11
 */
int OH_Rdb_SetDistributedTables(OH_Rdb_Store *store, const char *tables[], uint32_t count, Rdb_DistributedType type,
    const Rdb_DistributedConfig *config);

/**
 * @brief 获取数据库表中数据的最后修改时间。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param tableName 要查找的分布式数据库表表名。
 * @param columnName 指定要查询的数据库表的列名。
 * @param values 指定要查询的行的主键。如果数据库表无主键，参数columnName需传入"rowid"，此时values为要查询的数据库表的行号。
 * @return 如果操作成功则返回一个指向{@link OH_Rdb_Store}结构体实例的指针，否则返回NULL。
 * @see OH_Rdb_Store.
 * @since 11
 */
OH_Cursor *OH_Rdb_FindModifyTime(OH_Rdb_Store *store, const char *tableName, const char *columnName,
    OH_VObject *values);

/**
 * @brief 描述数据变更类型。
 *
 * @since 11
 */
typedef enum Rdb_ChangeType {
    /** 表示是数据发生变更。 */
    RDB_DATA_CHANGE,
    /** 表示是资产附件发生了变更。 */
    RDB_ASSET_CHANGE
} Rdb_ChangeType;

/**
 * @brief 描述发生变化的行的主键或者行号。
 *
 * @since 11
 */
typedef struct Rdb_KeyInfo {
    /** 表示发生变化的主键或者行号的数量。 */
    int count;
    /** 表示主键的类型{@link OH_ColumnType}。 */
    int type;
    /**
     * @brief 存放变化的具体数据
     *
     * @since 11
     */
    union Rdb_KeyData {
        /** 存放uint64_t类型的数据。 */
        uint64_t integer;
        /** 存放double类型的数据。 */
        double real;
        /** 存放char *类型的数据。 */
        const char *text;
    } *data;
} Rdb_KeyInfo;

/**
 * @brief 描述{@link Rdb_ChangeInfo}的版本。
 *
 * @since 11
 */
#define DISTRIBUTED_CHANGE_INFO_VERSION 1

/**
 * @brief 记录端云同步过程详情。
 *
 * @since 11
 */
typedef struct Rdb_ChangeInfo {
    /** 用于唯一标识Rdb_DistributedConfig结构的版本。 */
    int version;
    /** 表示发生变化的表的名称。 */
    const char *tableName;
    /** 表示发生变化的数据的类型，数据或者资产附件发生变化。 */
    int ChangeType;
    /** 记录插入数据的位置，如果该表的主键是string类型，该值是主键的值，否则该值表示插入数据的行号。 */
    Rdb_KeyInfo inserted;
    /** 记录更新数据的位置，如果该表的主键是string类型，该值是主键的值，否则该值表示更新数据的行号。 */
    Rdb_KeyInfo updated;
    /** 记录删除数据的位置，如果该表的主键是string类型，该值是主键的值，否则该值表示删除数据的行号。 */
    Rdb_KeyInfo deleted;
} Rdb_ChangeInfo;

/**
 * @brief 描述订阅类型。
 *
 * @since 11
 */
typedef enum Rdb_SubscribeType {
    /** 订阅云端数据更改。 */
    RDB_SUBSCRIBE_TYPE_CLOUD,
    /** 订阅云端数据更改详情。 */
    RDB_SUBSCRIBE_TYPE_CLOUD_DETAILS,
    /**
     * 订阅本地数据更改详情。
     * @since 12
     */
    RDB_SUBSCRIBE_TYPE_LOCAL_DETAILS,
} Rdb_SubscribeType;

/**
 * @brief 端云数据更改事件的回调函数。
 *
 * @param context 表示数据观察者的上下文。
 * @param values 表示更改的端云帐户。
 * @param count 表示更改的端云帐户数量。
 * @since 11
 */
typedef void (*Rdb_BriefObserver)(void *context, const char *values[], uint32_t count);

/**
 * @brief 端云数据更改事件的细节的回调函数。
 *
 * @param context 表示数据观察者的上下文。
 * @param changeInfo 表示已更改表的信息{@link Rdb_ChangeInfo}。
 * @param count 表示更改的表的数量。
 * @see Rdb_ChangeInfo.
 * @since 11
 */
typedef void (*Rdb_DetailsObserver)(void *context, const Rdb_ChangeInfo **changeInfo, uint32_t count);

/**
 * @brief 表示回调函数。
 *
 * @since 11
 */
typedef union Rdb_SubscribeCallback {
    /** 端云数据更改事件的细节的回调函数。 */
    Rdb_DetailsObserver detailsObserver;

    /** 端云数据更改事件的回调函数。 */
    Rdb_BriefObserver briefObserver;
} Rdb_SubscribeCallback;

/**
 * @brief 表示数据观察者。
 *
 * @since 11
 */
typedef struct Rdb_DataObserver {
    /** 表示数据观察者的上下文。 */
    void *context;

    /** 数据观察者的回调。 */
    Rdb_SubscribeCallback callback;
} Rdb_DataObserver;

/**
 * @brief 为数据库注册观察者。当分布式数据库或本地数据库中的数据发生更改时，将调用回调。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param type 表示在{@link Rdb_SubscribeType}中定义的订阅类型。如果其值为RDB_SUBSCRIBE_TYPE_LOCAL_DETAILS，则在本地数据库中的数据更改时调用回调。
 * @param observer 数据库中更改事件的观察者{@link Rdb_DataObserver}。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_Store.
 * @see Rdb_DataObserver.
 * @since 11
 */
int OH_Rdb_Subscribe(OH_Rdb_Store *store, Rdb_SubscribeType type, const Rdb_DataObserver *observer);

/**
 * @brief 从数据库中删除指定类型的指定观察者。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针.
 * @param type 表示在{@link Rdb_SubscribeType}中定义的订阅类型。
 * @param observer 数据库中更改事件的观察者{@link Rdb_DataObserver}。如果这是nullptr，表示删除该类型的所有观察者。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_Store.
 * @see Rdb_DataObserver.
 * @since 11
 */
int OH_Rdb_Unsubscribe(OH_Rdb_Store *store, Rdb_SubscribeType type, const Rdb_DataObserver *observer);

/**
 * @brief 表示数据库的同步模式
 *
 * @since 11
 */
typedef enum Rdb_SyncMode {
    /** 表示数据从修改时间较近的一端同步到修改时间较远的一端。 */
    RDB_SYNC_MODE_TIME_FIRST,
    /** 表示数据从本地设备同步到云端。 */
    RDB_SYNC_MODE_NATIVE_FIRST,
    /** 表示数据从云端同步到本地设备。 */
    RDB_SYNC_MODE_CLOUD_FIRST
} Rdb_SyncMode;

/**
 * @brief 描述数据库表的端云同步过程的统计信息。
 *
 * @since 11
 */
typedef struct Rdb_Statistic {
    /** 表示数据库表中需要端云同步的总行数。 */
    int total;
    /** 表示数据库表中端云同步成功的行数。 */
    int successful;
    /** 表示数据库表中端云同步失败的行数。 */
    int failed;
    /** 表示数据库表中端云同步剩余未执行的行数。 */
    int remained;
} Rdb_Statistic;

/**
 * @brief 描述数据库表执行端云同步任务上传和下载的统计信息。
 *
 * @since 11
 */
typedef struct Rdb_TableDetails {
    /** 数据库表名 */
    const char *table;
    /** 表示数据库表中端云同步上传过程的统计信息。 */
    Rdb_Statistic upload;
    /** 表示数据库表中端云同步下载过程的统计信息。 */
    Rdb_Statistic download;
} Rdb_TableDetails;

/**
 * 描述端云同步过程。
 *
 * @since 11
 */
typedef enum Rdb_Progress {
    /** 表示端云同步过程开始。 */
    RDB_SYNC_BEGIN,
    /** 表示正在端云同步过程中。 */
    RDB_SYNC_IN_PROGRESS,
    /** 表示端云同步过程已完成。 */
    RDB_SYNC_FINISH
} Rdb_Progress;


/**
 * 表示端云同步过程的状态。
 *
 * @since 11
 */
typedef enum Rdb_ProgressCode {
    /** 表示端云同步过程成功。 */
    RDB_SUCCESS,
    /** 表示端云同步过程遇到未知错误。 */
    RDB_UNKNOWN_ERROR,
    /** 表示端云同步过程遇到网络错误。 */
    RDB_NETWORK_ERROR,
    /** 表示云端不可用。 */
    RDB_CLOUD_DISABLED,
    /** 表示有其他设备正在端云同步，本设备无法进行端云同步。 */
    RDB_LOCKED_BY_OTHERS,
    /** 表示本次端云同步需要同步的条目或大小超出最大值。由云端配置最大值。 */
    RDB_RECORD_LIMIT_EXCEEDED,
    /** 表示云空间剩余空间小于待同步的资产大小。 */
    RDB_NO_SPACE_FOR_ASSET
} Rdb_ProgressCode;

/**
 * @brief 描述{@link OH_ProgressDetails}的版本。
 *
 * @since 11
 */
#define DISTRIBUTED_PROGRESS_DETAIL_VERSION 1

/**
 * @brief 描述数据库整体执行端云同步任务上传和下载的统计信息。
 *
 * @since 11
 */
typedef struct Rdb_ProgressDetails {
    /** 用于唯一标识OH_TableDetails结构的版本。 */
    int version;
    /** 表示端云同步过程。 */
    int schedule;
    /** 表示端云同步过程的状态。 */
    int code;
    /** 表示端云同步的表的数量 */
    int32_t tableLength;
} Rdb_ProgressDetails;

/**
 * @brief 从端云同步任务的统计信息中获取数据库表的统计信息。
 *
 * @param progress 表示指向{@link OH_ProgressDetails}实例的指针。
 * @param version 表示当前{@link Rdb_ProgressDetails}的版本。
 * @return 如果操作成功，会返回一个{@link Rdb_TableDetails}结构体的指针，否则返回NULL。
 * @see Rdb_ProgressDetails
 * @see Rdb_TableDetails
 * @since 11
 */
Rdb_TableDetails *OH_Rdb_GetTableDetails(Rdb_ProgressDetails *progress, int32_t version);

/**
 * @brief 端云同步进度的回调函数。
 *
 * @param progressDetails 端云同步进度的详细信息。
 * @see Rdb_ProgressDetails.
 * @since 11
 */
typedef void (*Rdb_ProgressCallback)(void *context, Rdb_ProgressDetails *progressDetails);

/**
 * @brief 数据库端云同步的回调函数。
 *
 * @param progressDetails 数据库端云同步的统计信息。
 * @see OH_Rdb_Store.
 * @since 11
 */
typedef void (*Rdb_SyncCallback)(Rdb_ProgressDetails *progressDetails);

/**
 * @brief 端云同步进度观察者。
 *
 * @since 11
 */
typedef struct Rdb_ProgressObserver {
    /** 端云同步进度观察者的上下文。 */
    void *context;

    /** 端云同步进度观察者的回调函数。 */
    Rdb_ProgressCallback callback;
} Rdb_ProgressObserver;

/**
 * @brief 进行端云同步。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param mode 表示同步过程的类型{@link Rdb_SyncMode}.
 * @param tables 表示需要同步的表名。
 * @param count 同步的表的数量，如果传入的值为0，同步数据库的所有表。
 * @param observer 端云同步进度的观察者{@link Rdb_ProgressObserver}。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_Store.
 * @since 11
 */
int OH_Rdb_CloudSync(OH_Rdb_Store *store, Rdb_SyncMode mode, const char *tables, int count,
    const Rdb_ProgressObserver *observer);

/**
 * @brief 订阅RDB存储的自动同步进度。
 * 当收到自动同步进度的通知时，将调用回调。
 *
 * @param store 表示指向目标{@Link OH_Rdb_Store}实例的指针。
 * @param observer 用于自动同步进度的观察者{@link Rdb_ProgressObserver}。表示调用返回自动同步进度的回调。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_Store.
 * @see Rdb_ProgressObserver.
 * @since 11
 **/
int OH_Rdb_SubscribeAutoSyncProgress(OH_Rdb_Store *store, const Rdb_ProgressObserver *observer);

/**
 * @brief 取消订阅RDB存储的自动同步进程。
 *
 * @param store 表示指向目标{@Link OH_Rdb_Store}实例的指针。
 * @param observer 表示自动同步进度的观察者{@link Rdb_ProgressObserver}。如果是空指针，则自动同步进程的所有回调都将被取消注册。
 * @return 返回操作是否成功，出错时返回对应的错误码。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_Store.
 * @see Rdb_ProgressObserver.
 * @since 11
 */
int OH_Rdb_UnsubscribeAutoSyncProgress(OH_Rdb_Store *store, const Rdb_ProgressObserver *observer);

/**
 * @brief 根据指定的条件锁定数据库中的数据，锁定数据不执行端云同步。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param predicates 表示指向{@link OH_Predicates}实例的指针，指定锁定条件。
 * @return 返回锁定结果。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_Store, OH_Predicates.
 * @since 12
 */
int OH_Rdb_LockRow(OH_Rdb_Store *store, OH_Predicates *predicates);

/**
 * @brief 根据指定的条件锁解锁数据库中的数据。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param predicates 表示指向{@link OH_Predicates}实例的指针，指定解锁条件。
 * @return 返回解锁结果。
 * RDB_OK 表示成功。
 * RDB_E_INVALID_ARGS 表示无效参数。
 * @see OH_Rdb_Store, OH_Predicates.
 * @since 12
 */
int OH_Rdb_UnlockRow(OH_Rdb_Store *store, OH_Predicates *predicates);

/**
 * @brief 根据指定条件查询数据库中锁定的数据。
 *
 * @param store 表示指向{@link OH_Rdb_Store}实例的指针。
 * @param predicates 表示指向{@link OH_Predicates}实例的指针，指定查询条件。
 * @param columnNames 表示要查询的列。如果值为空，则查询应用于所有列。
 * @param length 表示columnNames数组的长度。若length大于columnNames数组的实际长度，则会访问越界。
 * @return 如果查询成功则返回一个指向{@link OH_Cursor}结构体实例的指针，否则返回NULL。
 * @see OH_Rdb_Store, OH_Predicates, OH_Cursor.
 * @since 12
 */
OH_Cursor *OH_Rdb_QueryLockedRow(
    OH_Rdb_Store *store, OH_Predicates *predicates, const char *const *columnNames, int length);

#ifdef __cplusplus
};
#endif

#endif // RELATIONAL_STORE_H
