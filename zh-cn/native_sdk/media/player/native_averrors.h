/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Core
 * @{
 *
 * @brief Core模块提供用于播放框架的基础骨干能力，包含内存、错误码、格式载体等相关函数。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 *
 * @since 9
 * @version 1.0
 */

/**
 * @file native_averrors.h
 *
 * @brief 声明了媒体播放框架的错误码。
 *
 * @since 9
 * @version 1.0
 */

#ifndef NATIVE_AVERRORS_H
#define NATIVE_AVERRORS_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 音视频错误码。
 * 
 * @syscap SystemCapability.Multimedia.Media.Core
 * @since 9
 * @version 1.0
 */
typedef enum OH_AVErrCode {
    /**
     * @error 操作成功
     */
    AV_ERR_OK = 0,
    /**
     * @error 无内存
     */
    AV_ERR_NO_MEMORY = 1,
    /**
     * @error 无效参数
     */
    AV_ERR_OPERATE_NOT_PERMIT = 2,
    /**
     * @error 无效值
     */
    AV_ERR_INVALID_VAL = 3,
    /**
     * @error IO 错误
     */
    AV_ERR_IO = 4,
    /**
     * @error 超时错误
     */
    AV_ERR_TIMEOUT = 5,
    /**
     * @error 未知错误
     */
    AV_ERR_UNKNOWN = 6,
    /**
     * @error 媒体服务死亡
     */
    AV_ERR_SERVICE_DIED = 7,
    /**
     * @error 当前状态不支持此操作
     */
    AV_ERR_INVALID_STATE = 8,
    /**
     * @error 未支持的接口
     */
    AV_ERR_UNSUPPORT = 9,
    /**
     * @error 扩展错误码初始值
     */
    AV_ERR_EXTEND_START = 100,
} OH_AVErrCode;

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVERRORS_H