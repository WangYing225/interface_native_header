/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OHAVSession
 * @{
 *
 * @brief 提供播控模块C接口定义。
 *
 * @syscap SystemCapability.Multimedia.AVSession.Core
 *
 * @since 13
 * @version 1.0
 */

/**
 * @file native_avsession.h
 *
 * @brief 媒体会话定义，可用于设置元数据、播放状态信息等操作。
 *
 * @library libohavsession.so
 * @syscap SystemCapability.Multimedia.AVSession.Core
 * @kit AVSessionKit
 * @since 13
 * @version 1.0
 */

#ifndef NATIVE_AVSESSION_H
#define NATIVE_AVSESSION_H

#include <stdint.h>
#include "native_avsession_errors.h"
#include "native_avmetadata.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 会话类型。
 *
 * @since 13
 * @version 1.0
 */
typedef enum {
    /**
     * @brief 音频。
     */
    SESSION_TYPE_AUDIO = 0,

    /**
     * @brief 视频。
     */
    SESSION_TYPE_VIDEO = 1,

    /**
     * @brief 音频通话。
     */
    SESSION_TYPE_VOICE_CALL = 2,

    /**
     * @brief 视频通话。
     */
    SESSION_TYPE_VIDEO_CALL = 3
} AVSession_Type;

/**
 * @brief 媒体播放状态的相关属性。
 *
 * @since 13
 * @version 1.0
 */
typedef enum {
    /**
     * @brief 初始状态。
     */
    PLAYBACK_STATE_INITIAL = 0,

    /**
     * @brief 准备状态。
     */
    PLAYBACK_STATE_PREPARING = 1,

    /**
     * @brief 正在播放。
     */
    PLAYBACK_STATE_PLAYING = 2,

    /**
     * @brief 暂停。
     */
    PLAYBACK_STATE_PAUSED = 3,

    /**
     * @brief 快进。
     */
    PLAYBACK_STATE_FAST_FORWARDING = 4,

    /**
     * @brief 快退。
     */
    PLAYBACK_STATE_REWINDED = 5,

    /**
     * @brief 停止。
     */
    PLAYBACK_STATE_STOPPED = 6,

    /**
     * @brief 播放完成。
     */
    PLAYBACK_STATE_COMPLETED = 7,

    /**
     * @brief 释放。
     */
    PLAYBACK_STATE_RELEASED = 8,

    /**
     * @brief 错误。
     */
    PLAYBACK_STATE_ERROR = 9,

    /**
     * @brief 空闲。
     */
    PLAYBACK_STATE_IDLE = 10,

    /**
     * @brief 缓冲。
     */
    PLAYBACK_STATE_BUFFERING = 11,

    /**
     * @brief 最大状态。（当state为12时，返回错误码401）
     */
    PLAYBACK_STATE_MAX = 12,
} AVSession_PlaybackState;

/**
 * @brief 媒体播放位置的相关属性。
 *
 * @since 13
 */
typedef struct AVSession_PlaybackPosition {
    /**
     * @brief 已用时间，单位毫秒（ms）。
     */
    int64_t elapsedTime;

    /**
     * @brief 更新时间，单位毫秒（ms）。
     */
    int64_t updateTime;
} AVSession_PlaybackPosition;

/**
 * @brief 媒体播放循环模式。
 *
 * @since 13
 */
typedef enum {
    /**
     * @brief 顺序播放。
     */
    LOOP_MODE_SEQUENCE = 0,

    /**
     * @brief 单曲循环。
     */
    LOOP_MODE_SINGLE = 1,

    /**
     * @brief 表单循环。
     */
    LOOP_MODE_LIST = 2,

    /**
     * @brief 随机播放。
     */
    LOOP_MODE_SHUFFLE = 3,

    /**
     * @brief 自定义播放。
     */
    LOOP_MODE_CUSTOM = 4,
} AVSession_LoopMode;

/**
 * @brief 播控命令。
 *
 * @since 13
 * @version 1.0
 */
typedef enum AVSession_ControlCommand {
    /**
     * @brief 无效控制命令。
     */
    CONTROL_CMD_INVALID = -1,

    /**
     * @brief 播放命令。
     */
    CONTROL_CMD_PLAY = 0,

    /**
     * @brief 暂停命令。
     */
    CONTROL_CMD_PAUSE = 1,

    /**
     * @brief 停止命令。
     */
    CONTROL_CMD_STOP = 2,

    /**
     * @brief 播放下一首命令。
     */
    CONTROL_CMD_PLAY_NEXT = 3,

    /**
     * @brief 播放上一首命令。
     */
    CONTROL_CMD_PLAY_PREVIOUS = 4,
} AVSession_ControlCommand;

/**
 * @brief 回调执行的结果。
 *
 * @since 13
 */
typedef enum {
    /**
     * @brief 执行成功。
     */
    AVSESSION_CALLBACK_RESULT_SUCCESS = 0,

    /**
     * @brief 执行失败。
     */
    AVSESSION_CALLBACK_RESULT_FAILURE = -1,
} AVSessionCallback_Result;

/**
 * @brief 通用的执行播控命令的回调。
 *
 * @since 13
 * @version 1.0
 */
typedef AVSessionCallback_Result (*OH_AVSessionCallback_OnCommand)(OH_AVSession* session,
    AVSession_ControlCommand command, void* userData);

/**
 * @brief 快进的回调。
 *
 * @since 13
 * @version 1.0
 */
typedef AVSessionCallback_Result (*OH_AVSessionCallback_OnFastForward)(OH_AVSession* session,
    uint32_t seekTime, void* userData);

/**
 * @brief 快退的回调。
 *
 * @since 13
 * @version 1.0
 */
typedef AVSessionCallback_Result (*OH_AVSessionCallback_OnRewind)(OH_AVSession* session,
    uint32_t seekTime, void* userData);

/**
 * @brief 进度调节的回调。
 *
 * @since 13
 * @version 1.0
 */
typedef AVSessionCallback_Result (*OH_AVSessionCallback_OnSeek)(OH_AVSession* session,
    uint64_t seekTime, void* userData);

/**
 * @brief 设置循环模式的回调。
 *
 * @since 13
 * @version 1.0
 */
typedef AVSessionCallback_Result (*OH_AVSessionCallback_OnSetLoopMode)(OH_AVSession* session,
    AVSession_LoopMode curLoopMode, void* userData);

/**
 * @brief 收藏的回调。
 *
 * @since 13
 * @version 1.0
 */
typedef AVSessionCallback_Result (*OH_AVSessionCallback_OnToggleFavorite)(OH_AVSession* session,
    const char* assetId, void* userData);

/**
 * @brief 播控会话对象定义。
 *
 * 可以用OH_AVSession_Create创建一个会话对象。
 *
 * @since 13
 * @version 1.0
 */
typedef struct OH_AVSession OH_AVSession;

/**
 * @brief 创建会话对象。
 *
 * @param sessionType  会话类型{@link AVSession_Type}。
 * @param sessionTag   会话标签。
 * @param bundleName   创建会话的包名。
 * @param abilityName  创建会话的ability名。
 * @param avsession    返回的媒体会话对象。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常或session重复创建。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}: 
 *                                                 1.参数sessionType无效。
 *                                                 2.参数sessionTag为nullptr。
 *                                                 3.参数bundleName为nullptr。
 *                                                 4.参数abilityName为nullptr。
 *                                                 5.参数avsession为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_Create(AVSession_Type sessionType, const char* sessionTag,
    const char* bundleName, const char* abilityName, OH_AVSession** avsession);

/**
 * @brief 销毁会话对象。
 *
 * @param avsession 媒体会话对象。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} 参数avsession为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_Destroy(OH_AVSession* avsession);

/**
 * @brief 激活会话。
 *
 * @param avsession 媒体会话对象。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} 参数avsession为nullptr。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_Activate(OH_AVSession* avsession);

/**
 * @brief 取消激活媒体会话。
 *
 * @param avsession 媒体会话对象。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER} 参数avsession为nullptr。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_Deactivate(OH_AVSession* avsession);

/**
 * @brief 获取会话类型。
 *
 * @param avsession 媒体会话对象。
 * @param sessionType 返回的会话类型。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常，获取session type错误。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}:
 *                                                  1.参数avsession为nullptr。
 *                                                  2.参数sessionType为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_GetSessionType(OH_AVSession* avsession, AVSession_Type* sessionType);

/**
 * @brief 获取会话id。
 *
 * @param avsession 媒体会话对象。
 * @param sessionId 返回的会话类型id。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常，获取session Id错误。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}:
 *                                                 1.参数avsession为nullptr。
 *                                                 2.参数sessionId为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_GetSessionId(OH_AVSession* avsession, const char** sessionId);

/**
 * @brief 设置媒体元数据。
 *
 * @param avsession 媒体会话对象。
 * @param avmetadata 设置媒体元数据信息。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}:
 *                                                 1.参数avsession为nullptr。
 *                                                 2.参数avmetadata为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_SetAVMetadata(OH_AVSession* avsession, OH_AVMetadata* avmetadata);

/**
 * @brief 设置播放状态。
 *
 * @param avsession 媒体会话对象。
 * @param playbackState 播放状态。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}: 
 *                                                  1.参数avsession为nullptr。
 *                                                  2.参数playbackState无效。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_SetPlaybackState(OH_AVSession* avsession,
    AVSession_PlaybackState playbackState);

/**
 * @brief 设置播放位置。
 *
 * @param avsession 媒体会话对象。
 * @param playbackPosition 播放位置对象。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}:
 *                                                 1.参数avsession为nullptr。
 *                                                 2.参数playbackPosition为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_SetPlaybackPosition(OH_AVSession* avsession,
    AVSession_PlaybackPosition* playbackPosition);

/**
 * @brief 设置收藏状态。
 *
 * @param avsession 媒体会话对象。
 * @param favorite 收藏状态，true表示收藏，false表示取消收藏。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}: 参数avsession为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_SetFavorite(OH_AVSession* avsession, bool favorite);

/**
 * @brief 设置循环模式。
 *
 * @param avsession 媒体会话对象。
 * @param loopMode 循环模式。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}: 
 *                                                   1.参数avsession为nullptr。
 *                                                   2.参数loopMode无效。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_SetLoopMode(OH_AVSession* avsession, AVSession_LoopMode loopMode);

/**
 * @brief 注册通用播控的回调。
 *
 * @param avsession 媒体会话对象。
 * @param command   播控的控制命令。
 * @param callback  控制命令的回调{@link OH_AVSessionCallback_OnCommand}。
 * @param userData  指向通过回调函数传递的应用数据指针。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_CODE_COMMAND_INVALID} 控制命令无效。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}: 
 *                                                  1.参数avsession为nullptr。
 *                                                  2.参数callback为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_RegisterCommandCallback(OH_AVSession* avsession,
    AVSession_ControlCommand command, OH_AVSessionCallback_OnCommand callback, void* userData);

/**
 * @brief 取消注册通用播控的回调。
 *
 * @param avsession 媒体会话对象。
 * @param command   播控的控制命令。
 * @param callback  控制命令的回调{@link OH_AVSessionCallback_OnCommand}。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_CODE_COMMAND_INVALID} 控制命令无效。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}: 
 *                                                  1.参数avsession为nullptr。
 *                                                  2.参数callback为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_UnregisterCommandCallback(OH_AVSession* avsession,
    AVSession_ControlCommand command, OH_AVSessionCallback_OnCommand callback);

/**
 * @brief 注册快进的回调。
 *
 * @param avsession 媒体会话对象。
 * @param callback 快进命令的回调{@link OH_AVSessionCallback_OnFastForward}。
 * @param userData 指向通过回调函数传递的应用数据指针。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}: 
 *                                                  1.参数avsession为nullptr。
 *                                                  2.参数callback为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_RegisterForwardCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnFastForward callback, void* userData);

/**
 * @brief 取消注册快进的回调。
 *
 * @param avsession 媒体会话对象。
 * @param callback 快进命令的回调{@link OH_AVSessionCallback_OnFastForward}。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}: 
 *                                                  1.参数avsession为nullptr。
 *                                                  2.参数callback为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_UnregisterForwardCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnFastForward callback);

/**
 * @brief 注册快退的回调。
 *
 * @param avsession 媒体会话对象。
 * @param callback 快退命令的回调{@link OH_AVSessionCallback_OnRewind}。
 * @param userData 指向通过回调函数传递的应用数据指针。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}: 
 *                                                  1.参数avsession为nullptr。
 *                                                  2.参数callback为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_RegisterRewindCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnRewind callback, void* userData);

/**
 * @brief 取消注册快退的回调。
 *
 * @param avsession 媒体会话对象。
 * @param callback 快退命令的回调{@link OH_AVSessionCallback_OnRewind}。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}: 
 *                                                  1.参数avsession为nullptr。
 *                                                  2.参数callback为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_UnregisterRewindCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnRewind callback);

/**
 * @brief 注册跳转的回调。
 *
 * @param avsession 媒体会话对象。
 * @param callback 跳转命令的回调{@link OH_AVSessionCallback_OnSeek}。
 * @param userData 指向通过回调函数传递的应用数据指针。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}: 
 *                                                  1.参数avsession为nullptr。
 *                                                  2.参数callback为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_RegisterSeekCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnSeek callback, void* userData);

/**
 * @brief 取消注册跳转的回调。
 *
 * @param avsession 媒体会话对象。
 * @param callback 跳转命令的回调{@link OH_AVSessionCallback_OnSeek}。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}: 
 *                                                  1.参数avsession为nullptr。
 *                                                  2.参数callback为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_UnregisterSeekCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnSeek callback);

/**
 * @brief 注册设置循环模式的回调。
 *
 * @param avsession 媒体会话对象。
 * @param callback 设置循环模式命令的回调{@link OH_AVSessionCallback_OnSetLoopMode}。
 * @param userData 指向通过回调函数传递的应用数据指针。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}: 
 *                                                  1.参数avsession为nullptr。
 *                                                  2.参数callback为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_RegisterSetLoopModeCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnSetLoopMode callback, void* userData);

/**
 * @brief 取消注册设置循环模式的回调。
 *
 * @param avsession 媒体会话对象。
 * @param callback 设置循环模式命令的回调{@link OH_AVSessionCallback_OnSetLoopMode}。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}: 
 *                                                  1.参数avsession为nullptr。
 *                                                  2.参数callback为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_UnregisterSetLoopModeCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnSetLoopMode callback);

/**
 * @brief 设置收藏的回调。
 *
 * @param avsession 媒体会话对象。
 * @param callback 设置收藏命令的回调{@link OH_AVSessionCallback_OnToggleFavorite}。
 * @param userData 指向通过回调函数传递的应用数据指针。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}: 
 *                                                  1.参数avsession为nullptr。
 *                                                  2.参数callback为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_RegisterToggleFavoriteCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnToggleFavorite callback, void* userData);

/**
 * @brief 取消设置收藏的回调。
 *
 * @param avsession 媒体会话对象。
 * @param callback 设置收藏命令的回调{@link OH_AVSessionCallback_OnToggleFavorite}。
 * @return 函数返回值：
 *         {@link AV_SESSION_ERR_SUCCESS} 函数执行成功。
 *         {@link AV_SESSION_ERR_SERVICE_EXCEPTION} 会话服务异常。
 *         {@link AV_SESSION_ERR_INVALID_PARAMETER}: 
 *                                                  1.参数avsession为nullptr。
 *                                                  2.参数callback为nullptr。
 * @since 13
 */
AVSession_ErrCode OH_AVSession_UnregisterToggleFavoriteCallback(OH_AVSession* avsession,
    OH_AVSessionCallback_OnToggleFavorite callback);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVSESSION_H
/** @} */
