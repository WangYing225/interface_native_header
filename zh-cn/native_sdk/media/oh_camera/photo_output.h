/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OH_Camera
 * @{
 *
 * @brief 为相机模块提供C接口的定义。
 *
 * @syscap SystemCapability.Multimedia.Camera.Core
 *
 * @since 11
 * @version 1.0
 */

/**
 * @file photo_output.h
 *
 * @brief 声明拍照输出概念。
 *
 * @library libohcamera.so
 * @syscap SystemCapability.Multimedia.Camera.Core
 * @since 11
 * @version 1.0
 */

#ifndef NATIVE_INCLUDE_CAMERA_PHOTOOUTPUT_H
#define NATIVE_INCLUDE_CAMERA_PHOTOOUTPUT_H

#include <stdint.h>
#include <stdio.h>
#include "camera.h"
#include "photo_native.h"
#include "multimedia/media_library/media_asset_base_capi.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 拍照输出对象
 *
 * 可以使用{@link OH_CameraManager_CreatePhotoOutput}方法创建指针。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_PhotoOutput Camera_PhotoOutput;

/**
 * @brief 在{@link PhotoOutput_Callbacks}中被调用的拍照输出帧启动回调。
 *
 * @param photoOutput 传递回调的{@link Camera_PhotoOutput}。
 * @since 11
 */
typedef void (*OH_PhotoOutput_OnFrameStart)(Camera_PhotoOutput* photoOutput);

/**
 * @brief 在{@link PhotoOutput_Callbacks}中被调用的拍照输出帧快门回调。
 *
 * @param photoOutput 传递回调的{@link Camera_PhotoOutput}。
 * @param info 回调传递的{@link Camera_FrameShutterInfo}。
 * @since 11
 */
typedef void (*OH_PhotoOutput_OnFrameShutter)(Camera_PhotoOutput* photoOutput, Camera_FrameShutterInfo* info);

/**
 * @brief 在{@link PhotoOutput_Callbacks}中被调用的拍照输出帧结束回调。
 *
 * @param photoOutput 传递回调的{@link Camera_PhotoOutput}。
 * @param frameCount 回调传递的帧计数。
 * @since 11
 */
typedef void (*OH_PhotoOutput_OnFrameEnd)(Camera_PhotoOutput* photoOutput, int32_t frameCount);

/**
 * @brief 在{@link PhotoOutput_Callbacks}中被调用的拍照输出错误回调。
 *
 * @param photoOutput 传递回调的{@link Camera_PhotoOutput}。
 * @param errorCode 拍照输出的{@link Camera_ErrorCode}。
 *
 * @see CAMERA_SERVICE_FATAL_ERROR
 * @since 11
 */
typedef void (*OH_PhotoOutput_OnError)(Camera_PhotoOutput* photoOutput, Camera_ErrorCode errorCode);

/**
 * @brief 拍照结束回调。
 *
 * @param photoOutput 传递回调的{@link Camera_PhotoOutput}。
 * @param frameCount 回调传递的帧数。
 * @since 12
 */
typedef void (*OH_PhotoOutput_CaptureEnd) (Camera_PhotoOutput* photoOutput, int32_t frameCount);

/**
 * @brief 拍照开始回调。
 *
 * @param photoOutput 传递回调的{@link Camera_PhotoOutput}。
 * @param Info 回调传递的{@link Camera_CaptureStartInfo}。
 * @since 12
 */
typedef void (*OH_PhotoOutput_CaptureStartWithInfo) (Camera_PhotoOutput* photoOutput, Camera_CaptureStartInfo* Info);

/**
 * @brief 拍照曝光结束回调。
 *
 * @param photoOutput 传递回调的{@link Camera_PhotoOutput}。
 * @param Info 回调传递的{@link Camera_FrameShutterInfo}。
 * @since 12
 */
typedef void (*OH_PhotoOutput_OnFrameShutterEnd) (Camera_PhotoOutput* photoOutput, Camera_FrameShutterInfo* Info);

/**
 * @brief 拍照准备就绪回调。收到回调后，可以继续进行下一次拍照。
 *
 * @param photoOutput 传递回调的{@link Camera_PhotoOutput}。
 * @since 12
 */
typedef void (*OH_PhotoOutput_CaptureReady) (Camera_PhotoOutput* photoOutput);

/**
 * @brief 预计拍照时间回调。
 *
 * @param photoOutput 传递回调的{@link Camera_PhotoOutput}。
 * @param duration 回调传递的预计拍照时间。
 * @since 12
 */
typedef void (*OH_PhotoOutput_EstimatedCaptureDuration) (Camera_PhotoOutput* photoOutput, int64_t duration);

/**
 * @brief 照片输出可用高分辨率图像回调。
 *
 * @param photoOutput 传递回调的{@link Camera_PhotoOutput}。
 * @param photo 回调传递的{@link OH_PhotoNative}。
 * @since 12
 */
typedef void (*OH_PhotoOutput_PhotoAvailable)(Camera_PhotoOutput* photoOutput, OH_PhotoNative* photo);

/**
 * @brief 输出照片资源可用回调。
 *
 * @param photoOutput 传递回调的{@link Camera_PhotoOutput}。
 * @param photoAsset 回调传递的{@link OH_MediaAsset}。
 * @since 12
 */
typedef void (*OH_PhotoOutput_PhotoAssetAvailable)(Camera_PhotoOutput* photoOutput, OH_MediaAsset* photoAsset);

/**
 * @brief 拍照输出的回调。
 *
 * @see OH_PhotoOutput_RegisterCallback
 * @since 11
 * @version 1.0
 */
typedef struct PhotoOutput_Callbacks {
    /**
     * 拍照输出帧启动事件。
     */
    OH_PhotoOutput_OnFrameStart onFrameStart;

    /**
     * 拍照输出框快门事件。
     */
    OH_PhotoOutput_OnFrameShutter onFrameShutter;

    /**
     * 拍照输出帧结束事件。
     */
    OH_PhotoOutput_OnFrameEnd onFrameEnd;

    /**
     * 拍照输出错误事件。
     */
    OH_PhotoOutput_OnError onError;
} PhotoOutput_Callbacks;

/**
 * @brief 注册拍照输出更改事件回调。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注册的{@link PhotoOutput_Callbacks}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 11
 */
Camera_ErrorCode OH_PhotoOutput_RegisterCallback(Camera_PhotoOutput* photoOutput, PhotoOutput_Callbacks* callback);

/**
 * @brief 注销拍照输出更改事件回调。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注销的{@link PhotoOutput_Callbacks}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 11
 */
Camera_ErrorCode OH_PhotoOutput_UnregisterCallback(Camera_PhotoOutput* photoOutput, PhotoOutput_Callbacks* callback);

/**
 * @brief 注册拍照开始事件回调。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注册的{@link OH_PhotoOutput_CaptureStartWithInfo}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_RegisterCaptureStartWithInfoCallback(Camera_PhotoOutput* photoOutput,
    OH_PhotoOutput_CaptureStartWithInfo callback);

/**
 * @brief 注销拍照开始事件回调。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注销的{@link OH_PhotoOutput_CaptureStartWithInfo}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_UnregisterCaptureStartWithInfoCallback(Camera_PhotoOutput* photoOutput,
    OH_PhotoOutput_CaptureStartWithInfo callback);

/**
 * @brief 注册拍照结束事件回调。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注册的{@link OH_PhotoOutput_CaptureEnd}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_RegisterCaptureEndCallback(Camera_PhotoOutput* photoOutput,
    OH_PhotoOutput_CaptureEnd callback);

/**
 * @brief 注销拍照结束事件回调。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注销的{@link OH_PhotoOutput_CaptureEnd}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_UnregisterCaptureEndCallback(Camera_PhotoOutput* photoOutput,
    OH_PhotoOutput_CaptureEnd callback);

/**
 * @brief 注册拍照曝光结束事件回调。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注册的{@link OH_PhotoOutput_OnFrameShutterEnd}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_RegisterFrameShutterEndCallback(Camera_PhotoOutput* photoOutput,
    OH_PhotoOutput_OnFrameShutterEnd callback);

/**
 * @brief 注销拍照曝光结束事件回调。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注销的{@link OH_PhotoOutput_OnFrameShutterEnd}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_UnregisterFrameShutterEndCallback(Camera_PhotoOutput* photoOutput,
    OH_PhotoOutput_OnFrameShutterEnd callback);

/**
 * @brief 注册拍照就绪事件回调。收到回调后，可以继续进行下一次拍照。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注册的{@link OH_PhotoOutput_CaptureReady}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_RegisterCaptureReadyCallback(Camera_PhotoOutput* photoOutput,
    OH_PhotoOutput_CaptureReady callback);

/**
 * @brief 注销拍照就绪事件回调。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注销的{@link OH_PhotoOutput_CaptureReady}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_UnregisterCaptureReadyCallback(Camera_PhotoOutput* photoOutput,
    OH_PhotoOutput_CaptureReady callback);

/**
 * @brief 注册预计拍照时间事件回调。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注册的{@link OH_PhotoOutput_EstimatedCaptureDuration}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_RegisterEstimatedCaptureDurationCallback(Camera_PhotoOutput* photoOutput,
    OH_PhotoOutput_EstimatedCaptureDuration callback);

/**
 * @brief 注销预计拍照时间事件回调。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注销的{@link OH_PhotoOutput_EstimatedCaptureDuration}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_UnregisterEstimatedCaptureDurationCallback(Camera_PhotoOutput* photoOutput,
    OH_PhotoOutput_EstimatedCaptureDuration callback);

/**
 * @brief 注册输出照片可用回调。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注册的{@link OH_PhotoOutput_PhotoAvailable}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link #CAMERA_SERVICE_FATAL_ERROR}相机服务出现致命错误。
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_RegisterPhotoAvailableCallback(Camera_PhotoOutput* photoOutput,
    OH_PhotoOutput_PhotoAvailable callback);

/**
 * @brief 注销输出照片可用回调。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注销的{@link PhotoOutput_Callbacks}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link #CAMERA_SERVICE_FATAL_ERROR}相机服务出现致命错误。
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_UnregisterPhotoAvailableCallback(Camera_PhotoOutput* photoOutput,
    OH_PhotoOutput_PhotoAvailable callback);

/**
 * @brief 注册输出照片资源可用回调。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注册的{@link OH_PhotoOutput_PhotoAssetAvailable}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_RegisterPhotoAssetAvailableCallback(Camera_PhotoOutput* photoOutput,
    OH_PhotoOutput_PhotoAssetAvailable callback);

/**
 * @brief 注销输出照片资源可用回调。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注销的{@link OH_PhotoOutput_PhotoAssetAvailable}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_UnregisterPhotoAssetAvailableCallback(Camera_PhotoOutput* photoOutput,
    OH_PhotoOutput_PhotoAssetAvailable callback);

/**
 * @brief 拍摄照片。
 *
 * @param photoOutput 用于捕获拍照的{@link Camera_PhotoOutput}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_RUNNING}如果捕获会话未运行。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_PhotoOutput_Capture(Camera_PhotoOutput* photoOutput);

/**
 * @brief 使用捕获设置捕获拍照。
 *
 * @param photoOutput 用于捕获拍照的{@link Camera_PhotoOutput}实例。
 * @param setting 用于捕获拍照的{@link Camera_PhotoCaptureSetting}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_RUNNING}如果捕获会话未运行。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_PhotoOutput_Capture_WithCaptureSetting(Camera_PhotoOutput* photoOutput,
    Camera_PhotoCaptureSetting setting);

/**
 * @brief 释放拍照输出。
 *
 * @param photoOutput 要释放的{@link Camera_PhotoOutput}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_PhotoOutput_Release(Camera_PhotoOutput* photoOutput);

/**
 * @brief 检查是否支持镜像拍照。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例，用于检查是否支持镜像。
 * @param isSupported 是否支持镜像的结果。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_PhotoOutput_IsMirrorSupported(Camera_PhotoOutput* photoOutput, bool* isSupported);

/**
 * @brief 是否启用镜像拍照。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例，用于是否启用镜像拍照。
 * @param enabled 是否启用镜像的结果，true为开启镜像拍照，false为关闭镜像拍照。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 13
 */
Camera_ErrorCode OH_PhotoOutput_EnableMirror(Camera_PhotoOutput* photoOutput, bool enabled);

/**
 * @brief 获取当前照片输出配置文件。
 *
 * @param photoOutput 传递当前配置文件的{@link Camera_PhotoOutput}实例。
 * @param profile 如果方法调用成功，则将记录{@link Camera_Profile}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_GetActiveProfile(Camera_PhotoOutput* photoOutput, Camera_Profile** profile);

/**
 * @brief 删除照片配置文件实例。
 *
 * @param profile 要被删除的{@link Camera_Profile}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_DeleteProfile(Camera_Profile* profile);

/**
 * @brief 检查是否支持动态照片。
 *
 * @param photoOutput 用来检查是否支持动态照片的{@link Camera_PhotoOutput}实例。
 * @param isSupported 是否支持动态照片的结果。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_IsMovingPhotoSupported(Camera_PhotoOutput* photoOutput, bool* isSupported);

/**
 * @brief 是否启用动态照片。
 *
 * @param photoOutput 用来启用或禁用动态照片的{@link Camera_PhotoOutput}实例。
 * @param enabled 是否启用动态照片。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#CAMERA_INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @permission ohos.permission.MICROPHONE
 * @since 12
 */
Camera_ErrorCode OH_PhotoOutput_EnableMovingPhoto(Camera_PhotoOutput* photoOutput, bool enabled);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_INCLUDE_CAMERA_PHOTOOUTPUT_H
/** @} */