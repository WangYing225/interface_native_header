/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup image
 * @{
 *
 * @brief 提供图像Native Api接口。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */

/**
 * @file image_source_mdk.h
 *
 * @brief 声明将图片源解码成像素位图的方法。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */

#ifndef INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_SOURCE_MDK_H_
#define INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_SOURCE_MDK_H_
#include <cstdint>
#include "napi/native_api.h"
#include "image_mdk_common.h"
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 为图像源方法定义native层图像源对象。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct ImageSourceNative_;

/**
 * @brief 为图像源方法定义native层图像源对象。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
typedef struct ImageSourceNative_ ImageSourceNative;

/**
 * @brief 定义每个样本比特的图像属性关键字。
 * 此标签给{@link OH_ImageSource_GetImageProperty}和{@link OH_ImageSource_ModifyImageProperty}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_BITS_PER_SAMPLE = "BitsPerSample";

/**
 * @brief 定义方向的图像属性关键字。
 * 此标签给{@link OH_ImageSource_GetImageProperty}和{@link OH_ImageSource_ModifyImageProperty}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_ORIENTATION = "Orientation";

/**
 * @brief 定义图像长度的图像属性关键字。
 * 此标签给{@link OH_ImageSource_GetImageProperty}和{@link OH_ImageSource_ModifyImageProperty}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_IMAGE_LENGTH = "ImageLength";

/**
 * @brief 定义图像宽度的图像属性关键字。
 * 此标签给{@link OH_ImageSource_GetImageProperty}和{@link OH_ImageSource_ModifyImageProperty}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_IMAGE_WIDTH = "ImageWidth";

/**
 * @brief 定义GPS纬度的图像属性关键字。
 * 此标签给{@link OH_ImageSource_GetImageProperty}和{@link OH_ImageSource_ModifyImageProperty}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_GPS_LATITUDE = "GPSLatitude";

/**
 * @brief 定义GPS经度的图像属性关键字。
 * 此标签给{@link OH_ImageSource_GetImageProperty}和{@link OH_ImageSource_ModifyImageProperty}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_GPS_LONGITUDE = "GPSLongitude";

/**
 * @brief 定义GPS纬度参考的图像属性关键字。
 * 此标签给{@link OH_ImageSource_GetImageProperty}和{@link OH_ImageSource_ModifyImageProperty}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_GPS_LATITUDE_REF = "GPSLatitudeRef";

/**
 * @brief 定义GPS经度参考的图像属性关键字。
 * 此标签给{@link OH_ImageSource_GetImageProperty}和{@link OH_ImageSource_ModifyImageProperty}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_GPS_LONGITUDE_REF = "GPSLongitudeRef";

/**
 * @brief 定义初始日期时间的图像属性关键字。
 * 此标签给{@link OH_ImageSource_GetImageProperty}和{@link OH_ImageSource_ModifyImageProperty}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_DATE_TIME_ORIGINAL = "DateTimeOriginal";

/**
 * @brief 定义曝光时间的图像属性关键字。
 * 此标签给{@link OH_ImageSource_GetImageProperty}和{@link OH_ImageSource_ModifyImageProperty}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_EXPOSURE_TIME = "ExposureTime";

/**
 * @brief 定义场景类型的图像属性关键字。
 * 此标签给{@link OH_ImageSource_GetImageProperty}和{@link OH_ImageSource_ModifyImageProperty}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_SCENE_TYPE = "SceneType";

/**
 * @brief 定义ISO速度等级的图像属性关键字。
 * 此标签给{@link OH_ImageSource_GetImageProperty}和{@link OH_ImageSource_ModifyImageProperty}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_ISO_SPEED_RATINGS = "ISOSpeedRatings";

/**
 * @brief 定义FNumber的图像属性关键字。
 * 此标签给{@link OH_ImageSource_GetImageProperty}和{@link OH_ImageSource_ModifyImageProperty}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_F_NUMBER = "FNumber";

/**
 * @brief 定义每个像素的压缩比特的图像属性关键字。
 * 此标签给{@link OH_ImageSource_GetImageProperty}和{@link OH_ImageSource_ModifyImageProperty}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
const char* OHOS_IMAGE_PROPERTY_COMPRESSED_BITS_PER_PIXEL = "CompressedBitsPerPixel";

/**
 * @brief 定义图像源解码的范围选项。
 * {@link OhosImageDecodingOps}, {@link OH_ImageSource_CreatePixelMap} and
 * {@link OH_ImageSource_CreatePixelMapList}.
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageRegion {
    /** 起始x坐标，用pixels表示 */
    int32_t x;
    /** 起始y坐标，用pixels表示 */
    int32_t y;
    /** 宽度范围，用pixels表示 */
    int32_t width;
    /** 高度范围，用pixels表示 */
    int32_t height;
};

/**
 * @brief 定义图像源选项信息。
 * 此选项给{@link OH_ImageSource_Create}和{@link OH_ImageSource_CreateIncremental}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageSourceOps {
    /** 图像源像素密度 */
    int32_t density;
    /** 图像源像素格式，通常用于描述YUV缓冲区 */
    int32_t pixelFormat;
    /** 图像源像素宽高的大小 */
    struct OhosImageSize size;
};

/**
 * @brief 定义图像源解码选项。
 * 此选项给{@link OH_ImageSource_CreatePixelMap}和{@link OH_ImageSource_CreatePixelMapList}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageDecodingOps {
    /** 定义输出的像素位图是否可编辑 */
    int8_t editable;
    /** 定义输出的像素格式 */
    int32_t pixelFormat;
    /** 定义解码目标的像素密度 */
    int32_t fitDensity;
    /** 定义图像源解码指数 */
    uint32_t index;
    /** 定义解码样本大小选项 */
    uint32_t sampleSize;
    /** 定义解码旋转选项 */
    uint32_t rotate;
    /** 定义解码目标像素宽高的大小 */
    struct OhosImageSize size;
    /** 定义图像源解码的像素范围 */
    struct OhosImageRegion region;
};

/**
 * @brief 定义图像源信息，由{@link OH_ImageSource_GetImageInfo}获取。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageSourceInfo {
    /** 图像源像素格式, 由 {@link OH_ImageSource_Create} 设置 */
    int32_t pixelFormat;
    /** 图像源色彩空间 */
    int32_t colorSpace;
    /** 图像源透明度类型 */
    int32_t alphaType;
    /** 图像源密度, 由 {@link OH_ImageSource_Create} 设置 */
    int32_t density;
    /** 图像源像素宽高的大小 */
    struct OhosImageSize size;
};

/**
 * @brief 定义图像源输入资源，每次仅接收一种类型。由{@link OH_ImageSource_Create}获取。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 * @deprecated since 11
 */
struct OhosImageSource {
    /** 图像源资源标识符，接受文件资源或者base64资源 */
    char* uri = nullptr;
    /** 图像源资源长度 */
    size_t uriSize = 0;
    /** 图像源文件资源描述符 */
    int32_t fd = -1;
    /** 图像源缓冲区资源，解手格式化包缓冲区或者base64缓冲区 */
    uint8_t* buffer = nullptr;
    /** 图像源缓冲区资源大小 */
    size_t bufferSize = 0;
};

/**
 * @brief 定义图像源延迟时间列表。由{@link OH_ImageSource_GetDelayTime}获取。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageSourceDelayTimeList {
    /** 图像源延迟时间列表头地址 */
    int32_t* delayTimeList;
    /** 图像源延迟时间列表大小 */
    size_t size = 0;
};

/**
 * @brief 定义图像源支持的格式字符串。
 * 此选项给{@link OhosImageSourceSupportedFormatList}和{@link OH_ImageSource_GetSupportedFormats}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageSourceSupportedFormat {
    /** 图像源支持的格式字符串头地址 */
    char* format = nullptr;
    /** 图像源支持的格式字符串大小 */
    size_t size = 0;
};

/**
 * @brief 定义图像源支持的格式字符串列表。
 * 由{@link OH_ImageSource_GetSupportedFormats}获取
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageSourceSupportedFormatList {
    /** 图像源支持的格式字符串列表头地址 */
    struct OhosImageSourceSupportedFormat** supportedFormatList = nullptr;
    /** 图像源支持的格式字符串列表大小 */
    size_t size = 0;
};

/**
 * @brief 定义图像源属性键值字符串。
 * 此选项给{@link OH_ImageSource_GetImageProperty} and {@link OH_ImageSource_ModifyImageProperty}这两个接口使用。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageSourceProperty {
    /** 定义图像源属性键值字符串头地址 */
    char* value = nullptr;
    /** 定义图像源属性键值字符串大小 */
    size_t size = 0;
};

/**
 * @brief 定义图像源更新数据选项，由{@link OH_ImageSource_UpdateData}获取。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
struct OhosImageSourceUpdateData {
    /** 图像源更新数据缓冲区 */
    uint8_t* buffer = nullptr;
    /** 图像源更新数据缓冲区大小 */
    size_t bufferSize = 0;
    /** 图像源更新数据缓冲区的开端 */
    uint32_t offset = 0;
    /** 图像源更新数据缓冲区的更新数据长度 */
    uint32_t updateLength = 0;
    /** 图像源更新数据在此节中完成 */
    int8_t isCompleted = 0;
};

/**
 * @brief 通过给定的信息{@link OhosImageSource} and {@link OhosImageSourceOps}结构体，获取JavaScript native层API<b>ImageSource</b>对象。
 *
 * @param env 表明JNI环境的指针。
 * @param src 表明创建一个图像源的信息。查看{@link OhosImageSource}获取更多细节。
 * @param ops 表明创建一个图像源的选项。查看{@link OhosImageSourceOps}。
 * @param res 表明JavaScript native层API<b>ImageSource</b>对象的指针。
 * @return 如果操作成功返回{@link OHOS_IMAGE_RESULT_SUCCESS}；
 * 如果参数错误，返回{@link IMAGE_RESULT_BAD_PARAMETER}；
 * 如果 JNI 环境异常，返回{@link IMAGE_RESULT_JNI_ENV_ABNORMAL}；
 * 如果参数无效，{@link IMAGE_RESULT_INVALID_PARAMETER}；
 * 如果图像源数据不完整，返回{@link IMAGE_RESULT_SOURCE_DATA_INCOMPLETE}；
 * 如果图像源数据错误，返回{@link IMAGE_RESULT_SOURCE_DATA}；
 * 如果图像获取数据错误，返回{@link IMAGE_RESULT_GET_DATA_ABNORMAL}；
 * 如果图像数据太大，返回{@link IMAGE_RESULT_TOO_LARGE}；
 * 如果解码失败，返回{@link IMAGE_RESULT_DECODE_FAILED}；
 * 如果图像解码头错误，返回{@link IMAGE_RESULT_DECODE_HEAD_ABNORMAL}；
 * 如果图像解码 EXIF 不支持，返回{@link IMAGE_RESULT_DECODE_EXIF_UNSUPPORT}；
 * 如果图像属性不存在，返回{@link IMAGE_RESULT_PROPERTY_NOT_EXIST}；
 * 如果文件损坏，返回{@link IMAGE_RESULT_FILE_DAMAGED}；
 * 如果文件 FD 错误，返回{@link IMAGE_RESULT_FILE_FD_ERROR}；
 * 如果数据流错误，返回{@link IMAGE_RESULT_STREAM_SIZE_ERROR}；
 * 如果查找文件失败，返回{@link IMAGE_RESULT_SEEK_FAILED}；
 * 如果速览文件失败，返回{@link IMAGE_RESULT_PEEK_FAILED}；
 * 如果读取文件失败，返回{@link IMAGE_RESULT_FREAD_FAILED}。
 * @see {@link OhosImageSource}, {@link OhosImageSourceOps}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 * @deprecated since 11
 * @useinstead image#OH_ImageSource_CreateFromUri
 * @useinstead image#OH_ImageSource_CreateFromFd
 * @useinstead image#OH_ImageSource_CreateFromData
 */
int32_t OH_ImageSource_Create(napi_env env, struct OhosImageSource* src,
    struct OhosImageSourceOps* ops, napi_value *res);

/**
 * @brief 通过给定的标识符URI 和 {@link OhosImageSourceOps}结构体，获取JavaScript native层API<b>ImageSource</b>对象。
 *
 * @param env 表明JNI环境的指针。
 * @param uri 表明图像源资源标识符，接受文件资源或者base64资源.
 * @param size 表明图像源资源URI的长度.
 * @param ops 表明创建一个图像源的选项。查看{@link OhosImageSourceOps}。
 * @param res 表明JavaScript native层API<b>ImageSource</b>对象的指针。
 * @return 如果操作成功返回{@link OHOS_IMAGE_RESULT_SUCCESS}；
 * 如果参数错误，返回{@link IMAGE_RESULT_BAD_PARAMETER}；
 * 如果 JNI 环境异常，返回{@link IMAGE_RESULT_JNI_ENV_ABNORMAL}；
 * 如果参数无效，{@link IMAGE_RESULT_INVALID_PARAMETER}；
 * @see {@link OhosImageSourceOps}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 11
 * @version 4.1
 */
int32_t OH_ImageSource_CreateFromUri(napi_env env, char* uri, size_t size,
    struct OhosImageSourceOps* ops, napi_value *res);

/**
 * @brief 通过给定的文件描述符 fd 和 {@link OhosImageSourceOps}结构体，获取JavaScript native层API<b>ImageSource</b>对象。
 *
 * @param env 表明JNI环境的指针。
 * @param fd 表明图像源文件资源描述符。
 * @param ops 表明创建一个图像源的选项。查看{@link OhosImageSourceOps}。
 * @param res 表明JavaScript native层API<b>ImageSource</b>对象的指针。
 * @return 如果操作成功返回{@link OHOS_IMAGE_RESULT_SUCCESS}；
 * 如果参数错误，返回{@link IMAGE_RESULT_BAD_PARAMETER}；
 * 如果 JNI 环境异常，返回{@link IMAGE_RESULT_JNI_ENV_ABNORMAL}；
 * 如果参数无效，{@link IMAGE_RESULT_INVALID_PARAMETER}；
 * @see {@link OhosImageSourceOps}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 11
 * @version 4.1
 */
int32_t OH_ImageSource_CreateFromFd(napi_env env, int32_t fd,
    struct OhosImageSourceOps* ops, napi_value *res);

/**
 * @brief 通过给定的图像源缓冲区资源 data 和 {@link OhosImageSourceOps}结构体，获取JavaScript native层API<b>ImageSource</b>对象。
 *
 * @param env 表明JNI环境的指针。
 * @param data 表明图像源缓冲区资源，接受格式化包缓冲区或者base64缓冲区。
 * @param dataSize 表明图像源缓冲区资源大小。
 * @param ops 表明创建一个图像源的选项。查看{@link OhosImageSourceOps}。
 * @param res 表明JavaScript native层API<b>ImageSource</b>对象的指针。
 * @return 如果操作成功返回{@link OHOS_IMAGE_RESULT_SUCCESS}；
 * 如果参数错误，返回{@link IMAGE_RESULT_BAD_PARAMETER}；
 * 如果 JNI 环境异常，返回{@link IMAGE_RESULT_JNI_ENV_ABNORMAL}；
 * 如果参数无效，{@link IMAGE_RESULT_INVALID_PARAMETER}；
 * @see {@link OhosImageSourceOps}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 11
 * @version 4.1
 */
int32_t OH_ImageSource_CreateFromData(napi_env env, uint8_t* data, size_t dataSize,
    struct OhosImageSourceOps* ops, napi_value *res);

/**
 * @brief 通过给定的资源描述符 {@link RawFileDescriptor} 和 {@link OhosImageSourceOps}结构体，
 * 获取JavaScript native层API<b>ImageSource</b>对象。
 *
 * @param env 表明JNI环境的指针。
 * @param rawFile 表明图像源资源描述符。
 * @param ops 表明创建一个图像源的选项。查看{@link OhosImageSourceOps}。
 * @param res 表明JavaScript native层API<b>ImageSource</b>对象的指针。
 * @return 如果操作成功返回{@link OHOS_IMAGE_RESULT_SUCCESS}；
 * 如果参数错误，返回{@link IMAGE_RESULT_BAD_PARAMETER}；
 * 如果 JNI 环境异常，返回{@link IMAGE_RESULT_JNI_ENV_ABNORMAL}；
 * 如果参数无效，{@link IMAGE_RESULT_INVALID_PARAMETER}；
 * @see {@link OhosImageSourceOps}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 11
 * @version 4.1
 */
int32_t OH_ImageSource_CreateFromRawFile(napi_env env, RawFileDescriptor rawFile,
    struct OhosImageSourceOps* ops, napi_value *res);

/**
 * @brief 通过给定的infomations{@link OhosImageSource}和{@link OhosImageSourceOps}结构，
 * 获取增量类型的JavaScript Native API ImageSource对象，图像数据应通过{@link-OH_ImageSource_UpdateData}更新。
 *
 * @param env 表明JNI环境的指针。
 * @param src 表明创建一个图像源的信息。这里只接收缓冲区类型。查看{@link OhosImageSource}获取更多细节
 * @param ops 表明创建一个图像源的选项。查看{@link OhosImageSourceOps}。
 * @param res 表明JavaScript native层API<b>ImageSource</b>对象的指针。
 * @return 如果操作成功返回{@link OHOS_IMAGE_RESULT_SUCCESS}；
 * 如果参数错误，返回{@link IMAGE_RESULT_BAD_PARAMETER}；
 * 如果 JNI 环境异常，返回{@link IMAGE_RESULT_JNI_ENV_ABNORMAL}；
 * 如果参数无效，{@link IMAGE_RESULT_INVALID_PARAMETER}；
 * 如果图像源数据不完整，返回{@link IMAGE_RESULT_SOURCE_DATA_INCOMPLETE}；
 * 如果图像源数据错误，返回{@link IMAGE_RESULT_SOURCE_DATA}；
 * 如果图像获取数据错误，返回{@link IMAGE_RESULT_GET_DATA_ABNORMAL}；
 * 如果图像数据太大，返回{@link IMAGE_RESULT_TOO_LARGE}；
 * 如果解码失败，返回{@link IMAGE_RESULT_DECODE_FAILED}；
 * 如果图像解码头错误，返回{@link IMAGE_RESULT_DECODE_HEAD_ABNORMAL}；
 * 如果图像解码 EXIF 不支持，返回{@link IMAGE_RESULT_DECODE_EXIF_UNSUPPORT}；
 * 如果图像属性不存在，返回{@link IMAGE_RESULT_PROPERTY_NOT_EXIST}；
 * 如果文件损坏，返回{@link IMAGE_RESULT_FILE_DAMAGED}；
 * 如果文件 FD 错误，返回{@link IMAGE_RESULT_FILE_FD_ERROR}；
 * 如果数据流错误，返回{@link IMAGE_RESULT_STREAM_SIZE_ERROR}；
 * 如果查找文件失败，返回{@link IMAGE_RESULT_SEEK_FAILED}；
 * 如果速览文件失败，返回{@link IMAGE_RESULT_PEEK_FAILED}；
 * 如果读取文件失败，返回{@link IMAGE_RESULT_FREAD_FAILED}。
 * @see {@link OhosImageSource}, {@link OhosImageSourceOps}, {@link OH_ImageSource_UpdateData}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 * @deprecated since 11
 * @useinstead image#OH_ImageSource_CreateIncrementalFromData
 */
int32_t OH_ImageSource_CreateIncremental(napi_env env, struct OhosImageSource* source,
    struct OhosImageSourceOps* ops, napi_value *res);

/**
 * @brief 通过给定的图像源缓冲区资源 data 和 {@link OhosImageSourceOps}结构体，
 * 获取增量类型的JavaScript Native API ImageSource对象，图像数据应通过{@link-OH_ImageSource_UpdateData}更新。
 *
 * @param env 表明JNI环境的指针。
 * @param data 表明图像源缓冲区资源，接受格式化包缓冲区或者base64缓冲区。
 * @param dataSize 表明图像源缓冲区资源大小。
 * @param ops 表明创建一个图像源的选项。查看{@link OhosImageSourceOps}。
 * @param res 表明JavaScript native层API<b>ImageSource</b>对象的指针。
 * @return 如果操作成功返回{@link OHOS_IMAGE_RESULT_SUCCESS}；
 * 如果参数错误，返回{@link IMAGE_RESULT_BAD_PARAMETER}；
 * 如果 JNI 环境异常，返回{@link IMAGE_RESULT_JNI_ENV_ABNORMAL}；
 * 如果参数无效，{@link IMAGE_RESULT_INVALID_PARAMETER}；
 * @see {@link OhosImageSourceOps}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 11
 * @version 4.1
 */
int32_t OH_ImageSource_CreateIncrementalFromData(napi_env env, uint8_t* data, size_t dataSize,
    struct OhosImageSourceOps* ops, napi_value *res);

/**
 * @brief 获取所有支持的解码格式元标记。
 *
 * @param res 表明指向<b>OhosImageSourceSupportedFormatList</b>结构的列表指针。
 * 当<b>supportedFormatList</b>为nullptr并且<b>size</b>以res为0作为输入时，它将以res<b>size</b>返回支持的格式大小。
 * 为了获得所有的格式标记，它需要比<b>supportedFormatList</b>中的结果大小大的足够空间，
 * 还需要为{@link-OhosImageSourceSupportedFormat}项目中的每个<b>格式</b>提供足够的空间。
 * @return 如果操作成功返回{@link OHOS_IMAGE_RESULT_SUCCESS}；
 * 如果参数错误，返回{@link IMAGE_RESULT_BAD_PARAMETER}；
 * 如果 JNI 环境异常，返回{@link IMAGE_RESULT_JNI_ENV_ABNORMAL}；
 * 如果参数无效，{@link IMAGE_RESULT_INVALID_PARAMETER}；
 * 如果解码失败，返回{@link IMAGE_RESULT_DECODE_FAILED}；
 * 如果检查格式不对，返回{@IMAGE_RESULT_CHECK_FORMAT_ERROR }。
 * @see {@link OhosImageSourceSupportedFormatList}, {@link OhosImageSourceSupportedFormat}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_GetSupportedFormats(struct OhosImageSourceSupportedFormatList* res);
/**
 * @brief 从输入JavaScript native层API <b>ImageSource</b> 对象中，转换成{@link ImageSourceNative}值。
 *
 * @param env 表明JNI环境的指针。
 * @param source 表明JavaScript native层API<b>ImageSource</b>对象的指针。
 * @return 如果操作成功返回{@link ImageSourceNative}指针；如果操作失败，返回空指针。
 * @see {@link ImageSourceNative}, {@link OH_ImageSource_Release}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
ImageSourceNative* OH_ImageSource_InitNative(napi_env env, napi_value source);

/**
 * @brief 通过一个给定的选项{@link OhosImageDecodingOps}结构体，从<b>ImageSource</b>中解码JavaScript native层API<b>PixelMap</b>对象
 *
 * @param native 表明native层{@link ImageSourceNative}值的指针。
 * @param ops 表明为了解码图像源的选项，查看{@link OhosImageDecodingOps}。
 * @param res 表明JavaScript native层API<b>PixelMap</b>对象的指针。
 * @return 如果操作成功返回{@link OHOS_IMAGE_RESULT_SUCCESS}；
 * 如果参数错误，返回{@link IMAGE_RESULT_BAD_PARAMETER}；
 * 如果 JNI 环境异常，返回{@link IMAGE_RESULT_JNI_ENV_ABNORMAL}；
 * 如果参数无效，{@link IMAGE_RESULT_INVALID_PARAMETER}；
 * 如果获取图片数据异常，返回 {@link IMAGE_RESULT_GET_DATA_ABNORMAL}；
 * 如果解码失败，返回{@link IMAGE_RESULT_DECODE_FAILED}；
 * 如果图像解码头错误，返回{@link IMAGE_RESULT_DECODE_HEAD_ABNORMAL}；
 * 如果创建解码器失败，返回 {@link IMAGE_RESULT_CREATE_DECODER_FAILED}；
 * 如果创建编码器失败，返回 {@link IMAGE_RESULT_CREATE_ENCODER_FAILED}；
 * 如果检查格式不对，返回{@IMAGE_RESULT_CHECK_FORMAT_ERROR }；
 * 如果skia错误，返回 {@link IMAGE_RESULT_THIRDPART_SKIA_ERROR}；
 * 如果输入图片数据错误，返回 {@link IMAGE_RESULT_DATA_ABNORMAL}。
 * 如果共享内存错误，返回 {@link IMAGE_RESULT_ERR_SHAMEM_NOT_EXIST}；
 * 如果共享内存数据异常，返回 {@link IMAGE_RESULT_ERR_SHAMEM_DATA_ABNORMAL}；
 * 如果图片解码异常，返回 {@link IMAGE_RESULT_DECODE_ABNORMAL}；
 * 如果图像错误，返回 {@link IMAGE_RESULT_MALLOC_ABNORMAL}；
 * 如果图片初始化错误，返回 {@link IMAGE_RESULT_DATA_UNSUPPORT}；
 * 如果图片输入数据错误，返回 {@link IMAGE_RESULT_INIT_ABNORMAL}；
 * 如果裁剪错误，返回 {@link IMAGE_RESULT_CROP}；
 * 如果图片格式未知，返回 {@link IMAGE_RESULT_UNKNOWN_FORMAT}；
 * 如果注册插件失败，返回 {@link IMAGE_RESULT_PLUGIN_REGISTER_FAILED}；
 * 如果创建插件失败。返回 {@link IMAGE_RESULT_PLUGIN_CREATE_FAILED}；
 * 如果增加位图失败，返回 {@link IMAGE_RESULT_ENCODE_FAILED}；
 * 如果不支持图片硬解码，返回 {@link IMAGE_RESULT_HW_DECODE_UNSUPPORT}；
 * 如果硬解码失败，返回 {@link IMAGE_RESULT_HW_DECODE_FAILED}；
 * 如果ipc失败，返回 {@link IMAGE_RESULT_ERR_IPC}；
 * 如果索引无效，返回 {@link IMAGE_RESULT_INDEX_INVALID}；
 * 如果硬解码失败，返回 {@link IMAGE_RESULT_ALPHA_TYPE_ERROR}；
 * 如果硬解码失败，返回 {@link IMAGE_RESULT_ALLOCATER_TYPE_ERROR}。
 * @see {@link ImageSourceNative}, {@link OhosImageDecodingOps}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_CreatePixelMap(const ImageSourceNative* native,
    struct OhosImageDecodingOps* ops, napi_value *res);

/**
 * @brief 通过一个给定的选项{@link OhosImageDecodingOps}结构体，从<b>ImageSource</b>中解码所有的JavaScript native层API<b>PixelMap</b>对象列表
 *
 * @param native 表明native层 {@link ImageSourceNative} 值的指针。
 * @param ops 表明为了解码图像源的选项，查看{@link OhosImageDecodingOps}。
 * @param res 表明JavaScript native层API<b>PixelMap</b> 列表对象的指针。
 * @return 如果操作成功返回{@link OHOS_IMAGE_RESULT_SUCCESS}；
 * 如果参数错误，返回{@link IMAGE_RESULT_BAD_PARAMETER}；
 * 如果 JNI 环境异常，返回{@link IMAGE_RESULT_JNI_ENV_ABNORMAL}；
 * 如果参数无效，{@link IMAGE_RESULT_INVALID_PARAMETER}；
 * 如果获取图片数据异常，返回 {@link IMAGE_RESULT_GET_DATA_ABNORMAL}；
 * 如果解码失败，返回{@link IMAGE_RESULT_DECODE_FAILED}；
 * 如果图像解码头错误，返回{@link IMAGE_RESULT_DECODE_HEAD_ABNORMAL}；
 * 如果创建解码器失败，返回 {@link IMAGE_RESULT_CREATE_DECODER_FAILED}；
 * 如果创建编码器失败，返回 {@link IMAGE_RESULT_CREATE_ENCODER_FAILED}；
 * 如果检查格式不对，返回{@IMAGE_RESULT_CHECK_FORMAT_ERROR }；
 * 如果skia错误，返回 {@link IMAGE_RESULT_THIRDPART_SKIA_ERROR}；
 * 如果输入图片数据错误，返回 {@link IMAGE_RESULT_DATA_ABNORMAL}。
 * 如果共享内存错误，返回 {@link IMAGE_RESULT_ERR_SHAMEM_NOT_EXIST}；
 * 如果共享内存数据异常，返回 {@link IMAGE_RESULT_ERR_SHAMEM_DATA_ABNORMAL}；
 * 如果图片解码异常，返回 {@link IMAGE_RESULT_DECODE_ABNORMAL}；
 * 如果图像错误，返回 {@link IMAGE_RESULT_MALLOC_ABNORMAL}；
 * 如果图片初始化错误，返回 {@link IMAGE_RESULT_DATA_UNSUPPORT}；
 * 如果图片输入数据错误，返回 {@link IMAGE_RESULT_INIT_ABNORMAL}；
 * 如果裁剪错误，返回 {@link IMAGE_RESULT_CROP}；
 * 如果图片格式未知，返回 {@link IMAGE_RESULT_UNKNOWN_FORMAT}；
 * 如果注册插件失败，返回 {@link IMAGE_RESULT_PLUGIN_REGISTER_FAILED}；
 * 如果创建插件失败。返回 {@link IMAGE_RESULT_PLUGIN_CREATE_FAILED}；
 * 如果增加位图失败，返回 {@link IMAGE_RESULT_ENCODE_FAILED}；
 * 如果不支持图片硬解码，返回 {@link IMAGE_RESULT_HW_DECODE_UNSUPPORT}；
 * 如果硬解码失败，返回 {@link IMAGE_RESULT_HW_DECODE_FAILED}；
 * 如果ipc失败，返回 {@link IMAGE_RESULT_ERR_IPC}；
 * 如果索引无效，返回 {@link IMAGE_RESULT_INDEX_INVALID}；
 * 如果硬解码失败，返回 {@link IMAGE_RESULT_ALPHA_TYPE_ERROR}；
 * 如果硬解码失败，返回 {@link IMAGE_RESULT_ALLOCATER_TYPE_ERROR}；
 * 如果解码的EXIF不支持，返回 {@link IMAGE_RESULT_DECODE_EXIF_UNSUPPORT}；
 * 如果图片属性不存在，返回 {@link IMAGE_RESULT_PROPERTY_NOT_EXIST}。
 * @see {@link ImageSourceNative}, {@link OhosImageDecodingOps}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_CreatePixelMapList(const ImageSourceNative* native,
    struct OhosImageDecodingOps* ops, napi_value *res);

/**
 * @brief 从一些<b>ImageSource</b>（如GIF图像源）获取延迟时间列表。
 *
 * @param native 表明native层 {@link ImageSourceNative} 值的指针。
 * @param res 表明延迟时间列表 {@link OhosImageSourceDelayTimeList} 的指针。
 * 当输入的res中<b>delayTimeList</b>是空指针并且<b>size</b>是0时，将通过res的<b>size</b>中返回延迟时间列表大小
 * 为了获取延迟时间，需要比返回的<b>delayTimeList</b>大小值大的足够空间
 * @return 如果操作成功返回{@link OHOS_IMAGE_RESULT_SUCCESS}；
 * 如果参数错误，返回{@link IMAGE_RESULT_BAD_PARAMETER}；
 * 如果 JNI 环境异常，返回{@link IMAGE_RESULT_JNI_ENV_ABNORMAL}；
 * 如果参数无效，{@link IMAGE_RESULT_INVALID_PARAMETER}；
 * 如果获取图片数据异常，返回 {@link IMAGE_RESULT_GET_DATA_ABNORMAL}；
 * 如果解码失败，返回{@link IMAGE_RESULT_DECODE_FAILED}；
 * 如果图像解码头错误，返回{@link IMAGE_RESULT_DECODE_HEAD_ABNORMAL}；
 * 如果创建解码器失败，返回 {@link IMAGE_RESULT_CREATE_DECODER_FAILED}；
 * 如果skia错误，返回 {@link IMAGE_RESULT_THIRDPART_SKIA_ERROR}；
 * 如果输入图片数据错误，返回 {@link IMAGE_RESULT_DATA_ABNORMAL}；
 * 如果图片解码异常， {@link IMAGE_RESULT_DECODE_ABNORMAL}；
 * 如果图片初始化错误，返回 {@link IMAGE_RESULT_DATA_UNSUPPORT}；
 * 如果图片格式未知，返回 {@link IMAGE_RESULT_UNKNOWN_FORMAT}；
 * 如果注册插件失败，返回 {@link IMAGE_RESULT_PLUGIN_REGISTER_FAILED}；
 * 如果创建插件失败。返回 {@link IMAGE_RESULT_PLUGIN_CREATE_FAILED}；
 * 如果索引无效，返回 {@link IMAGE_RESULT_INDEX_INVALID}；
 * 如果解码的EXIF不支持，返回 {@link IMAGE_RESULT_DECODE_EXIF_UNSUPPORT}；
 * 如果图片属性不存在，返回 {@link IMAGE_RESULT_PROPERTY_NOT_EXIST}。
 * @see {@link ImageSourceNative}, {@link OhosImageSourceDelayTimeList}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_GetDelayTime(const ImageSourceNative* native,
    struct OhosImageSourceDelayTimeList* res);

/**
 * @brief 从<b>ImageSource</b>中获取帧计数。
 *
 * @param native 表明native层 {@link ImageSourceNative} 值的指针。
 * @param res 表明帧计数的指针。
 * @return 如果操作成功返回{@link OHOS_IMAGE_RESULT_SUCCESS}；
 * 如果参数错误，返回{@link IMAGE_RESULT_BAD_PARAMETER}；
 * 如果 JNI 环境异常，返回{@link IMAGE_RESULT_JNI_ENV_ABNORMAL}；
 * 如果参数无效，{@link IMAGE_RESULT_INVALID_PARAMETER}；
 * 如果获取图片数据异常，返回 {@link IMAGE_RESULT_GET_DATA_ABNORMAL}；
 * 如果解码失败，返回{@link IMAGE_RESULT_DECODE_FAILED}；
 * 如果图像解码头错误，返回{@link IMAGE_RESULT_DECODE_HEAD_ABNORMAL}；
 * 如果创建解码器失败，返回 {@link IMAGE_RESULT_CREATE_DECODER_FAILED}；
 * 如果skia错误，返回 {@link IMAGE_RESULT_THIRDPART_SKIA_ERROR}；
 * 如果输入图片数据错误，返回 {@link IMAGE_RESULT_DATA_ABNORMAL}。
 * 如果图片解码异常， {@link IMAGE_RESULT_DECODE_ABNORMAL}；
 * 如果图片初始化错误，返回 {@link IMAGE_RESULT_DATA_UNSUPPORT}；
 * 如果图片格式未知，返回 {@link IMAGE_RESULT_UNKNOWN_FORMAT}；
 * 如果注册插件失败，返回 {@link IMAGE_RESULT_PLUGIN_REGISTER_FAILED}；
 * 如果创建插件失败。返回 {@link IMAGE_RESULT_PLUGIN_CREATE_FAILED}；
 * 如果索引无效，返回 {@link IMAGE_RESULT_INDEX_INVALID}；
 * 如果解码的EXIF不支持，返回 {@link IMAGE_RESULT_DECODE_EXIF_UNSUPPORT}；
 * 如果图片属性不存在，返回 {@link IMAGE_RESULT_PROPERTY_NOT_EXIST}。
 * @see {@link ImageSourceNative}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_GetFrameCount(const ImageSourceNative* native, uint32_t *res);

/**
 * @brief 通过索引从<b>ImageSource</b>获取图像源信息。
 *
 * @param native 表明native层 {@link ImageSourceNative} 值的指针。
 * @param index 表明帧计数的指针。
 * @param info 表明图像源信息{@link OhosImageSourceInfo}的指针。
 * @return 如果操作成功返回{@link OHOS_IMAGE_RESULT_SUCCESS}；
 * 如果参数错误，返回{@link IMAGE_RESULT_BAD_PARAMETER}；
 * 如果 JNI 环境异常，返回{@link IMAGE_RESULT_JNI_ENV_ABNORMAL}；
 * 如果参数无效，{@link IMAGE_RESULT_INVALID_PARAMETER}；
 * 如果获取图片数据异常，返回 {@link IMAGE_RESULT_GET_DATA_ABNORMAL}；
 * 如果解码失败，返回{@link IMAGE_RESULT_DECODE_FAILED}；
 * 如果图像解码头错误，返回{@link IMAGE_RESULT_DECODE_HEAD_ABNORMAL}；
 * 如果创建解码器失败，返回 {@link IMAGE_RESULT_CREATE_DECODER_FAILED}；
 * 如果skia错误，返回 {@link IMAGE_RESULT_THIRDPART_SKIA_ERROR}；
 * 如果输入图片数据错误，返回 {@link IMAGE_RESULT_DATA_ABNORMAL}。
 * 如果图片解码异常， {@link IMAGE_RESULT_DECODE_ABNORMAL}；
 * 如果图片初始化错误，返回 {@link IMAGE_RESULT_DATA_UNSUPPORT}；
 * 如果图片格式未知，返回 {@link IMAGE_RESULT_UNKNOWN_FORMAT}；
 * 如果注册插件失败，返回 {@link IMAGE_RESULT_PLUGIN_REGISTER_FAILED}；
 * 如果创建插件失败。返回 {@link IMAGE_RESULT_PLUGIN_CREATE_FAILED}；
 * 如果索引无效，返回 {@link IMAGE_RESULT_INDEX_INVALID}；
 * 如果解码的EXIF不支持，返回 {@link IMAGE_RESULT_DECODE_EXIF_UNSUPPORT}；
 * 如果图片属性不存在，返回 {@link IMAGE_RESULT_PROPERTY_NOT_EXIST}。
 * @see {@link ImageSourceNative}, {@link OhosImageSourceInfo}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_GetImageInfo(const ImageSourceNative* native, int32_t index,
    struct OhosImageSourceInfo* info);

/**
 * @brief 通过关键字从<b>ImageSource</b>中获取图像源属性。
 *
 * @param native 表明native层 {@link ImageSourceNative} 值的指针。
 * @param key 表明属性关键字{@link OhosImageSourceProperty}的指针。
 * @param value 表明作为结果的属性值{@link OhosImageSourceProperty}的指针。
 * 当输入的value中<b>value</b>是空指针并且<b>size</b>是0时，将通过value中的<b>size</b>返回属性值的大小。
 * 为了获取属性值，需要比<b>value</b>中的结果大小大的足够的空间。
 * @return 如果操作成功返回{@link OHOS_IMAGE_RESULT_SUCCESS}；
 * 如果参数错误，返回{@link IMAGE_RESULT_BAD_PARAMETER}；
 * 如果 JNI 环境异常，返回{@link IMAGE_RESULT_JNI_ENV_ABNORMAL}；
 * 如果参数无效，{@link IMAGE_RESULT_INVALID_PARAMETER}；
 * 如果获取图片数据异常，返回 {@link IMAGE_RESULT_GET_DATA_ABNORMAL}；
 * 如果解码失败，返回{@link IMAGE_RESULT_DECODE_FAILED}；
 * 如果图像解码头错误，返回{@link IMAGE_RESULT_DECODE_HEAD_ABNORMAL}；
 * 如果创建解码器失败，返回 {@link IMAGE_RESULT_CREATE_DECODER_FAILED}；
 * 如果skia错误，返回 {@link IMAGE_RESULT_THIRDPART_SKIA_ERROR}；
 * 如果输入图片数据错误，返回 {@link IMAGE_RESULT_DATA_ABNORMAL}。
 * 如果图片解码异常， {@link IMAGE_RESULT_DECODE_ABNORMAL}；
 * 如果图片初始化错误，返回 {@link IMAGE_RESULT_DATA_UNSUPPORT}；
 * 如果图片格式未知，返回 {@link IMAGE_RESULT_UNKNOWN_FORMAT}；
 * 如果注册插件失败，返回 {@link IMAGE_RESULT_PLUGIN_REGISTER_FAILED}；
 * 如果创建插件失败。返回 {@link IMAGE_RESULT_PLUGIN_CREATE_FAILED}；
 * 如果索引无效，返回 {@link IMAGE_RESULT_INDEX_INVALID}；
 * 如果解码的EXIF不支持，返回 {@link IMAGE_RESULT_DECODE_EXIF_UNSUPPORT}；
 * 如果图片属性不存在，返回 {@link IMAGE_RESULT_PROPERTY_NOT_EXIST}。
 * @see {@link ImageSourceNative}, {@link OhosImageSourceProperty}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_GetImageProperty(const ImageSourceNative* native,
    struct OhosImageSourceProperty* key, struct OhosImageSourceProperty* value);

/**
 * @brief 通过关键字为<b>ImageSource</b>修改图像源属性。
 *
 * @param native 表明native层 {@link ImageSourceNative} 值的指针
 * @param key 表明属性关键字{@link OhosImageSourceProperty}的指针。
 * @param value 为了修改表明属性值{@link OhosImageSourceProperty}的指针。
 * @return 如果操作成功返回{@link OHOS_IMAGE_RESULT_SUCCESS}；
 * 如果参数错误，返回{@link IMAGE_RESULT_BAD_PARAMETER}；
 * 如果 JNI 环境异常，返回{@link IMAGE_RESULT_JNI_ENV_ABNORMAL}；
 * 如果参数无效，{@link IMAGE_RESULT_INVALID_PARAMETER}；
 * 如果获取图片数据异常，返回 {@link IMAGE_RESULT_GET_DATA_ABNORMAL}；
 * 如果解码失败，返回{@link IMAGE_RESULT_DECODE_FAILED}；
 * 如果图像解码头错误，返回{@link IMAGE_RESULT_DECODE_HEAD_ABNORMAL}；
 * 如果创建解码器失败，返回 {@link IMAGE_RESULT_CREATE_DECODER_FAILED}；
 * 如果skia错误，返回 {@link IMAGE_RESULT_THIRDPART_SKIA_ERROR}；
 * 如果输入图片数据错误，返回 {@link IMAGE_RESULT_DATA_ABNORMAL}；
 * 如果图片解码异常， {@link IMAGE_RESULT_DECODE_ABNORMAL}；
 * 如果图片初始化错误，返回 {@link IMAGE_RESULT_DATA_UNSUPPORT}；
 * 如果图片格式未知，返回 {@link IMAGE_RESULT_UNKNOWN_FORMAT}；
 * 如果注册插件失败，返回 {@link IMAGE_RESULT_PLUGIN_REGISTER_FAILED}；
 * 如果创建插件失败。返回 {@link IMAGE_RESULT_PLUGIN_CREATE_FAILED}；
 * 如果索引无效，返回 {@link IMAGE_RESULT_INDEX_INVALID}；
 * 如果解码的EXIF不支持，返回 {@link IMAGE_RESULT_DECODE_EXIF_UNSUPPORT}；
 * 如果图片属性不存在，返回 {@link IMAGE_RESULT_PROPERTY_NOT_EXIST}。
 * @see {@link ImageSourceNative}, {@link OhosImageSourceProperty}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_ModifyImageProperty(const ImageSourceNative* native,
    struct OhosImageSourceProperty* key, struct OhosImageSourceProperty* value);

/**
 * @brief 为了增量类型的<b>ImageSource</b>更新源数据。
 *
 * @param native 表明native层 {@link ImageSourceNative} 值的指针。
 * @param data 表明更新数据信息{@link OhosImageSourceUpdateData}的指针。
 * @return 如果操作成功返回{@link OHOS_IMAGE_RESULT_SUCCESS}；
 * 如果参数错误，返回{@link IMAGE_RESULT_BAD_PARAMETER}；
 * 如果 JNI 环境异常，返回{@link IMAGE_RESULT_JNI_ENV_ABNORMAL}；
 * 如果参数无效，{@link IMAGE_RESULT_INVALID_PARAMETER}；
 * 如果获取图片数据异常，返回 {@link IMAGE_RESULT_GET_DATA_ABNORMAL}；
 * 如果解码失败，返回{@link IMAGE_RESULT_DECODE_FAILED}；
 * 如果图像解码头错误，返回{@link IMAGE_RESULT_DECODE_HEAD_ABNORMAL}；
 * 如果创建解码器失败，返回 {@link IMAGE_RESULT_CREATE_DECODER_FAILED}；
 * 如果创建编码器失败，返回 {@link IMAGE_RESULT_CREATE_ENCODER_FAILED}；
 * 如果检查格式不对，返回{@IMAGE_RESULT_CHECK_FORMAT_ERROR }；
 * 如果skia错误，返回 {@link IMAGE_RESULT_THIRDPART_SKIA_ERROR}；
 * 如果输入图片数据错误，返回 {@link IMAGE_RESULT_DATA_ABNORMAL}；
 * 如果共享内存错误，返回 {@link IMAGE_RESULT_ERR_SHAMEM_NOT_EXIST}；
 * 如果共享内存数据异常，返回 {@link IMAGE_RESULT_ERR_SHAMEM_DATA_ABNORMAL}；
 * 如果图片解码异常，返回{@link IMAGE_RESULT_DECODE_ABNORMAL}；
 * 如果图像错误，返回 {@link IMAGE_RESULT_MALLOC_ABNORMAL}；
 * 如果图片初始化错误，返回 {@link IMAGE_RESULT_DATA_UNSUPPORT}；
 * 如果图片输入数据错误，返回 {@link IMAGE_RESULT_INIT_ABNORMAL}；
 * 如果裁剪错误，返回 {@link IMAGE_RESULT_CROP}；
 * 如果图片格式未知，返回 {@link IMAGE_RESULT_UNKNOWN_FORMAT}；
 * 如果注册插件失败，返回 {@link IMAGE_RESULT_PLUGIN_REGISTER_FAILED}；
 * 如果创建插件失败。返回 {@link IMAGE_RESULT_PLUGIN_CREATE_FAILED}；
 * 如果增加位图失败，返回 {@link IMAGE_RESULT_ENCODE_FAILED}；
 * 如果不支持图片硬解码，返回 {@link IMAGE_RESULT_HW_DECODE_UNSUPPORT}；
 * 如果硬解码失败，返回 {@link IMAGE_RESULT_HW_DECODE_FAILED}；
 * 如果ipc失败，返回 {@link IMAGE_RESULT_ERR_IPC}；
 * 如果索引无效，返回 {@link IMAGE_RESULT_INDEX_INVALID}；
 * 如果硬解码失败，返回 {@link IMAGE_RESULT_ALPHA_TYPE_ERROR}；
 * 如果硬解码失败，返回 {@link IMAGE_RESULT_ALLOCATER_TYPE_ERROR}。
 * @see {@link ImageSourceNative}, {@link OhosImageSourceUpdateData}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_UpdateData(const ImageSourceNative* native, struct OhosImageSourceUpdateData* data);


/**
 * @brief 释放native层图像源 <b>ImageSourceNative</b>。
 *
 * @param native 表明native层 {@link ImageSourceNative} 值的指针。
 * @return 如果操作成功返回{@link OHOS_IMAGE_RESULT_SUCCESS}；
 * 如果参数错误，返回{@link IMAGE_RESULT_BAD_PARAMETER}；
 * 如果 JNI 环境异常，返回{@link IMAGE_RESULT_JNI_ENV_ABNORMAL}；
 * 如果参数无效，{@link IMAGE_RESULT_INVALID_PARAMETER}；
 * 如果获取图片数据异常，返回 {@link IMAGE_RESULT_GET_DATA_ABNORMAL}；
 * 如果输入图片数据错误，返回 {@link IMAGE_RESULT_DATA_ABNORMAL}。
 * @see {@link ImageSourceNative}, {@link OH_ImageSource_Create}, {@link OH_ImageSource_CreateIncremental}
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 10
 * @version 4.0
 */
int32_t OH_ImageSource_Release(ImageSourceNative* native);
#ifdef __cplusplus
};
#endif
#endif // INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_SOURCE_MDK_H_