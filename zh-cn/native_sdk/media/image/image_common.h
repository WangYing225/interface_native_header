/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Image_NativeModule
 * @{
 *
 * @brief 提供图片编解码能力。
 *
 * @since 12
 */

/**
 * @file image_common.h
 *
 * @brief 声明图像接口使用的公共枚举和结构体。
 *
 * @syscap SystemCapability.Multimedia.Image.Core
 * @since 12
 */

#ifndef INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_IMAGE_COMMON_H_
#define INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_IMAGE_COMMON_H_
#include <stdint.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 图像大小结构体。
 *
 * @since 12
 */
struct Image_Size {
    /** 图片的宽，单位：像素。 */
    uint32_t width;
    /** 图片的高，单位：像素。 */
    uint32_t height;
};

/**
 * @brief 声明图像大小结构。
 *
 * @since 12
 */
typedef struct Image_Size Image_Size;

/**
 * @brief 待解码的图像源区域结构体。
 *
 * @since 12
 */
struct Image_Region {
    /** 区域横坐标，不能大于原图的宽度。 */
    uint32_t x;
    /** 区域纵坐标，不能大于原图的高度。 */
    uint32_t y;
    /** 输出图片的宽，单位：像素。*/
    uint32_t width;
    /** 输出图片的高，单位：像素。*/
    uint32_t height;
};

/**
 * @brief 声明要解码的图像源区域结构体类型名称。
 *
 * @since 12
 */
typedef struct Image_Region Image_Region;

/**
 * @brief 字符串结构。
 *
 * @since 12
 */
struct Image_String {
    /** 字符类型数据。 */
    char *data = nullptr;
    /** 数据长度。 */
    size_t size = 0;
};

/**
 * @brief 声明用于Picture的元数据。
 *
 * @since 13
 */
struct OH_PictureMetadata;

/**
 * @brief 声明用于Picture的元数据。
 *
 * @since 13
 */
typedef struct OH_PictureMetadata OH_PictureMetadata;

/**
 * @brief 声明字符串结构的名称。
 *
 * @since 12
 */
typedef struct Image_String Image_String;

/**
 * @brief 声明一个图片格式类型的名称。
 *
 * @since 12
 */
typedef struct Image_String Image_MimeType;

/**
 * @brief 错误码。
 *
 * @since 12
 */
typedef enum {
    /**
     * @error 操作成功。
     */
    IMAGE_SUCCESS = 0,
    /**
     * @error 无效参数。
     */
    IMAGE_BAD_PARAMETER = 401,
    /**
     * @error 不支持的mime type。
     */
    IMAGE_UNSUPPORTED_MIME_TYPE = 7600101,
    /**
     * @error 未知的mime type。
     */
    IMAGE_UNKNOWN_MIME_TYPE = 7600102,
    /**
     * @error 过大的数据或图片。
     */
    IMAGE_TOO_LARGE = 7600103,
    /**
     * @error 内存不是DMA内存。
     */
    IMAGE_DMA_NOT_EXIST = 7600173,
    /**
     * @error DMA内存操作失败。
     */
    IMAGE_DMA_OPERATION_FAILED = 7600174,
    /**
     * @error 不支持的操作。
     */
    IMAGE_UNSUPPORTED_OPERATION = 7600201,
    /**
     * @error 不支持的 metadata。
     */
    IMAGE_UNSUPPORTED_METADATA = 7600202,
    /**
     * @error 不支持的转换。
     */
    IMAGE_UNSUPPORTED_CONVERSION = 7600203,
    /**
     * @error 无效区域。
     */
    IMAGE_INVALID_REGION = 7600204,
    /**
     * @error 不支持的内存格式。
     *
     * @since 13
     */
    IMAGE_UNSUPPORTED_MEMORY_FORMAT = 7600205,
    /**
     * @error 申请内存失败。
     */
    IMAGE_ALLOC_FAILED = 7600301,
    /**
     * @error 内存拷贝失败。
     */
    IMAGE_COPY_FAILED = 7600302,
    /**
     * @error 内存加锁或解锁失败。
     * @since 16
     */
    IMAGE_LOCK_UNLOCK_FAILED = 7600303,
    /** 
     * @error 未知错误。
     */
    IMAGE_UNKNOWN_ERROR = 7600901,
    /**
     * @error 解码数据源异常。
     */
    IMAGE_BAD_SOURCE = 7700101,
    /**
     * @error 解码失败。
     */
    IMAGE_DECODE_FAILED = 7700301,
    /**
     * @error 编码失败。
     */
    IMAGE_ENCODE_FAILED = 7800301,
} Image_ErrorCode;

/**
 * @brief 定义元数据类型。
 *
 * @since 13
 */
typedef enum {
    /*
    * EXIF元数据。
    */
    EXIF_METADATA = 1,
    /*
    * 水印裁剪图元数据。
    */
    FRAGMENT_METADATA = 2,
} Image_MetadataType;

/**
 * @brief 创建OH_PictureMetadata指针。
 *
 * @param metadataType 元数据的类型。
 * @param metadata 被操作的OH_PictureMetadata指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PictureMetadata_Create(Image_MetadataType metadataType, OH_PictureMetadata **metadata);

/**
 * @brief 根据key获取Metadata的单条属性。
 *
 * @param metadata 被操作的OH_PictureMetadata指针。
 * @param key 属性的键。
 * @param value 属性的值。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果是不支持的元数据类型或元数据类型与辅助图类型不匹配返回 IMAGE_UNSUPPORTED_METADATA，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PictureMetadata_GetProperty(OH_PictureMetadata *metadata, Image_String *key, Image_String *value);

/**
 * @brief 根据key修改Metadata的单条属性。
 *
 * @param metadata 被操作的OH_PictureMetadata指针。
 * @param key 属性的键。
 * @param value 属性的值。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果是不支持的元数据类型或元数据类型与辅助图类型不匹配返回 IMAGE_UNSUPPORTED_METADATA，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PictureMetadata_SetProperty(OH_PictureMetadata *metadata, Image_String *key, Image_String *value);

/**
 * @brief 释放OH_PictureMetadata指针。
 *
 * @param metadata 被操作的OH_PictureMetadata指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PictureMetadata_Release(OH_PictureMetadata *metadata);

/**
 * @brief 拷贝元数据。
 *
 * @param oldMetadata 被操作的OH_PictureMetadata指针。
 * @param newMetadata 拷贝得到的OH_PictureMetadata指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果内存分配失败返回 IMAGE_ALLOC_FAILED，如果内存拷贝失败返回 IMAGE_COPY_FAILED，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PictureMetadata_Clone(OH_PictureMetadata *oldMetadata, OH_PictureMetadata **newMetadata);

/**
 * @brief bmp图片格式。
 *
 * @since 12
 */
static const char* MIME_TYPE_BMP = "image/bmp";

/**
 * @brief jpeg图片格式。
 *
 * @since 12
 */
static const char* MIME_TYPE_JPEG = "image/jpeg";

/**
 * @brief heif图片格式。
 *
 * @since 12
 */
static const char* MIME_TYPE_HEIC = "image/heic";

/**
 * @brief png图片格式。
 *
 * @since 12
 */
static const char* MIME_TYPE_PNG = "image/png";

/**
 * @brief webp图片格式。
 *
 * @since 12
 */
static const char* MIME_TYPE_WEBP = "image/webp";

/**
 * @brief gif图片格式。
 *
 * @since 12
 */
static const char* MIME_TYPE_GIF = "image/gif";

/**
 * @brief ico图片格式。
 *
 * @since 12
 */
static const char* MIME_TYPE_ICON = "image/x-icon";

/**
 * @brief 每个像素比特数。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_BITS_PER_SAMPLE = "BitsPerSample";

/**
 * @brief 图片方向。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_ORIENTATION = "Orientation";

/**
 * @brief 图片长度。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_IMAGE_LENGTH = "ImageLength";

/**
 * @brief 图片宽度。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_IMAGE_WIDTH = "ImageWidth";

/**
 * @brief 图片纬度。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_LATITUDE = "GPSLatitude";

/**
 * @brief 图片经度。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_LONGITUDE = "GPSLongitude";

/**
 * @brief 纬度引用，例如N或S。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_LATITUDE_REF = "GPSLatitudeRef";

/**
 * @brief 经度引用，例如W或E。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_LONGITUDE_REF = "GPSLongitudeRef";

/**
 * @brief 拍摄时间，例如2022:09:06 15:48:00。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_DATE_TIME_ORIGINAL = "DateTimeOriginal";

/**
 * @brief 曝光时间，例如1/33 sec。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_EXPOSURE_TIME = "ExposureTime";

/**
 * @brief 拍摄场景模式，例如人像、风光、运动、夜景等。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SCENE_TYPE = "SceneType";

/**
 * @brief ISO感光度，例如400。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_ISO_SPEED_RATINGS = "ISOSpeedRatings";

/**
 * @brief 光圈值，例如f/1.8。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_F_NUMBER = "FNumber";

/**
 * @brief 用于压缩图像的压缩模式，单位为每像素位数。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_COMPRESSED_BITS_PER_PIXEL = "CompressedBitsPerPixel";

/**
 * @brief 图像压缩方案。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_COMPRESSION = "Compression";

/**
 * @brief 像素构成，例如RGB或YCbCr。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_PHOTOMETRIC_INTERPRETATION = "PhotometricInterpretation";

/**
 * @brief 每个strip的字节偏移量。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_STRIP_OFFSETS = "StripOffsets";

/**
 * @brief 每个像素的分量数。由于该标准适用于 RGB 和 YCbCr 图像，因此该标签的值设置为 3。
 * 在JPEG压缩数据中，使用JPEG标记代替该标签。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SAMPLES_PER_PIXEL = "SamplesPerPixel";

/**
 * @brief 每个strip的图像数据行数。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_ROWS_PER_STRIP = "RowsPerStrip";

/**
 * @brief 每个图像数据带的总字节数。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_STRIP_BYTE_COUNTS = "StripByteCounts";

/**
 * @brief 图像宽度方向的分辨率。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_X_RESOLUTION = "XResolution";

/**
 * @brief 图像高度方向的分辨率。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_Y_RESOLUTION = "YResolution";

/**
 * @brief 表示像素组件的记录格式，chunky格式或是planar格式。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_PLANAR_CONFIGURATION = "PlanarConfiguration";

/**
 * @brief 用于测量XResolution和YResolution的单位。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_RESOLUTION_UNIT = "ResolutionUnit";

/**
 * @brief 图像的传递函数，通常用于颜色校正。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_TRANSFER_FUNCTION = "TransferFunction";

/**
 * @brief 用于生成图像的软件的名称和版本。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SOFTWARE = "Software";

/**
 * @brief 创建图像的用户名称。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_ARTIST = "Artist";

/**
 * @brief 图像的白点色度。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_WHITE_POINT = "WhitePoint";

/**
 * @brief 图像的主要颜色的色度。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_PRIMARY_CHROMATICITIES = "PrimaryChromaticities";

/**
 * @brief 从RGB到YCbCr图像数据的转换矩阵系数。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_YCBCR_COEFFICIENTS = "YCbCrCoefficients";

/**
 * @brief 色度分量与亮度分量的采样比率。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_YCBCR_SUB_SAMPLING = "YCbCrSubSampling";

/**
 * @brief 色度分量相对于亮度分量的位置。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_YCBCR_POSITIONING = "YCbCrPositioning";

/**
 * @brief 参考黑点值和参考白点值。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_REFERENCE_BLACK_WHITE = "ReferenceBlackWhite";

/**
 * @brief 图像的版权信息。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_COPYRIGHT = "Copyright";

/**
 * @brief JPEG压缩缩略图数据开始字节（SOI）的偏移。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_JPEG_INTERCHANGE_FORMAT = "JPEGInterchangeFormat";

/**
 * @brief JPEG压缩缩略图数据的字节数。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_JPEG_INTERCHANGE_FORMAT_LENGTH = "JPEGInterchangeFormatLength";

/**
 * @brief 拍照时相机用来设置曝光的程序的类别。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_EXPOSURE_PROGRAM = "ExposureProgram";

/**
 * @brief 表示所用相机的每个通道的光谱灵敏度。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SPECTRAL_SENSITIVITY = "SpectralSensitivity";

/**
 * @brief 表示ISO 14524中规定的光电转换函数（OECF）。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_OECF = "OECF";

/**
 * @brief 支持的Exif标准版本。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_EXIF_VERSION = "ExifVersion";

/**
 * @brief 图像作为数字数据存储的日期和时间，格式为YYYY:MM:DD HH:MM:SS。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_DATE_TIME_DIGITIZED = "DateTimeDigitized";

/**
 * @brief 压缩数据的特定信息。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_COMPONENTS_CONFIGURATION = "ComponentsConfiguration";

/**
 * @brief 快门速度，以APEX（摄影曝光的加法系统）值表示。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SHUTTER_SPEED_VALUE = "ShutterSpeedValue";

/**
 * @brief 图像的亮度值，以APEX单位表示。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_BRIGHTNESS_VALUE = "BrightnessValue";

/**
 * @brief 最小F数镜头。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_MAX_APERTURE_VALUE = "MaxApertureValue";

/**
 * @brief 测量单位为米的主体距离。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SUBJECT_DISTANCE = "SubjectDistance";

/**
 * @brief 该标签指示整个场景中主要主体的位置和区域。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SUBJECT_AREA = "SubjectArea";

/**
 * @brief Exif/DCF制造商使用的标签，用于记录任何所需信息。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_MAKER_NOTE = "MakerNote";

/**
 * @brief 用于为DateTime标签记录秒的分数的标签。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SUBSEC_TIME = "SubsecTime";

/**
 * @brief 用于为DateTimeOriginal标签记录秒的分数的标签。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SUBSEC_TIME_ORIGINAL = "SubsecTimeOriginal";

/**
 * @brief 用于为DateTimeDigitized标签记录秒的分数的标签。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SUBSEC_TIME_DIGITIZED = "SubsecTimeDigitized";

/**
 * @brief 该标签表示FPXR文件支持的Flashpix格式版本，增强了设备兼容性。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_FLASHPIX_VERSION = "FlashpixVersion";

/**
 * @brief 色彩空间信息标签，通常记录为色彩空间指定符。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_COLOR_SPACE = "ColorSpace";

/**
 * @brief 与图像数据相关的音频文件的名称。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_RELATED_SOUND_FILE = "RelatedSoundFile";

/**
 * @brief 图像捕获时的闪光能量，以BCPS表示。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_FLASH_ENERGY = "FlashEnergy";

/**
 * @brief 相机或输入设备的空间频率表。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SPATIAL_FREQUENCY_RESPONSE = "SpatialFrequencyResponse";

/**
 * @brief 图像宽度中每FocalPlaneResolutionUnit的像素。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_FOCAL_PLANE_X_RESOLUTION = "FocalPlaneXResolution";

/**
 * @brief 图像高度中每FocalPlaneResolutionUnit的像素。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_FOCAL_PLANE_Y_RESOLUTION = "FocalPlaneYResolution";

/**
 * @brief 测量FocalPlaneXResolution和FocalPlaneYResolution的单位。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_FOCAL_PLANE_RESOLUTION_UNIT = "FocalPlaneResolutionUnit";

/**
 * @brief 主要对象相对于左边缘的位置。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SUBJECT_LOCATION = "SubjectLocation";

/**
 * @brief 捕获时选定的曝光指数。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_EXPOSURE_INDEX = "ExposureIndex";

/**
 * @brief 相机上的图像传感器类型。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SENSING_METHOD = "SensingMethod";

/**
 * @brief 表明图像来源。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_FILE_SOURCE = "FileSource";

/**
 * @brief 图像传感器的色彩滤光片（CFA）几何图案。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_CFA_PATTERN = "CFAPattern";

/**
 * @brief 指示图像数据上的特殊处理。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_CUSTOM_RENDERED = "CustomRendered";

/**
 * @brief 拍摄时设置的曝光模式。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_EXPOSURE_MODE = "ExposureMode";

/**
 * @brief 捕获时的数字变焦比率。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_DIGITAL_ZOOM_RATIO = "DigitalZoomRatio";

/**
 * @brief 捕获的场景类型。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SCENE_CAPTURE_TYPE = "SceneCaptureType";

/**
 * @brief 整体图像增益调整的程度。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GAIN_CONTROL = "GainControl";

/**
 * @brief 相机应用的对比度处理方向。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_CONTRAST = "Contrast";

/**
 * @brief 相机应用的饱和度处理方向。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SATURATION = "Saturation";

/**
 * @brief 相机应用的锐度处理方向。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SHARPNESS = "Sharpness";

/**
 * @brief 特定相机模型的拍照条件信息。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_DEVICE_SETTING_DESCRIPTION = "DeviceSettingDescription";

/**
 * @brief 表示主体到相机的距离范围。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SUBJECT_DISTANCE_RANGE = "SubjectDistanceRange";

/**
 * @brief 为每张图片唯一分配的标识符。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_IMAGE_UNIQUE_ID = "ImageUniqueID";

/**
 * @brief GPSInfoIFD的版本。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_VERSION_ID = "GPSVersionID";

/**
 * @brief 用于GPS高度的参照高度。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_ALTITUDE_REF = "GPSAltitudeRef";

/**
 * @brief 基于GPSAltitudeRef的高度。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_ALTITUDE = "GPSAltitude";

/**
 * @brief 用于测量的GPS卫星。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_SATELLITES = "GPSSatellites";

/**
 * @brief 录制图像时GPS接收器的状态。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_STATUS = "GPSStatus";

/**
 * @brief GPS测量模式。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_MEASURE_MODE = "GPSMeasureMode";

/**
 * @brief GPS DOP（数据精度等级）。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_DOP = "GPSDOP";

/**
 * @brief 用来表示GPS接收器移动速度的单位。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_SPEED_REF = "GPSSpeedRef";

/**
 * @brief GPS接收器的移动速度。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_SPEED = "GPSSpeed";

/**
 * @brief GPS接收机移动方向的参照。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_TRACK_REF = "GPSTrackRef";

/**
 * @brief GPS接收机的移动方向。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_TRACK = "GPSTrack";

/**
 * @brief 图像方向的参照。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_IMG_DIRECTION_REF = "GPSImgDirectionRef";

/**
 * @brief 拍摄时图像的方向。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_IMG_DIRECTION = "GPSImgDirection";

/**
 * @brief GPS接收器使用的大地测量数据。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_MAP_DATUM = "GPSMapDatum";

/**
 * @brief 目的地点的纬度参照。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_DEST_LATITUDE_REF = "GPSDestLatitudeRef";

/**
 * @brief 目的地点的纬度。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_DEST_LATITUDE = "GPSDestLatitude";

/**
 * @brief 目的地点的经度参照。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_DEST_LONGITUDE_REF = "GPSDestLongitudeRef";

/**
 * @brief 记录定位方法名的字符字符串。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_PROCESSING_METHOD = "GPSProcessingMethod";

/**
 * @brief 记录GPS区域名的字符字符串。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_AREA_INFORMATION = "GPSAreaInformation";

/**
 * @brief 此字段表示GPS数据是否应用了差分校正，对于精确的位置准确性至关重要。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_DIFFERENTIAL = "GPSDifferential";

/**
 * @brief 相机机身的序列号。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_BODY_SERIAL_NUMBER = "BodySerialNumber";

/**
 * @brief 相机所有者的姓名。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_CAMERA_OWNER_NAME = "CameraOwnerName";

/**
 * @brief 表示图像是否为合成图像。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_COMPOSITE_IMAGE = "CompositeImage";

/**
 * @brief DNG版本标签编码了符合DNG规范的四级版本号。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_DNG_VERSION = "DNGVersion";

/**
 * @brief 目的地点的经度。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_DEST_LONGITUDE = "GPSDestLongitude";

/**
 * @brief 指向目的地点的方位参照。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_DEST_BEARING_REF = "GPSDestBearingRef";

/**
 * @brief 目的地方位。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_DEST_BEARING = "GPSDestBearing";

/**
 * @brief 目标点距离的测量单位。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_DEST_DISTANCE_REF = "GPSDestDistanceRef";

/**
 * @brief 到目的地点的距离。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_DEST_DISTANCE = "GPSDestDistance";

/**
 * @brief DefaultCropSize指定了原始坐标中的最终图像大小，考虑了额外的边缘像素。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_DEFAULT_CROP_SIZE = "DefaultCropSize";

/**
 * @brief 表示系数伽马的值。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GAMMA = "Gamma";

/**
 * @brief 该标签指示摄像机或输入设备的ISO速度纬度yyy值，该值在ISO 12232中定义。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_ISO_SPEED_LATITUDEYYY = "ISOSpeedLatitudeyyy";

/**
 * @brief 该标签指示摄像机或输入设备的ISO速度纬度zzz值，该值在ISO 12232中定义。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_ISO_SPEED_LATITUDEZZZ = "ISOSpeedLatitudezzz";

/**
 * @brief 镜头的制造商。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_LENS_MAKE = "LensMake";

/**
 * @brief 镜头的型号名称。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_LENS_MODEL = "LensModel";

/**
 * @brief 镜头的序列号。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_LENS_SERIAL_NUMBER = "LensSerialNumber";

/**
 * @brief 使用的镜头规格。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_LENS_SPECIFICATION = "LensSpecification";

/**
 * @brief 在Exif中，"NewSubfileType"字段用于标识子文件的数据类型，如全分辨率图像、缩略图或多帧图像的一部分。
 * 其值是位掩码，0代表全分辨率图像，1代表缩略图，2代表多帧图像的一部分。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_NEW_SUBFILE_TYPE = "NewSubfileType";

/**
 * @brief 在Exif中，OffsetTime字段表示与UTC（协调世界时）的时间偏移，格式为±HH:MM，用于确定照片拍摄的本地时间。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_OFFSET_TIME = "OffsetTime";

/**
 * @brief 此标签记录图像数字化时的UTC偏移量，有助于准确调整时间戳。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_OFFSET_TIME_DIGITIZED = "OffsetTimeDigitized";

/**
 * @brief 此标签记录原始图像创建时的UTC偏移量，对于时间敏感的应用至关重要。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_OFFSET_TIME_ORIGINAL = "OffsetTimeOriginal";

/**
 * @brief 合成图像的源图像曝光时间。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SOURCE_EXPOSURE_TIMES_OF_COMPOSITE_IMAGE = "SourceExposureTimesOfCompositeImage";

/**
 * @brief 用于合成图像的源图像数量。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SOURCE_IMAGE_NUMBER_OF_COMPOSITE_IMAGE = "SourceImageNumberOfCompositeImage";

/**
 * @brief 此标签指示此子文件中的数据类型。标签已弃用，请使用NewSubfileType替代。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SUBFILE_TYPE = "SubfileType";

/**
 * @brief 此标签指示水平定位误差，单位为米。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GPS_H_POSITIONING_ERROR = "GPSHPositioningError";

/**
 * @brief 此标签指示拍摄图像时相机或输入设备的灵敏度。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_PHOTOGRAPHIC_SENSITIVITY = "PhotographicSensitivity";

/**
 * @brief 连拍次数。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_BURST_NUMBER = "HwMnoteBurstNumber";

/**
 * @brief 人脸置信度。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_FACE_CONF = "HwMnoteFaceConf";

/**
 * @brief 左眼中心。被用于 {@link OH_ImageSource_GetImageProperty} 和 {@link OH_ImageSource_ModifyImageProperty}。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_FACE_LEYE_CENTER = "HwMnoteFaceLeyeCenter";

/**
 * @brief 嘴中心。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_FACE_MOUTH_CENTER = "HwMnoteFaceMouthCenter";

/**
 * @brief 脸部指针。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_FACE_POINTER = "HwMnoteFacePointer";

/**
 * @brief 脸部矩形。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_FACE_RECT = "HwMnoteFaceRect";

/**
 * @brief 右眼中心。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_FACE_REYE_CENTER = "HwMnoteFaceReyeCenter";

/**
 * @brief FaceCount张人脸的笑脸分数。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_FACE_SMILE_SCORE = "HwMnoteFaceSmileScore";

/**
 * @brief 人脸算法版本信息。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_FACE_VERSION = "HwMnoteFaceVersion";

/**
 * @brief 是否是前置相机自拍。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_FRONT_CAMERA = "HwMnoteFrontCamera";

/**
 * @brief 场景指针。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SCENE_POINTER = "HwMnoteScenePointer";

/**
 * @brief 场景算法版本信息。
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_SCENE_VERSION = "HwMnoteSceneVersion";

/**
 * @brief GIF图片循环次数
 *
 * @since 12
 */
static const char *OHOS_IMAGE_PROPERTY_GIF_LOOP_COUNT = "GIFLoopCount";

/**
 * @brief 水印裁剪图左上角在原始图中的X坐标。
 *
 * @since 13
 */
static const char *OHOS_IMAGE_PROPERTY_X_IN_ORIGINAL = "XInOriginal";

/**
 * @brief 水印裁剪图左上角在原始图中的Y坐标。
 *
 * @since 13
 */
static const char *OHOS_IMAGE_PROPERTY_Y_IN_ORIGINAL = "YInOriginal";

/**
 * @brief 水印裁剪图的宽。
 *
 * @since 13
 */
static const char *OHOS_IMAGE_PROPERTY_FRAGMENT_WIDTH = "FragmentImageWidth";

/**
 * @brief 水印裁剪图的高。
 *
 * @since 13
 */
static const char *OHOS_IMAGE_PROPERTY_FRAGMENT_HEIGHT = "FragmentImageHeight";
#ifdef __cplusplus
};
#endif
/** @} */

#endif // INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_IMAGE_COMMON_H_