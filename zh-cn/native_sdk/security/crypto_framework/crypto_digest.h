/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CRYPTO_DIGEST_H
#define CRYPTO_DIGEST_H

/**
 * @addtogroup CryptoDigestApi
 * @{
 *
 * @brief 为应用提供摘要算法的相关接口。
 *
 * @since 12
 */

/**
 * @file crypto_digest.h
 *
 * @brief 定义摘要算法API。
 *
 * @library libohcrypto.z.so
 * @kit Crypto Architecture Kit
 * @syscap SystemCapability.Security.CryptoFramework
 * @since 12
 */

#include "crypto_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义摘要结构体。
 * @since 12
 */
typedef struct OH_CryptoDigest OH_CryptoDigest;

/**
 * @brief 根据给定的算法名称创建一个摘要实例。
 *
 * @param algoName 用于生成摘要实例的算法名称。例如："SHA256"
 * @param ctx 指向摘要实例的指针。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoDigest_Create(const char *algoName, OH_CryptoDigest **ctx);

/**
 * @brief 传入消息进行摘要更新计算。
 *
 * @param ctx 指向摘要实例。
 * @param in 传入的消息。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @see OH_CryptoDigest_Final
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoDigest_Update(OH_CryptoDigest *ctx, Crypto_DataBlob *in);

/**
 * @brief 计算最终摘要。
 *
 * @param ctx 指向摘要实例。
 * @param out 返回的Md的计算结果。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @see OH_CryptoDigest_Update
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoDigest_Final(OH_CryptoDigest *ctx, Crypto_DataBlob *out);

/**
 * @brief 获取摘要长度。
 *
 * @param ctx 指向摘要实例。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
uint32_t OH_CryptoDigest_GetLength(OH_CryptoDigest *ctx);

/**
 * @brief 获取摘要算法。
 *
 * @param ctx 指向摘要实例。
 * @return 摘要算法名。
 * @since 12
 */
const char *OH_CryptoDigest_GetAlgoName(OH_CryptoDigest *ctx);

/**
 * @brief 销毁摘要实例。
 *
 * @param ctx 指向摘要实例。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
void OH_DigestCrypto_Destroy(OH_CryptoDigest *ctx);

#ifdef __cplusplus
}
#endif

/** @} */
#endif /* CRYPTO_DIGEST_H */