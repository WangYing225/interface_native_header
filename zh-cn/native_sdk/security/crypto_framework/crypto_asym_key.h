/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CRYPTO_ASYM_KEY_H
#define CRYPTO_ASYM_KEY_H

/**
 * @addtogroup CryptoAsymKeyApi
 * @{
 *
 * @brief 为应用提供非对称密钥相关接口功能。
 *
 * @since 12
 */

/**
 * @file crypto_asym_key.h
 *
 * @brief 声明非对称密钥接口。
 *
 * @library libohcrypto.z.so
 * @kit Crypto Architecture Kit
 * @syscap SystemCapability.Security.CryptoFramework
 * @since 12
 */

#include "crypto_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义密钥对结构体。
 *
 * @since 12
 */
typedef struct OH_CryptoKeyPair OH_CryptoKeyPair;

/**
 * @brief 定义公钥结构体。
 *
 * @since 12
 */
typedef struct OH_CryptoPubKey OH_CryptoPubKey;

/**
 * @brief 定义非对称密钥参数类型。
 *
 * @since 12
 */
typedef enum {
    /** DSA算法的素模数p。 */
    CRYPTO_DSA_P_DATABLOB = 101,
    /** DSA算法中密钥参数q（p-1的素因子）。 */
    CRYPTO_DSA_Q_DATABLOB = 102,
    /** DSA算法的参数g。 */
    CRYPTO_DSA_G_DATABLOB = 103,
    /** DSA算法的私钥sk。 */
    CRYPTO_DSA_SK_DATABLOB = 104,
    /** DSA算法的公钥pk。 */
    CRYPTO_DSA_PK_DATABLOB = 105,
    /** ECC算法中表示椭圆曲线Fp域的素数p。 */
    CRYPTO_ECC_FP_P_DATABLOB = 201,
    /** ECC算法中椭圆曲线的第一个系数a。 */
    CRYPTO_ECC_A_DATABLOB = 202,
    /** ECC算法中椭圆曲线的第二个系数b。 */
    CRYPTO_ECC_B_DATABLOB = 203,
    /** ECC算法中基点g的x坐标。 */
    CRYPTO_ECC_G_X_DATABLOB = 204,
    /** ECC算法中基点g的y坐标。 */
    CRYPTO_ECC_G_Y_DATABLOB = 205,
    /** ECC算法中基点g的阶n。 */
    CRYPTO_ECC_N_DATABLOB = 206,
    /** ECC算法中的余因子h。 */
    CRYPTO_ECC_H_INT = 207,
    /** ECC算法中的私钥sk。 */
    CRYPTO_ECC_SK_DATABLOB = 208,
    /** ECC算法中，公钥pk（椭圆曲线上的一个点）的x坐标。 */
    CRYPTO_ECC_PK_X_DATABLOB = 209,
    /** ECC算法中，公钥pk（椭圆曲线上的一个点）的y坐标。 */
    CRYPTO_ECC_PK_Y_DATABLOB = 210,
    /** ECC算法中，椭圆曲线的域类型（当前只支持Fp域）。 */
    CRYPTO_ECC_FIELD_TYPE_STR = 211,
    /** ECC算法中域的大小，单位为bits（注：对于Fp域，域的大小为素数p的bits长度）。*/
    CRYPTO_ECC_FIELD_SIZE_INT = 212,
    /** ECC算法中的SECG(Standards for Efficient Cryptography Group)曲线名称。 */
    CRYPTO_ECC_CURVE_NAME_STR = 213,
    /** RSA算法中的模数n。 */
    CRYPTO_RSA_N_DATABLOB = 301,
    /** RSA算法中的私钥sk（即私钥指数d）。 */
    CRYPTO_RSA_D_DATABLOB = 302,
    /** RSA算法中的公钥pk（即公钥指数e）。 */
    CRYPTO_RSA_E_DATABLOB = 303,
    /** DH算法中的素数p。 */
    CRYPTO_DH_P_DATABLOB = 401,
    /** DH算法中的参数g。 */
    CRYPTO_DH_G_DATABLOB = 402,
    /** DH算法中私钥长度，单位为bit。 */
    CRYPTO_DH_L_NUM = 403,
    /** DH算法中的私钥sk。 */
    CRYPTO_DH_SK_DATABLOB = 404,
    /** DH算法中的公钥pk。 */
    CRYPTO_DH_PK_DATABLOB = 405,
    /** ED25519算法中的私钥sk。 */
    CRYPTO_ED25519_SK_DATABLOB = 501,
    /** ED25519算法中的公钥pk。 */
    CRYPTO_ED25519_PK_DATABLOB = 502,
    /** X25519算法中的私钥sk。 */
    CRYPTO_X25519_SK_DATABLOB = 601,
     /** X25519算法中的公钥pk。 */
    CRYPTO_X25519_PK_DATABLOB = 602,
} CryptoAsymKey_ParamType;

/**
 * @brief 定义编码格式。
 *
 * @since 12
 */
typedef enum {
    /** PEM格式密钥类型。 */
    CRYPTO_PEM = 0,
    /** DER格式密钥类型。 */
    CRYPTO_DER = 1,
} Crypto_EncodingType;

/**
 * @brief 定义非对称密钥生成器结构体。
 *
 * @since 12
 */
typedef struct OH_CryptoAsymKeyGenerator OH_CryptoAsymKeyGenerator;

/**
 * @brief 通过指定算法名称的字符串，获取相应的非对称密钥生成器实例。
 *
 * @param algoName 指定生成对称密钥生成器的算法名称。例如："RSA1024|PRIMES_2"
 * @param ctx 指向非对称密钥生成器实例的指针。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoAsymKeyGenerator_Create(const char *algoName, OH_CryptoAsymKeyGenerator **ctx);

/**
 * @brief 随机生成非对称密钥（密钥对）。
 *
 * @param ctx 非对称密钥生成器实例。
 * @param keyCtx 指向非对称密钥对实例的指针。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoAsymKeyGenerator_Generate(OH_CryptoAsymKeyGenerator *ctx, OH_CryptoKeyPair **keyCtx);

/**
 * @brief 转换非对称密钥数据为密钥对。
 *
 * @param ctx 非对称密钥生成器实例。
 * @param type 编码格式。
 * @param pubKeyData 公钥数据。
 * @param priKeyData 私钥数据。
 * @param keyCtx 指向非对称密钥对实例的指针。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoAsymKeyGenerator_Convert(OH_CryptoAsymKeyGenerator *ctx, Crypto_EncodingType type,
    Crypto_DataBlob *pubKeyData, Crypto_DataBlob *priKeyData, OH_CryptoKeyPair **keyCtx);
/**
 * @brief 获取非对称密钥算法名称。
 *
 * @param ctx 非对称密钥生成器实例。
 * @return 返回非对称密钥算法名称。
 * @since 12
 */
const char *OH_CryptoAsymKeyGenerator_GetAlgoName(OH_CryptoAsymKeyGenerator *ctx);

/**
 * @brief 销毁非对称密钥生成器实例。
 *
 * @param ctx 非对称密钥生成器实例。
 * @since 12
 */
void OH_CryptoAsymKeyGenerator_Destroy(OH_CryptoAsymKeyGenerator *ctx);

/**
 * @brief 销毁非对称密钥对实例。
 *
 * @param keyCtx 非对称密钥对实例。
 * @since 12
 */
void OH_CryptoKeyPair_Destroy(OH_CryptoKeyPair *keyCtx);

/**
 * @brief 从密钥对中获取公钥实例。
 *
 * @param keyCtx 密钥对实例。
 * @return 返回从密钥对中得到的公钥实例。
 * @since 12
 */
OH_CryptoPubKey *OH_CryptoKeyPair_GetPubKey(OH_CryptoKeyPair *keyCtx);

/**
 * @brief 根据指定的编码格式输出公钥数据。
 *
 * @param key 公钥实例。
 * @param type 编码类型。
 * @param encodingStandard 编码格式。
 * @param out 输出的公钥结果。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoPubKey_Encode(OH_CryptoPubKey *key, Crypto_EncodingType type,
    const char *encodingStandard, Crypto_DataBlob *out);

/**
 * @brief 从公钥实例获取指定参数。
 *
 * @param key 公钥实例。
 * @param item 非对称密钥参数类型。
 * @param value 参数输出值。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoPubKey_GetParam(OH_CryptoPubKey *key, CryptoAsymKey_ParamType item, Crypto_DataBlob *value);

#ifdef __cplusplus
}
#endif

/** @} */
#endif /* CRYPTO_ASYM_KEY_H */
