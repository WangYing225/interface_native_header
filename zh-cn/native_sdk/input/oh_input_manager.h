/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OH_INPUT_MANAGER_H
#define OH_INPUT_MANAGER_H

/**
 * @addtogroup input
 * @{
 *
 * @brief 提供多模态输入域的C接口。
 *
 * @since 12
 */

/**
 * @file oh_input_manager.h
 *
 * @brief 提供事件注入和关键状态查询等功能。
 *
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @library libohinput.so
 * @since 12
 */

#include <stdint.h>

#include "oh_axis_type.h"
#include "oh_key_code.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 按键状态的枚举值。
 *
 * @since 12
 */
typedef enum Input_KeyStateAction {
    /** 默认状态 */
    KEY_DEFAULT = -1,
    /** 按键按下 */
    KEY_PRESSED = 0,
    /** 按键抬起 */
    KEY_RELEASED = 1,
    /** 按键开关使能 */
    KEY_SWITCH_ON = 2,
    /** 按键开关去使能 */
    KEY_SWITCH_OFF = 3
} Input_KeyStateAction;

/**
 * @brief 按键事件类型的枚举值。
 *
 * @since 12
 */
typedef enum Input_KeyEventAction {
    /** 按键动作取消 */
    KEY_ACTION_CANCEL = 0,
    /** 按键按下 */
    KEY_ACTION_DOWN = 1,
    /** 按键抬起 */
    KEY_ACTION_UP = 2,
} Input_KeyEventAction;

/**
 * @brief 鼠标动作的枚举值。
 *
 * @since 12
 */
typedef enum Input_MouseEventAction {
    /** 取消鼠标动作 */
    MOUSE_ACTION_CANCEL = 0,
    /** 移动鼠标 */
    MOUSE_ACTION_MOVE = 1,
    /** 按下鼠标 */
    MOUSE_ACTION_BUTTON_DOWN = 2,
    /** 抬起鼠标按键 */
    MOUSE_ACTION_BUTTON_UP = 3,
    /** 鼠标轴事件开始 */
    MOUSE_ACTION_AXIS_BEGIN = 4,
    /** 更新鼠标轴事件 */
    MOUSE_ACTION_AXIS_UPDATE = 5,
    /** 鼠标轴事件结束 */
    MOUSE_ACTION_AXIS_END = 6,
} Input_MouseEventAction;

/**
 * @brief 鼠标轴事件类型。
 *
 * @since 12
 */
typedef enum InputEvent_MouseAxis {
    /** 垂直滚动轴 */
    MOUSE_AXIS_SCROLL_VERTICAL = 0,
    /** 水平滚动轴 */
    MOUSE_AXIS_SCROLL_HORIZONTAL = 1,
} InputEvent_MouseAxis;

/**
 * @brief 鼠标按键的枚举值。
 *
 * @since 12
 */
typedef enum Input_MouseEventButton {
    /** 无效按键 */
    MOUSE_BUTTON_NONE = -1,
    /** 鼠标左键 */
    MOUSE_BUTTON_LEFT = 0,
    /** 鼠标中间键 */
    MOUSE_BUTTON_MIDDLE = 1,
    /** 鼠标右键 */
    MOUSE_BUTTON_RIGHT = 2,
    /** 鼠标前进键 */
    MOUSE_BUTTON_FORWARD = 3,
    /** 鼠标返回键 */
    MOUSE_BUTTON_BACK = 4,
} Input_MouseEventButton;

/**
 * @brief 触摸动作的枚举值。
 *
 * @since 12
 */
typedef enum Input_TouchEventAction {
    /** 触摸取消 */
    TOUCH_ACTION_CANCEL = 0,
    /** 触摸按下 */
    TOUCH_ACTION_DOWN = 1,
    /** 触摸移动 */
    TOUCH_ACTION_MOVE = 2,
    /** 触摸抬起 */
    TOUCH_ACTION_UP = 3,
} Input_TouchEventAction;

/**
 * @brief 输入事件源类型。
 *
 * @since 12
 */
typedef enum InputEvent_SourceType {
    /**
     * 表示输入源生成鼠标光标移动、按钮按下和释放以及滚轮滚动的事件。
     *
     * @since 12
     */
    SOURCE_TYPE_MOUSE = 1,
    /**
     * 表示输入源产生触摸屏多点触摸事件。
     *
     * @since 12
     */
    SOURCE_TYPE_TOUCHSCREEN = 2,
    /**
     * 表示输入源产生触摸板多点触摸事件。
     *
     * @since 12
     */
    SOURCE_TYPE_TOUCHPAD = 3
} InputEvent_SourceType;

/**
 * @brief 输入设备的键盘类型。
 *
 * @since 13
 */
typedef enum Input_KeyboardType {
    /** 表示无按键设备。 */
    KEYBOARD_TYPE_NONE = 0,
    /** 表示未知按键设备。 */
    KEYBOARD_TYPE_UNKNOWN = 1,
    /** 表示全键盘设备。 */
    KEYBOARD_TYPE_ALPHABETIC = 2,
    /** 表示数字键盘设备。 */
    KEYBOARD_TYPE_DIGITAL = 3,
    /** 表示手写笔设备。 */
    KEYBOARD_TYPE_STYLUS = 4,
    /** 表示遥控器设备。 */
    KEYBOARD_TYPE_REMOTE_CONTROL = 5,
} Input_KeyboardType;

/**
 * @brief 定义按键信息，用于标识按键行为。例如，“Ctrl”按键信息包含键值和键类型。
 *
 * @since 12
 */
typedef struct Input_KeyState Input_KeyState;

/**
 * @brief 要注入的按键事件。
 *
 * @since 12
 */
typedef struct Input_KeyEvent Input_KeyEvent;

/**
 * @brief 要注入的鼠标事件。
 *
 * @since 12
 */
typedef struct Input_MouseEvent Input_MouseEvent;

/**
 * @brief 要注入的触摸事件。
 *
 * @since 12
 */
typedef struct Input_TouchEvent Input_TouchEvent;

/**
 * @brief 轴事件。
 *
 * @since 12
 */
typedef struct Input_AxisEvent Input_AxisEvent;

/**
 * @brief 错误码枚举值。
 *
 * @since 12
 */
typedef enum Input_Result {
    /** 操作成功 */
    INPUT_SUCCESS = 0,
    /** 权限验证失败 */
    INPUT_PERMISSION_DENIED = 201,
    /** 非系统应用 */
    INPUT_NOT_SYSTEM_APPLICATION = 202,
    /** 参数检查失败 */
    INPUT_PARAMETER_ERROR = 401,
    /** 服务异常 */
    INPUT_SERVICE_EXCEPTION = 3800001,
    /** 应用创建拦截后，再次执行创建拦截的操作 */
    INPUT_REPEAT_INTERCEPTOR = 4200001,
    /**
     * @brief 已经被系统应用占用
     * @since 14
     */
    INPUT_OCCUPIED_BY_SYSTEM = 4200002,
    /**
     * @brief 已经被其他应用占用
     * @since 14
     */
    INPUT_OCCUPIED_BY_OTHER = 4200003
} Input_Result;

/**
 * @brief 按键事件的回调函数，keyEvent的生命周期为回调函数内。
 * @since 12
 */
typedef void (*Input_KeyEventCallback)(const Input_KeyEvent* keyEvent);

/**
 * @brief 鼠标事件的回调函数，mouseEvent的生命周期为回调函数内。
 * @since 12
 */
typedef void (*Input_MouseEventCallback)(const Input_MouseEvent* mouseEvent);

/**
 * @brief 触摸事件的回调函数，touchEvent的生命周期为回调函数内。
 * @since 12
 */
typedef void (*Input_TouchEventCallback)(const Input_TouchEvent* touchEvent);

/**
 * @brief 轴事件的回调函数，axisEvent的生命周期为回调函数内。
 * @since 12
 */
typedef void (*Input_AxisEventCallback)(const Input_AxisEvent* axisEvent);

/**
 * @brief 回调函数，用于回调快捷键事件。
 * @since 14
 */
typedef void (*Input_HotkeyCallback)(Input_Hotkey* hotkey);

/**
 * @brief 回调函数，用于回调输入设备的热插事件。
 * @param deviceId 设备的ID。
 * @since 13
 */
typedef void (*Input_DeviceAddedCallback)(int32_t deviceId);

/**
 * @brief 回调函数，用于回调输入设备的热拔事件。
 * @param deviceId 设备的ID。
 * @since 13
 */
typedef void (*Input_DeviceRemovedCallback)(int32_t deviceId);

/**
 * @brief 拦截回调事件结构体，拦截鼠标事件、触摸事件和轴事件。
 * @since 12
 */
typedef struct Input_InterceptorEventCallback {
    /**
     * @brief 鼠标事件的回调函数。
     * @since 12
    */
    Input_MouseEventCallback mouseCallback;
    /**
     * @brief 触摸事件的回调函数。
     * @since 12
    */
    Input_TouchEventCallback touchCallback;
    /**
     * @brief 轴事件的回调函数。
     * @since 12
    */
    Input_AxisEventCallback axisCallback;
} Input_InterceptorEventCallback;

/**
 * @brief 定义一个结构体用于监听设备热插拔。
 * @since 13
 */
typedef struct Input_DeviceListener {
    /** 定义一个回调函数，用于回调设备热插事件。 */
    Input_DeviceAddedCallback deviceAddedCallback;
    /** 定义一个回调函数，用于回调设备热拔事件。 */
    Input_DeviceRemovedCallback deviceRemovedCallback;
} Input_DeviceListener;

/**
 * @brief 事件拦截选项。
 * @since 12
 */
typedef struct Input_InterceptorOptions Input_InterceptorOptions;

/**
 * @brief 定义快捷键结构体。
 *
 * @since 14
 */
typedef struct Input_Hotkey Input_Hotkey;

/**
 * @brief 输入设备信息。
 *
 * @since 13
 */
typedef struct Input_DeviceInfo Input_DeviceInfo;

/**
 * @brief 查询按键状态的枚举对象。
 *
 * @param keyState 按键状态的枚举对象，具体请参考{@link Input_KeyStateAction}
 *
 * @return 如果操作成功，@return返回{@link INPUT_SUCCESS}； 
 * 否则返回{@link Input_Result}中定义的其他错误代码。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_GetKeyState(struct Input_KeyState* keyState);

/**
 * @brief 创建按键状态的枚举对象。
 *
 * @return 如果操作成功，@return返回一个{@link Input_KeyState}指针对象；否则返回空指针。
 *
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
struct Input_KeyState* OH_Input_CreateKeyState();

/**
 * @brief 销毁按键状态的枚举对象。
 *
 * @param keyState 按键状态的枚举对象，具体请参考{@link Input_KeyStateAction}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_DestroyKeyState(struct Input_KeyState** keyState);

/**
 * @brief 设置按键状态对象的键值。
 *
 * @param keyState 按键状态的枚举对象，具体请参考{@link Input_KeyStateAction}。
 * @param keyCode 按键键值。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetKeyCode(struct Input_KeyState* keyState, int32_t keyCode);

/**
 * @brief 获取按键状态对象的键值。
 * 
 * @param keyState 按键状态的枚举对象，具体请参考{@link Input_KeyStateAction}。
 * @return 返回按键状态对象的键值。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetKeyCode(const struct Input_KeyState* keyState);

/**
 * @brief 设置按键状态对象的按键是否按下。
 * 
 * @param keyState 按键状态的枚举对象，具体请参考{@link Input_KeyStateAction}。
 * @param keyAction 按键是否按下，具体请参考{@link Input_KeyEventAction}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetKeyPressed(struct Input_KeyState* keyState, int32_t keyAction);

/**
 * @brief 获取按键状态对象的按键是否按下。
 * 
 * @param keyState 按键状态的枚举对象，具体请参考{@link Input_KeyStateAction}。
 * @return 返回按键状态对象的按键按下状态。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetKeyPressed(const struct Input_KeyState* keyState);

/**
 * @brief 设置按键状态对象的按键开关。
 * 
 * @param keyState 按键状态的枚举对象，具体请参考{@link Input_KeyStateAction}。
 * @param keySwitch 按键开关。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetKeySwitch(struct Input_KeyState* keyState, int32_t keySwitch);

/**
 * @brief 获取按键状态对象的按键开关。
 * 
 * @param keyState 按键状态的枚举对象，具体请参考{@link Input_KeyStateAction}。
 * @return 返回按键状态对象的按键开关。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetKeySwitch(const struct Input_KeyState* keyState);

/**
 * @brief 注入按键事件。
 *
 * @param keyEvent 要注入的按键事件。
 * @return OH_Input_InjectKeyEvent 函数错误码。\n
 *         若注入成功，返回{@link INPUT_SUCCESS}；
 *         若缺少权限，返回{@link INPUT_PERMISSION_DENIED}；
 *         若参数错误，返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_InjectKeyEvent(const struct Input_KeyEvent* keyEvent);

/**
 * @brief 创建按键事件对象。
 *
 * @return 如果操作成功返回一个{@link Input_KeyEvent}指针对象，否则返回空指针。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
struct Input_KeyEvent* OH_Input_CreateKeyEvent();

/**
 * @brief 销毁按键事件对象。
 *
 * @param keyEvent 按键事件对象。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_DestroyKeyEvent(struct Input_KeyEvent** keyEvent);

/**
 * @brief 设置按键事件类型。
 *
 * @param keyEvent 按键事件对象。
 * @param action 按键事件类型。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetKeyEventAction(struct Input_KeyEvent* keyEvent, int32_t action);

/**
 * @brief 获取按键事件类型。
 * @param keyEvent 按键事件对象。
 * @return 返回按键事件类型。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetKeyEventAction(const struct Input_KeyEvent* keyEvent);

/**
 * @brief 设置按键事件的键值。
 *
 * @param keyEvent 按键事件对象。
 * @param keyCode 按键的键值。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetKeyEventKeyCode(struct Input_KeyEvent* keyEvent, int32_t keyCode);

/**
 * @brief 获取按键事件的键值。
 *
 * @param keyEvent 按键事件对象。
 * @return Key code.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetKeyEventKeyCode(const struct Input_KeyEvent* keyEvent);

/**
 * @brief 设置按键事件发生的时间。
 *
 * @param keyEvent 按键事件对象。
 * @param actionTime 按键事件发生的时间。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetKeyEventActionTime(struct Input_KeyEvent* keyEvent, int64_t actionTime);

/**
 * @brief 获取按键事件发生的时间。
 *
 * @param keyEvent 按键事件对象。
 * @return 返回按键事件发生的时间。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int64_t OH_Input_GetKeyEventActionTime(const struct Input_KeyEvent* keyEvent);

/**
 * @brief 注入鼠标事件。
 *
 * @param mouseEvent 要注入的鼠标事件。
 * @return OH_Input_InjectKeyEvent 函数错误码。\n
 *         若注入成功，返回{@link INPUT_SUCCESS}；
 *         若缺少权限，返回{@link INPUT_PERMISSION_DENIED}；
 *         若参数错误，返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_InjectMouseEvent(const struct Input_MouseEvent* mouseEvent);

/**
 * @brief 创建鼠标事件对象。
 *
 * @return 如果操作成功返回一个{@link Input_MouseEvent}指针对象，否则返回空指针。
 * 
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
struct Input_MouseEvent* OH_Input_CreateMouseEvent();

/**
 * @brief 销毁鼠标事件对象。
 *
 * @param mouseEvent 鼠标事件对象。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_DestroyMouseEvent(struct Input_MouseEvent** mouseEvent);

/**
 * @brief 设置鼠标事件的动作。
 *
 * @param mouseEvent 鼠标事件对象。
 * @param action 鼠标的动作。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetMouseEventAction(struct Input_MouseEvent* mouseEvent, int32_t action);

/**
 * @brief 获取鼠标事件的动作。
 *
 * @param mouseEvent 鼠标事件对象。
 * @return 鼠标的动作。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetMouseEventAction(const struct Input_MouseEvent* mouseEvent);

/**
 * @brief 设置鼠标事件的屏幕X坐标。
 *
 * @param mouseEvent 鼠标事件对象。
 * @param displayX 屏幕X坐标。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetMouseEventDisplayX(struct Input_MouseEvent* mouseEvent, int32_t displayX);

/**
 * @brief 获取鼠标事件的屏幕X坐标。
 *
 * @param mouseEvent 鼠标事件对象。
 * @return 屏幕X坐标。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetMouseEventDisplayX(const struct Input_MouseEvent* mouseEvent);

/**
 * @brief 设置鼠标事件的屏幕Y坐标。
 *
 * @param mouseEvent 鼠标事件对象。
 * @param displayY 屏幕Y坐标。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetMouseEventDisplayY(struct Input_MouseEvent* mouseEvent, int32_t displayY);

/**
 * @brief 获取鼠标事件的屏幕Y坐标。
 *
 * @param mouseEvent 鼠标事件对象。
 * @return 屏幕Y坐标。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetMouseEventDisplayY(const struct Input_MouseEvent* mouseEvent);

/**
 * @brief 设置鼠标事件的按键。
 *
 * @param mouseEvent 鼠标事件对象。
 * @param button 鼠标按键。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetMouseEventButton(struct Input_MouseEvent* mouseEvent, int32_t button);

/**
 * @brief 获取鼠标事件的按键。
 *
 * @param mouseEvent 鼠标事件对象。
 * @return 鼠标按键。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetMouseEventButton(const struct Input_MouseEvent* mouseEvent);

/**
 * @brief 设置鼠标轴事件的类型。
 *
 * @param mouseEvent 鼠标事件对象。
 * @param axisType 轴类型，比如垂直轴、水平轴。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetMouseEventAxisType(struct Input_MouseEvent* mouseEvent, int32_t axisType);

/**
 * @brief 获取鼠标轴事件的类型。
 *
 * @param mouseEvent 鼠标事件对象。
 * @return 轴类型。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetMouseEventAxisType(const struct Input_MouseEvent* mouseEvent);

/**
 * @brief 设置鼠标轴事件的值。
 *
 * @param mouseEvent 鼠标事件对象。
 * @param axisValue 轴事件的值，正数向前滚动，负数向后滚动。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetMouseEventAxisValue(struct Input_MouseEvent* mouseEvent, float axisValue);

/**
 * @brief 获取鼠标轴事件的值。
 *
 * @param mouseEvent 鼠标事件对象。
 * @return 轴事件的值。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
float OH_Input_GetMouseEventAxisValue(const struct Input_MouseEvent* mouseEvent);

/**
 * @brief 设置鼠标事件发生的时间。
 *
 * @param mouseEvent 鼠标事件对象。
 * @param actionTime 鼠标事件发生的时间。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetMouseEventActionTime(struct Input_MouseEvent* mouseEvent, int64_t actionTime);

/**
 * @brief 获取鼠标事件发生的时间。
 *
 * @param keyEvent 鼠标事件对象。
 * @return 返回鼠标事件发生的时间。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int64_t OH_Input_GetMouseEventActionTime(const struct Input_MouseEvent* mouseEvent);

/**
 * @brief 注入触摸事件。
 *
 * @param touchEvent - 要注入的触摸事件。
 * @return OH_Input_InjectKeyEvent 函数错误码。
 *         若注入成功，返回{@link INPUT_SUCCESS}；
 *         若缺少权限，返回{@link INPUT_PERMISSION_DENIED}；
 *         若参数错误，返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_InjectTouchEvent(const struct Input_TouchEvent* touchEvent);

/**
 * @brief 创建触屏事件对象。
 *
 * @return 如果操作成功返回一个{@link Input_TouchEvent}指针对象，否则返回空指针。
 * 
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
struct Input_TouchEvent* OH_Input_CreateTouchEvent();

/**
 * @brief 销毁触屏事件对象。
 *
 * @param touchEvent 触屏事件对象。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_DestroyTouchEvent(struct Input_TouchEvent** touchEvent);

/**
 * @brief 设置触屏事件的动作。
 *
 * @param touchEvent 触屏事件对象。
 * @param 触屏的动作。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetTouchEventAction(struct Input_TouchEvent* touchEvent, int32_t action);

/**
 * @brief 获取触屏事件的动作。
 *
 * @param touchEvent 触屏事件对象。
 * @return 触屏的动作。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetTouchEventAction(const struct Input_TouchEvent* touchEvent);

/**
 * @brief 设置触屏事件的手指ID。
 *
 * @param touchEvent 触屏事件对象。
 * @param id 触屏的手指ID。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetTouchEventFingerId(struct Input_TouchEvent* touchEvent, int32_t id);

/**
 * @brief 获取触屏事件的手指ID。
 *
 * @param touchEvent 触屏事件对象。
 * @return 触屏的手指ID。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetTouchEventFingerId(const struct Input_TouchEvent* touchEvent);

/**
 * @brief 设置触屏事件的屏幕X坐标。
 *
 * @param touchEvent 触屏事件对象。
 * @param 触屏的屏幕X坐标。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetTouchEventDisplayX(struct Input_TouchEvent* touchEvent, int32_t displayX);

/**
 * @brief 获取触屏事件的屏幕X坐标。
 *
 * @param touchEvent 触屏事件对象。
 * @return 触屏的屏幕X坐标。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetTouchEventDisplayX(const struct Input_TouchEvent* touchEvent);

/**
 * @brief 设置触屏事件的屏幕Y坐标。
 *
 * @param touchEvent 触屏事件对象。
 * @param 触屏的屏幕Y坐标。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetTouchEventDisplayY(struct Input_TouchEvent* touchEvent, int32_t displayY);

/**
 * @brief 获取触屏事件的屏幕Y坐标。
 *
 * @param touchEvent 触屏事件对象。
 * @return 触屏的屏幕Y坐标。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetTouchEventDisplayY(const struct Input_TouchEvent* touchEvent);

/**
 * @brief 设置触摸事件发生的时间。
 *
 * @param keyEvent 触屏事件对象。
 * @param actionTime 触摸事件发生的时间。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetTouchEventActionTime(struct Input_TouchEvent* touchEvent, int64_t actionTime);

/**
 * @brief 获取触摸事件发生的时间。
 *
 * @param keyEvent 触屏事件对象。
 * @return 返回触摸事件发生的时间。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int64_t OH_Input_GetTouchEventActionTime(const struct Input_TouchEvent* touchEvent);

/**
 * @brief 取消事件注入并撤销授权。
 *
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_CancelInjection();

/**
 * @brief 创建轴事件对象实例。
 *
 * @return 成功返回{@link Input_AxisEvent}对象实例，失败则返回null。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_AxisEvent* OH_Input_CreateAxisEvent(void);

/**
 * @brief 销毁轴事件对象实例。
 * 
 * @param axisEvent 轴事件对象实例的指针。
 * @return 若销毁成功，则返回{@link INTO_SUCCESS}；若axisEvent为NULL或者*axisEvent为NULL，则返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_DestroyAxisEvent(Input_AxisEvent** axisEvent);

/**
 * @brief 设置轴事件的动作。
 *
 * @param axisEvent 轴事件对象。
 * @param action 轴事件动作，具体请参考{@link InputEvent_AxisAction}。
 * @return 若设置轴事件的动作成功，则返回{@link INTO_SUCCESS}；若axisEvent为NULL，则返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_SetAxisEventAction(Input_AxisEvent* axisEvent, InputEvent_AxisAction action);

/**
 * @brief 获取轴事件的动作。
 *
 * @param axisEvent 轴事件对象。
 * @param action 出参，返回轴事件动作，具体请参考在{@link InputEvent_AxisAction}。
 * @return 若获取轴事件的动作成功，则返回{@link INTO_SUCCESS}；若axisEvent或者action为NULL，则返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_GetAxisEventAction(const Input_AxisEvent* axisEvent, InputEvent_AxisAction *action);

/**
 * @brief 设置轴事件的X坐标。
 *
 * @param axisEvent 轴事件对象。
 * @param displayX 轴事件X坐标。
 * @return 若设置轴事件的X坐标成功，则返回{@link INTO_SUCCESS}；若axisEvent为NULL，则返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_SetAxisEventDisplayX(Input_AxisEvent* axisEvent, float displayX);

/**
 * @brief 获取轴事件的X坐标。
 *
 * @param axisEvent 轴事件对象。
 * @param displayX 出参，返回轴事件X坐标。
 * @return 若获取轴事件的X坐标成功，则返回{@link INTO_SUCCESS}；若axisEvent或者displayX为NULL，则返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_GetAxisEventDisplayX(const Input_AxisEvent* axisEvent, float* displayX);

/**
 * @brief 设置轴事件的Y坐标。
 *
 * @param axisEvent 轴事件对象，请参考{@link Input_AxisEvent}。
 * @param displayY 轴事件Y坐标。
 * @return 若设置轴事件的Y坐标成功，则返回{@link INTO_SUCCESS}；若axisEvent为NULL，则返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_SetAxisEventDisplayY(Input_AxisEvent* axisEvent, float displayY);

/**
 * @brief 获取轴事件的Y坐标。
 *
 * @param axisEvent 轴事件对象，请参考{@link Input_AxisEvent}。
 * @param displayY 出参，返回轴事件Y坐标。
 * @return 若获取轴事件的Y坐标成功，则返回{@link INTO_SUCCESS}；若axisEvent或者displayY为NULL，则返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_GetAxisEventDisplayY(const Input_AxisEvent* axisEvent, float* displayY);

/**
 * @brief 设置轴事件指定轴类型的轴值。
 *
 * @param axisEvent 轴事件对象，请参考{@link Input_AxisEvent}。
 * @param axisType 轴类型，具体请参考{@link InputEvent_AxisType}。
 * @param axisValue 轴事件轴值。
 * @return 若设置轴事件指定轴类型的轴值成功，则返回{@link INTO_SUCCESS}；若axisEvent为NULL，则返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_SetAxisEventAxisValue(Input_AxisEvent* axisEvent,
                                            InputEvent_AxisType axisType, double axisValue);

/**
 * @brief 获取轴事件指定轴类型的轴值。
 *
 * @param axisEvent 轴事件对象，请参考{@link Input_AxisEvent}。
 * @param axisType 轴类型，具体请参考{@link InputEvent_AxisType}。
 * @param axisValue 出参，返回轴事件轴值。
 * @return 若获取轴事件指定轴类型的轴值成功，则返回{@link INTO_SUCCESS}；若axisEvent或者axisValue为NULL，则返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_GetAxisEventAxisValue(const Input_AxisEvent* axisEvent,
                                            InputEvent_AxisType axisType, double* axisValue);

/**
 * @brief 设置轴事件发生的时间。
 *
 * @param axisEvent 轴事件对象，请参考{@link Input_AxisEvent}。
 * @param actionTime 轴事件发生的时间。
 * @return 若设置轴事件发生的时间成功，则返回{@link INTO_SUCCESS}；若axisEvent为NULL，则返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_SetAxisEventActionTime(Input_AxisEvent* axisEvent, int64_t actionTime);

/**
 * @brief 获取轴事件发生的时间。
 *
 * @param axisEvent 轴事件对象，请参考{@link Input_AxisEvent}。
 * @param actionTime 出参，返回轴事件发生的时间。
 * @return 若获取轴事件发生的时间成功，则返回{@link INTO_SUCCESS}；若axisEvent或者actionTime为NULL，则返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_GetAxisEventActionTime(const Input_AxisEvent* axisEvent, int64_t* actionTime);

/**
 * @brief 设置轴事件类型。
 *
 * @param axisEvent 轴事件对象，请参考{@link Input_AxisEvent}。
 * @param axisEventType 轴事件类型，具体请参考{@link InputEvent_AxisEventType}。
 * @return 若设置轴事件类型成功，则返回{@link INTO_SUCCESS}；若axisEvent为NULL，则返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_SetAxisEventType(Input_AxisEvent* axisEvent, InputEvent_AxisEventType axisEventType);

/**
 * @brief 获取轴事件类型。
 *
 * @param axisEvent 轴事件对象。
 * @param axisEventType 出参，返回轴事件类型，具体请参考{@link InputEvent_AxisEventType}。
 * @return 若获取轴事件类型成功，则返回{@link INTO_SUCCESS}；若axisEvent或者axisEventType为NULL，则返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_GetAxisEventType(const Input_AxisEvent* axisEvent,
                                       InputEvent_AxisEventType* axisEventType);

/**
 * @brief 设置轴事件源类型。
 *
 * @param axisEvent 轴事件对象。
 * @param sourceType 轴事件源类型,具体请参考{@link InputEvent_SourceType}。
 * @return 若设置轴事件源类型成功，则返回{@link INTO_SUCCESS}；若axisEvent为NULL，则返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_SetAxisEventSourceType(Input_AxisEvent* axisEvent, InputEvent_SourceType sourceType);

/**
 * @brief 获取轴事件源类型。
 *
 * @param axisEvent 轴事件对象。
 * @param sourceType 出参，返回轴事件源类型，具体请参考{@link InputEvent_SourceType}。
 * @return 若获取轴事件源类型成功，则返回{@link INTO_SUCCESS}；若axisEvent或者sourceType为NULL，则返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_GetAxisEventSourceType(const Input_AxisEvent* axisEvent,
                                             InputEvent_SourceType* sourceType);

/**
 * @brief 添加按键事件监听。
 *
 * @permission ohos.permission.INPUT_MONITORING
 * @param callback 回调函数，用于接收按键事件。
 * @return 若添加按键事件监听成功，则返回{@link INTO_SUCCESS}；若权限校验失败，则返回{@link INPUT_PERMISSION_DENIED}；
 *         若callback为空，则返回{@link INPUT_PARAMETER_ERROR}；若服务异常，则返回{@link INPUT_SERVICE_EXCEPTION}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_AddKeyEventMonitor(Input_KeyEventCallback callback);

/**
 * @brief 添加鼠标事件监听,包含鼠标点击，移动，不包含滚轮事件，滚轮事件归属于轴事件。
 *
 * @permission ohos.permission.INPUT_MONITORING
 * @param callback 回调函数，用于接收鼠标事件。
 * @return 若添加鼠标事件监听成功，则返回{@link INTO_SUCCESS}；若权限校验失败，则返回{@link INPUT_PERMISSION_DENIED}；
 *         若callback为空，则返回{@link INPUT_PARAMETER_ERROR}；若服务异常，则返回{@link INPUT_SERVICE_EXCEPTION}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_AddMouseEventMonitor(Input_MouseEventCallback callback);

/**
 * @brief 添加触摸事件监听。
 *
 * @permission ohos.permission.INPUT_MONITORING
 * @param callback 回调函数，用于接收触摸事件。
 * @return 若添加触摸事件监听成功，则返回{@link INTO_SUCCESS}；若权限校验失败，则返回{@link INPUT_PERMISSION_DENIED}；
 *         若callback为空，则返回{@link INPUT_PARAMETER_ERROR}；若服务异常，则返回{@link INPUT_SERVICE_EXCEPTION}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_AddTouchEventMonitor(Input_TouchEventCallback callback);

/**
 * @brief 添加所有类型轴事件监听，轴事件类型定义在{@link InputEvent_AxisEventType}中。
 *
 * @permission ohos.permission.INPUT_MONITORING
 * @param callback 回调函数，用于接收轴事件。
 * @return 若添加轴事件监听成功，则返回{@link INTO_SUCCESS}；若权限校验失败，则返回{@link INPUT_PERMISSION_DENIED}；
 *         若callback为空，则返回{@link INPUT_PARAMETER_ERROR}；若服务异常，则返回{@link INPUT_SERVICE_EXCEPTION}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_AddAxisEventMonitorForAll(Input_AxisEventCallback callback);

/**
 * @brief 添加指定类型的轴事件监听，轴事件类型定义在{@link InputEvent_AxisEventType}中。
 *
 * @permission ohos.permission.INPUT_MONITORING
 * @param axisEventType 要监听的轴事件类型，轴事件类型定义在{@link InputEvent_AxisEventType}中。
 * @param callback 回调函数，用于接收指定类型的轴事件
 * @return 若添加轴事件监听成功，则返回{@link INTO_SUCCESS}；若权限校验失败，则返回{@link INPUT_PERMISSION_DENIED}；
 *         若callback为空，则返回{@link INPUT_PARAMETER_ERROR}；若服务异常，则返回{@link INPUT_SERVICE_EXCEPTION}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_AddAxisEventMonitor(InputEvent_AxisEventType axisEventType, Input_AxisEventCallback callback);

/**
 * @brief 移除按键事件监听。
 *
 * @permission ohos.permission.INPUT_MONITORING
 * @param callback 指定要被移除的用于按键事件监听的回调函数。
 * @return 若移除按键事件监听成功，则返回{@link INTO_SUCCESS}；若权限校验失败，则返回{@link INPUT_PERMISSION_DENIED}；
 *         若callback为空或者没有被添加监听，则返回{@link INPUT_PARAMETER_ERROR}；若服务异常，则返回{@link INPUT_SERVICE_EXCEPTION}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_RemoveKeyEventMonitor(Input_KeyEventCallback callback);

/**
 * @brief 移除鼠标事件监听。
 *
 * @permission ohos.permission.INPUT_MONITORING
 * @param callback 指定要被移除的用于鼠标事件监听的回调函数。
 * @return 若移除鼠标事件监听成功，则返回{@link INTO_SUCCESS}；若权限校验失败，则返回{@link INPUT_PERMISSION_DENIED}；
 *         若callback为空或者没有被添加监听，则返回{@link INPUT_PARAMETER_ERROR}；若服务异常，则返回{@link INPUT_SERVICE_EXCEPTION}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_RemoveMouseEventMonitor(Input_MouseEventCallback callback);

/**
 * @brief 移除触摸事件监听。
 *
 * @permission ohos.permission.INPUT_MONITORING
 * @param callback 指定要被移除的用于触摸事件监听的回调函数。
 * @return 若移除触摸事件监听成功，则返回{@link INTO_SUCCESS}；若权限校验失败，则返回{@link INPUT_PERMISSION_DENIED}；
 *         若callback为空或者没有被添加监听，则返回{@link INPUT_PARAMETER_ERROR}；若服务异常，则返回{@link INPUT_SERVICE_EXCEPTION}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_RemoveTouchEventMonitor(Input_TouchEventCallback callback);

/**
 * @brief 移除所有类型轴事件监听。
 *
 * @permission ohos.permission.INPUT_MONITORING
 * @param callback 指定要被移除的用于所有类型轴事件监听的回调函数。
 * @return 若移除轴事件监听成功，则返回{@link INTO_SUCCESS}；若权限校验失败，则返回{@link INPUT_PERMISSION_DENIED}；
 *         若callback为空或者没有被添加监听，则返回{@link INPUT_PARAMETER_ERROR}；若服务异常，则返回{@link INPUT_SERVICE_EXCEPTION}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_RemoveAxisEventMonitorForAll(Input_AxisEventCallback callback);

/**
 * @brief 移除指定类型轴事件监听，轴事件类型定义在{@link InputEvent_AxisEventType}中。
 *
 * @permission ohos.permission.INPUT_MONITORING
 * @param axisEventType 指定要移除监听的轴事件类型，轴事件类型定义在{@link InputEvent_AxisEventType}中。
 * @param callback 指定要被移除的用于指定类型轴事件监听的回调函数。
 * @return 若移除轴事件监听成功，则返回{@link INTO_SUCCESS}；若权限校验失败，则返回{@link INPUT_PERMISSION_DENIED}；
 *         若callback为空或者没有被添加监听，则返回{@link INPUT_PARAMETER_ERROR}；若服务异常，则返回{@link INPUT_SERVICE_EXCEPTION}。 
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_RemoveAxisEventMonitor(InputEvent_AxisEventType axisEventType, Input_AxisEventCallback callback);

/**
 * @brief 添加按键事件的拦截,重复添加只有第一次生效。
 *
 * @permission ohos.permission.INTERCEPT_INPUT_EVENT
 * @param callback 回调函数，用于接收按键事件。
 * @param option 输入事件拦截的可选项，传null则使用默认值。
 * @return 若添加按键事件的拦截成功，则返回{@link INTO_SUCCESS}；若权限校验失败，则返回{@link INPUT_PERMISSION_DENIED}；
 *         若callback为空，则返回{@link INPUT_PARAMETER_ERROR}；若重复添加拦截器，则返回{@link INPUT_REPEAT_INTERCEPTOR}；
 *         若服务异常；则返回{@link INPUT_SERVICE_EXCEPTION}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_AddKeyEventInterceptor(Input_KeyEventCallback callback, Input_InterceptorOptions *option);

/**
 * @brief 添加输入事件拦截，包括鼠标、触摸和轴事件，重复添加只有第一次生效。
 *
 * @permission ohos.permission.INTERCEPT_INPUT_EVENT
 * @param callback 用于回调输入事件的结构体指针，请参考定义{@link Input_InterceptorEventCallback}。
 * @param option 输入事件拦截的可选项，传null则使用默认值。
 * @return 若添加输入事件的拦截成功，则返回{@link INTO_SUCCESS}；若权限校验失败，则返回{@link INPUT_PERMISSION_DENIED}；
 *         若callback为空，则返回{@link INPUT_PARAMETER_ERROR}；若重复添加拦截器，则返回{@link INPUT_REPEAT_INTERCEPTOR}；
 *         若服务异常；则返回{@link INPUT_SERVICE_EXCEPTION}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_AddInputEventInterceptor(Input_InterceptorEventCallback *callback
                                               Input_InterceptorOptions *option);

/**
 * @brief 移除按键事件拦截。
 *
 * @permission ohos.permission.INTERCEPT_INPUT_EVENT
 * @return 若移除按键事件拦截成功，则返回{@link INTO_SUCCESS}；若权限校验失败，则返回{@link INPUT_PERMISSION_DENIED}；
 *         若服务异常，则返回{@link INPUT_SERVICE_EXCEPTION}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_RemoveKeyEventInterceptor();

/**
 * @brief 移除输入事件拦截，包括鼠标、触摸和轴事件。
 *
 * @permission ohos.permission.INTERCEPT_INPUT_EVENT
 * @return 若移除输入事件拦截成功，则返回{@link INTO_SUCCESS}；若权限校验失败，则返回{@link INPUT_PERMISSION_DENIED}；
 *         若服务异常，则返回{@link INPUT_SERVICE_EXCEPTION}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_RemoveInputEventInterceptor();

/**
 * @brief 获取距离上次系统输入事件的时间间隔。
 * 
 * @param timeInterval 时间间隔，单位为微秒。
 * @return OH_Input_GetIntervalSinceLastInput 函数错误码。\n
 *         若获取时间间隔成功，则返回{@link INPUT_SUCCESS}；若获取失败，返回{@link INPUT_SERVICE_EXCEPTION}。
 *         
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 14
 */
int32_t OH_Input_GetIntervalSinceLastInput(int64_t *timeInterval);

/**
 * @brief 创建快捷键对象的实例。
 *
 * @return 如果操作成功,则返回一个{@link Input_Hotkey}指针对象。否则, 返回一个空指针， 可能的原因是内存分配失败。
 * 
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 14
 */
Input_Hotkey *OH_Input_CreateHotkey();

/**
 * @brief 销毁快捷键对象的实例。
 *
 * @param hotkey 快捷键对象的实例。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 14
 */
void OH_Input_DestroyHotkey(Input_Hotkey **hotkey);

/**
 * @brief 设置修饰键。
 *
 * @param hotkey 快捷键对象的实例。
 * @param preKeys 修饰键列表。
 * @param size 修饰键个数， 取值范围1~2个。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 14
 */
void OH_Input_SetPreKeys(Input_Hotkey *hotkey, int32_t *preKeys, int32_t size);

/**
 * @brief 获取修饰键。
 *
 * @param hotkey 快捷键对象的实例。
 * @param preKeys 返回修饰键列表。
 * @param preKeyCount 返回修饰键个数。
 * @return OH_Input_GetpressedKeys 函数错误码。\n
 *         若获取成功，返回{@link INPUT_SUCCESS}；若获取失败，返回{@link INPUT_PARAMETER_ERROR}。
 *         
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 14
 */
Input_Result OH_Input_GetPreKeys(const Input_Hotkey *hotkey, int32_t **preKeys, int32_t *preKeyCount);

/**
 * @brief 设置被修饰键。
 *
 * @param hotkey 快捷键对象的实例。
 * @param finalKey 被修饰键值，被修饰键值只能是1个。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 14
 */
void OH_Input_SetFinalKey(Input_Hotkey* hotkey, int32_t finalKey);

/**
 * @brief 获取被修饰键。
 *
 * @param hotkey 快捷键对象的实例。
 * @param finalKeyCode 返回被修饰键键值。
 * @return OH_Input_GetfinalKey 函数错误码。\n
 *         若获取成功，返回{@link INPUT_SUCCESS}；
 *         若获取失败，返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 14
 */
Input_Result OH_Input_GetFinalKey(const Input_Hotkey* hotkey, int32_t *finalKeyCode);

/**
 * @brief 创建{@link Input_Hotkey}类型实例的数组。
 *
 * @param count 创建{@link Input_Hotkey}实例的数量。
 * @return OH_Input_CreateAllSystemHotkey 函数错误码。
 *         {@link INPUT_SUCCESS} 创建实例数组的双指针成功。\n
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 14
 */
Input_Hotkey **OH_Input_CreateAllSystemHotkeys(int32_t count);

/**
 * @brief 销毁{@link Input_Hotkey}实例数组并回收内存。
 *
 * @param hotkeys 指向{@link Input_Hotkey}实例数组的双指针。
 * @param count 销毁{@link Input_Hotkey}实例的数量。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 14
 */
void OH_Input_DestroyAllSystemHotkeys(Input_Hotkey **hotkeys, int32_t count);

/**
 * @brief 获取设置的所有快捷键。
 *
 * @param hotkey 返回{@link Input_Hotkey} 类型实例数组。首次调用可传入NULL，可获取数组长度。
 * @param count 返回支持快捷键的个数。
 * @return OH_Input_GetAllSystemHotkeys 函数错误码。\n
 *         若获取成功，返回{@link INPUT_SUCCESS}；
 *         若获取失败，返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 14
 */
Input_Result OH_Input_GetAllSystemHotkeys(Input_Hotkey **hotkey, int32_t *count);

/**
 * @brief 设置是否上报重复key事件。
 *
 * @param hotkey 快捷键对象的实例。
 * @param isRepeat 是否上报重复key事件。true表示上报，false表示不上报。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 14
 */
void OH_Input_SetRepeat(Input_Hotkey* hotkey, bool isRepeat);

/**
 * @brief 获取是否上报重复key事件。
 *
 * @param hotkey 快捷键对象的实例。
 * @param isRepeat 返回Key事件是否重复。
 * @return OH_Input_GetIsRepeat 函数错误码。\n
 *         若获取成功，返回{@link INPUT_SUCCESS}；
 *         若获取失败，返回{@link INPUT_PARAMETER_ERROR}。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 14
 */
Input_Result OH_Input_GetRepeat(const Input_Hotkey* hotkey, bool *isRepeat);

/**
 * @brief 订阅快捷键事件。
 *
 * @param hotkey 指定要订阅的快捷键对象。
 * @param callback 回调函数，用于回调快捷键事件。
 * @return OH_Input_AddHotkeyMonitor 函数错误码。\n
 *         {@link INPUT_SUCCESS} 表示订阅组合按键成功。\n
 *         {@link INPUT_PARAMETER_ERROR} 参数检查失败。\n
 *         {@Link INPUT_OCCUPIED_BY_SYSTEM} 该快捷键已被系统占用，可以通过接口{@link OH_Input_GetAllSystemHotkeys}查询所有的系统快捷键。\n
 *         {@Link INPUT_OCCUPIED_BY_OTHER} 已被抢占订阅。\n
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 14
 */
Input_Result OH_Input_AddHotkeyMonitor(const Input_Hotkey* hotkey, Input_HotkeyCallback callback);

/**
 * @brief 取消订阅快捷键。
 *
 * @param hotkey 指定要取消订阅的快捷键对象。
 * @param callback 回调函数，用于回调快捷键事件。
 * @return OH_Input_RemoveHotkeyMonitor 函数错误码。\n
 *         {@link INPUT_SUCCESS} 取消订阅组合按键成功， {@link INPUT_PARAMETER_ERROR} 参数检查失败。
 *        
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 14
 */
Input_Result OH_Input_RemoveHotkeyMonitor(const Input_Hotkey* hotkey, Input_HotkeyCallback callback);

/**
 * @brief 获取所有输入设备的ID列表。
 *
 * @param deviceIds 保存输入设备ID的列表。
 * @param inSize 保存输入设备ID列表的大小。
 * @param outSize 输出输入设备ID列表的长度，值小于等于inSize长度。
 * @return {@link INPUT_SUCCESS} 表示操作成功，
 *         {@link INPUT_PARAMETER_ERROR} 表示deviceIds或outSize为空指针或inSize小于0。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_GetDeviceIds(int32_t *deviceIds, int32_t inSize, int32_t *outSize);

/**
 * @brief 获取输入设备信息。
 *
 * @param deviceId 设备ID。
 * @param deviceInfo 指向输入设备信息{@link Input_DeviceInfo}的指针。
 * @return {@link INPUT_SUCCESS} 表示操作成功，
 *         {@link INPUT_PARAMETER_ERROR} 表示deviceInfo为空指针或deviceId无效，
 * 可以通过 {@link OH_Input_GetDeviceIds} 表示接口查询系统支持的设备ID。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_GetDevice(int32_t deviceId, Input_DeviceInfo **deviceInfo);

/**
 * @brief 创建输入设备信息的对象。
 *
 * @return 如果操作成功，返回设备信息{@link Input_DeviceInfo}实例的指针。否则返回空指针，可能的原因是分配内存失败。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_DeviceInfo* OH_Input_CreateDeviceInfo(void);

/**
 * @brief 销毁输入设备信息的对象。
 *
 * @param deviceInfo 设备信息的对象。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
void OH_Input_DestroyDeviceInfo(Input_DeviceInfo **deviceInfo);

/**
 * @brief 获取输入设备的键盘类型。
 *
 * @param deviceId 设备ID。
 * @param keyboardType 指向输入设备的键盘指针。
 * @return {@link INPUT_SUCCESS} 表示操作成功，
 *         {@link INPUT_PARAMETER_ERROR} 表示设备ID为无效值或者keyboardType是空指针。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_GetKeyboardType(int32_t deviceId, int32_t *keyboardType);

/**
 * @brief 获取输入设备的id。
 *
 * @param deviceInfo 输入设备信息{@link Input_DeviceInfo}。
 * @param id 指向输入设备ID的指针。
 * @return {@link INPUT_SUCCESS} 表示操作成功，
 *         {@link INPUT_PARAMETER_ERROR} 表示deviceInfo或者id是空指针。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_GetDeviceId(Input_DeviceInfo *deviceInfo, int32_t *id);

/**
 * @brief 获取输入设备的名称。
 *
 * @param deviceInfo 输入设备信息{@link Input_DeviceInfo}。
 * @param name 指向输入设备名称的指针。
 * @return {@link INPUT_SUCCESS} 表示操作成功，
 *         {@link INPUT_PARAMETER_ERROR} 表示deviceInfo或者name是空指针。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_GetDeviceName(Input_DeviceInfo *deviceInfo, char **name);

/**
 * @brief 获取有关输入设备能力信息，比如设备是触摸屏、触控板、键盘等。
 *
 * @param deviceInfo 输入设备信息{@link Input_DeviceInfo}。
 * @param capabilities 指向输入设备能力信息的指针。
 * @return {@link INPUT_SUCCESS} 表示操作成功，
 *         {@link INPUT_PARAMETER_ERROR} 表示deviceInfo或者capabilities是空指针。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_GetCapabilities(Input_DeviceInfo *deviceInfo, int32_t *capabilities);

/**
 * @brief 获取输入设备的版本信息。
 *
 * @param deviceInfo 输入设备信息{@link Input_DeviceInfo}。
 * @param version 指向输入设备版本信息的指针。
 * @return {@link INPUT_SUCCESS} 表示操作成功，
 *         {@link INPUT_PARAMETER_ERROR} 表示deviceInfo或者version是空指针。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_GetDeviceVersion(Input_DeviceInfo *deviceInfo, int32_t *version);

/**
 * @brief 获取输入设备的产品信息。
 *
 * @param deviceInfo 输入设备信息{@link Input_DeviceInfo}。
 * @param product 指向输入设备产品信息的指针。
 * @return {@link INPUT_SUCCESS} 表示操作成功，
 *         {@link INPUT_PARAMETER_ERROR} 表示deviceInfo或者product是空指针。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_GetDeviceProduct(Input_DeviceInfo *deviceInfo, int32_t *product);

/**
 * @brief 获取输入设备的厂商信息。
 *
 * @param deviceInfo 输入设备信息{@link Input_DeviceInfo}。
 * @param vendor 指向输入设备厂商信息的指针。
 * @return {@link INPUT_SUCCESS} 表示操作成功，
 *         {@link INPUT_PARAMETER_ERROR} 表示deviceInfo或者vendor是空指针。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_GetDeviceVendor(Input_DeviceInfo *deviceInfo, int32_t *vendor);

/**
 * @brief 获取输入设备的物理地址。
 *
 * @param deviceInfo 输入设备信息{@link Input_DeviceInfo}。
 * @param address 指向输入设备物理地址的指针。
 * @return {@link INPUT_SUCCESS} 表示操作成功，
 *         {@link INPUT_PARAMETER_ERROR} 表示deviceInfo或者address是空指针。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_GetDeviceAddress(Input_DeviceInfo *deviceInfo, char **address);

/**
 * @brief 注册设备热插拔的监听器。
 *
 * @param listener 指向设备热插拔监听器{@link Input_DeviceListener}的指针。
 * @return OH_Input_RegisterDeviceListener 的返回值。
 *         {@link INPUT_SUCCESS} 表示注册成功，
 *         {@link INPUT_PARAMETER_ERROR} 表示listener 为NULL。
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_RegisterDeviceListener(Input_DeviceListener* listener);

/**
 * @brief 取消注册设备热插拔的监听。
 *
 * @param listener  指向设备热插拔监听器{@link Input_DeviceListener}的指针。
 * @return OH_Input_UnregisterDeviceListener 的返回值。
 *         {@link INPUT_SUCCESS} 表示取消注册成功。\n
 *         {@link INPUT_PARAMETER_ERROR} 表示listener 为 NULL 或者 listener 未被注册。\n
 *         {@link INPUT_SERVICE_EXCEPTION} 表示由于服务异常调用失败。\n
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_UnregisterDeviceListener(Input_DeviceListener* listener);

/**
 * @brief 取消注册所有的设备热插拔的监听。
 *
 * @return OH_Input_UnregisterDeviceListener 的返回值。
 *         {@link INPUT_SUCCESS} 表示调用成功，
 *         {@link INPUT_SERVICE_EXCEPTION} 表示由于服务异常调用失败。\n
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_UnregisterDeviceListeners();
#ifdef __cplusplus
}
#endif
/** @} */

#endif /* OH_INPUT_MANAGER_H */