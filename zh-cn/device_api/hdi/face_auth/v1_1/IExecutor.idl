/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfFaceAuth
 * @{
 *
 * @brief 提供人脸认证驱动的标准API接口。
 *
 * 人脸认证驱动为人脸认证服务提供统一的访问接口。获取人脸认证驱动代理后，人脸认证服务可以调用相关接口获取执行器，获取人脸认证执行器后，
 * 人脸认证服务可以调用相关接口获取执行器，获取凭据模版信息，注册人脸特征模版，进行用户人脸认证，删除人脸特征模版等。
 *
 * @since 4.0
 */

/**
 * @file IExecutor.idl
 *
 * @brief 定义执行器接口，用于获取执行器，获取凭据模版信息，注册人脸特征模版，进行用户人脸认证，删除人脸特征模版等。
 *
 * 模块包路径：ohos.hdi.face_auth.v1_1
 *
 * 引用：
 * - ohos.hdi.face_auth.v1_0.FaceAuthTypes
 * - ohos.hdi.face_auth.v1_0.IExecutor
 * - ohos.hdi.face_auth.v1_1.FaceAuthTypes
 * - ohos.hdi.face_auth.v1_1.ISaCommandCallback
 *
 * @since 4.0
 */

package ohos.hdi.face_auth.v1_1;

import ohos.hdi.face_auth.v1_0.FaceAuthTypes;
import ohos.hdi.face_auth.v1_0.IExecutor;
import ohos.hdi.face_auth.v1_1.FaceAuthTypes;
import ohos.hdi.face_auth.v1_1.ISaCommandCallback;

/**
 * @brief 定义执行器接口，用于获取执行器，获取凭据模版信息，注册人脸特征模版，进行用户人脸认证，删除人脸特征模版等。
 *
 * @since 4.0
 * @version 1.1
 */
interface IExecutor extends ohos.hdi.face_auth.v1_0.IExecutor {
    /**
     * @brief 获得人脸执行器属性。
     *
     * @param templateIdList 模板id列表。
     * @param propertyTypes 人脸执行器属性类型。详细说明请参考{@link GetPropertyType}。
     * @param property 人脸执行器属性。详细说明请参考{@link Property}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     */
    GetProperty([in] unsigned long[] templateIdList, [in] enum GetPropertyType[] propertyTypes, [out] struct Property property);
    /**
     * @brief 设置缓存模板。
     *
     * @param templateIdList 人脸缓存的模板列表。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     */
    SetCachedTemplates([in] unsigned long[] templateIdList);

    /**
     * @brief 注册sa命令回调。
     *
     * @param callbackObj 表示sa命令回调。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     */
    RegisterSaCommandCallback([in] ISaCommandCallback callbackObj);
}
/** @} */