/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfFaceAuth
 * @{
 *
 * @brief 提供人脸认证驱动的标准API接口。
 *
 * 人脸认证驱动为人脸认证服务提供统一的访问接口。获取人脸认证驱动代理后，人脸认证服务可以调用相关接口获取执行器，获取人脸认证执行器后，
 * 人脸认证服务可以调用相关接口获取执行器，获取凭据模版信息，注册人脸特征模版，进行用户人脸认证，删除人脸特征模版等。
 *
 * @since 4.0
 */

/**
 * @file FaceAuthTypes.idl
 *
 * @brief 定义人脸认证驱动的枚举类和数据结构，包括AuthType, ExecutorRole, ExecutorSecureLevel,
 * CommandId, FaceTipsCode, ExecutorInfo, 和TemplateInfo。
 *
 * 模块包路径：ohos.hdi.face_auth.v1_1
 *
 * @since 4.0
 */

package ohos.hdi.face_auth.v1_1;

/**
 * @brief 枚举获得属性类型。
 *
 * @since 4.0
 * @version 1.1
 */
enum GetPropertyType : int {
    /** 人脸认证子类型。 */
    AUTH_SUB_TYPE = 1,
    /** 认证方式被冻结的时间。 */
    LOCKOUT_DURATION = 2,
    /** 认证方式距离被冻结的可处理认证请求次数。 */
    REMAIN_ATTEMPTS = 3,
    /** 人脸录入进程。 */
    ENROLL_PROGRESS = 4,
    /** 传感器信息。 */
    SENSOR_INFO = 5
};

/**
 * @brief 执行器相关属性。
 *
 * @since 4.0
 * @version 1.1
 */
struct Property {
    /** 人脸认证子类型。 */
    unsigned long authSubType;
    /** 认证方式被冻结的时间。 */
    int lockoutDuration;
    /** 认证方式距离被冻结的可处理认证请求次数。 */
    int remainAttempts;
    /** 人脸录入进程。 */
    String enrollmentProgress;
    /** 传感器信息。 */
    String sensorInfo;
};

/**
 * @brief 枚举sa命令id。
 *
 * @since 4.0
 * @version 1.1
 */
enum SaCommandId : int {
    /** 开始增加屏幕亮度 */
    BEGIN_SCREEN_BRIGHTNESS_INCREASE = 1,
    /** 结束增加屏幕亮度 */
    END_SCREEN_BRIGHTNESS_INCREASE = 2,
};

/**
 * @brief sa命令参数为空。
 *
 * @since 4.0
 * @version 1.1
 */
struct SaCommandParamNone {
};

/**
 * @brief sa命令参数。
 *
 * @since 4.0
 * @version 1.1
 */
union SaCommandParam {
    /** sa命令参数为空。详细说明请参考{@link SaCommandParamNone}。 */
    struct SaCommandParamNone none;
};

/**
 * @brief SA命令相关。
 *
 * @since 4.0
 * @version 1.1
 */
struct SaCommand {
    /** sa命令ID。详细说明请参考{@link SaCommandId}。 */
    enum SaCommandId id;
    /** sa命令参数。详细说明请参考{@link SaCommandParam}。 */
    union SaCommandParam param;
};

/**
 * @brief 枚举人脸认证功能相关操作命令。
 *
 * @since 4.0
 * @version 1.1
 */
enum CommandId : int {
    /** 锁定人脸模版。 */
    LOCK_TEMPLATE = 1,
    /** 解锁人脸模版。 */
    UNLOCK_TEMPLATE = 2,
    /** 初始化算法。 */
    INIT_ALGORITHM = 3,
    /** 用于厂商自定义操作指令。 */
    VENDOR_COMMAND_BEGIN = 10000
};
/** @} */