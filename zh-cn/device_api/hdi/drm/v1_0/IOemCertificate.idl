/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiDrm
 * @{
 * @brief DRM模块接口定义。
 *
 * DRM是数字版权管理，用于对多媒体内容加密，以便保护价值内容不被非法获取，
 * DRM模块接口定义了DRM实例管理、DRM会话管理、DRM内容解密的接口。
 *
 * @since 4.1
 * @version 1.0
 */

/**
 * @file IOemCertificate.idl
 *
 * @brief 定义了不同厂商证书下载的接口。
 *
 * 模块包路径：ohos.hdi.drm.v1_0
 *
 * 引用：ohos.hdi.drm.v1_0.MediaKeySystemTypes
 *
 * @since 4.1
 * @version 1.0
 */


package ohos.hdi.drm.v1_0;

import ohos.hdi.drm.v1_0.MediaKeySystemTypes;

/**
 * @brief 厂商证书下载接口。
 *
 * 产生证书下载请求，处理下载的证书。
 *
 * @since 4.1
 * @version 1.0
*/
interface IOemCertificate {
    /**
     * @brief 产生证书下载请求。
     *
     * @param defaultUrl 默认的证书服务器URL地址。
     * @param request 证书下载请求报文，以字节数组定义。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    GenerateOemKeySystemRequest([out] String defaultUrl, [out] unsigned char[] request);
    /**
     * @brief 处理下载的证书。
     *
     * @param response 下载的证书报文。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    ProcessOemKeySystemResponse([in] unsigned char[] response);
};
/** @} */