/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Codec
 * @{
 *
 * @brief Codec模块接口定义。
 *
 * Codec模块涉及自定义类型、音视频编解码组件初始化、参数设置、数据的轮转和控制等。
 *
 * @since 4.1
 * @version 2.0
 */

/**
 * @file ICodecComponentManager.idl
 *
 * @brief 主要包括Codec组件管理类接口。
 *
 * Codec模块获取组件编解码能力集、创建组件和销毁组件等接口定义。
 *
 * 模块包路径：ohos.hdi.codec.v2_0
 *
 * 引用：
 * - ohos.hdi.codec.v2_0.CodecTypes
 * - ohos.hdi.codec.v2_0.ICodecComponent
 *
 * @since 4.1
 * @version 2.0
 */

package ohos.hdi.codec.v2_0;

import ohos.hdi.codec.v2_0.CodecTypes;
import ohos.hdi.codec.v2_0.ICodecComponent;

/**
 * @brief Codec组件管理类接口定义。
 *
 * 主要提供以下功能:
 * - 获取Codec编解码组件数量以及编解码能力集表。
 * - 创建/销毁Codec组件。
 *
 * @since 4.1
 * @version 2.0
 */

interface ICodecComponentManager {

    /**
     * @brief 获取Codec编解码组件数量。
     *
     * 通过此接口获取Codec编解码组件数量，用来获取全部编解码能力集。
     *
     * @param count 编解码组件数量。
     *
     * @return HDF_SUCCESS 表示获取编解码组件数量成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，获取编解码组件数量失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败。
     *
     * @since 4.1
     */
    GetComponentNum([out] int count);

    /**
     * @brief 获取编解码能力集表。
     *
     * 用户可通过此接口了解Codec模块提供了哪些编解码能力，对应的能力体现在{@link CodecCompCapability}结构体。
     *
     * @param capList 返回全部组件的能力集表{@link CodecCompCapability}。
     * @param count 编解码组件数量，由{@link GetComponentNum}获得。
     *
     * @return HDF_SUCCESS 表示获取能力集表成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，获取能力集表失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败。
     *
     * @since 4.1
     */
    GetComponentCapabilityList([out] struct CodecCompCapability[] capList, [in] int count);

    /**
     * @brief 创建Codec组件实例。
     *
     * 根据组件名称创建Codec组件实例。
     *
     * @param component 指向Codec组件的指针。
     * @param componentId 创建组件的Id。
     * @param compName 组件名称。
     * @param appData 指向应用程序定义的值的指针，该值将在回调期间返回。
     * @param callbacks 回调接口，指向OMX_CALLBACKTYPE结构的指针，详见{@link ICodecCallback}。
     *
     * @return HDF_SUCCESS 表示创建组件成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，创建组件失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    CreateComponent([out] ICodecComponent component, [out] unsigned int componentId, [in] String compName, [in] long appData, [in] ICodecCallback callbacks);

    /**
     * @brief 销毁组件实例。
     *
     * 销毁指定的Codec组件。
     *
     * @param componentId 需要销毁的Codec组件。
     *
     * @return HDF_SUCCESS 表示销毁组件成功。
     * @return HDF_ERR_INVALID_PARAM 表示参数无效，销毁组件失败。
     * @return HDF_FAILURE 表示执行失败。
     * @return 其他值表示底层返回失败，具体错误码详见OpenMAX IL定义的OMX_ERRORTYPE。
     *
     * @since 4.1
     */
    DestoryComponent([in] unsigned int componentId);
}
/** @} */