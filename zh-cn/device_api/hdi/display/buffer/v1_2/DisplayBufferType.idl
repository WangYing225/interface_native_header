/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给上层图形服务使用的驱动接口，包括图层管理、设备控制、显示内存管理等相关接口。
 *
 * @since 3.2
 * @version 1.2
 */

/**
 * @file DisplayBufferType.idl
 *
 * @brief 显示内存类型定义，定义显示内存操作相关接口所使用的数据类型。
 *
 * 模块包路径：ohos.hdi.display.buffer.v1_2
 *
 * 引用：
 * - ohos.hdi.display.buffer.v1_0.DisplayBufferType
 *
 * @since 3.2
 * @version 1.2
 */

package ohos.hdi.display.buffer.v1_2;
import ohos.hdi.display.buffer.v1_0.DisplayBufferType;

/**
 * @brief 定义UV偏移信息。
 *
 * @since 3.2
 * @version 1.2
 */
struct ImagePlane {
    unsigned int offset;             /* 偏移量 */
    unsigned int hStride;            /* 水平增量 */
    unsigned int vStride;            /* 垂直增量 */
};

/**
 * @brief 定义格式和位置相关信息。
 *
 * @since 3.2
 * @version 1.2
 */
struct ImageLayout {
    unsigned int pixelFormat;        /**< 图像格式 */
    struct ImagePlane[] planes;      /**< 平面数据 */
};
/** @} */