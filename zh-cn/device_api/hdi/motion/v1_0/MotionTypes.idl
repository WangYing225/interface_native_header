/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Motion
 * @{
 *
 * @brief 手势识别设备驱动对硬件服务提供通用的接口能力。
 *
 * 模块提供硬件服务对手势识别驱动模块访问统一接口，服务获取驱动对象或者代理后，通过其提供的各类方法，实现使能手势识别/
 * 去使能手势识别、订阅/取消订阅手势识别数据。
 *
 * @since 3.2
 */

/**
 * @file MotionTypes.idl
 *
 * @brief 定义手势识别模块用到的数据结构，包括手势识别类型、上报的手势识别数据结构。
 *
 * 模块包路径：ohos.hdi.motion.v1_0
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.motion.v1_0;

/**
 * @brief 枚举手势识别类型。
 *
 * @since 3.2
 */
enum HdfMotionTypeTag {
    /** 拿起类型。 */
    HDF_MOTION_TYPE_PICKUP = 0,
    /** 翻转类型。 */
    HDF_MOTION_TYPE_FLIP,
    /** 靠近耳朵类型。 */
    HDF_MOTION_CLOSE_TO_EAR,
    /** 摇一摇类型。 */
    HDF_MOTION_TYPE_SHAKE,
    /** 旋转屏类型。 */
    HDF_MOTION_TYPE_ROTATION,
    /** 口袋模式类型。 */
    HDF_MOTION_TYPE_POCKET_MODE,
    /** 拿离耳朵类型。 */
    HDF_MOTION_TYPE_LEAVE_EAR,
    /** 腕朝上类型。 */
    HDF_MOTION_TYPE_WRIST_UP,
    /** 腕朝下类型。 */
    HDF_MOTION_TYPE_WRIST_DOWN,
    /** 最大手势识别类型。*/
    HDF_MOTION_TYPE_MAX,
};

/**
 * @brief 上报手势识别数据结构。
 *
 * 上报手势识别数据事件信息包括手势识别类型、手势识别结果、手势识别状态、手势识别数据长度、手势识别数据。
 *
 * @since 3.2
 */
struct HdfMotionEvent {
    /** 手势识别类型。 */
    int motion;
    /** 手势识别结果。 */
    int result;
    /** 手势识别状态。 */
    int status;
    /** 手势识别数据长度。 */
    int datalen;
    /** 手势识别数据。 */
    int [] data;
};
/** @} */