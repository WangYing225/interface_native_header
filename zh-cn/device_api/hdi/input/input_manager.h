/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Input
 * @{
 *
 * @brief Input模块驱动接口声明。
 *
 * 本模块为Input服务提供相关驱动接口，包括Input设备的打开和关闭、Input事件获取、设备信息查询、回调函数注册、特性状态控制等接口。
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file input_manager.h
 *
 * @brief 描述Input设备管理相关的接口声明。
 *
 * 引用：
 * - input_controller.h
 * - input_reporter.h
 * - input_type.h
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#include "input_controller.h"
#include "input_reporter.h"
#include "input_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 提供Input设备管理相关的接口。
 *
 * 此类接口包含Input设备的扫描、打开和关闭、特定设备信息查询，以及所有设备列表信息获取等接口。
 *
 * @since 1.0
 * @version 1.0
 */
typedef struct {
    /**
     * @brief Input服务用于扫描所有在线设备。
     *
     * @param staArr 输出参数，存放Input设备扫描信息的数组，信息包含设备索引以及设备类型。
     * @param arrLen 输入参数，staArr数组的长度信息。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetStatus}。
     *
     *
     * @since 1.0
     */
    int32_t (*ScanInputDevice)(InputDevDesc *staArr, uint32_t arrLen);

    /**
     * @brief Input服务打开对应设备的设备文件。
     *
     * @param devIndex 输入参数，Input设备索引，用于标志多个Input设备，取值从0开始，最多支持32个设备。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetStatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*OpenInputDevice)(uint32_t devIndex);

    /**
     * @brief Input服务关闭对应设备的设备文件。
     *
     * @param devIndex 输入参数，Input设备索引，用于标志多个Input设备，取值从0开始，最多支持32个设备。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetStatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*CloseInputDevice)(uint32_t devIndex);

    /**
     * @brief Input服务获取对应ID的设备信息。
     *
     * @param devIndex 输入参数，Input设备索引，用于标志多个Input设备，取值从0开始，最多支持32个设备。
     * @param devInfo 输出参数，即devIndex对应的设备的设备信息，具体参考{@link InputDeviceInfo}。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetStatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetInputDevice)(uint32_t devIndex, InputDeviceInfo **devInfo);

    /**
     * @brief Input服务获取所有Input设备列表的设备信息。
     *
     * @param devNum 输出参数，当前已经注册过的所有Input设备的总数。
     * @param devList 输出参数，Input设备列表所对应的设备信息，具体参考{@link InputDeviceInfo}。
     * @param size 输入参数，即指定deviceList数组对应的元素个数。
     *
     * @return INPUT_SUCCESS 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link RetStatus}。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*GetInputDeviceList)(uint32_t *devNum, InputDeviceInfo **devList, uint32_t size);
} InputManager;

/**
 * @brief 定义用于提供Input设备驱动程序功能的接口。
 *
 * @since 1.0
 * @version 1.0
 */
typedef struct {
    InputManager *iInputManager;          /**< Input设备的设备管理接口 */
    InputController *iInputController;    /**< Input设备的业务控制接口 */
    InputReporter *iInputReporter;        /**< Input设备的数据上报接口 */
} IInputInterface;

/**
 * @brief Input服务通过调用此接口获取操作Input设备的所有接口。
 *
 * @param interface 对Input设备进行接口操作的指针，通常在Input服务启动后，通过调用此函数获取Input设备操作接口。
 *
 * @return INPUT_SUCCESS 表示执行成功。
 * @return 其他值表示执行失败，具体错误码查看{@link RetStatus}。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t GetInputInterface(IInputInterface **interface);

/**
 * @brief Input服务通过调用此接口释放操作Input设备的所有接口。
 *
 * @param inputInterface 对Input设备进行接口操作的指针。
 *
 * @return INPUT_SUCCESS 表示执行成功。
 * @return 其他值表示执行失败，具体错误码查看{@link RetStatus}。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t ReleaseInputInterface(IInputInterface *inputInterface)

#ifdef __cplusplus
}
#endif
#endif
/** @} */