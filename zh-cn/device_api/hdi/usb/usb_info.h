/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup USB
 * @{
 *
 * @brief 定义（USB）功能的标准API接口。
 *
 * 该模块用于获取描述符、接口对象、请求对象和提交请求的自定义数据类型和函数。
 *
 * @since 3.0
 * @version 1.0
 */

/**
 * @file usb_info.h
 *
 * @brief USB驱动订阅模块使用的数据类型。
 *
 * @since 3.0
 * @version 1.0
 */

#ifndef USB_INFO_H
#define USB_INFO_H

#include <string>
#include <vector>

namespace OHOS {
namespace USB {
/**
 * @brief USB设备信息。
 *
 */
struct USBDeviceInfo {
    /** USB设备状态 */
    int32_t status;
    /** USB总线编号 */
    int32_t busNum;
    /** USB设备编号 */
    int32_t devNum;
};

/**
 * @brief USB设备信息类。
 *
 */
class UsbInfo {
public:
    /**
     * @brief 设置USB设备状态。
     *
     * @param status 输入参数，设备状态。
     *
     * @since 3.0
     * @version 1.0
     */
    void setDevInfoStatus(int32_t status)
    {
        devInfo.status = status;
    }

    /**
     * @brief 设置USB总线编号。
     *
     * @param status 输入参数，USB总线编号。
     *
     * @since 3.0
     * @version 1.0
     */
    void setDevInfoBusNum(int32_t busNum)
    {
        devInfo.busNum = busNum;
    }

    /**
     * @brief 设置USB设备编号。
     *
     * @param status 输入参数，USB设备编号。
     *
     * @since 3.0
     * @version 1.0
     */
    void setDevInfoDevNum(int32_t devNum)
    {
        devInfo.devNum = devNum;
    }

    /**
     * @brief 获取USB设备状态。
     *
     * @param status 输出参数，设备状态。
     *
     * @since 3.0
     * @version 1.0
     */
    int32_t getDevInfoStatus() const
    {
        return devInfo.status;
    }

    /**
     * @brief 获取USB总线编号。
     *
     * @param status 输出参数，USB总线编号。
     *
     * @since 3.0
     * @version 1.0
     */
    int32_t getDevInfoBusNum() const
    {
        return devInfo.busNum;
    }

    /**
     * @brief 获取USB设备编号。
     *
     * @param status 输出参数，USB设备编号。
     *
     * @since 3.0
     * @version 1.0
     */
    int32_t getDevInfoDevNum() const
    {
        return devInfo.devNum;
    }

private:
    /** USB设备信息 */
    USBDeviceInfo devInfo;
};
} /** namespace USB */
} /** namespace OHOS */

#endif /** USBMGR_USB_INFO_H */
